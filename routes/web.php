<?php

use Illuminate\Support\Facades\Route;
use App\Mail\ConfirmationMail;
use Illuminate\Support\Facades\Mail;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/home-welcome', function () {
    return view('welcome');
})->name('home');
Route::get('/home', function () {
    return view('welcome');
})->name('layouts.welcome');

Route::get('test_email', function () {
    // Mail::to('shaikhabbas193@gmail.com')->send(new ConfirmationMail('Test Mail','Testing'));
    // Role::create(['name' => 'customer']);
    // Role::create(['name' => 'affiliate_customer']);
    return view('customer.verify_code');
})->name('test_email');

Route::get('/forgot-password','ForgotPasswordController@add')->name('forgot-password');
Route::post('/forgot-password','ForgotPasswordController@store')->name('forgot-password.store');
Route::get('/reset-password/{token}','ForgotPasswordController@reset')->name('reset-password');
Route::post('/reset-password/store','ForgotPasswordController@resetStore')->name('reset-password.store');

Route::get('verification_code', function () {
    return view('customer.verify_code');
})->name('verification_code');

Route::post('activeAccount','UserController@activeAccount')->name('activeAccount');

Route::get('/cms', function () { return view('auth/login'); });
Route::get('/admin/login', function () { return view('auth/login'); });
Route::post('/admin/login', 'Auth\LoginController@login')->name('login');

Route::post('/login-submit', 'UserController@checklogin')->name('customer.login-submit');
Route::get('/login-form', function () { return view('customer/login'); })->name('customer.login');

Route::post('logout', 'Auth\LoginController@logout')->name('logout');
Route::get('logout', 'Auth\LoginController@logout')->name('logout');

Route::get('/customer-register/{affiliate_code?}', 'UserController@register_customer_view')->name('customer.register');
Route::post('/customer-register-form', 'UserController@register_customer')->name('customer.register.form');

Route::get('verification-send','UserController@verificationSend')->name('customer.verification');

Auth::routes();

Route::get('add-permission', 'SuperAdmin\RoleController@index')->name('add.permission');
Route::get('add-role', 'SuperAdmin\RoleController@add_role')->name('add.role');
Route::get('assign-role', 'SuperAdmin\RoleController@assign_role')->name('assign.role');

Route::group(['prefix' => 'admin', 'middleware' => ['role:SuperAdmin', 'auth'],], function () {
    Route::get('home/', 'SuperAdmin\HomeController@index')->name('admin.home');
    Route::get('affiliate_user/', 'SuperAdmin\HomeController@affiliate_users')->name('admin.affiliate_users');
    Route::get('users/{status?}', 'SuperAdmin\HomeController@users')->name('admin.users');
    Route::get('users/details/{id}', 'SuperAdmin\HomeController@user_detail')->name('admin.user.details');
    Route::post('email-credentials',"SuperAdmin\HomeController@emailSettingPost")->name('admin.email_credentials.post');
    Route::get('email-credentials',"SuperAdmin\HomeController@emailSetting")->name('admin.email_credentials');

    
    Route::post('reject-email-template',"SuperAdmin\HomeController@rejectEmailTemplatePost")->name('admin.reject_email_template.post');
    Route::get('reject-email-template',"SuperAdmin\HomeController@rejectEmailTemplate")->name('admin.reject_email_template');

    Route::post('accept-email-template',"SuperAdmin\HomeController@acceptEmailTemplatePost")->name('admin.accept_email_template.post');
    Route::get('accept-email-template',"SuperAdmin\HomeController@acceptEmailTemplate")->name('admin.accept_email_template');
    Route::post('change-application-status',"SuperAdmin\HomeController@changeApplicationStatus")->name('admin.change_application_status');
    
    Route::get('CustomerPDF/{id}',"SuperAdmin\HomeController@CustomerPDF")->name('CustomerPDF');
});

Route::group(['prefix' => 'affiliate_customer', 'middleware' => ['role:affiliate_customer', 'auth'],], function () {
    Route::get('home/', 'Affiliate_customer\HomeController@index')->name('affilate_customer.home');
    Route::get('users/{status?}', 'Affiliate_customer\HomeController@users')->name('affilate_customer.users');
});

Route::group(['prefix' => 'customer', 'middleware' => ['role:customer', 'auth'],], function () {
    Route::get('home/', 'customer\HomeController@index')->name('customer.home');
    
});
Route::group(['middleware' => ['role:customer', 'auth']],function(){
    Route::get('fetch/states/{countryId}','customer\ApplicationController@fetchStates')->name('fetch.states');
    Route::get('application-form', 'customer\ApplicationController@showApplication')->name('application.create');
    Route::post('/application/step-one', 'customer\ApplicationController@personalDetail')->name('application.stepone');
    Route::post('/application/step-two', 'customer\ApplicationController@professionalDetail')->name('application.steptwo');
    Route::post('/application/step-three', 'customer\ApplicationController@idInformation')->name('application.stepthree');
    Route::post('/application/step-four', 'customer\ApplicationController@incomeDetail')->name('application.stepfour');
    Route::post('/application/step-five', 'customer\ApplicationController@fundingDetail')->name('application.stepfive');
    Route::post('/application/step-six', 'customer\ApplicationController@riskAcceptance')->name('application.stepsix');
    Route::post('/application/step-seven', 'customer\ApplicationController@financialSituation')->name('application.stepseven');
    Route::post('/application/step-eight', 'customer\ApplicationController@investmentExperience')->name('application.stepeight');
    Route::post('/application/step-nine', 'customer\ApplicationController@identificationProof')->name('application.stepnine');
    Route::post('/application/step-ten', 'customer\ApplicationController@disclosures')->name('application.stepten');
    Route::post('/application/step-eleven', 'customer\ApplicationController@signatures')->name('application.stepeleven');
    Route::get('/register-detail',"customer\HomeController@registerDetail");
});



