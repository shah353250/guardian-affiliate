<?php

function limit_string($x, $length){
    if(strlen($x)<=$length)
    {
      echo $x;
    }
    else
    {
      $y=mb_substr($x,0,$length) . '...';
      echo $y;
    }
 }

 function dt_format($date) {
    return date("M-d-Y", strtotime($date));
 }
 
 function dt_time_format($date) {
     $dt   = date("M-d-Y", strtotime($date));
     $time = date("H:i:s", strtotime($date));
    return [$dt, $time];
 }