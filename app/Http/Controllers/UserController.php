<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Validator;
use App\User;
use App;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Facades\Mail;
use App\Mail\ConfirmationMail;
use Str;


class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    use AuthenticatesUsers;

    protected $redirectTo = '/home';
    public function __construct()
    {
        // $this->middleware(['auth']);

    }
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('customer.login')->with(['title'=> 'Login']);
    }

    function checklogin(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'email'   => 'required|email',
            'password'  => 'required'
        ]);
        if($validator->passes())
        {
            $user_data = array(
                'email'  => $request->get('email'),
                'password' => $request->get('password')
            );
            if(User::where('email',$request->email)->where('is_active',0)->exists())
            {
                return response()->json(['status'=>false,'url' => route('verification_code')]);
            }

            if (Auth::attempt($user_data)) {

                if (!Auth::user()->hasRole(['affiliate_customer', 'customer'])) {
                    $this->guard()->logout();
                    $request->session()->invalidate();
                    return ["error" => 'You are unauthorized to login'];
                }
    
                if (Auth::user()->hasRole('affiliate_customer')) {
                    $redirect = route('affilate_customer.home');
                }
                if (Auth::user()->hasRole('customer')) {
                    $redirect = route('application.create');
                }
                return response()->json(["success" => 'Successfully Logged In', "url" => $redirect]);
            } else {
                return response()->json(["error" => ['Invalid User Email or Password']]);
            }

        }
        return response()->json(['error'=>$validator->errors()->all()]);
    }

   function register_customer_view(Request $request)
   {
        $data = '';
        if ($request->affiliate_code) {
            $data = $request->affiliate_code;
        }
        return view('customer.register')->with(['title'=> 'Register','data' => $data]);
   }

   function register_customer(Request $request)
   {
        if(User::where('email', $request->email)->count() > 0){
            return ["error" => "User Email already in use."];
        }
        $generate_verify_code = rand(100000,999999);
        $userExist = User::where('verification_code', $generate_verify_code)->exists();
        while($userExist)
        {
            $generate_verify_code = rand(100000,999999);
            $userExist = User::where('verification_code', $generate_verify_code)->exists();
        }
        $affiliate_user = 0;
        if(User::where('affiliate_code', $request->affiliate_code)->count() > 0){
            $affiliate_user = User::where('affiliate_code', $request->affiliate_code)->first()->id;
        }

        
        $password                   = bcrypt($request->password);
        $data                       = new User();
        $data->name                 = @$request->name;
        $data->email                = $request->email;
        $data->password             = $password;
        $data->registration_type    = $request->registrationType;
        $data->product_you_want     = $request->productTrade;
        $data->hear_about_us        = $request->hearAbout;
        $data->verification_code    = $generate_verify_code;
        $data->affiliate_id = $affiliate_user;
        $data->save();

        if($affiliate_user != 0)
        {
            User::where('affiliate_code', $request->affiliate_code)->increment('user_count', 1);
        }
        

        $role_r = Role::where('name', '=', 'customer')->firstOrFail();  
        $data->assignRole($role_r);
        session()->put("user_email",$request->email);
        Mail::to($request->email)->send(new ConfirmationMail('Verification Code',$generate_verify_code));
       
        return ["success" => "Successfully Added.", "redirect" => route('verification_code')];
   }

   public function activeAccount(Request $request)
   {
       $user = User::where('verification_code',$request->verification_code)->first();
       if($user != null)
       {
            $user->is_active = 1;
            $user->save();
            Auth::login($user);
            return redirect()->route('application.create');
       }
       else
       {
           return redirect()->back()->withError("Invalid verification code.");
       }
   }

   public function verificationSend(Request $request)
   {
        $userEmail = session()->get("user_email");
        $generate_verify_code = rand(100000,999999);
        $userExist = User::where('verification_code', $generate_verify_code)->exists();
        while($userExist)
        {
            $generate_verify_code = rand(100000,999999);
            $userExist = User::where('verification_code', $generate_verify_code)->exists();
        }
        $user = User::where('email',$userEmail)->first();
        $user->verification_code = $generate_verify_code;
        $user->save();
        Mail::to($userEmail)->send(new ConfirmationMail('Verification Code',$generate_verify_code));
        return redirect()->route('verification_code');
   }
}
