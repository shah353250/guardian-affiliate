<?php

namespace App\Http\Controllers\SuperAdmin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\User;
use App\Models\EmailSetting;
use App\Models\EmailTemplate;
use Session;
use App\Models\Country;
use App\Models\State;
use PDF;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('admin.home');
        
    }
    public function emailSetting()
    {
        $setting = EmailSetting::first();
        if($setting != null)
        {
            return view('admin.settings.email_settings',compact('setting'));
        }
        return view('admin.settings.email_settings');
    }
    public function emailSettingPost(Request $request)
    {
        EmailSetting::create([
            "host" => $request->smtp_host,
            "port" => $request->smtp_port,
            "password" => $request->password,
            "email" => $request->email
        ]);
        Session::flash('message', 'This is a message!'); 
        return redirect()->route("admin.email_credentials");
    }

    public function rejectEmailTemplate()
    {
        $template = EmailTemplate::where("template_type","reject_email")->first();
        if($template != null)
        {
            return view('admin.settings.reject_email_template',compact('template'));
        }
        return view("admin.settings.reject_email_template");
    }

    public function rejectEmailTemplatePost(Request $request)
    {
        EmailTemplate::create([
            "subject" => $request->subject,
            "content" => $request->compose_email,
            "template_type" =>"reject_email"
        ]);
        Session::flash('message', 'This is a message!'); 
        return redirect()->route("admin.reject_email_template");
    }

    public function acceptEmailTemplate()
    {
        $template = EmailTemplate::where("template_type","accept_email")->first();
        if($template != null)
        {
            return view('admin.settings.accept_email_template',compact('template'));
        }
        return view("admin.settings.accept_email_template");
    }

    public function acceptEmailTemplatePost(Request $request)
    {
        EmailTemplate::create([
            "subject" => $request->subject,
            "content" => $request->compose_email,
            "template_type" =>"accept_email"
        ]);
        Session::flash('message', 'This is a message!'); 
        return redirect()->route("admin.accept_email_template");
    }

    public function affiliate_users()
    {
        $data = User::role('affiliate_customer')->orderBy('id', 'desc')->paginate(25);
        return view('admin/customers/manage')->with(['title' => 'User', 'data' => $data]);
    }

    public function users(Request $request)
    {
        $title = 'User';
        $data = User::role('customer')->orderBy('id', 'desc')->paginate(25);
        $application = 0;
        // dd($request->status);
        if ($request->status != null) {
            $data = User::role('customer')->where('status','=',$request->status)->orderBy('id', 'desc')->paginate(25);
            $application = 1;
            $title = 'Applications';
        }
        return view('admin/customers/manage')->with(['title' => $title, 'data' => $data, 'application' => $application]);
    }

    public function CustomerPDF($id)
    {
        $User = User::where('id','=', $id)->first();
        // return view('admin.signatureImage',compact('User'))->render();

        // $signature = view('admin.signatureImage',compact('User'))->render();

        $data = [
            'User'    => $User,
            // 'signature' => $signature,
        ];
        // dd($User);
        // PDF::setOptions(['isJavascriptEnabled' => true]);
        $pdf = PDF::loadView('admin.CustomerPDF',$data);
        
        $pdf->setOptions(['isJavascriptEnabled' => true]);

        return $pdf->setPaper('a4', 'PORTAIRT')->stream('CustomerPDF.pdf');
    }

    public function user_detail(Request $request)
    {
        $countries = Country::all();
        $states = State::all();
        $data = User::with(['personalDetail','mailing','contact_person','companyDetail','professionalDetail','idInformation','incomeDetail','fundDetail','riskAcceptance','financialSituation','investmentExperience','identificationProof','disclosure','signature'])->where('id','=', $request->id)->first();
        return view('admin/customers/detail')->with(['title' => 'User Detail', 'data' => $data,'countries' => $countries,'states' => $states]);
    }

    public function changeApplicationStatus(Request $request)
    {
        $user = User::find($request->user_id);
        if($user){
            if($request->status == "accept")
            {
                $user->status = 3;
            }elseif($request->status == "reject"){
                $user->status = 4;
            }
            $user->signature = $request->signature_val;
            $user->save();
        }
        return response()->json(["status"=>true]);
    }
}
