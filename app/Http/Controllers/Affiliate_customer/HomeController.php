<?php

namespace App\Http\Controllers\Affiliate_customer;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use App\User;
class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');   
    }

    public function users(Request $request)
    {
        $title = 'User';
        $data = User::where('affiliate_id', Auth::user()->id)->orderBy('id', 'desc')->paginate(25);
        $application =0;
        if ($request->status) 
        {
            $data = User::where('affiliate_id', Auth::user()->id)->where('status','=',$request->status)->orderBy('id', 'desc')->paginate(25);
            $application = 1;
            $title = 'Applications';
        }
        return view('admin/customers/manage')->with(['title' => $title, 'data' => $data,'application' => $application]);
    }
}
