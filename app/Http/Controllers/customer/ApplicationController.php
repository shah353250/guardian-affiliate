<?php

namespace App\Http\Controllers\customer;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Country;
use App\Models\State;
use App\Models\PersonalDetail;
use App\Models\MailingAddress;
use App\Models\TrustedContactPerson;
use App\Models\ProfessionalDetail;
use App\Models\IdInformation;
use App\Models\IncomeDetail;
use App\Models\FundDetail;
use App\Models\RiskAcceptance;
use App\Models\FinancialSituation;
use App\Models\InvestmentExperience;
use App\Models\IdentificationProof;
use App\Services\ImageInsertionService;
use App\Models\Disclosure;
use App\Models\Signature;
use Validator;
use Auth;
use Illuminate\Validation\Rule;
use App\Models\CompanyDetail;
use App\User;
class ApplicationController extends Controller
{
    public function showApplication()
    {
        $countries = Country::all();
        $signature = @Auth::user()->signature->signature_image;
        return view("customer.application",compact('countries','signature'));   
    }
    //First Step,Step One
    public function personalDetail(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'first_name' => 'required',
            'last_name' => 'required',
            'address' => 'required',
            'country' => 'required',
            'state' => 'required',
            'city' => 'required',
            'zip_code' => 'required',
            'phone_number' => 'required',
            'number_of_dependents' => 'required|not_in:0',
            //mailing address exist
            'mailing_address' => 'required_with:mailing',
            'mailing_country' => 'required_with:mailing',
            'mailing_state' =>'required_with:mailing',
            'mailing_city' => 'required_with:mailing',
            'mailing_zip_code' => 'required_with:mailing',
            //contact person data required
            'contact_person_name' => 'required_with:contact_person',
            'contact_person_phone' => 'required_with:contact_person',
            'contact_person_email' => 'required_with:contact_person|email:rfc|nullable',
            'contact_person_address' => 'required_with:contact_person',
            'contact_person_country' => 'required_with:contact_person',
            'contact_person_state' => 'required_with:contact_person',
            'contact_person_city' => 'required_with:contact_person',
            'contact_person_zip_code' => 'required_with:contact_person',
            'contact_person_relation_account_holder' => 'required_with:contact_person',
            'date_of_birth' => 'required_with:contact_person',
            'company_name'      => [Rule::requiredIf(function () use ($request) { return (Auth::user()->registration_type == "Limited Partnership" || Auth::user()->registration_type == "Corporate" || Auth::user()->registration_type == "Limited Liability Company"); })],
            'company_address'   => [Rule::requiredIf(function () use ($request) { return (Auth::user()->registration_type == "Limited Partnership" || Auth::user()->registration_type == "Corporate" || Auth::user()->registration_type == "Limited Liability Company"); })],
            'company_apt_suite' => [Rule::requiredIf(function () use ($request) { return (Auth::user()->registration_type == "Limited Partnership" || Auth::user()->registration_type == "Corporate" || Auth::user()->registration_type == "Limited Liability Company"); })],
            'company_country'   => [Rule::requiredIf(function () use ($request) { return (Auth::user()->registration_type == "Limited Partnership" || Auth::user()->registration_type == "Corporate" || Auth::user()->registration_type == "Limited Liability Company"); })],
            'company_state'     => [Rule::requiredIf(function () use ($request) { return (Auth::user()->registration_type == "Limited Partnership" || Auth::user()->registration_type == "Corporate" || Auth::user()->registration_type == "Limited Liability Company"); })],
            'company_city'      => [Rule::requiredIf(function () use ($request) { return (Auth::user()->registration_type == "Limited Partnership" || Auth::user()->registration_type == "Corporate" || Auth::user()->registration_type == "Limited Liability Company"); })],
            'company_zip_code'  => [Rule::requiredIf(function () use ($request) { return (Auth::user()->registration_type == "Limited Partnership" || Auth::user()->registration_type == "Corporate" || Auth::user()->registration_type == "Limited Liability Company"); })],
            'company_email'     => ['email:rfc',Rule::requiredIf(function () use ($request) { return (Auth::user()->registration_type == "Limited Partnership" || Auth::user()->registration_type == "Corporate" || Auth::user()->registration_type == "Limited Liability Company"); })],
            'company_phone'     => [Rule::requiredIf(function () use ($request) { return (Auth::user()->registration_type == "Limited Partnership" || Auth::user()->registration_type == "Corporate" || Auth::user()->registration_type == "Limited Liability Company"); })],
        ]);
        
        if ($validator->passes()) {

            if(Auth::user()->personalDetail != null){
                $personalDetail = PersonalDetail::where("user_id",Auth::user()->id)->first();
            }else{
                $personalDetail = new PersonalDetail;
            }
            $personalDetail->user_id = Auth::user()->id;
            $personalDetail->first_name = $request->first_name;
            $personalDetail->last_name = $request->last_name;
            $personalDetail->address = $request->address;
            $personalDetail->apt_suite = $request->apt_suite;
            $personalDetail->country_id = $request->country;
            $personalDetail->state_id = $request->state;
            $personalDetail->city = $request->city;
            $personalDetail->zip_code = $request->zip_code;
            $personalDetail->phone_number = $request->phone_number;
            $personalDetail->number_of_dependents = $request->number_of_dependents;
            $personalDetail->marital_status = $request->marital_status;
            $personalDetail->is_mailing_address = $request->has('mailing')?1:0;
            $personalDetail->is_trusted_contact_person = $request->has('contact_person')?1:0;
            $personalDetail->save();
            if($request->has('mailing'))
            {
                if(Auth::user()->mailing != null){
                    $mailing = MailingAddress::where("user_id",Auth::user()->id)->first();
                }else{
                    $mailing = new MailingAddress;
                }
                $mailing->user_id = Auth::user()->id;
                $mailing->address = $request->mailing_address;
                $mailing->apt_suite = $request->mailing_apt_suite;
                $mailing->country_id = $request->mailing_country;
                $mailing->state_id = $request->mailing_state;
                $mailing->city = $request->mailing_city;
                $mailing->zip_code = $request->mailing_zip_code;
                $mailing->save();
            }else{
                MailingAddress::where("user_id",Auth::user()->id)->delete();
            }
            if($request->has('contact_person'))
            {
                if(Auth::user()->contact_person != null){
                    $contactPerson = TrustedContactPerson::where("user_id",Auth::user()->id)->first();
                }else{
                    $contactPerson = new TrustedContactPerson;
                }
                $contactPerson->user_id = Auth::user()->id;
                $contactPerson->name = $request->contact_person_name;
                $contactPerson->phone = $request->contact_person_phone;
                $contactPerson->email = $request->contact_person_email;
                $contactPerson->street_address = $request->contact_person_address;
                
                $contactPerson->country_id = $request->contact_person_country;
                $contactPerson->state_id = $request->contact_person_state;
                $contactPerson->city = $request->contact_person_city;
                $contactPerson->zip_code = $request->contact_person_zip_code;
                $contactPerson->relation_holder = $request->contact_person_relation_account_holder;
                $contactPerson->date_of_birth = $request->date_of_birth;
                $contactPerson->save();
            }else{
                TrustedContactPerson::where("user_id",Auth::user()->id)->delete();
            }
            if(Auth::user()->registration_type == "Limited Partnership" || Auth::user()->registration_type == "Corporate" || Auth::user()->registration_type == "Limited Liability Company")
            {
                if(Auth::user()->companyDetail != null){
                    $companyDetail = CompanyDetail::where("user_id",Auth::user()->id)->first();
                }else{
                    $companyDetail = new CompanyDetail;
                }
                $companyDetail->user_id = Auth::user()->id;
                $companyDetail->company_name = $request->company_name;
                $companyDetail->company_address = $request->company_address;
                $companyDetail->apt_suite = $request->company_apt_suite;
                $companyDetail->country_id = $request->company_country;
                $companyDetail->state_id = $request->company_state;
                $companyDetail->city = $request->company_city;
                $companyDetail->zip_code = $request->company_zip_code;
                $companyDetail->company_email = $request->company_email;
                $companyDetail->company_phone = $request->company_phone;
                $companyDetail->save();

            }
			return response()->json(['success'=>'Added new records.']);
        }
    	return response()->json(['error'=>$validator->errors()]);
    }
    //First Step,Step Two
    public function professionalDetail(Request $request)
    {
            // dd($request);
            $validator = Validator::make($request->all(), [
                'employee_name' => 'required_if:second_form_selected_value,==,employed|required_if:second_form_selected_value,==,self employed',
                'occupation' => 'required_if:second_form_selected_value,==,employed|required_if:second_form_selected_value,==,self employed',
                'employee_address' => 'required_if:second_form_selected_value,==,employed|required_if:second_form_selected_value,==,self employed',
                'year_of_employment' => 'required_if:second_form_selected_value,==,employed|required_if:second_form_selected_value,==,self employed',
                'employee_country' => 'required_if:second_form_selected_value,==,employed|required_if:second_form_selected_value,==,self employed',
                'employee_state' => 'required_if:second_form_selected_value,==,employed|required_if:second_form_selected_value,==,self employed',
                'employee_city' => 'required_if:second_form_selected_value,==,employed|required_if:second_form_selected_value,==,self employed',
                'employee_apt_suite' => 'required_if:second_form_selected_value,==,employed|required_if:second_form_selected_value,==,self employed',
                'employee_phone_no' => 'required_if:second_form_selected_value,==,employed|required_if:second_form_selected_value,==,self employed',
                'employee_fax' => 'required_if:second_form_selected_value,==,employed|required_if:second_form_selected_value,==,self employed',
                
            ]);
        
        
        if ($validator->passes()) {
            if(Auth::user()->professionalDetail != null){
                $professionalDetail = ProfessionalDetail::where("user_id",Auth::user()->id)->first();
            }else{
                $professionalDetail = new ProfessionalDetail;
            }
            $professionalDetail->user_id = Auth::user()->id;
            $professionalDetail->employment_status = $request->second_form_selected_value;
            if($request->second_form_selected_value != 'retired')
            {
                $professionalDetail->name = $request->employee_name;
                $professionalDetail->occupation = $request->occupation;
                $professionalDetail->address = $request->employee_address;
                $professionalDetail->year_of_employment = $request->year_of_employment;
                $professionalDetail->country_id = $request->employee_country;
                $professionalDetail->state_id = $request->employee_state;
                $professionalDetail->city = $request->employee_city;
                $professionalDetail->apt_suite = $request->employee_apt_suite;
                $professionalDetail->phone = $request->employee_phone_no;
                $professionalDetail->fax = $request->employee_fax;
            }
            else{
                $professionalDetail->name = null;
                $professionalDetail->occupation = null;
                $professionalDetail->address = null;
                $professionalDetail->year_of_employment = null;
                $professionalDetail->country_id = null;
                $professionalDetail->state_id = null;
                $professionalDetail->city = null;
                $professionalDetail->apt_suite = null;
                $professionalDetail->phone = null;
                $professionalDetail->fax = null;
            }
            $professionalDetail->save();
			return response()->json(['success'=>'Added new records.']);
        }
    	return response()->json(['error'=>$validator->errors()]);
    }
    //Step Three
    public function idInformation(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'three_tax_id' => 'required',
            'three_date_of_birth' => 'required',
            'three_country' => 'required',
            'three_state' => 'required',
            'three_id_type' => 'required',
            'three_name_of_id' => 'required_if:three_id_type,Local ID',
            'three_id_number' => 'required',
            'three_country_residence' => 'required',
            'three_state_residence' => 'required',
            'three_issue_date' => 'required',
            'three_expiration' => 'required',
        ],[
            "three_tax_id.required" => "Tax ID field is required.",
            'three_date_of_birth.required' => 'Date of birth field is required.',
            'three_country.required' => 'Country field is required.',
            'three_state.required' => 'State field is required.',
            'three_id_type.required' => 'ID Type field is required.',
            'three_name_of_id.required' => 'Name of ID field is required.',
            'three_id_number.required' => 'ID Number field is required.',
            'three_country_residence.required' => 'Country field is required.',
            'three_state_residence.required' => 'State field is required.',
            'three_issue_date.required' => 'Issue Date field is required.',
            'three_expiration.required' => 'Expiration Date field is required.',
        ]);
        if ($validator->passes()) {
            if(Auth::user()->idInformation != null){
                $idInformation = IdInformation::where("user_id",Auth::user()->id)->first();
            }else{
                $idInformation = new IdInformation;
            }
            $idInformation->user_id = Auth::user()->id;
            $idInformation->tax_id = $request->three_tax_id;
            $idInformation->date_of_birth = $request->three_date_of_birth;
            $idInformation->tax_country_id = $request->three_country;
            $idInformation->tax_state_id = $request->three_state;
            $idInformation->id_type = $request->three_id_type;
            $idInformation->name_of_id = $request->three_name_of_id;
            $idInformation->id_number = $request->three_id_number;
            $idInformation->identity_country_id = $request->three_country_residence;
            $idInformation->identity_state_id = $request->three_state_residence;
            $idInformation->issue_date = $request->three_issue_date;
            $idInformation->expiration_date = $request->three_expiration;
            $idInformation->save();
			return response()->json(['success'=>'Added new records.']);
        }
    	return response()->json(['error'=>$validator->errors()]);
    }
    //Step Four
    public function incomeDetail(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'annual_income' => 'required',
            'net_worth' => 'required',
            'liquid_net_worth' => 'required',
            'tax_rate' => 'required'
        ]);

        if ($validator->passes()) {
            if(Auth::user()->incomeDetail != null){
                $incomeDetail = IncomeDetail::where("user_id",Auth::user()->id)->first();
            }else{
                $incomeDetail = new IncomeDetail;
            }
            $incomeDetail->user_id = Auth::user()->id;
            $incomeDetail->annual_income = $request->annual_income;
            $incomeDetail->net_worth = $request->net_worth;
            $incomeDetail->liquid_net_worth = $request->liquid_net_worth;
            $incomeDetail->tax_rate = $request->tax_rate;
            $incomeDetail->save();
			return response()->json(['success'=>'Added new records.']);
        }
    	return response()->json(['error'=>$validator->errors()]);
    }
    //Step Five
    public function fundingDetail(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'funding_bank_name' => 'required',
            'aba_or_swift' => 'required',
            'account_number' => 'required',
            'funding_other_input' => 'required_with:funding_other',        
        ]);

        if ($validator->passes()) {
            if(Auth::user()->fundDetail != null){
                $fundDetail = FundDetail::where("user_id",Auth::user()->id)->first();
            }else{
                $fundDetail = new FundDetail;
            }
            $fundDetail->user_id = Auth::user()->id;
            $fundDetail->is_income = $request->has('funding_income')?1:0;
            $fundDetail->is_pension = $request->has('funding_saving')?1:0;
            $fundDetail->is_gift = $request->has('funding_benefit')?1:0;
            $fundDetail->is_sale_of_business = $request->has('funding_sales')?1:0;
            $fundDetail->is_inheritance = $request->has('funding_inheritance')?1:0;
            $fundDetail->is_social_security = $request->has('funding_social')?1:0;
            $fundDetail->is_other = $request->has('funding_other')?1:0;
            $fundDetail->other_income = $request->has('funding_other')?$request->funding_other_input:'';
            $fundDetail->bank_name = $request->funding_bank_name;
            $fundDetail->aba_or_swift = $request->aba_or_swift;
            $fundDetail->account_number = $request->account_number;
            $fundDetail->save();
			return response()->json(['success'=>'Added new records.']);
        }
    	return response()->json(['error'=>$validator->errors()]);
    }
    //Step Six
    public function riskAcceptance(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'conservative' => 'required_without_all:moderatelyConservative,moderate,moderatelyAggressive,significantRisk',

        ],[
            'conservative.required_without_all' => 'Please Select Atleast 1 Risk Tolerance Option Below'
        ]);

        if ($validator->passes()) {
            if(Auth::user()->riskAcceptance != null){
                $riskAcceptance = RiskAcceptance::where("user_id",Auth::user()->id)->first();
            }else{
                $riskAcceptance = new RiskAcceptance;
            }
            $riskAcceptance->user_id = Auth::user()->id;
            $riskAcceptance->account_risk = $request->accountRisk;
            $riskAcceptance->is_conservative = $request->conservative;
            $riskAcceptance->is_moderately_conservative = $request->moderatelyConservative;
            $riskAcceptance->is_moderate = $request->moderate;
            $riskAcceptance->is_moderately_aggressive = $request->moderatelyAggressive;
            $riskAcceptance->is_significant_risk = $request->significantRisk;
            $riskAcceptance->save();
			return response()->json(['success'=>'Added new records.']);
        }
    	return response()->json(['error'=>$validator->errors()]);
    }
    //Step Seven
    public function financialSituation(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'special_expenses' => 'required',
            'liquidity_need' => 'required',
            'financial_goal_plan' => 'required',
        ]);
        
        if ($validator->passes()) {
            
            if(Auth::user()->financialSituation != null){
                $financialSituation = FinancialSituation::where("user_id",Auth::user()->id)->first();
            }else{
                $financialSituation = new FinancialSituation;
            }
            $financialSituation->user_id = Auth::user()->id;
            $financialSituation->annual_expenses = $request->annual_expenses;
            $financialSituation->special_expenses = $request->special_expenses;
            $financialSituation->liquidity_needs = $request->liquidity_need;
            $financialSituation->financial_goal_period = $request->financial_goal_plan;
            $financialSituation->save();
			return response()->json(['success'=>'Added new records.']);
        }
    	return response()->json(['error'=>$validator->errors()]);
    }
    //Step Eight
    public function investmentExperience(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'investStock' => 'required_without_all:investBond,investOptions,investFuture'
        ],
        [   
            'investStock.required_without_all'    => 'Investment Experience must be select one.',
            'stockExpertise.in' => "The Level of Investment Experience you selected is Inconsistent with the level of Investor Sophistication for a Pattern Day Trader. Please make another selection ONLY IF you selected this in error. If you did NOT make this selection in error, we are unable to open an account for you at this time."
        ]);

        $validator->sometimes('stockExpertise', Rule::in(['Good', 'Extensive']) , function ($input) {
            return $input->investStock != null;
        });
        if ($validator->passes()) {
            
            if(Auth::user()->investmentExperience != null){
                $investmentExperience = InvestmentExperience::where("user_id",Auth::user()->id)->first();
            }else{
                $investmentExperience = new InvestmentExperience;
            }
            $investmentExperience->user_id = Auth::user()->id;
            $investmentExperience->is_stocks = $request->has('investStock')?1:0;
            $investmentExperience->is_fixed_income = $request->has('investBond')?1:0;
            $investmentExperience->is_options = $request->has('investOptions')?1:0;
            $investmentExperience->is_futures = $request->has('investFuture')?1:0;
            $investmentExperience->stock_year_experience = $request->stockExp;
            $investmentExperience->stock_knowledge = $request->stockExpertise;
            $investmentExperience->fixed_income_year_experience = $request->bondExp;
            $investmentExperience->fixed_income_knowledge = $request->bondExpertise;
            $investmentExperience->options_year_experience = $request->optionsExp;
            $investmentExperience->options_knowledge = $request->optionsExpertise;
            $investmentExperience->futures_year_experience = $request->futureExp;
            $investmentExperience->futures_knowledge = $request->futureExpertise;
            $investmentExperience->save();
			return response()->json(['success'=>'Added new records.']);
        }
        // dd($validator->errors());
    	return response()->json(['error'=>$validator->errors()]);
    }
    //Step Nine
    public function identificationProof(Request $request,ImageInsertionService $service)
    {
        if(Auth::user()->registration_type == "Limited Partnership" || Auth::user()->registration_type == "Corporate" || Auth::user()->registration_type == "Limited Liability Company")
        {
            if(Auth::user()->identificationProof == null)
            {
                $validator = Validator::make($request->all(), [
                    'first_photo_front'     => 'required|image',
                    'first_photo_back'      => 'required|image',
                    'second_photo_front'    => 'required|image',
                    'second_photo_back'     => 'required|image',
                    'third_photo_front'     => 'required|image',
                    'third_photo_back'      => 'required|image',
                    'fourth_photo_front'    => 'required|image',
                    'fourth_photo_back'     => 'required|image',
                ]);
            }
            else{
                $validator = Validator::make($request->all(), [
                    'first_photo_front'     => ['image',Rule::requiredIf(function () use ($request) { return (Auth::user()->identificationProof->first_photo_front == null ); })],
                    'first_photo_back'      => ['image',Rule::requiredIf(function () use ($request) { return (Auth::user()->identificationProof->first_photo_back == null ); })],
                    'second_photo_front'    => ['image',Rule::requiredIf(function () use ($request) { return (Auth::user()->identificationProof->second_photo_front == null ); })],
                    'second_photo_back'     => ['image',Rule::requiredIf(function () use ($request) { return (Auth::user()->identificationProof->second_photo_back == null ); })],
                    'third_photo_front'     => ['image',Rule::requiredIf(function () use ($request) { return (Auth::user()->identificationProof->third_photo_front == null ); })],
                    'third_photo_back'      => ['image',Rule::requiredIf(function () use ($request) { return (Auth::user()->identificationProof->third_photo_back == null ); })],
                    'fourth_photo_front'    => ['image',Rule::requiredIf(function () use ($request) { return (Auth::user()->identificationProof->fourth_photo_front == null ); })],
                    'fourth_photo_back'     => ['image',Rule::requiredIf(function () use ($request) { return (Auth::user()->identificationProof->fourth_photo_back == null ); })],
                ]);
            }
        }
        else{
            if(Auth::user()->identificationProof == null)
            {
                $validator = Validator::make($request->all(), [
                    'first_photo_front'     => 'required|image',
                    'first_photo_back'      => 'required|image',
                    'second_photo_front'    => 'required|image',
                    'second_photo_back'     => 'required|image',
                ]);
            }
            else{
                $validator = Validator::make($request->all(), [
                    'first_photo_front'     => ['image',Rule::requiredIf(function () use ($request) { return (Auth::user()->identificationProof->first_photo_front == null ); })],
                    'first_photo_back'      => ['image',Rule::requiredIf(function () use ($request) { return (Auth::user()->identificationProof->first_photo_back == null ); })],
                    'second_photo_front'    => ['image',Rule::requiredIf(function () use ($request) { return (Auth::user()->identificationProof->second_photo_front == null ); })],
                    'second_photo_back'     => ['image',Rule::requiredIf(function () use ($request) { return (Auth::user()->identificationProof->second_photo_back == null ); })],
                ]);
            }
        }
        
        if ($validator->passes()) {
            
            if(Auth::user()->identificationProof != null){
                $identificationProof = IdentificationProof::where("user_id",Auth::user()->id)->first();
            }else{
                $identificationProof = new IdentificationProof;
            }
            $identificationProof->user_id = Auth::user()->id;
            $files = [
                "first_photo_front","first_photo_back",
                "second_photo_front","second_photo_back",
                "third_photo_front","third_photo_back",
                "fourth_photo_front","fourth_photo_back"
            ];
            foreach ($files as $key => $value) {
                if($request->hasFile($value))
                {
                    if($identificationProof->$value != null)
                    {
                        $service->deleteImage("/applicationform/images/".$identificationProof->$value);
                    }
                    $identificationProof->$value = $service->insertImage("/applicationform/images",$request->$value);
                }
            }
            $identificationProof->save();
			return response()->json(['success'=>'Added new records.']);
        }
    	return response()->json(['error'=>$validator->errors()]);
    }
    //Step Ten    
    public function disclosures(Request $request,ImageInsertionService $service)
    {
        $validator = Validator::make($request->all(), [
            'agreement_opt' => 'required|in:1',
            'initial_deposit' => 'required|numeric|gte:25000',
            'brokage_account_total' => Rule::requiredIf(function () use ($request) {
                return $request->brokage_account == 1;
            }),
            'beneficial_account_no' => Rule::requiredIf(function () use ($request) {
                return $request->beneficial == 1;
            }),
            'beneficial_account_name' => Rule::requiredIf(function () use ($request) {
                return $request->beneficial == 1;
            }),
            'shareholder_number'=> Rule::requiredIf(function () use ($request) {
                return $request->shareholder == 1;
            }),
            'shareholder_name'=> Rule::requiredIf(function () use ($request) {
                return $request->shareholder == 1;
            }),
            'shareholder_relation'=> Rule::requiredIf(function () use ($request) {
                return $request->shareholder == 1;
            }),
            'immediate_company_name'=> Rule::requiredIf(function () use ($request) {
                return $request->immediate == 1;
            }),
            'immediate_company_address'=> Rule::requiredIf(function () use ($request) {
                return $request->immediate == 1;
            }),
            'immediate_relation'=> Rule::requiredIf(function () use ($request) {
                return $request->immediate == 1;
            }),
            'securities_firm_name'=> Rule::requiredIf(function () use ($request) {
                return $request->securities == 1;
            }),
            'securities_firm_address'=> Rule::requiredIf(function () use ($request) {
                return $request->securities == 1;
            }),
            'securities_permission'=> Rule::requiredIf(function () use ($request) {
                return ($request->securities == 1 && ((Auth::user()->disclosure != null)? (Auth::user()->disclosure->securities == 0):(true)));
            }),
            'disclosure_institution_firm_name'=> Rule::requiredIf(function () use ($request) {
                return $request->disclosure_institution == 1;
            }),
            'disclosure_institution_firm_address'=> Rule::requiredIf(function () use ($request) {
                return $request->disclosure_institution == 1;
            }),
            'disclosure_institution_firm_position'=> Rule::requiredIf(function () use ($request) {
                return $request->disclosure_institution == 1;
            }),
        ]);

        if ($validator->passes()) {
            
            if(Auth::user()->disclosure != null){
                $disclosure = Disclosure::where("user_id",Auth::user()->id)->first();
            }else{
                $disclosure = new Disclosure;
            }
            $disclosure->user_id = Auth::user()->id;
            $disclosure->agreement =$request->agreement_opt;
            $disclosure->initial_deposit =$request->initial_deposit;
            $disclosure->brokage_account =$request->brokage_account;
            $disclosure->brokage_account_data =json_encode([
                'brokage_account_total'=> $request->brokage_account ==1?$request->brokage_account_total:null
                ]);
            $disclosure->beneficial =$request->beneficial;
            $disclosure->beneficial_data =json_encode([
                'beneficial_account_no'=> $request->beneficial == 1? $request->beneficial_account_no:null,
                'beneficial_account_name' => $request->beneficial == 1? $request->beneficial_account_name:null
                ]);
            $disclosure->shareholder =$request->shareholder;
            $disclosure->shareholder_data =json_encode([
                'shareholder_number'=> $request->shareholder ==1?$request->shareholder_number:null,
                'shareholder_name' => $request->shareholder ==1?$request->shareholder_name:null,
                'shareholder_relation' => $request->shareholder ==1?$request->shareholder_relation:null,
                ]);
            $disclosure->immediate =$request->immediate;
            $disclosure->immediate_data =json_encode([
                'immediate_company_name'=>$request->immediate ==1? $request->immediate_company_name:null,
                'immediate_company_address' => $request->immediate ==1? $request->immediate_company_address:null,
                'immediate_relation' => $request->immediate ==1? $request->immediate_relation:null,
                ]);
            $disclosure->securities =$request->securities;
            if($request->hasFile('securities_permission'))
            {
                $security_data = json_decode($disclosure->securities_data);
                if(@$security_data->securities_permission != null)
                {
                    $service->deleteImage("/applicationform/images/".$security_data->securities_permission);
                }
                $imageName = $service->insertImage("/applicationform/images",$request->securities_permission);
            }
            $disclosure->securities_data =json_encode([
                'securities_firm_name'=>$request->securities ==1?$request->securities_firm_name:null,
                'securities_firm_address' =>$request->securities ==1? $request->securities_firm_address:null,
                'securities_permission' => @$imageName,
                ]);

            $disclosure->institution =$request->disclosure_institution;
            $disclosure->institution_data =json_encode([
                'disclosure_institution_firm_name'=> $request->disclosure_institution ==1? $request->disclosure_institution_firm_name:null,
                'disclosure_institution_firm_address' => $request->disclosure_institution ==1? $request->disclosure_institution_firm_address:null,
                'disclosure_institution_firm_position' => $request->disclosure_institution ==1? $request->disclosure_institution_firm_position:null,
                ]);
            $disclosure->tax_with_holding = $request->taxWitholding;
            $disclosure->under_us = $request->underUs;
            $disclosure->under_us_detail = $request->underUs2;
            $disclosure->save();
			return response()->json(['success'=>'Added new records.']);
        }
    	return response()->json(['error'=>$validator->errors()]);
    }
    //Step Eleven
    public function signatures(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'accountTerms' => 'required',
        ]);

        if ($validator->passes()) {
            if(Auth::user()->signature != null){
                $signature = Signature::where("user_id",Auth::user()->id)->first();
            }else{
                $signature = new Signature;
            }
            $signature->user_id = Auth::user()->id;
            $signature->terms_and_condition = $request->has('accountTerms')?1:0;
            $signature->risk_disclosure = $request->has('riskDis')?1:0;
            $signature->stocks_disclosure = $request->has('pennyStocks')?1:0;
            $signature->trading_Agreement = $request->has('electronicAccess')?1:0;
            $signature->disclosure_statement = $request->has('marginDisclosure')?1:0;
            $signature->w9_certification = $request->has('w9_Certification')?1:0;
            $signature->locate_agreement = $request->has('stock_Locate')?1:0;
            $signature->margin_agreement = $request->has('marginDisc')?1:0;
            $signature->confirm_electronic = $request->has('confirmedElectronic')?1:0;
            $signature->signature_image =$request->signature_val;
            $signature->save();
            $user = User::find(Auth::id());
            $user->status = 1;
            $user->save();
			return response()->json(['success'=>'Added new records.']);
        }
    	return response()->json(['error'=>$validator->errors()]);
    }

    public function fetchStates($countryId)
    {
       return response()->json(['states' => State::where("country_id",$countryId)->select("id","name")->get()]);
    }
}
