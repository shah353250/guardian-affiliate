<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\User;
use App\Models\Country;
use App\Models\State;
class ProfessionalDetail extends Model
{
    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function country()
    {
        return $this->belongsTo(Country::class);

    }
    public function state()
    {
        return $this->belongsTo(State::class);

    }
}
