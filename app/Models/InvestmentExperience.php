<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use App\user;
class InvestmentExperience extends Model
{
    public function user()
    {
        return $this->belongsTo(User::class);
    }
}
