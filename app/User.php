<?php

namespace App;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Spatie\Permission\Traits\HasRoles;
use App\Models\PersonalDetail;
use App\Models\MailingAddress;
use App\Models\TrustedContactPerson;
use App\Models\ProfessionalDetail;
use App\Models\IdInformation;
use App\Models\IncomeDetail;
use App\Models\FundDetail;
use App\Models\RiskAcceptance;
use App\Models\FinancialSituation;
use App\Models\InvestmentExperience;
use App\Models\IdentificationProof;
use App\Models\Disclosure;
use App\Models\Signature;
use App\Models\CompanyDetail;

class User extends Authenticatable
{
    use  Notifiable, HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password','affiliate_code','is_active'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function personalDetail()
    {
        return $this->hasOne(PersonalDetail::class);
    }
    public function mailing()
    {
        return $this->hasOne(MailingAddress::class);
    }
    public function contact_person()
    {
        return $this->hasOne(TrustedContactPerson::class);
    }
    public function companyDetail()
    {
        return $this->hasOne(CompanyDetail::class);
    }
    public function professionalDetail()
    {
        return $this->hasOne(ProfessionalDetail::class);
    }
    public function idInformation()
    {
        return $this->hasOne(IdInformation::class);
    }
    public function incomeDetail()
    {
        return $this->hasOne(IncomeDetail::class);
    }
    public function fundDetail()
    {
        return $this->hasOne(FundDetail::class);
    }
    public function riskAcceptance()
    {
        return $this->hasOne(RiskAcceptance::class);
    }
    public function financialSituation()
    {
        return $this->hasOne(FinancialSituation::class);
    }
    public function investmentExperience()
    {
        return $this->hasOne(InvestmentExperience::class);
    }
    
    public function identificationProof()
    {
        return $this->hasOne(IdentificationProof::class);
    }
    public function disclosure()
    {
        return $this->hasOne(Disclosure::class);
    }
    public function signature()
    {
        return $this->hasOne(Signature::class);
    }
    
}
