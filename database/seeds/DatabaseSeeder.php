<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\User;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(CountryStateSeeder::class);
        DB::table('users')->insert([
            'name' => 'Admin',
            'email' => 'admin@affiliate.com',
            'password' => Hash::make("12345678"),
        ]);
        Permission::create(['name' => 'SuperAdmin']);
        Role::create(['name' =>  'SuperAdmin']);

        $user = User::where('id', 1)->first();
        $role_r = Role::where('id', '=', 1)->firstOrFail();  
        $user->assignRole($role_r);

        Role::create(['name' => 'customer']);
        Role::create(['name' => 'affiliate_customer']);
    }
}
