<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProfessionalDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('professional_details', function (Blueprint $table) {
            $table->id();
            $table->foreignId("user_id")->constrained();
            $table->string("name")->nullable();
            $table->string("occupation")->nullable();
            $table->text("address")->nullable();
            $table->string("year_of_employment")->nullable();
            $table->foreignId("country_id")->nullable()->constrained();
            $table->foreignId("state_id")->nullable()->constrained();
            $table->text("apt_suite")->nullable();
            $table->text("city")->nullable();
            $table->string("phone")->nullable();
            $table->string("fax")->nullable();
            $table->enum("employment_status",["employed","self employed","retired"]);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('professional_details');
    }
}
