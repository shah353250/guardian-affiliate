<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSignaturesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('signatures', function (Blueprint $table) {
            $table->id();
            $table->foreignId("user_id")->constrained();
            $table->boolean("terms_and_condition")->default(0);
            $table->boolean("risk_disclosure")->default(0);
            $table->boolean("stocks_disclosure")->default(0);
            $table->boolean("trading_Agreement")->default(0);
            $table->boolean("disclosure_statement")->default(0);
            $table->boolean("w9_certification")->default(0);
            $table->boolean("locate_agreement")->default(0);
            $table->boolean("margin_agreement")->default(0);
            $table->boolean("confirm_electronic")->default(0);
            $table->text("signature_image");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('signatures');
    }
}
