<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class AddColumnsUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->integer("status")->after('hear_about_us')->nullable()->default(0)->comment = '0 => draft, 1 => pending, 3 => accept, 4 => reject';
            $table->text("affiliate_code")->after('remember_token')->nullable();
            $table->integer("user_count")->after('remember_token')->nullable()->default(0);
            $table->integer("affiliate_id")->after('remember_token')->nullable()->default(0);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('users', function (Blueprint $table) {
            $table->dropColumn(["affiliate_code","user_count","affiliate_id"]);
        });
    }
}
