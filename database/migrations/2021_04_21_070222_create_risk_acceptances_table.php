<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRiskAcceptancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('risk_acceptances', function (Blueprint $table) {
            $table->id();
            $table->foreignId("user_id")->constrained();
            $table->string('account_risk');
            $table->boolean('is_conservative')->nullable()->default('0');
            $table->boolean('is_moderately_conservative')->nullable()->default('0');
            $table->boolean('is_moderate')->nullable()->default('0');
            $table->boolean('is_moderately_aggressive')->nullable()->default('0');
            $table->boolean('is_significant_risk')->nullable()->default('0');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('risk_acceptances');
    }
}
