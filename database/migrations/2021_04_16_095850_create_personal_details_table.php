<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePersonalDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('personal_details', function (Blueprint $table) {
            $table->id();
            $table->foreignId("user_id")->constrained();
            $table->string("first_name");
            $table->string("last_name");
            $table->text("address");
            $table->text("apt_suite")->nullable();
            $table->foreignId("country_id")->constrained();
            $table->foreignId("state_id")->constrained();
            $table->text("city");
            $table->string("zip_code");
            $table->string("phone_number");
            $table->string("number_of_dependents");
            $table->string("marital_status");
            $table->boolean("is_trusted_contact_person");
            $table->boolean("is_mailing_address");
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('personal_details');
    }
}
