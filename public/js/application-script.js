//Step One Variables
var mailingCheckbox = document.querySelector('#mailing')
var contactPersonCheckbox = document.querySelector('#contact_person')
var stepOneButton = document.querySelector('#btnStepOne')
var countryStepOne = document.querySelector('#country')
var stateStepOne = document.querySelector('#state')
var mailingCountry = document.querySelector('#mailing_country')
var mailingState = document.querySelector('#mailing_state')
var contactPersonCountry = document.querySelector('#contact_person_country')
var contactPersonState = document.querySelector('#contact_person_state')
var companyCountry = document.querySelector('#company_country');
var companyState = document.querySelector('#company_state');

//Step Two Variables
var stepTwoButton = document.querySelector('#btnStepTwo')
var professionalDetailCountry = document.querySelector('#employee_country')
var professionalDetailState = document.querySelector('#employee_state')

//Step Three Variables 
var stepThreeButton = document.querySelector('#btnStepThree');
var threeCountry = document.querySelector('#three_country')
var threeState = document.querySelector('#three_state')
var threeResidenceCountry = document.querySelector('#three_country_residence')
var threeResidenceState = document.querySelector('#three_state_residence')
//Step Four Variables
var stepFourButton = document.querySelector('#btnStepFour')
//Step Five Variables
var stepFiveButton = document.querySelector('#btnStepFive')
//Step Six Variables
var stepSixButton = document.querySelector('#btnStepSix');
//Step Seven Variables
var stepSevenButton = document.querySelector('#btnStepSeven')
//Step Eight Variables
var stepEightButton = document.querySelector('#btnStepEight')
//Step Nine Variables
var stepNineButton = document.querySelector('#btnStepNine')
//Step Ten Variables
var stepTenButton = document.querySelector('#btnStepTen')
//Step Eleven Variables
var stepElevenButton = document.querySelector('#btnStepEleven')



//core function for validation,post and dropdown filling
function gotoPreviousTab(tabNumber)
{
    $('#myTab li:nth-child('+tabNumber+') a').tab('show')
}

function fillStatesDropDown(e,t) //e element and t target dropdown
{
    $.getJSON("/fetch/states/"+e.value,function(data){
        t.innerHTML = '<option value>Select States</option>';
        $.each(data.states,function(index,val){
            var node = document.createElement("option");
            var textnode = document.createTextNode(val.name);
            node.setAttribute("value",val.id);
            node.appendChild(textnode);
            t.appendChild(node);     
            // t.appendChild(`<option value="${val.id}">${val.name}</option>`);
        })
    })
}

function printErrorMessage(errors,formId)
{
    $(formId).find("input.invalid-field").removeAttr('data-original-title').removeClass('invalid-field');
    for (var key of Object.keys(errors)) {
        fieldElement = document.querySelector('#'+key)
        if(typeof(fieldElement) != 'undefined' && fieldElement != null)
        {
            fieldElement.classList.add("invalid-field");
            fieldElement.setAttribute("data-original-title",errors[key]);
        }
  
        var element =  document.querySelector('.'+key+'-error');
        if (typeof(element) != 'undefined' && element != null)
        {
            document.querySelector('.'+key+'-error').innerHTML = errors[key];
        }
    }
    $('[data-toggle="tooltip"]').tooltip()
}

function changeWizard(step)
{
    $('ul.form-wizard-steps li:nth-child('+(step-1)+')').addClass("activated").removeClass("active");
    $('ul.form-wizard-steps li:nth-child('+step+')').addClass("active");
}

function ajaxCall(formId,url,submitbutton,redirect,step)
{
    $.ajax({
        url: url,
        type: "POST",
        data:  new FormData(document.querySelector(formId)),
        contentType: false,
        cache: false,
        processData:false,
        beforeSend : function()
        {
            submitbutton.setAttribute("disabled","disabled");
            submitbutton.children[0].style.display = 'inline';
        },
        success: function(data)
        {
            // console.log(data);
            if($.isEmptyObject(data.error)){
                $(redirect).tab('show')
                changeWizard(step)
            }else{
                // console.log(data.error);
                printErrorMessage(data.error,formId);
            }
        },
        error: function(e) 
        {
            console.log(e);
        },
        complete:function(){
            submitbutton.removeAttribute("disabled");
            submitbutton.children[0].style.display = 'none';
        }      
   });
}

function changeThis(sender) { 
    if(sender.id == "investStock")
    {
        if(sender.checked){
            document.getElementById("stockExp").removeAttribute('disabled');
            document.getElementById("stockExp1").removeAttribute('disabled');
            document.getElementById("stockExp2").removeAttribute('disabled');
            $('#stockExp,#stockExpertise').prop('checked',true)
            document.getElementById("stockExpertise").removeAttribute('disabled');
            document.getElementById("stockExpertise1").removeAttribute('disabled');
            document.getElementById("stockExpertise2").removeAttribute('disabled');
            document.getElementById("stockExpertise3").removeAttribute('disabled');
        }
        else
        {
            document.getElementById("stockExp").disabled = true
            document.getElementById("stockExp1").disabled = true
            document.getElementById("stockExp2").disabled = true
            $('#stockExp,#stockExp1,#stockExp2').prop('checked',false)
            document.getElementById("stockExpertise").disabled = true
            document.getElementById("stockExpertise1").disabled = true
            document.getElementById("stockExpertise2").disabled = true
            document.getElementById("stockExpertise3").disabled = true
            $('#stockExpertise,#stockExpertise1,#stockExpertise2,#stockExpertise3').prop('checked',false)
        }
        
    }
    if(sender.id == 'investBond')
    {
        if(sender.checked){
            document.getElementById("bondExp").removeAttribute('disabled');
            document.getElementById("bondExp1").removeAttribute('disabled');
            document.getElementById("bondExp2").removeAttribute('disabled');
            $('#bondExp,#bondExpertise').prop('checked',true)
            document.getElementById("bondExpertise").removeAttribute('disabled');
            document.getElementById("bondExpertise1").removeAttribute('disabled');
            document.getElementById("bondExpertise2").removeAttribute('disabled');
            document.getElementById("bondExpertise3").removeAttribute('disabled');
        }else{
            document.getElementById("bondExp").disabled = true
            document.getElementById("bondExp1").disabled = true
            document.getElementById("bondExp2").disabled = true
            $('#bondExp,#bondExp1,#bondExp2').prop('checked',false)
            document.getElementById("bondExpertise").disabled = true
            document.getElementById("bondExpertise1").disabled = true
            document.getElementById("bondExpertise2").disabled = true
            document.getElementById("bondExpertise3").disabled = true
            $('#bondExpertise,#bondExpertise1,#bondExpertise2,#bondExpertise3').prop('checked',false)
        }
        
    }

    
    if(sender.id == 'investOptions')
    {
        if(sender.checked)
        {
            document.getElementById("optionsExp").removeAttribute('disabled');
            document.getElementById("optionsExp1").removeAttribute('disabled');
            document.getElementById("optionsExp2").removeAttribute('disabled');
            $('#optionsExp,#optionsExpertise').prop('checked',true)
            document.getElementById("optionsExpertise").removeAttribute('disabled');
            document.getElementById("optionsExpertise1").removeAttribute('disabled');
            document.getElementById("optionsExpertise2").removeAttribute('disabled');
            document.getElementById("optionsExpertise3").removeAttribute('disabled');
        }else{
            document.getElementById("optionsExp").disabled = true
            document.getElementById("optionsExp1").disabled = true
            document.getElementById("optionsExp2").disabled = true
            $('#optionsExp,#optionsExp1,#optionsExp2').prop('checked',false)
            document.getElementById("optionsExpertise").disabled = true
            document.getElementById("optionsExpertise1").disabled = true
            document.getElementById("optionsExpertise2").disabled = true
            document.getElementById("optionsExpertise3").disabled = true
            $('#optionsExpertise,#optionsExpertise1,#optionsExpertise2,#optionsExpertise3').prop('checked',false)
        }
        
    }

    if(sender.id == 'investFuture')
    {
        if(sender.checked)
        {
            document.getElementById("futureExp").removeAttribute('disabled');
            document.getElementById("futureExp1").removeAttribute('disabled');
            document.getElementById("futureExp2").removeAttribute('disabled');
            $('#futureExp,#futureExpertise').prop('checked',true)
            document.getElementById("futureExpertise").removeAttribute('disabled');
            document.getElementById("futureExpertise1").removeAttribute('disabled');
            document.getElementById("futureExpertise2").removeAttribute('disabled');
            document.getElementById("futureExpertise3").removeAttribute('disabled');
        }else{
            document.getElementById("futureExp").disabled = true;
            document.getElementById("futureExp1").disabled = true;
            document.getElementById("futureExp2").disabled = true;
            $('#futureExp,#futureExp1,#futureExp2').prop('checked',false)
    
            document.getElementById("futureExpertise").disabled = true;
            document.getElementById("futureExpertise1").disabled = true;
            document.getElementById("futureExpertise2").disabled = true;
            document.getElementById("futureExpertise3").disabled = true;
            $('#futureExpertise,#futureExpertise1,#futureExpertise2,#futureExpertise3').prop('checked',false)
        }
    }
}

//============= Event Listeners for StepOne
mailingCheckbox.addEventListener("change",function(){
    let formElement = document.querySelector('#mailing-div');
    if(this.checked){
        formElement.style.display = 'block';
    }else{
        formElement.style.display = 'none';
    }
})

contactPersonCheckbox.addEventListener("change",function(){
    let formElement = document.querySelector('#contact-person-div');
    if(this.checked){
        formElement.style.display = 'block';
    }else{
        formElement.style.display = 'none';
    }
})

countryStepOne.addEventListener("change",function(){
    fillStatesDropDown(this,stateStepOne);
})
mailingCountry.addEventListener("change",function(){
    fillStatesDropDown(this,mailingState);
})
contactPersonCountry.addEventListener("change",function(){
    fillStatesDropDown(this,contactPersonState);
})
if(companyCountry != null)
{
    companyCountry.addEventListener("change",function(){
        fillStatesDropDown(this,companyState);
    })
}


stepOneButton.addEventListener("click",function(){
    ajaxCall('#steponeForm','/application/step-one',this,'#myTab li:nth-child(2) a',2)
})

//=================Event Listener for Step Two

professionalDetailCountry.addEventListener("change",function(){
    fillStatesDropDown(this,professionalDetailState);
})
stepTwoButton.addEventListener("click",function(){
    ajaxCall('#steptwoForm','/application/step-two',this,'#myTab li:nth-child(3) a',3)
})

//=================Event Listener for Step Three

threeCountry.addEventListener("change",function(){
    fillStatesDropDown(this,threeState);
})
threeResidenceCountry.addEventListener("change",function(){
    fillStatesDropDown(this,threeResidenceState);
})

stepThreeButton.addEventListener("click",function(){
    ajaxCall('#stepthreeForm','/application/step-three',this,'#myTab li:nth-child(4) a',4)
})
//=================Event Listener for Step Four
stepFourButton.addEventListener("click",function(){
    ajaxCall('#stepfourForm','/application/step-four',this,'#myTab li:nth-child(5) a',5)
})
//=================Event Listener for Step Five
stepFiveButton.addEventListener("click",function(){
    ajaxCall('#stepfiveForm','/application/step-five',this,'#myTab li:nth-child(6) a',6)
})
//=================Event Listener for Step Six
stepSixButton.addEventListener("click",function(){
    ajaxCall('#stepsixForm','/application/step-six',this,'#myTab li:nth-child(7) a',7)
})
//=================Event Listener for Step Seven
stepSevenButton.addEventListener("click",function(){
    ajaxCall('#stepsevenForm','/application/step-seven',this,'#myTab li:nth-child(8) a',8)
})
//=================Event Listener for Step Eight
stepEightButton.addEventListener("click",function(){
    ajaxCall('#stepeightForm','/application/step-eight',this,'#myTab li:nth-child(9) a',9)
})
//=================Event Listener for Step Nine
stepNineButton.addEventListener("click",function(){
    ajaxCall('#stepnineForm','/application/step-nine',this,'#myTab li:nth-child(10) a',10)
})
//=================Event Listener for Step Ten
stepTenButton.addEventListener("click",function(){
    ajaxCall('#steptenForm','/application/step-ten',this,'#myTab li:nth-child(11) a',11)
})
//=================Event Listener for Step Eleven
var signatureContainer;
$(function() {
    signatureContainer = $('#signatureContainer').signature();
    // signatureContainer.signature('draw', data.Signature);    
});
stepElevenButton.addEventListener("click",function(){
    if (signatureContainer.signature('isEmpty')) {
        return alert("Signature  Is required");
    }
    document.querySelector('#signature-val').value = signatureContainer.signature('toJSON');
    ajaxCall('#stepelevenForm','/application/step-eleven',this,'#myTab li:nth-child(12) a')
})


$('#clear').on('click',function() {
    signatureContainer.signature('clear');
});

$(function() {
    $('[data-toggle="tooltip"]').tooltip()

    $("body").on('click', '.employee', function() {
        var employee = $(this).val();
        // console.log(employee);
        $('.employee').prop('checked',false);
        $(this).prop('checked',true);
        if(employee == 'employed')
        {
            $('.work_fields').css('display','block');
            $('label[for="employee_name"]').text('Employee Name*');
            $('label[for="occupation"]').text('Occupation*');
            $('label[for="employee_address"]').text('Address Of Employer*');
            $('label[for="year_of_employment"]').text('Years With Employer*');
            $('label[for="employee_phone_no"]').text('Employee Phone Number*');
        }
        else if(employee == 'self employed')
        {
            $('.work_fields').css('display','block');
            $('label[for="employee_name"]').text('Business Name*');
            $('label[for="occupation"]').text('Company Type*');
            $('label[for="employee_address"]').text('Business Address*');
            $('label[for="year_of_employment"]').text('Years of buisness*');
            $('label[for="employee_phone_no"]').text('Business Phone Number*');
            
        }
        else if(employee == 'retired')
        {
            $('.work_fields').css('display','none');
        }
  });

  $('#funding_other').change(function(){
    if ($('#funding_other').is(':checked') == true){
        $('#funding_other_input').val('').prop('disabled', false);
    } else {
        $('#funding_other_input').prop('disabled', true);
    }
   });

   $('.radio_div_display').change(function(){
    var selected = $(this).val();
    var div = $(this).data('div');
    if (selected == '1'){
        $(div).css('display', 'block');
    } else if(selected == '0'){
        $(div).css('display', 'none');
    }
   }); 
   
   $('.label-icon.radiocurrent').removeClass("radiocurrent");

});

function show1() {
    document.getElementById('undershow1').style.display = 'block';
}

function show2() {
    document.getElementById('undershow2').style.display = 'block';
}

function show3() {
    document.getElementById('undershow2').style.display = 'none';
}

function show4() {
    document.getElementById('undershow1').style.display = 'none';
    document.getElementById('undershow2').style.display = 'none';

}

$('#steptwoForm .label-icon input').on('click', function(e) {
  $(".radioholder").removeClass("radiocurrent");
  $(this).parent(".radioholder").addClass("radiocurrent");
  e.preventDefault();
});