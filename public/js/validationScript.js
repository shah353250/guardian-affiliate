




        const isNumericInput = (event) => {
            const key = event.keyCode;
            return ((key >= 48 && key <= 57) || // Allow number line
                (key >= 96 && key <= 105) // Allow number pad
            );
        };

        const isModifierKey = (event) => {
            const key = event.keyCode;
            return (event.shiftKey === true || key === 35 || key === 36) || // Allow Shift, Home, End
                (key === 8 || key === 9 || key === 13 || key === 46) || // Allow Backspace, Tab, Enter, Delete
                (key > 36 && key < 41) || // Allow left, up, right, down
                (
                    // Allow Ctrl/Command + A,C,V,X,Z
                    (event.ctrlKey === true || event.metaKey === true) &&
                    (key === 65 || key === 67 || key === 86 || key === 88 || key === 90)
                )
        };

        const enforceFormat = (event) => {
            // Input must be of a valid number format or a modifier key, and not longer than ten digits
            if(!isNumericInput(event) && !isModifierKey(event)){
                event.preventDefault();
            }
        };

        const formatToPhone = (event) => {
            if(isModifierKey(event)) {return;}

            const input = event.target.value.replace(/\D/g,'').substring(0,30); // 25 digits of input only
            event.target.value = `${input}`;
            // const areaCode = input.substring(0,3);
            // const middle = input.substring(3,6);
            // const last = input.substring(6,10);

            // if(input.length > 6){event.target.value = `+${areaCode} ${middle} - ${last}`;}
            // else if(input.length > 3){event.target.value = `+${areaCode} ${middle}`;}
            // else if(input.length > 0){event.target.value = `+${areaCode}`;}
        };

        function phoneNumberValidation(element) 
        {  
            const event = document.getElementById(element.id);
            event.addEventListener('keydown',enforceFormat);
            event.addEventListener('keyup',formatToPhone);
        }

        const phone_number = document.getElementById('phone_number');
        phone_number.addEventListener('keydown',enforceFormat);
        phone_number.addEventListener('keyup',formatToPhone);

        // function IntegerLimit(event,element,limit)
        // {
        //     var max_chars = limit;

        //     var n =  element.value;
        //     var digits = (""+n).split("");
        //     // console.log(digits.length);
        //     if(digits.length === 0)
        //     {
        //         element.value = "";
        //     }
        //     else if(digits.length === 1)
        //     {
        //         if(digits[0] == 0)
        //         {
        //             element.value = "";
        //         }
        //     }
        //     if(element.value.length > max_chars) 
        //     {
        //         element.value = element.value.substr(0, max_chars);
        //     }
        // }

        function ValidateEmail(element,msg_id) 
        {
            if(element.value == "")
            {
                element.value = "";
                RemoveClassJS(msg_id, "text-success");
                AddClassJS(msg_id, "text-danger");
                document.getElementById(msg_id).innerHTML = "Please enter the email address!";
            }
            else if (/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(element.value))
            {
                RemoveClassJS(msg_id, "text-danger");
                AddClassJS(msg_id, "text-success");
                document.getElementById(msg_id).innerHTML = "You have entered an valid email address!";
                return (true)
            }
            else{

                RemoveClassJS(msg_id, "text-success");
                AddClassJS(msg_id, "text-danger");
                document.getElementById(msg_id).innerHTML = "You have entered an invalid email address!";

                return (false)
            }
        }

        function AddClassJS(Id, name)
        {
            var element, arr;
            element = document.getElementById(Id);
            // name = "mystyle";
            arr = element.className.split(" ");
            if (arr.indexOf(name) == -1) {
                element.className += " " + name;
            }
        }

        function RemoveClassJS(Id, name) 
        {
            var element = document.getElementById(Id);
            element.classList.remove(name);
        }

        function ValidExpiry() 
        {
            let issueDate = $("#three_issue_date").val();
            let ExpiryDate = $("#three_expiration").val();

            issueDate = new Date(issueDate);
            ExpiryDate = new Date(ExpiryDate);

            if(ExpiryDate > issueDate){
                // alert('Given date is greater than the current date.');
            }else{
                $("#three_expiration").val("");
            }
        }
        
        function alphaOnly(event) 
        {
            var key = event.keyCode;
            return ((key >= 65 && key <= 90) || key == 8 || key == 32 || (isModifierKey(event)));
        };


        function alphaNumeric(event)
        {
            return (alphaOnly(event) || onlyNumberKey(event));
        }

        function onlyNumberKey(event) 
        {
            var key = event.keyCode;
            return ((key >= 48 && key <= 57) || (key >= 96 && key <= 105) || key == 8 || (isModifierKey(event)) || key == 110 || key == 190 );
        }

        function IntegerLimit(event,element,limit)
        {
            var max_chars = limit;
            var key = event.keyCode;

            var n =  element.value;
            var digits = (""+n).split("");
            // console.log("digits.length",digits.length);

            if((digits.length === 1) && key != 8 )
            {
                if(digits[0] == "0"  || digits[0] == null)
                {
                    element.value = "";
                }
            }
            if((key >= 48 && key <= 57) || (key >= 96 && key <= 105) || key == 8 )
            {
                if(digits.length > (max_chars) )
                {
                    if(key == 8)
                    {
                        return true;
                    }
                    element.value = element.value.substr(0, max_chars);
                }
                return true;
            }
            else{
                return false;
            }
        }

        // Global variable For Employed
        var e_employee_name       = "";
        var e_occupation          = "";
        var e_employee_address    = "";
        var e_year_of_employment  = "";
        var e_employee_country    = "";
        var e_employee_state      = "";
        var e_employee_city       = "";
        var e_employee_apt_suite  = "";
        var e_employee_phone_no   = "";
        var e_employee_fax        = "";

        // Global variable For Self Employed
        var se_employee_name       = "";
        var se_occupation          = "";
        var se_employee_address    = "";
        var se_year_of_employment  = "";
        var se_employee_country    = "";
        var se_employee_state      = "";
        var se_employee_city       = "";
        var se_employee_apt_suite  = "";
        var se_employee_phone_no   = "";
        var se_employee_fax        = "";

        $(document).on('click', 'input[name=employee]', function() {
            
            let _this = $(this);
            var val = _this.val();
            // console.log("bilal");
            if(val == "self employed")
            {
                if($("#second_form_selected_value").val() == "employed")
                {
                    e_employee_name         = $('#employee_name').val();
                    e_occupation            = $('#occupation').val();
                    e_employee_address      = $('#employee_address').val();
                    e_year_of_employment    = $('#year_of_employment').val();
                    e_employee_country      = $('#employee_country').val();
                    e_employee_state        = $('#employee_state').val();
                    e_employee_city         = $('#employee_city').val();
                    e_employee_apt_suite    = $('#employee_apt_suite').val();
                    e_employee_phone_no     = $('#employee_phone_no').val();
                    e_employee_fax          = $('#employee_fax').val();

                    // $('input[type=radio][name=employee][value=employed]').attr("checked", false);
                }
                else if($("#second_form_selected_value").val() == "retired")
                {
                    // $('input[type=radio][name=employee][value=retired]').attr("checked", false);
                }
                $('#employee_name').val(se_employee_name);
                $('#occupation').val(se_occupation);
                $('#employee_address').val(se_employee_address);
                $('#year_of_employment').val(se_year_of_employment);
                $('#employee_country').val(se_employee_country);
                $('#employee_state').val(se_employee_state);
                $('#employee_city').val(se_employee_city);
                $('#employee_apt_suite').val(se_employee_apt_suite);
                $('#employee_phone_no').val(se_employee_phone_no);
                $('#employee_fax').val(se_employee_fax);
                $("#second_form_selected_value").val("self employed");

                // _this.attr("checked", true);

                // $('input[type=radio][name=employee][value=self employed]').attr("checked", true);
            }
            else if(val == "employed")
            {   
                if($("#second_form_selected_value").val() == "self employed")
                {
                    se_employee_name         = $('#employee_name').val();
                    se_occupation            = $('#occupation').val();
                    se_employee_address      = $('#employee_address').val();
                    se_year_of_employment    = $('#year_of_employment').val();
                    se_employee_country      = $('#employee_country').val();
                    se_employee_state        = $('#employee_state').val();
                    se_employee_city         = $('#employee_city').val();
                    se_employee_apt_suite    = $('#employee_apt_suite').val();
                    se_employee_phone_no     = $('#employee_phone_no').val();
                    se_employee_fax          = $('#employee_fax').val();
                    // $('input[type=radio][name=employee][value=self employed]').attr("checked", false);

                }
                else if($("#second_form_selected_value").val() == "retired")
                {
                    // $('input[type=radio][name=employee][value=retired]').attr("checked", false);
                }
                $('#employee_name').val(e_employee_name);
                $('#occupation').val(e_occupation);
                $('#employee_address').val(e_employee_address);
                $('#year_of_employment').val(e_year_of_employment);
                $('#employee_country').val(e_employee_country);
                $('#employee_state').val(e_employee_state);
                $('#employee_city').val(e_employee_city);
                $('#employee_apt_suite').val(e_employee_apt_suite);
                $('#employee_phone_no').val(e_employee_phone_no);
                $('#employee_fax').val(e_employee_fax);
                $("#second_form_selected_value").val("employed");

                // _this.attr("checked", true);

                // $('input[type=radio][name=employee][value=employed]').attr("checked", true);

                // $('input[type=radio][name=employee][value=self employed]').attr("checked", false);
            }
            else if(val == "retired")
            {
                if($("#second_form_selected_value").val() == "self employed")
                {
                    se_employee_name         = $('#employee_name').val();
                    se_occupation            = $('#occupation').val();
                    se_employee_address      = $('#employee_address').val();
                    se_year_of_employment    = $('#year_of_employment').val();
                    se_employee_country      = $('#employee_country').val();
                    se_employee_state        = $('#employee_state').val();
                    se_employee_city         = $('#employee_city').val();
                    se_employee_apt_suite    = $('#employee_apt_suite').val();
                    se_employee_phone_no     = $('#employee_phone_no').val();
                    se_employee_fax          = $('#employee_fax').val();
                    // $('input[type=radio][name=employee][value=retired]').attr("checked", true);

                    // $('input[type=radio][name=employee][value=self employed]').attr("checked", false);
                }
                else if($("#second_form_selected_value").val() == "employed")
                {
                    e_employee_name         = $('#employee_name').val();
                    e_occupation            = $('#occupation').val();
                    e_employee_address      = $('#employee_address').val();
                    e_year_of_employment    = $('#year_of_employment').val();
                    e_employee_country      = $('#employee_country').val();
                    e_employee_state        = $('#employee_state').val();
                    e_employee_city         = $('#employee_city').val();
                    e_employee_apt_suite    = $('#employee_apt_suite').val();
                    e_employee_phone_no     = $('#employee_phone_no').val();
                    e_employee_fax          = $('#employee_fax').val();
                    // $('input[type=radio][name=employee][value=retired]').attr("checked", true);

                    // $('input[type=radio][name=employee][value=employed]').attr("checked", false);
                }
                    // $('input[type=radio][name=employee][value=retired]').attr("checked", true);

                // _this.attr("checked", true);

                $("#second_form_selected_value").val("retired");

            }
        });
  
 