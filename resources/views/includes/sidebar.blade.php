 <!-- Navbar -->
 <nav class="main-header navbar navbar-expand navbar-white navbar-light">
            <!-- Left navbar links -->
            <ul class="navbar-nav">
                <li class="nav-item">
                    <a class="nav-link" data-widget="pushmenu" href="#" role="button"><i class="fas fa-bars"></i></a>
                </li>
                <li class="nav-item d-none d-sm-inline-block">
                    <a href="" class="nav-link">Home</a>
                </li>
                
                <?php if(Auth::user()->hasRole('affiliate_customer'))
                {
                    ?>
                        <li class="nav-item d-none d-sm-inline-block">
                            <p class="nav-link">Affiliate Code: <b><?= Auth::user()->affiliate_code?></b></p>
                        </li>
                        <li class="nav-item d-none d-sm-inline-block">
                            <p class="nav-link"> Total Affiliates: <?= Auth::user()->user_count; ?>

                                
                            </p>
                         
                        </li>
                        <li class="nav-item d-none d-sm-inline-block">
                            <p class="nav-link">Affiliate Link: <b><a target="_blank" href="<?= url('customer-register/'.Auth::user()->affiliate_code)?>"><?= url('customer-register/'.Auth::user()->affiliate_code)?></a></b></p>
                        </li>
                    <?php
                }
                ?>
                
            </ul>

           

            <!-- Right navbar links -->
            <ul class="navbar-nav ml-auto">
                
                <li class="nav-item">
                    <a class="nav-link" data-widget="fullscreen" href="#" role="button">
                        <i class="fas fa-expand-arrows-alt"></i>
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" data-widget="control-sidebar" data-slide="true" href="#" role="button">
                        <i class="fas fa-th-large"></i>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- /.navbar -->

        <!-- Main Sidebar Container -->
        <aside class="main-sidebar sidebar-dark-primary elevation-4">
            <!-- Brand Logo -->
            <a href="{{route('admin.home')}}" class="brand-link">
                <img src="{{ asset('assets/AdminLTELogo.png')}}" alt="AdminLTE Logo" style="opacity: 0.8; width: 80%">
                {{-- <span class="brand-text font-weight-light">Affiliate</span> --}}
            </a>

            <!-- Sidebar -->
            <div class="sidebar">
                

                <!-- Sidebar Menu -->
                <nav class="mt-2">
                    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                        <!-- Add icons to the links using the .nav-icon class with font-awesome or any other icon font library -->
                       
                        <li class="nav-item">
                            <a href="{{route('admin.home')}}" class="nav-link ">
                            <i class="nav-icon fas fa-tachometer-alt"></i>
                                <p>
                                    Dashboard
                                    <!-- <span class="right badge badge-danger">New</span> -->
                                </p>
                            </a>
                        </li>
                        <?php if(Auth::user()->hasRole('SuperAdmin'))
                              {
                        ?>

                            <li class="nav-item <?= (@$title == 'Applications')?'menu-is-opening menu-open':''?>">
                                <a href="#" class="nav-link">
                                    <i class="nav-icon fas fa-chart-pie"></i>
                                    <p>
                                      Applications
                                      <i class="right fas fa-angle-left"></i>
                                    </p>
                                  </a>
                                  <ul class="nav nav-treeview">
                                    <li class="nav-item">
                                      <a href="{{route('admin.users', 1)}}" class="nav-link">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Pending Approvals</p>
                                      </a>
                                    </li>
                                    <li class="nav-item">
                                      <a href="{{route('admin.users', 3)}}" class="nav-link">
                                        <i class="far fa-circle nav-icon"></i>
                                        <p>Accepted</p>
                                      </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="{{route('admin.users', 4)}}" class="nav-link">
                                          <i class="far fa-circle nav-icon"></i>
                                          <p>Rejected</p>
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a href="{{route('admin.users', 0)}}" class="nav-link">
                                          <i class="far fa-circle nav-icon"></i>
                                          <p>Draft</p>
                                        </a>
                                    </li>
                                </ul>
                            </li>

                             <li class="nav-item">
                                <a href="{{ route('admin.affiliate_users') }}" class="nav-link ">
                                <i class="nav-icon fas fa-user"></i>
                                    <p>
                                        Affiliate Users
                                    </p>
                                </a>
                            </li>

                            <li class="nav-item">
                                <a href="{{ route('admin.users') }}" class="nav-link ">
                                <i class="nav-icon fas fa-user"></i>
                                    <p>
                                        Users
                                    </p>
                                </a>
                            </li>
                        <?php } ?>

                        <?php if(Auth::user()->hasRole('affiliate_customer'))
                              {
                        ?>
                        <li class="nav-item <?= (@$title == 'Applications')?'menu-is-opening menu-open':''?>">
                            <a href="#" class="nav-link">
                                <i class="nav-icon fas fa-chart-pie"></i>
                                <p>
                                  Applications
                                  <i class="right fas fa-angle-left"></i>
                                </p>
                              </a>
                              <ul class="nav nav-treeview">
                                <li class="nav-item">
                                  <a href="{{route('affilate_customer.users', 1)}}" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Pending Approvals</p>
                                  </a>
                                </li>
                                <li class="nav-item">
                                  <a href="{{route('affilate_customer.users', 3)}}" class="nav-link">
                                    <i class="far fa-circle nav-icon"></i>
                                    <p>Accepted</p>
                                  </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{route('affilate_customer.users', 4)}}" class="nav-link">
                                      <i class="far fa-circle nav-icon"></i>
                                      <p>Rejected</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{route('affilate_customer.users', 0)}}" class="nav-link">
                                      <i class="far fa-circle nav-icon"></i>
                                      <p>Draft</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                             <li class="nav-item">
                                <a href="{{ route('affilate_customer.users') }}" class="nav-link ">
                                <i class="nav-icon fas fa-user"></i>
                                    <p>
                                        Users
                                    </p>
                                </a>
                            </li>
                        <?php } ?>

                        <li class="nav-item selected">
                            <a href="#" class="nav-link active">
                                <i class="nav-icon fas fa-edit"></i>
                                <p>
                                    Setting
                                    <i class="fas fa-angle-left right"></i>
                                </p>
                            </a>
                            <ul class="nav nav-treeview in">
                                <li class="nav-item">
                                    <a href="/admin/email-credentials" class="nav-link active">
                                        <p>Email credentials</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{route('admin.reject_email_template')}}" class="nav-link">
                                        <p>Rejected Email Template</p>
                                    </a>
                                </li>
                                <li class="nav-item">
                                    <a href="{{route('admin.accept_email_template')}}" class="nav-link">
                                        <p>Accepted Email Template</p>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('logout') }}">
                                Logout
                            </a>
                        </li>

                    </ul>
                </nav>
                <!-- /.sidebar-menu -->
            </div>
            <!-- /.sidebar -->
        </aside>
