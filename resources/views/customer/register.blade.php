@extends('layouts.customer.login')

@push('custom-css')

    <style>
        label {
            font-size: 16px;
        }
        .error{
            margin-top: 0px;
            color: red;
        }
    </style>
@endpush

@section('content')
<section class="wpb_loader">
    <div class="loader"></div>
</section>

<div class="container-fluid" style="background-color: #000">
    <div class="container">
        <br /><br /><br />
        <!-- Rigister form -->
        <div class="row justify-content-center">
            <div class="col-md-8">
                <form method="POST" class="ajaxForm validate" action="{{ route('customer.register.form') }}">
                    @csrf
                    <div class="reg_form">
                        <h3 class="text-center"><b>Begin an Online Application</b></h3>
                        <p class="text-center">
                            Already have an account? <a href="{{route('customer.login')}}">Sign in...</a>
                        </p>
    
                        <div class="form-group">
                            <label for="email">Your Email*</label>
                            <input name="email" type="email" class="form-control" placeholder="Email" id="email" required=""
                                autofocus="">
                        </div>
    
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="registration_type">Registration Type*</label>
                                    <select name="registrationType" id="registration_type" class="selectpicker form-control"
                                        required="">
                                        <option value="" style="display: none">
                                            Select one
                                        </option>
                                        <option value="Individual Account">
                                            Individual Account
                                        </option>
                                        <option value="JTWROS">JTWROS</option>
                                        <option value="Tenants in Common">
                                            Tenants in Common
                                        </option>
    
                                        <option value="Corporate">Corporate</option>
                                        <option value="Trust">Trust</option>
                                        <option value="Limited Liability Company">
                                            Limited Liability Company
                                        </option>
                                        <option value="Limited Partnership">
                                            Limited Partnership
                                        </option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="product_trade">Product you want to trade*</label>
                                    <select name="productTrade" id="product_trade" class="selectpicker form-control"
                                        required="">
                                        <option value="Stock">Stocks</option>
                                    </select>
                                </div>
                            </div>
    
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="hear_about">How did you hear about us?</label>
                                    <select name="hearAbout" id="hear_about" class="selectpicker form-control" required="">
                                        <option value="" style="display: none">
                                            Select one
                                        </option>
                                        <option value="Google">Google</option>
                                        <option value="Bloomberg">Bloomberg</option>
                                        <option value="Other">Other</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="affiliate_code">Affiliate Code*</label>
                            <input name="affiliate_code" type="text" value="<?= @$data?>" class="form-control" placeholder="Affiliate Code" id="affiliate_code" 
                                autofocus="">
                        </div>
    
                        <div class="form-group">
                            <div class="pwdbox-holder">
                                <label for="password">Create Password *</label>
                                <span toggle="#password" class="fa fa-fw fa-eye field-icon toggle-password"></span>
                                <input name="password" type="password" id="password" class="form-control"
                                    placeholder="Password"
                                    required="required">
                                {{-- <input name="password" type="password" id="password" class="form-control"
                                    placeholder="Password"
                                    title="Must contain at least one  number , one uppercase , lowercase letter, at least 8 characters, maximum 25 characters and one special characters"
                                    pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{8,25}$" required="required"> --}}
                            </div>
                        </div>
    
                        <div class="form-group">
                            <div class="pwdbox-holder">
                                <label for="passwordConfirmation">Confirm Password *</label>
                                <span toggle="#passwordConfirmation"
                                    class="fa fa-fw fa-eye field-icon toggle-password"></span>
                                    <input type="password" name="passwordConfirmation" id="passwordConfirmation"
                                    class="form-control" placeholder="Password" required="">
                                    {{-- <input type="password" name="passwordConfirmation" id="passwordConfirmation"
                                    class="form-control" placeholder="Password"
                                    title="Must contain at least one  number , one uppercase , lowercase letter, at least 8 characters, maximum 25 characters and one special characters"
                                    pattern="^(?=.*[a-z])(?=.*[A-Z])(?=.*\d).{8,25}$" required=""> --}}
                            </div>
                        </div>
                        <input id="lat" name="lat" type="hidden" class="form-control">
                        <input id="long" name="long" type="hidden" class="form-control">
                        {{-- <input type="submit" value="Sign up" class="btn btn-primary btn-block"> --}}
                        <button type="button" class="btn btn-primary btn-block ajaxFormSubmitAlter">
                            Sign Up
                        </button>
                    </div>
                </form>
            </div>
        </div>
        <!-- End -->
        
        <br /><br /><br />
    </div>
</div>


@endsection

@section('footer')
<script>
    $(document).ready(function() {
        $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
          $("form.validate").validate({
            rules: {
            
              email: {
                required: true
              },
              password: {
                required: true
              },
              passwordConfirmation:
              {
                required: true,
                equalTo : "#password"
              }
            },
            messages: {
                name: "This field is required.",
                email: "This field is required.",
                password: "This field is required.",
                passwordConfirmation: "This field is required.",
                
            },
            invalidHandler: function(event, validator) {
              //display error alert on form submit 
              error(validator.event);
            },
            errorPlacement: function(label, element) { // render error placement for each input type  
              $(element).addClass("border-red");
            },
            highlight: function(element) { // hightlight error inputs
              $(element).removeClass('border-green').addClass("border-red");
            },
            unhighlight: function(element) { // revert the change done by hightlight
              $(element).removeClass('border-red').addClass("border-green");
            },
            success: function(label, element) {
              $(element).removeClass('border-red').addClass("border-green");
            }
            // submitHandler: function (form) {
            // }
          });
        });
    </script>
@endsection