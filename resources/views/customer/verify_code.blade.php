@extends('layouts.customer.login')

@push('custom-css')
<style>
    .forgot-pwd form input[type='submit'] {
        background: rgba(66, 144, 202, 1);
        border: none;
        margin-top: 15px;
        width: 100%;
        color: #fff;
        text-transform: uppercase;
        font-size: 18px;
        display: inline-block;
        padding: 10px 30px;
        font-weight: 700;
    }
</style>
@endpush

@section('content')
<section class="wpb_loader">
    <div class="loader"></div>
</section>

<div class="container-fluid" style="background-color: #000">
    <div class="container">
        <br /><br /><br />
        <!-- Rigister form -->
        <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="forgot-pwd reg_form">
                    <form  action="{{ route('activeAccount')}}" method="POST" class="p-0">
                        @csrf
                        <div class="">
                            <h5>Email Authentication</h5>
                            <hr />
                            <input type="password" name="verification_code" placeholder="Verification Code"
                                class="form-control validate" required />
                                @if (Session::has("error"))
                                    <p class="text-danger">{{Session::get('error')}}</p>
                                @endif
                                <p></p>
                            <input type="submit" name="submit" class="" style="width: 55%" />
                            {{-- <br> --}}
                            <a href="{{route('customer.verification')}}" class="float-right mt-4">Resend Code</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <!-- End -->
        <br /><br /><br />
    </div>
</div>
{{-- <div class="container-fluid">
    <div class="row">
      <div class="container">
        <div class="row">
          <div class="col-md-3"></div>
          <!-- // Col // -->
          <div class="col-md-6">
            <div class="forgot-pwd">
              <form action="/email-verify" method="post">
                <h5>Email Authentication</h5>
                <hr />
                
  

                <input
                  type="password"
                  name="verification_code"
                  placeholder="Verification Code"
                  class="form-control validate"
                  required
                />
                <input type="submit" name="submit" style="width: 55%" />
                <a href="verification-send" style="margin-left: 90px"
                  >Resend Code</a
                >
              </form>
            </div>
          </div>
          <!-- // Col // -->
          <div class="col-md-3"></div>
          <!-- // Col // -->
        </div>
        <!-- // Row // -->
      </div>
      <!-- // Container // -->
    </div>
    <!-- // Row // -->
  </div>
  <!-- // Container-fluid // --> --}}
@endsection

@section('footer')
<script>
    $(document).ready(function () {
        $("form.validate").validate({
            rules: {
                email: {
                    required: true
                },
                password: {
                    required: true
                },

            },
            messages: {
                email: "This field is required.",
                password: "This field is required.",

            },
            invalidHandler: function (event, validator) {
                //display error alert on form submit 
                $('#fail').show();
                $('#fail-text').html("Please fill all mandatory fields.");

            },
            errorPlacement: function (label, element) { // render error placement for each input type  
                $(element).addClass("border-red");
            },
            highlight: function (element) { // hightlight error inputs
                $(element).removeClass('border-green').addClass("border-red");
            },
            unhighlight: function (element) { // revert the change done by hightlight
                $(element).removeClass('border-red').addClass("border-green");
            },
            success: function (label, element) {
                $(element).removeClass('border-red').addClass("border-green");

            }
            // submitHandler: function (form) {
            // }
        });
    });
</script>
@endsection