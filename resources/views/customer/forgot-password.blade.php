@extends('layouts.customer.login')

@section('content')
<section class="wpb_loader">
    <div class="loader"></div>
</section>

<div class="container-fluid" style="background-color: #000">
    <div class="container">
        <br /><br /><br />
        <!-- Rigister form -->
        <div class="row">
            <div class="col-md-2">
                <div class="card-header"></div>
            </div>
            <div class="col-md-8">
                @if (Session::has('message'))
				<div class="alert alert-success alert-icon-info alert-dismissible" id="success" role="alert">
					<button type="button" class="close" data-dismiss="alert">×</button>
					<div class="alert-message">
					  <span id="success-text">{{Session::get('message')}}</span>
					</div>
				</div>
				@endif
                <form action="{{route('forgot-password.store')}}" method="post">
                @csrf
                    <div class="forgot-pwd">
                        <div class="forgot-pwd-div">
                            <h5>Forget Password</h5>
                            <hr>
                            <div class="col-xl-12 mb-4">
                                <input type="email" <?= (Session::has('message')) ? 'readonly' : ''; ?> name="email" placeholder="Enter Email Address*" class="mb-0" value="{{old('email')}}">
                                @if($errors->any())
                                <p class="text-left text-danger ml-2">{{$errors->first()}}</p>
                                @endif
                            </div>
                            <div class="col-xl-12">
                                <input type="submit" name="submit" value="Reset Password">
                            </div>
                        </div>
                    </div>
            </div>
            </form>
        </div>
        <div class="col-md-2"></div>
    </div>
    <!-- End -->
    <br /><br /><br />
</div>
</div>