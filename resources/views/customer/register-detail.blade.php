<!DOCTYPE html>
<html lang="en">
    <head>
    <title>Guardian</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="{{asset('assets/frontend/css/main.css')}}">
    <link rel="stylesheet" href="{{asset('assets/frontend/css/custom-register.css')}}">

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <link type="text/css" href="https://stackpath.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">

    <link rel="shortcut icon" href="{{ asset('assets/frontend/img/newfavicon.png') }}" type="image/x-icon" />
    <script src="https://github.com/pipwerks/PDFObject/blob/master/pdfobject.min.js"></script>
    
    </head>

    <body>
        <div class="container-fluid">
            <div class="row topheader">
                <div class="headwrap">
                    <div class="topnbmr">
                        <a href="tel:8449631512">
                            <i class="fa fa-phone" data-name="phone-alt" style="margin-right: 3px;font-size: 20px;"></i> 844-963-1512
                        </a>
                    </div>
                </div>
            </div>
            <div class="row navmain">
                <div class="headwrap">
                    <nav class="navbar navbar-expand-lg">
                        <a class="navbar-brand" href="#"><img src="assets/img/logo.png" alt="Guardian-Logo" title="Guardian-Logo"></a>
                        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
                            <span class="navbar-toggler-icon"></span>
                            <span class="navbar-toggler-icon"></span>
                            <span class="navbar-toggler-icon"></span>
                        </button>
                        <div class="collapse navbar-collapse" id="navbarNavDropdown">
                            <ul class="navbar-nav ml-auto mr-auto">
                                <li class="nav-item"><a class="nav-link" href="javascript:;">home</a></li>
                                <li class="nav-item"><a class="nav-link" href="javascript:;">About Us</a></li>
                                <li class="nav-item"><a class="nav-link" href="javascript:;">Services </a></li>
                                <li class="nav-item"><a class="nav-link" href="javascript:;">Platforms</a></li>
                                <li class="nav-item"><a class="nav-link" href="javascript:;">Pricing</a></li>
                                <li class="nav-item"><a class="nav-link" href="javascript:;">Contact Us</a></li>
        
        
                            </ul>
                            <ul class="login-area">
                                <li><a href="/logout">Logout</a></li>
                                <li><a href="javascript:;">Open an account</a></li>
                            </ul>
                        </div>
                    </nav>
                </div>
        
            </div>
        </div>
        <!--End Header-->
        <!--Main Content-->
        <div class="container-fluid">
            <div class="container-fluid">
                </div>

                <section class="wizard-section">
                    <div class="row no-gutters">
                      <div class="col-lg-12 col-md-12">
                        <div class="form-wizard">
                          
                            <div class="form-wizard-header">
                              <ul class="list-unstyled form-wizard-steps clearfix">
                                <li class="active">
                                    <span>1</span>
                                    <p class="form-name">PERSONAL DETAILS</p>
                                </li>
                                <li>
                                    <span>2</span>
                                    <p class="form-name">PROFESSIONAL DETAILS</p>
                                </li>
                                <li><span>3</span><p class="form-name">ID INFORMATION</p></li>
                                <li><span>4</span><p class="form-name">INCOME DETAILS</p></li>
                                <li><span>5</span><p class="form-name">FUNDING DETAILS</p></li>
                                <li><span>6</span><p class="form-name">RISK ACCEPTANCE</p></li>
                                <li><span>7</span><p class="form-name">FINANCIAL SITUATION</p></li>
                                <li><span>8</span><p class="form-name">INVESTMENT EXPEIRENCE</p></li>
                                <li><span>9</span><p class="form-name">IDENTIFICATION PROOF UPLOAD</p></li>
                                <li><span>10</span><p class="form-name">DISCLOSURES</p></li>
                                <li><span>11</span><p class="form-name">SIGNATURES</p></li>
                              </ul>
                            </div>
                            <div class="container" style="margin-top: 100px;">
                            <fieldset class="wizard-fieldset show">
                              <h5>PERSONAL DETAILS</h5>
                               <div class="row">
                               <form action="" method="post" class="row" id="steponeForm">
                                <!-- <div class="form-group col-md-6">
                                    <input type="text" class="form-control wizard-required" id="fname">
                                    <label for="fname" class="wizard-form-text-label">First Name*</label>
                                    <div class="wizard-form-error"></div>
                                </div>
                                <div class="form-group col-md-6">
                                    <input type="text" class="form-control wizard-required" id="lname">
                                    <label for="lname" class="wizard-form-text-label">Last Name*</label>
                                    <div class="wizard-form-error"></div>
                                </div>


                                <div class="form-group col-md-8">
                                    <input type="text" class="form-control wizard-required" id="address">
                                    <label for="address" class="wizard-form-text-label">Address*</label>
                                    <div class="wizard-form-error"></div>
                                </div>
                                <div class="form-group col-md-4">
                                    <input type="text" class="form-control wizard-required" id="aptsuite">
                                    <label for="aptsuite" class="wizard-form-text-label">Apt/Suite*</label>
                                    <div class="wizard-form-error"></div>
                                </div>


                                <div class="form-group col-md-5">
                                    <select name="country" id="country" class="form-control wizard-required" required="required"><option value="">Country*</option><option value="United States" id="5f80e1963884e632f4a55785">United States</option><option value="Afghanistan" id="5f80e1963884e632f4a55786">Afghanistan</option><option value="Aland Islands" id="5f80e1963884e632f4a55787">Aland Islands</option><option value="Albania" id="5f80e1973884e632f4a55788">Albania</option><option value="Angola" id="5f80e1973884e632f4a5578c">Angola</option><option value="Andorra" id="5f80e1973884e632f4a5578b">Andorra</option><option value="Anguilla" id="5f80e1973884e632f4a5578d">Anguilla</option><option value="Antarctica" id="5f80e1973884e632f4a5578e">Antarctica</option><option value="Algeria" id="5f80e1973884e632f4a55789">Algeria</option><option value="American Samoa" id="5f80e1973884e632f4a5578a">American Samoa</option><option value="Argentina" id="5f80e1973884e632f4a55790">Argentina</option><option value="Antigua And Barbuda" id="5f80e1973884e632f4a5578f">Antigua And Barbuda</option><option value="Australia" id="5f80e1973884e632f4a55793">Australia</option><option value="Aruba" id="5f80e1973884e632f4a55792">Aruba</option><option value="Armenia" id="5f80e1973884e632f4a55791">Armenia</option><option value="Azerbaijan" id="5f80e1973884e632f4a55795">Azerbaijan</option><option value="Austria" id="5f80e1973884e632f4a55794">Austria</option><option value="Bahamas The" id="5f80e1973884e632f4a55796">Bahamas The</option><option value="Bangladesh" id="5f80e1973884e632f4a55798">Bangladesh</option><option value="Bahrain" id="5f80e1973884e632f4a55797">Bahrain</option><option value="Belarus" id="5f80e1973884e632f4a5579a">Belarus</option><option value="Barbados" id="5f80e1973884e632f4a55799">Barbados</option><option value="Bouvet Island" id="5f80e1973884e632f4a557a3">Bouvet Island</option><option value="Belgium" id="5f80e1973884e632f4a5579b">Belgium</option><option value="Bolivia" id="5f80e1973884e632f4a557a0">Bolivia</option><option value="Botswana" id="5f80e1973884e632f4a557a2">Botswana</option><option value="Benin" id="5f80e1973884e632f4a5579d">Benin</option><option value="Bhutan" id="5f80e1973884e632f4a5579f">Bhutan</option><option value="Belize" id="5f80e1973884e632f4a5579c">Belize</option><option value="Brazil" id="5f80e1973884e632f4a557a4">Brazil</option><option value="Bermuda" id="5f80e1973884e632f4a5579e">Bermuda</option><option value="Bosnia and Herzegovina" id="5f80e1973884e632f4a557a1">Bosnia and Herzegovina</option><option value="British Indian Ocean Territory" id="5f80e1973884e632f4a557a5">British Indian Ocean Territory</option><option value="Cape Verde" id="5f80e1973884e632f4a557ad">Cape Verde</option><option value="Cambodia" id="5f80e1973884e632f4a557aa">Cambodia</option><option value="Cayman Islands" id="5f80e1973884e632f4a557ae">Cayman Islands</option><option value="Canada" id="5f80e1973884e632f4a557ac">Canada</option><option value="Burkina Faso" id="5f80e1973884e632f4a557a8">Burkina Faso</option><option value="Brunei" id="5f80e1973884e632f4a557a6">Brunei</option><option value="Cameroon" id="5f80e1973884e632f4a557ab">Cameroon</option><option value="Central African Republic" id="5f80e1973884e632f4a557af">Central African Republic</option><option value="Bulgaria" id="5f80e1973884e632f4a557a7">Bulgaria</option><option value="Burundi" id="5f80e1973884e632f4a557a9">Burundi</option><option value="Chad" id="5f80e1973884e632f4a557b0">Chad</option><option value="China" id="5f80e1973884e632f4a557b2">China</option><option value="Chile" id="5f80e1973884e632f4a557b1">Chile</option><option value="Colombia" id="5f80e1973884e632f4a557b5">Colombia</option><option value="Christmas Island" id="5f80e1973884e632f4a557b3">Christmas Island</option><option value="Cocos (Keeling) Islands" id="5f80e1973884e632f4a557b4">Cocos (Keeling) Islands</option><option value="Comoros" id="5f80e1973884e632f4a557b6">Comoros</option><option value="Costa Rica" id="5f80e1973884e632f4a557ba">Costa Rica</option><option value="Cuba" id="5f80e1973884e632f4a557bd">Cuba</option><option value="Cook Islands" id="5f80e1973884e632f4a557b9">Cook Islands</option><option value="Congo" id="5f80e1973884e632f4a557b7">Congo</option><option value="Congo The Democratic Republic Of The" id="5f80e1973884e632f4a557b8">Congo The Democratic Republic Of The</option><option value="Cote D'Ivoire (Ivory Coast)" id="5f80e1973884e632f4a557bb">Cote D'Ivoire (Ivory Coast)</option><option value="Croatia (Hrvatska)" id="5f80e1973884e632f4a557bc">Croatia (Hrvatska)</option><option value="Djibouti" id="5f80e1973884e632f4a557c1">Djibouti</option><option value="Dominican Republic" id="5f80e1973884e632f4a557c3">Dominican Republic</option><option value="Czech Republic" id="5f80e1973884e632f4a557bf">Czech Republic</option><option value="Denmark" id="5f80e1973884e632f4a557c0">Denmark</option><option value="Cyprus" id="5f80e1973884e632f4a557be">Cyprus</option><option value="Dominica" id="5f80e1973884e632f4a557c2">Dominica</option><option value="Ecuador" id="5f80e1973884e632f4a557c5">Ecuador</option><option value="East Timor" id="5f80e1973884e632f4a557c4">East Timor</option><option value="Egypt" id="5f80e1973884e632f4a557c6">Egypt</option><option value="Eritrea" id="5f80e1973884e632f4a557c9">Eritrea</option><option value="Equatorial Guinea" id="5f80e1973884e632f4a557c8">Equatorial Guinea</option><option value="El Salvador" id="5f80e1973884e632f4a557c7">El Salvador</option><option value="Faroe Islands" id="5f80e1973884e632f4a557cd">Faroe Islands</option><option value="Falkland Islands" id="5f80e1973884e632f4a557cc">Falkland Islands</option><option value="Finland" id="5f80e1973884e632f4a557cf">Finland</option><option value="France" id="5f80e1973884e632f4a557d0">France</option><option value="Ethiopia" id="5f80e1973884e632f4a557cb">Ethiopia</option><option value="Fiji Islands" id="5f80e1973884e632f4a557ce">Fiji Islands</option><option value="Estonia" id="5f80e1973884e632f4a557ca">Estonia</option><option value="Gambia The" id="5f80e1973884e632f4a557d5">Gambia The</option><option value="Georgia" id="5f80e1973884e632f4a557d6">Georgia</option><option value="Gabon" id="5f80e1973884e632f4a557d4">Gabon</option><option value="French Guiana" id="5f80e1973884e632f4a557d1">French Guiana</option><option value="Germany" id="5f80e1973884e632f4a557d7">Germany</option><option value="French Polynesia" id="5f80e1973884e632f4a557d2">French Polynesia</option><option value="French Southern Territories" id="5f80e1973884e632f4a557d3">French Southern Territories</option><option value="Guam" id="5f80e1973884e632f4a557de">Guam</option><option value="Grenada" id="5f80e1973884e632f4a557dc">Grenada</option><option value="Gibraltar" id="5f80e1973884e632f4a557d9">Gibraltar</option><option value="Ghana" id="5f80e1973884e632f4a557d8">Ghana</option><option value="Guadeloupe" id="5f80e1973884e632f4a557dd">Guadeloupe</option><option value="Greece" id="5f80e1973884e632f4a557da">Greece</option><option value="Greenland" id="5f80e1973884e632f4a557db">Greenland</option><option value="Guinea-Bissau" id="5f80e1973884e632f4a557e2">Guinea-Bissau</option><option value="Guyana" id="5f80e1973884e632f4a557e3">Guyana</option><option value="Guatemala" id="5f80e1973884e632f4a557df">Guatemala</option><option value="Heard and McDonald Islands" id="5f80e1973884e632f4a557e5">Heard and McDonald Islands</option><option value="Haiti" id="5f80e1973884e632f4a557e4">Haiti</option><option value="Guernsey and Alderney" id="5f80e1973884e632f4a557e0">Guernsey and Alderney</option><option value="Guinea" id="5f80e1973884e632f4a557e1">Guinea</option><option value="Hungary" id="5f80e1973884e632f4a557e8">Hungary</option><option value="Hong Kong S.A.R." id="5f80e1973884e632f4a557e7">Hong Kong S.A.R.</option><option value="India" id="5f80e1973884e632f4a557ea">India</option><option value="Honduras" id="5f80e1973884e632f4a557e6">Honduras</option><option value="Iceland" id="5f80e1973884e632f4a557e9">Iceland</option><option value="Iran" id="5f80e1973884e632f4a557ec">Iran</option><option value="Indonesia" id="5f80e1973884e632f4a557eb">Indonesia</option><option value="Ireland" id="5f80e1973884e632f4a557ee">Ireland</option><option value="Israel" id="5f80e1973884e632f4a557ef">Israel</option><option value="Iraq" id="5f80e1973884e632f4a557ed">Iraq</option><option value="Japan" id="5f80e1973884e632f4a557f2">Japan</option><option value="Jordan" id="5f80e1973884e632f4a557f4">Jordan</option><option value="Italy" id="5f80e1973884e632f4a557f0">Italy</option><option value="Jamaica" id="5f80e1973884e632f4a557f1">Jamaica</option><option value="Jersey" id="5f80e1973884e632f4a557f3">Jersey</option><option value="Kazakhstan" id="5f80e1973884e632f4a557f5">Kazakhstan</option><option value="Korea North" id="5f80e1973884e632f4a557f8">Korea North</option><option value="Korea South" id="5f80e1973884e632f4a557f9">Korea South</option><option value="Kiribati" id="5f80e1973884e632f4a557f7">Kiribati</option><option value="Kyrgyzstan" id="5f80e1973884e632f4a557fc">Kyrgyzstan</option><option value="Kosovo" id="5f80e1973884e632f4a557fa">Kosovo</option><option value="Laos" id="5f80e1973884e632f4a557fd">Laos</option><option value="Kuwait" id="5f80e1973884e632f4a557fb">Kuwait</option><option value="Kenya" id="5f80e1973884e632f4a557f6">Kenya</option><option value="Latvia" id="5f80e1973884e632f4a557fe">Latvia</option><option value="Lesotho" id="5f80e1973884e632f4a55800">Lesotho</option><option value="Liechtenstein" id="5f80e1973884e632f4a55803">Liechtenstein</option><option value="Libya" id="5f80e1973884e632f4a55802">Libya</option><option value="Lebanon" id="5f80e1973884e632f4a557ff">Lebanon</option><option value="Liberia" id="5f80e1973884e632f4a55801">Liberia</option><option value="Luxembourg" id="5f80e1973884e632f4a55805">Luxembourg</option><option value="Lithuania" id="5f80e1973884e632f4a55804">Lithuania</option><option value="Macedonia" id="5f80e1973884e632f4a55807">Macedonia</option><option value="Macau S.A.R." id="5f80e1973884e632f4a55806">Macau S.A.R.</option><option value="Madagascar" id="5f80e1973884e632f4a55808">Madagascar</option><option value="Mauritania" id="5f80e1973884e632f4a55811">Mauritania</option><option value="Mali" id="5f80e1973884e632f4a5580c">Mali</option><option value="Man (Isle of)" id="5f80e1973884e632f4a5580e">Man (Isle of)</option><option value="Malawi" id="5f80e1973884e632f4a55809">Malawi</option><option value="Malta" id="5f80e1973884e632f4a5580d">Malta</option><option value="Maldives" id="5f80e1973884e632f4a5580b">Maldives</option><option value="Malaysia" id="5f80e1973884e632f4a5580a">Malaysia</option><option value="Martinique" id="5f80e1973884e632f4a55810">Martinique</option><option value="Marshall Islands" id="5f80e1973884e632f4a5580f">Marshall Islands</option><option value="Mexico" id="5f80e1973884e632f4a55814">Mexico</option><option value="Micronesia" id="5f80e1973884e632f4a55815">Micronesia</option><option value="Moldova" id="5f80e1973884e632f4a55816">Moldova</option><option value="Mauritius" id="5f80e1973884e632f4a55812">Mauritius</option><option value="Mayotte" id="5f80e1973884e632f4a55813">Mayotte</option><option value="Montenegro" id="5f80e1973884e632f4a55819">Montenegro</option><option value="Montserrat" id="5f80e1973884e632f4a5581a">Montserrat</option><option value="Monaco" id="5f80e1973884e632f4a55817">Monaco</option><option value="Mongolia" id="5f80e1973884e632f4a55818">Mongolia</option><option value="Myanmar" id="5f80e1973884e632f4a5581d">Myanmar</option><option value="Namibia" id="5f80e1973884e632f4a5581e">Namibia</option><option value="Mozambique" id="5f80e1973884e632f4a5581c">Mozambique</option><option value="Morocco" id="5f80e1973884e632f4a5581b">Morocco</option><option value="Nauru" id="5f80e1973884e632f4a5581f">Nauru</option><option value="Nepal" id="5f80e1973884e632f4a55820">Nepal</option><option value="Netherlands Antilles" id="5f80e1973884e632f4a55821">Netherlands Antilles</option><option value="New Caledonia" id="5f80e1973884e632f4a55823">New Caledonia</option><option value="Nicaragua" id="5f80e1973884e632f4a55825">Nicaragua</option><option value="New Zealand" id="5f80e1973884e632f4a55824">New Zealand</option><option value="Netherlands The" id="5f80e1973884e632f4a55822">Netherlands The</option><option value="Niger" id="5f80e1973884e632f4a55826">Niger</option><option value="Nigeria" id="5f80e1973884e632f4a55827">Nigeria</option><option value="Niue" id="5f80e1973884e632f4a55828">Niue</option><option value="Northern Mariana Islands" id="5f80e1973884e632f4a5582a">Northern Mariana Islands</option><option value="Norfolk Island" id="5f80e1973884e632f4a55829">Norfolk Island</option><option value="Oman" id="5f80e1973884e632f4a5582c">Oman</option><option value="Norway" id="5f80e1973884e632f4a5582b">Norway</option><option value="Pakistan" id="5f80e1973884e632f4a5582d">Pakistan</option><option value="Panama" id="5f80e1973884e632f4a55830">Panama</option><option value="Paraguay" id="5f80e1973884e632f4a55832">Paraguay</option><option value="Romania" id="5f80e1973884e632f4a5583b">Romania</option><option value="Papua new Guinea" id="5f80e1973884e632f4a55831">Papua new Guinea</option><option value="Poland" id="5f80e1973884e632f4a55836">Poland</option><option value="Peru" id="5f80e1973884e632f4a55833">Peru</option><option value="Palau" id="5f80e1973884e632f4a5582e">Palau</option><option value="Pitcairn Island" id="5f80e1973884e632f4a55835">Pitcairn Island</option><option value="Puerto Rico" id="5f80e1973884e632f4a55838">Puerto Rico</option><option value="Qatar" id="5f80e1973884e632f4a55839">Qatar</option><option value="Palestinian Territory Occupied" id="5f80e1973884e632f4a5582f">Palestinian Territory Occupied</option><option value="Philippines" id="5f80e1973884e632f4a55834">Philippines</option><option value="Portugal" id="5f80e1973884e632f4a55837">Portugal</option><option value="Reunion" id="5f80e1973884e632f4a5583a">Reunion</option><option value="Russia" id="5f80e1973884e632f4a5583c">Russia</option><option value="Saint Helena" id="5f80e1973884e632f4a5583e">Saint Helena</option><option value="Rwanda" id="5f80e1973884e632f4a5583d">Rwanda</option><option value="Saint Kitts And Nevis" id="5f80e1973884e632f4a5583f">Saint Kitts And Nevis</option><option value="Saint Lucia" id="5f80e1973884e632f4a55840">Saint Lucia</option><option value="Saint Pierre and Miquelon" id="5f80e1973884e632f4a55841">Saint Pierre and Miquelon</option><option value="Saint Vincent And The Grenadines" id="5f80e1973884e632f4a55842">Saint Vincent And The Grenadines</option><option value="Sao Tome and Principe" id="5f80e1973884e632f4a55847">Sao Tome and Principe</option><option value="Samoa" id="5f80e1973884e632f4a55845">Samoa</option><option value="Saint-Barthelemy" id="5f80e1973884e632f4a55843">Saint-Barthelemy</option><option value="San Marino" id="5f80e1973884e632f4a55846">San Marino</option><option value="Saint-Martin (French part)" id="5f80e1973884e632f4a55844">Saint-Martin (French part)</option><option value="Saudi Arabia" id="5f80e1973884e632f4a55848">Saudi Arabia</option><option value="Serbia" id="5f80e1973884e632f4a5584a">Serbia</option><option value="Senegal" id="5f80e1973884e632f4a55849">Senegal</option><option value="Sierra Leone" id="5f80e1973884e632f4a5584c">Sierra Leone</option><option value="Singapore" id="5f80e1973884e632f4a5584d">Singapore</option><option value="Slovakia" id="5f80e1973884e632f4a5584e">Slovakia</option><option value="Seychelles" id="5f80e1973884e632f4a5584b">Seychelles</option><option value="South Africa" id="5f80e1973884e632f4a55852">South Africa</option><option value="Somalia" id="5f80e1973884e632f4a55851">Somalia</option><option value="Solomon Islands" id="5f80e1973884e632f4a55850">Solomon Islands</option><option value="Slovenia" id="5f80e1973884e632f4a5584f">Slovenia</option><option value="South Georgia" id="5f80e1973884e632f4a55853">South Georgia</option><option value="Svalbard And Jan Mayen Islands" id="5f80e1973884e632f4a55859">Svalbard And Jan Mayen Islands</option><option value="Sweden" id="5f80e1973884e632f4a5585b">Sweden</option><option value="Swaziland" id="5f80e1973884e632f4a5585a">Swaziland</option><option value="Taiwan" id="5f80e1973884e632f4a5585e">Taiwan</option><option value="Thailand" id="5f80e1973884e632f4a55861">Thailand</option><option value="Tajikistan" id="5f80e1973884e632f4a5585f">Tajikistan</option><option value="Suriname" id="5f80e1973884e632f4a55858">Suriname</option><option value="Switzerland" id="5f80e1973884e632f4a5585c">Switzerland</option><option value="Tanzania" id="5f80e1973884e632f4a55860">Tanzania</option><option value="Spain" id="5f80e1973884e632f4a55855">Spain</option><option value="Sri Lanka" id="5f80e1973884e632f4a55856">Sri Lanka</option><option value="South Sudan" id="5f80e1973884e632f4a55854">South Sudan</option><option value="Syria" id="5f80e1973884e632f4a5585d">Syria</option><option value="Sudan" id="5f80e1973884e632f4a55857">Sudan</option><option value="Trinidad And Tobago" id="5f80e1973884e632f4a55865">Trinidad And Tobago</option><option value="Tokelau" id="5f80e1973884e632f4a55863">Tokelau</option><option value="Togo" id="5f80e1973884e632f4a55862">Togo</option><option value="Tonga" id="5f80e1973884e632f4a55864">Tonga</option><option value="Tunisia" id="5f80e1973884e632f4a55866">Tunisia</option><option value="Turkey" id="5f80e1973884e632f4a55867">Turkey</option><option value="Ukraine" id="5f80e1973884e632f4a5586c">Ukraine</option><option value="Uganda" id="5f80e1973884e632f4a5586b">Uganda</option><option value="Turkmenistan" id="5f80e1973884e632f4a55868">Turkmenistan</option><option value="Tuvalu" id="5f80e1973884e632f4a5586a">Tuvalu</option><option value="Turks And Caicos Islands" id="5f80e1973884e632f4a55869">Turks And Caicos Islands</option><option value="United Arab Emirates" id="5f80e1973884e632f4a5586d">United Arab Emirates</option><option value="United Kingdom" id="5f80e1973884e632f4a5586e">United Kingdom</option><option value="Vatican City State (Holy See)" id="5f80e1973884e632f4a55873">Vatican City State (Holy See)</option><option value="United States Minor Outlying Islands" id="5f80e1973884e632f4a5586f">United States Minor Outlying Islands</option><option value="Uzbekistan" id="5f80e1973884e632f4a55871">Uzbekistan</option><option value="Vanuatu" id="5f80e1973884e632f4a55872">Vanuatu</option><option value="Uruguay" id="5f80e1973884e632f4a55870">Uruguay</option><option value="Virgin Islands (US)" id="5f80e1973884e632f4a55877">Virgin Islands (US)</option><option value="Virgin Islands (British)" id="5f80e1973884e632f4a55876">Virgin Islands (British)</option><option value="Vietnam" id="5f80e1973884e632f4a55875">Vietnam</option><option value="Venezuela" id="5f80e1973884e632f4a55874">Venezuela</option><option value="Zambia" id="5f80e1973884e632f4a5587b">Zambia</option><option value="Wallis And Futuna Islands" id="5f80e1973884e632f4a55878">Wallis And Futuna Islands</option><option value="Yemen" id="5f80e1973884e632f4a5587a">Yemen</option><option value="Western Sahara" id="5f80e1973884e632f4a55879">Western Sahara</option><option value="Zimbabwe" id="5f80e1973884e632f4a5587c">Zimbabwe</option></select>
                                    <div class="wizard-form-error"></div>
                                </div>
                                <div class="form-group col-md-3">
                                    <select name="province" id="province" class="selectpicker form-control wizard-required" required="required" data-toggle="tooltip" title="" data-original-title="States/Province is required"><option value="">States/Province*</option><option value="Abu Dhabi Emirate">Abu Dhabi Emirate</option><option value="Ajman Emirate">Ajman Emirate</option><option value="Dubai">Dubai</option><option value="Fujairah">Fujairah</option><option value="Ras al-Khaimah">Ras al-Khaimah</option><option value="Sharjah Emirate">Sharjah Emirate</option><option value="Umm al-Quwain">Umm al-Quwain</option></select>
                                    
                                    <div class="wizard-form-error"></div>
                                </div>
                                <div class="form-group col-md-2">
                                    <input type="text" class="form-control wizard-required" id="city">
                                    <label for="city" class="wizard-form-text-label">City*</label>
                                    <div class="wizard-form-error"></div>
                                </div>
                                <div class="form-group col-md-2">
                                    <input type="text" class="form-control wizard-required" id="zipcode">
                                    <label for="zipcode" class="wizard-form-text-label">Zip Code*</label>
                                    <div class="wizard-form-error"></div>
                                </div>

                                <div class="form-group col-md-12">
                                    <input class="mailingaddress" type="checkbox" name="mailingaddress" value="1" onchange="valueChanged()"/> Mailing Address (If Different)

                                </div>

                                <div class="form-group col-md-12 mailingdiv" style="display: none;">
                                    <div class="row">
                                        <div class="form-group col-md-8">
                                            <input type="text" class="form-control wizard-required" id="address">
                                            <label for="address" class="wizard-form-text-label">Address*</label>
                                            <div class="wizard-form-error"></div>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <input type="text" class="form-control wizard-required" id="aptsuite">
                                            <label for="aptsuite" class="wizard-form-text-label">Apt/Suite*</label>
                                            <div class="wizard-form-error"></div>
                                        </div>
                                        <div class="form-group col-md-5">
                                            <select name="country" id="country" class="form-control wizard-required" required="required"><option value="">Country*</option><option value="United States" id="5f80e1963884e632f4a55785">United States</option><option value="Afghanistan" id="5f80e1963884e632f4a55786">Afghanistan</option><option value="Aland Islands" id="5f80e1963884e632f4a55787">Aland Islands</option><option value="Albania" id="5f80e1973884e632f4a55788">Albania</option><option value="Angola" id="5f80e1973884e632f4a5578c">Angola</option><option value="Andorra" id="5f80e1973884e632f4a5578b">Andorra</option><option value="Anguilla" id="5f80e1973884e632f4a5578d">Anguilla</option><option value="Antarctica" id="5f80e1973884e632f4a5578e">Antarctica</option><option value="Algeria" id="5f80e1973884e632f4a55789">Algeria</option><option value="American Samoa" id="5f80e1973884e632f4a5578a">American Samoa</option><option value="Argentina" id="5f80e1973884e632f4a55790">Argentina</option><option value="Antigua And Barbuda" id="5f80e1973884e632f4a5578f">Antigua And Barbuda</option><option value="Australia" id="5f80e1973884e632f4a55793">Australia</option><option value="Aruba" id="5f80e1973884e632f4a55792">Aruba</option><option value="Armenia" id="5f80e1973884e632f4a55791">Armenia</option><option value="Azerbaijan" id="5f80e1973884e632f4a55795">Azerbaijan</option><option value="Austria" id="5f80e1973884e632f4a55794">Austria</option><option value="Bahamas The" id="5f80e1973884e632f4a55796">Bahamas The</option><option value="Bangladesh" id="5f80e1973884e632f4a55798">Bangladesh</option><option value="Bahrain" id="5f80e1973884e632f4a55797">Bahrain</option><option value="Belarus" id="5f80e1973884e632f4a5579a">Belarus</option><option value="Barbados" id="5f80e1973884e632f4a55799">Barbados</option><option value="Bouvet Island" id="5f80e1973884e632f4a557a3">Bouvet Island</option><option value="Belgium" id="5f80e1973884e632f4a5579b">Belgium</option><option value="Bolivia" id="5f80e1973884e632f4a557a0">Bolivia</option><option value="Botswana" id="5f80e1973884e632f4a557a2">Botswana</option><option value="Benin" id="5f80e1973884e632f4a5579d">Benin</option><option value="Bhutan" id="5f80e1973884e632f4a5579f">Bhutan</option><option value="Belize" id="5f80e1973884e632f4a5579c">Belize</option><option value="Brazil" id="5f80e1973884e632f4a557a4">Brazil</option><option value="Bermuda" id="5f80e1973884e632f4a5579e">Bermuda</option><option value="Bosnia and Herzegovina" id="5f80e1973884e632f4a557a1">Bosnia and Herzegovina</option><option value="British Indian Ocean Territory" id="5f80e1973884e632f4a557a5">British Indian Ocean Territory</option><option value="Cape Verde" id="5f80e1973884e632f4a557ad">Cape Verde</option><option value="Cambodia" id="5f80e1973884e632f4a557aa">Cambodia</option><option value="Cayman Islands" id="5f80e1973884e632f4a557ae">Cayman Islands</option><option value="Canada" id="5f80e1973884e632f4a557ac">Canada</option><option value="Burkina Faso" id="5f80e1973884e632f4a557a8">Burkina Faso</option><option value="Brunei" id="5f80e1973884e632f4a557a6">Brunei</option><option value="Cameroon" id="5f80e1973884e632f4a557ab">Cameroon</option><option value="Central African Republic" id="5f80e1973884e632f4a557af">Central African Republic</option><option value="Bulgaria" id="5f80e1973884e632f4a557a7">Bulgaria</option><option value="Burundi" id="5f80e1973884e632f4a557a9">Burundi</option><option value="Chad" id="5f80e1973884e632f4a557b0">Chad</option><option value="China" id="5f80e1973884e632f4a557b2">China</option><option value="Chile" id="5f80e1973884e632f4a557b1">Chile</option><option value="Colombia" id="5f80e1973884e632f4a557b5">Colombia</option><option value="Christmas Island" id="5f80e1973884e632f4a557b3">Christmas Island</option><option value="Cocos (Keeling) Islands" id="5f80e1973884e632f4a557b4">Cocos (Keeling) Islands</option><option value="Comoros" id="5f80e1973884e632f4a557b6">Comoros</option><option value="Costa Rica" id="5f80e1973884e632f4a557ba">Costa Rica</option><option value="Cuba" id="5f80e1973884e632f4a557bd">Cuba</option><option value="Cook Islands" id="5f80e1973884e632f4a557b9">Cook Islands</option><option value="Congo" id="5f80e1973884e632f4a557b7">Congo</option><option value="Congo The Democratic Republic Of The" id="5f80e1973884e632f4a557b8">Congo The Democratic Republic Of The</option><option value="Cote D'Ivoire (Ivory Coast)" id="5f80e1973884e632f4a557bb">Cote D'Ivoire (Ivory Coast)</option><option value="Croatia (Hrvatska)" id="5f80e1973884e632f4a557bc">Croatia (Hrvatska)</option><option value="Djibouti" id="5f80e1973884e632f4a557c1">Djibouti</option><option value="Dominican Republic" id="5f80e1973884e632f4a557c3">Dominican Republic</option><option value="Czech Republic" id="5f80e1973884e632f4a557bf">Czech Republic</option><option value="Denmark" id="5f80e1973884e632f4a557c0">Denmark</option><option value="Cyprus" id="5f80e1973884e632f4a557be">Cyprus</option><option value="Dominica" id="5f80e1973884e632f4a557c2">Dominica</option><option value="Ecuador" id="5f80e1973884e632f4a557c5">Ecuador</option><option value="East Timor" id="5f80e1973884e632f4a557c4">East Timor</option><option value="Egypt" id="5f80e1973884e632f4a557c6">Egypt</option><option value="Eritrea" id="5f80e1973884e632f4a557c9">Eritrea</option><option value="Equatorial Guinea" id="5f80e1973884e632f4a557c8">Equatorial Guinea</option><option value="El Salvador" id="5f80e1973884e632f4a557c7">El Salvador</option><option value="Faroe Islands" id="5f80e1973884e632f4a557cd">Faroe Islands</option><option value="Falkland Islands" id="5f80e1973884e632f4a557cc">Falkland Islands</option><option value="Finland" id="5f80e1973884e632f4a557cf">Finland</option><option value="France" id="5f80e1973884e632f4a557d0">France</option><option value="Ethiopia" id="5f80e1973884e632f4a557cb">Ethiopia</option><option value="Fiji Islands" id="5f80e1973884e632f4a557ce">Fiji Islands</option><option value="Estonia" id="5f80e1973884e632f4a557ca">Estonia</option><option value="Gambia The" id="5f80e1973884e632f4a557d5">Gambia The</option><option value="Georgia" id="5f80e1973884e632f4a557d6">Georgia</option><option value="Gabon" id="5f80e1973884e632f4a557d4">Gabon</option><option value="French Guiana" id="5f80e1973884e632f4a557d1">French Guiana</option><option value="Germany" id="5f80e1973884e632f4a557d7">Germany</option><option value="French Polynesia" id="5f80e1973884e632f4a557d2">French Polynesia</option><option value="French Southern Territories" id="5f80e1973884e632f4a557d3">French Southern Territories</option><option value="Guam" id="5f80e1973884e632f4a557de">Guam</option><option value="Grenada" id="5f80e1973884e632f4a557dc">Grenada</option><option value="Gibraltar" id="5f80e1973884e632f4a557d9">Gibraltar</option><option value="Ghana" id="5f80e1973884e632f4a557d8">Ghana</option><option value="Guadeloupe" id="5f80e1973884e632f4a557dd">Guadeloupe</option><option value="Greece" id="5f80e1973884e632f4a557da">Greece</option><option value="Greenland" id="5f80e1973884e632f4a557db">Greenland</option><option value="Guinea-Bissau" id="5f80e1973884e632f4a557e2">Guinea-Bissau</option><option value="Guyana" id="5f80e1973884e632f4a557e3">Guyana</option><option value="Guatemala" id="5f80e1973884e632f4a557df">Guatemala</option><option value="Heard and McDonald Islands" id="5f80e1973884e632f4a557e5">Heard and McDonald Islands</option><option value="Haiti" id="5f80e1973884e632f4a557e4">Haiti</option><option value="Guernsey and Alderney" id="5f80e1973884e632f4a557e0">Guernsey and Alderney</option><option value="Guinea" id="5f80e1973884e632f4a557e1">Guinea</option><option value="Hungary" id="5f80e1973884e632f4a557e8">Hungary</option><option value="Hong Kong S.A.R." id="5f80e1973884e632f4a557e7">Hong Kong S.A.R.</option><option value="India" id="5f80e1973884e632f4a557ea">India</option><option value="Honduras" id="5f80e1973884e632f4a557e6">Honduras</option><option value="Iceland" id="5f80e1973884e632f4a557e9">Iceland</option><option value="Iran" id="5f80e1973884e632f4a557ec">Iran</option><option value="Indonesia" id="5f80e1973884e632f4a557eb">Indonesia</option><option value="Ireland" id="5f80e1973884e632f4a557ee">Ireland</option><option value="Israel" id="5f80e1973884e632f4a557ef">Israel</option><option value="Iraq" id="5f80e1973884e632f4a557ed">Iraq</option><option value="Japan" id="5f80e1973884e632f4a557f2">Japan</option><option value="Jordan" id="5f80e1973884e632f4a557f4">Jordan</option><option value="Italy" id="5f80e1973884e632f4a557f0">Italy</option><option value="Jamaica" id="5f80e1973884e632f4a557f1">Jamaica</option><option value="Jersey" id="5f80e1973884e632f4a557f3">Jersey</option><option value="Kazakhstan" id="5f80e1973884e632f4a557f5">Kazakhstan</option><option value="Korea North" id="5f80e1973884e632f4a557f8">Korea North</option><option value="Korea South" id="5f80e1973884e632f4a557f9">Korea South</option><option value="Kiribati" id="5f80e1973884e632f4a557f7">Kiribati</option><option value="Kyrgyzstan" id="5f80e1973884e632f4a557fc">Kyrgyzstan</option><option value="Kosovo" id="5f80e1973884e632f4a557fa">Kosovo</option><option value="Laos" id="5f80e1973884e632f4a557fd">Laos</option><option value="Kuwait" id="5f80e1973884e632f4a557fb">Kuwait</option><option value="Kenya" id="5f80e1973884e632f4a557f6">Kenya</option><option value="Latvia" id="5f80e1973884e632f4a557fe">Latvia</option><option value="Lesotho" id="5f80e1973884e632f4a55800">Lesotho</option><option value="Liechtenstein" id="5f80e1973884e632f4a55803">Liechtenstein</option><option value="Libya" id="5f80e1973884e632f4a55802">Libya</option><option value="Lebanon" id="5f80e1973884e632f4a557ff">Lebanon</option><option value="Liberia" id="5f80e1973884e632f4a55801">Liberia</option><option value="Luxembourg" id="5f80e1973884e632f4a55805">Luxembourg</option><option value="Lithuania" id="5f80e1973884e632f4a55804">Lithuania</option><option value="Macedonia" id="5f80e1973884e632f4a55807">Macedonia</option><option value="Macau S.A.R." id="5f80e1973884e632f4a55806">Macau S.A.R.</option><option value="Madagascar" id="5f80e1973884e632f4a55808">Madagascar</option><option value="Mauritania" id="5f80e1973884e632f4a55811">Mauritania</option><option value="Mali" id="5f80e1973884e632f4a5580c">Mali</option><option value="Man (Isle of)" id="5f80e1973884e632f4a5580e">Man (Isle of)</option><option value="Malawi" id="5f80e1973884e632f4a55809">Malawi</option><option value="Malta" id="5f80e1973884e632f4a5580d">Malta</option><option value="Maldives" id="5f80e1973884e632f4a5580b">Maldives</option><option value="Malaysia" id="5f80e1973884e632f4a5580a">Malaysia</option><option value="Martinique" id="5f80e1973884e632f4a55810">Martinique</option><option value="Marshall Islands" id="5f80e1973884e632f4a5580f">Marshall Islands</option><option value="Mexico" id="5f80e1973884e632f4a55814">Mexico</option><option value="Micronesia" id="5f80e1973884e632f4a55815">Micronesia</option><option value="Moldova" id="5f80e1973884e632f4a55816">Moldova</option><option value="Mauritius" id="5f80e1973884e632f4a55812">Mauritius</option><option value="Mayotte" id="5f80e1973884e632f4a55813">Mayotte</option><option value="Montenegro" id="5f80e1973884e632f4a55819">Montenegro</option><option value="Montserrat" id="5f80e1973884e632f4a5581a">Montserrat</option><option value="Monaco" id="5f80e1973884e632f4a55817">Monaco</option><option value="Mongolia" id="5f80e1973884e632f4a55818">Mongolia</option><option value="Myanmar" id="5f80e1973884e632f4a5581d">Myanmar</option><option value="Namibia" id="5f80e1973884e632f4a5581e">Namibia</option><option value="Mozambique" id="5f80e1973884e632f4a5581c">Mozambique</option><option value="Morocco" id="5f80e1973884e632f4a5581b">Morocco</option><option value="Nauru" id="5f80e1973884e632f4a5581f">Nauru</option><option value="Nepal" id="5f80e1973884e632f4a55820">Nepal</option><option value="Netherlands Antilles" id="5f80e1973884e632f4a55821">Netherlands Antilles</option><option value="New Caledonia" id="5f80e1973884e632f4a55823">New Caledonia</option><option value="Nicaragua" id="5f80e1973884e632f4a55825">Nicaragua</option><option value="New Zealand" id="5f80e1973884e632f4a55824">New Zealand</option><option value="Netherlands The" id="5f80e1973884e632f4a55822">Netherlands The</option><option value="Niger" id="5f80e1973884e632f4a55826">Niger</option><option value="Nigeria" id="5f80e1973884e632f4a55827">Nigeria</option><option value="Niue" id="5f80e1973884e632f4a55828">Niue</option><option value="Northern Mariana Islands" id="5f80e1973884e632f4a5582a">Northern Mariana Islands</option><option value="Norfolk Island" id="5f80e1973884e632f4a55829">Norfolk Island</option><option value="Oman" id="5f80e1973884e632f4a5582c">Oman</option><option value="Norway" id="5f80e1973884e632f4a5582b">Norway</option><option value="Pakistan" id="5f80e1973884e632f4a5582d">Pakistan</option><option value="Panama" id="5f80e1973884e632f4a55830">Panama</option><option value="Paraguay" id="5f80e1973884e632f4a55832">Paraguay</option><option value="Romania" id="5f80e1973884e632f4a5583b">Romania</option><option value="Papua new Guinea" id="5f80e1973884e632f4a55831">Papua new Guinea</option><option value="Poland" id="5f80e1973884e632f4a55836">Poland</option><option value="Peru" id="5f80e1973884e632f4a55833">Peru</option><option value="Palau" id="5f80e1973884e632f4a5582e">Palau</option><option value="Pitcairn Island" id="5f80e1973884e632f4a55835">Pitcairn Island</option><option value="Puerto Rico" id="5f80e1973884e632f4a55838">Puerto Rico</option><option value="Qatar" id="5f80e1973884e632f4a55839">Qatar</option><option value="Palestinian Territory Occupied" id="5f80e1973884e632f4a5582f">Palestinian Territory Occupied</option><option value="Philippines" id="5f80e1973884e632f4a55834">Philippines</option><option value="Portugal" id="5f80e1973884e632f4a55837">Portugal</option><option value="Reunion" id="5f80e1973884e632f4a5583a">Reunion</option><option value="Russia" id="5f80e1973884e632f4a5583c">Russia</option><option value="Saint Helena" id="5f80e1973884e632f4a5583e">Saint Helena</option><option value="Rwanda" id="5f80e1973884e632f4a5583d">Rwanda</option><option value="Saint Kitts And Nevis" id="5f80e1973884e632f4a5583f">Saint Kitts And Nevis</option><option value="Saint Lucia" id="5f80e1973884e632f4a55840">Saint Lucia</option><option value="Saint Pierre and Miquelon" id="5f80e1973884e632f4a55841">Saint Pierre and Miquelon</option><option value="Saint Vincent And The Grenadines" id="5f80e1973884e632f4a55842">Saint Vincent And The Grenadines</option><option value="Sao Tome and Principe" id="5f80e1973884e632f4a55847">Sao Tome and Principe</option><option value="Samoa" id="5f80e1973884e632f4a55845">Samoa</option><option value="Saint-Barthelemy" id="5f80e1973884e632f4a55843">Saint-Barthelemy</option><option value="San Marino" id="5f80e1973884e632f4a55846">San Marino</option><option value="Saint-Martin (French part)" id="5f80e1973884e632f4a55844">Saint-Martin (French part)</option><option value="Saudi Arabia" id="5f80e1973884e632f4a55848">Saudi Arabia</option><option value="Serbia" id="5f80e1973884e632f4a5584a">Serbia</option><option value="Senegal" id="5f80e1973884e632f4a55849">Senegal</option><option value="Sierra Leone" id="5f80e1973884e632f4a5584c">Sierra Leone</option><option value="Singapore" id="5f80e1973884e632f4a5584d">Singapore</option><option value="Slovakia" id="5f80e1973884e632f4a5584e">Slovakia</option><option value="Seychelles" id="5f80e1973884e632f4a5584b">Seychelles</option><option value="South Africa" id="5f80e1973884e632f4a55852">South Africa</option><option value="Somalia" id="5f80e1973884e632f4a55851">Somalia</option><option value="Solomon Islands" id="5f80e1973884e632f4a55850">Solomon Islands</option><option value="Slovenia" id="5f80e1973884e632f4a5584f">Slovenia</option><option value="South Georgia" id="5f80e1973884e632f4a55853">South Georgia</option><option value="Svalbard And Jan Mayen Islands" id="5f80e1973884e632f4a55859">Svalbard And Jan Mayen Islands</option><option value="Sweden" id="5f80e1973884e632f4a5585b">Sweden</option><option value="Swaziland" id="5f80e1973884e632f4a5585a">Swaziland</option><option value="Taiwan" id="5f80e1973884e632f4a5585e">Taiwan</option><option value="Thailand" id="5f80e1973884e632f4a55861">Thailand</option><option value="Tajikistan" id="5f80e1973884e632f4a5585f">Tajikistan</option><option value="Suriname" id="5f80e1973884e632f4a55858">Suriname</option><option value="Switzerland" id="5f80e1973884e632f4a5585c">Switzerland</option><option value="Tanzania" id="5f80e1973884e632f4a55860">Tanzania</option><option value="Spain" id="5f80e1973884e632f4a55855">Spain</option><option value="Sri Lanka" id="5f80e1973884e632f4a55856">Sri Lanka</option><option value="South Sudan" id="5f80e1973884e632f4a55854">South Sudan</option><option value="Syria" id="5f80e1973884e632f4a5585d">Syria</option><option value="Sudan" id="5f80e1973884e632f4a55857">Sudan</option><option value="Trinidad And Tobago" id="5f80e1973884e632f4a55865">Trinidad And Tobago</option><option value="Tokelau" id="5f80e1973884e632f4a55863">Tokelau</option><option value="Togo" id="5f80e1973884e632f4a55862">Togo</option><option value="Tonga" id="5f80e1973884e632f4a55864">Tonga</option><option value="Tunisia" id="5f80e1973884e632f4a55866">Tunisia</option><option value="Turkey" id="5f80e1973884e632f4a55867">Turkey</option><option value="Ukraine" id="5f80e1973884e632f4a5586c">Ukraine</option><option value="Uganda" id="5f80e1973884e632f4a5586b">Uganda</option><option value="Turkmenistan" id="5f80e1973884e632f4a55868">Turkmenistan</option><option value="Tuvalu" id="5f80e1973884e632f4a5586a">Tuvalu</option><option value="Turks And Caicos Islands" id="5f80e1973884e632f4a55869">Turks And Caicos Islands</option><option value="United Arab Emirates" id="5f80e1973884e632f4a5586d">United Arab Emirates</option><option value="United Kingdom" id="5f80e1973884e632f4a5586e">United Kingdom</option><option value="Vatican City State (Holy See)" id="5f80e1973884e632f4a55873">Vatican City State (Holy See)</option><option value="United States Minor Outlying Islands" id="5f80e1973884e632f4a5586f">United States Minor Outlying Islands</option><option value="Uzbekistan" id="5f80e1973884e632f4a55871">Uzbekistan</option><option value="Vanuatu" id="5f80e1973884e632f4a55872">Vanuatu</option><option value="Uruguay" id="5f80e1973884e632f4a55870">Uruguay</option><option value="Virgin Islands (US)" id="5f80e1973884e632f4a55877">Virgin Islands (US)</option><option value="Virgin Islands (British)" id="5f80e1973884e632f4a55876">Virgin Islands (British)</option><option value="Vietnam" id="5f80e1973884e632f4a55875">Vietnam</option><option value="Venezuela" id="5f80e1973884e632f4a55874">Venezuela</option><option value="Zambia" id="5f80e1973884e632f4a5587b">Zambia</option><option value="Wallis And Futuna Islands" id="5f80e1973884e632f4a55878">Wallis And Futuna Islands</option><option value="Yemen" id="5f80e1973884e632f4a5587a">Yemen</option><option value="Western Sahara" id="5f80e1973884e632f4a55879">Western Sahara</option><option value="Zimbabwe" id="5f80e1973884e632f4a5587c">Zimbabwe</option></select>
                                            <div class="wizard-form-error"></div>
                                        </div>
                                        <div class="form-group col-md-3">
                                            <select name="province" id="province" class="selectpicker form-control wizard-required" required="required" data-toggle="tooltip" title="" data-original-title="States/Province is required"><option value="">States/Province*</option><option value="Abu Dhabi Emirate">Abu Dhabi Emirate</option><option value="Ajman Emirate">Ajman Emirate</option><option value="Dubai">Dubai</option><option value="Fujairah">Fujairah</option><option value="Ras al-Khaimah">Ras al-Khaimah</option><option value="Sharjah Emirate">Sharjah Emirate</option><option value="Umm al-Quwain">Umm al-Quwain</option></select>
                                            <div class="wizard-form-error"></div>
                                        </div>
                                        <div class="form-group col-md-2">
                                            <input type="text" class="form-control wizard-required" id="city">
                                            <label for="city" class="wizard-form-text-label">City*</label>
                                            <div class="wizard-form-error"></div>
                                        </div>
                                        <div class="form-group col-md-2">
                                            <input type="text" class="form-control wizard-required" id="zipcode">
                                            <label for="zipcode" class="wizard-form-text-label">Zip Code*</label>
                                            <div class="wizard-form-error"></div>
                                        </div>
                                    </div>
                                </div>


                                <div class="form-group col-md-6">
                                    <input type="number" class="form-control wizard-required" id="phnumber">
                                    <label for="phnumber" class="wizard-form-text-label">Phone Number*</label>
                                    <div class="wizard-form-error"></div>
                                    <span style="color: #888;
                                    font-size: 14px;">Phone numbers are checked for validity in the country that your are applying</span>
                                </div>
                                <div class="form-group col-md-6">
                                    <input type="number" class="form-control wizard-required" id="dependents">
                                    <label for="dependents" class="wizard-form-text-label">Number of Dependents*</label>
                                    <div class="wizard-form-error"></div>
                                </div>


                                <div class="form-group col-md-12">
                                    <span>Are You:*</span>
                                    <div class="form-check-inline">
                                        <label class="form-check-label" for="radio1">
                                          <input type="radio" class="form-check-input" id="radio1" name="optradio" value="option1" checked>Single
                                        </label>
                                      </div>
                                      <div class="form-check-inline">
                                        <label class="form-check-label" for="radio2">
                                          <input type="radio" class="form-check-input" id="radio2" name="optradio" value="option2">Married
                                        </label>
                                      </div>
                                      <div class="form-check-inline">
                                        <label class="form-check-label">
                                          <input type="radio" class="form-check-input">Divorced
                                        </label>
                                      </div>
                                      <div class="form-check-inline">
                                        <label class="form-check-label">
                                          <input type="radio" class="form-check-input">Widowed
                                        </label>
                                      </div>
                                </div>


                                <div class="form-group col-md-12">
                                    <input class="trusted_contact" type="checkbox" name="trusted_contact" value="1" onchange="valueChangedtwo()"/> Would you like to add a Trusted Contact Person

                                </div>
                                <div class="form-group col-md-12 trusteddiv" style="display: none;">
                                    <div class="row">
                                        <div class="form-group col-md-4">
                                            <input type="text" class="form-control" id="trusted_name">
                                            <label for="trusted_name" class="wizard-form-text-label">Name*</label>
                                            <div class="wizard-form-error"></div>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <input type="number" class="form-control" id="trusted_phnumber">
                                            <label for="trusted_phnumber" class="wizard-form-text-label">Phone Number*</label>
                                            <div class="wizard-form-error"></div>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <input type="email" class="form-control " id="trusted_email">
                                            <label for="trusted_email" class="wizard-form-text-label">Email Address*</label>
                                            <div class="wizard-form-error"></div>
                                        </div>

                                        <div class="form-group col-md-12">
                                            <input type="text" class="form-control" id="trusted_address">
                                            <label for="trusted_address" class="wizard-form-text-label">Street Address*</label>
                                            <div class="wizard-form-error"></div>
                                        </div>
                                        
                                        <div class="form-group col-md-5">
                                            <select name="country" id="country" class="form-control wizard-required" required="required"><option value="">Country*</option><option value="United States" id="5f80e1963884e632f4a55785">United States</option><option value="Afghanistan" id="5f80e1963884e632f4a55786">Afghanistan</option><option value="Aland Islands" id="5f80e1963884e632f4a55787">Aland Islands</option><option value="Albania" id="5f80e1973884e632f4a55788">Albania</option><option value="Angola" id="5f80e1973884e632f4a5578c">Angola</option><option value="Andorra" id="5f80e1973884e632f4a5578b">Andorra</option><option value="Anguilla" id="5f80e1973884e632f4a5578d">Anguilla</option><option value="Antarctica" id="5f80e1973884e632f4a5578e">Antarctica</option><option value="Algeria" id="5f80e1973884e632f4a55789">Algeria</option><option value="American Samoa" id="5f80e1973884e632f4a5578a">American Samoa</option><option value="Argentina" id="5f80e1973884e632f4a55790">Argentina</option><option value="Antigua And Barbuda" id="5f80e1973884e632f4a5578f">Antigua And Barbuda</option><option value="Australia" id="5f80e1973884e632f4a55793">Australia</option><option value="Aruba" id="5f80e1973884e632f4a55792">Aruba</option><option value="Armenia" id="5f80e1973884e632f4a55791">Armenia</option><option value="Azerbaijan" id="5f80e1973884e632f4a55795">Azerbaijan</option><option value="Austria" id="5f80e1973884e632f4a55794">Austria</option><option value="Bahamas The" id="5f80e1973884e632f4a55796">Bahamas The</option><option value="Bangladesh" id="5f80e1973884e632f4a55798">Bangladesh</option><option value="Bahrain" id="5f80e1973884e632f4a55797">Bahrain</option><option value="Belarus" id="5f80e1973884e632f4a5579a">Belarus</option><option value="Barbados" id="5f80e1973884e632f4a55799">Barbados</option><option value="Bouvet Island" id="5f80e1973884e632f4a557a3">Bouvet Island</option><option value="Belgium" id="5f80e1973884e632f4a5579b">Belgium</option><option value="Bolivia" id="5f80e1973884e632f4a557a0">Bolivia</option><option value="Botswana" id="5f80e1973884e632f4a557a2">Botswana</option><option value="Benin" id="5f80e1973884e632f4a5579d">Benin</option><option value="Bhutan" id="5f80e1973884e632f4a5579f">Bhutan</option><option value="Belize" id="5f80e1973884e632f4a5579c">Belize</option><option value="Brazil" id="5f80e1973884e632f4a557a4">Brazil</option><option value="Bermuda" id="5f80e1973884e632f4a5579e">Bermuda</option><option value="Bosnia and Herzegovina" id="5f80e1973884e632f4a557a1">Bosnia and Herzegovina</option><option value="British Indian Ocean Territory" id="5f80e1973884e632f4a557a5">British Indian Ocean Territory</option><option value="Cape Verde" id="5f80e1973884e632f4a557ad">Cape Verde</option><option value="Cambodia" id="5f80e1973884e632f4a557aa">Cambodia</option><option value="Cayman Islands" id="5f80e1973884e632f4a557ae">Cayman Islands</option><option value="Canada" id="5f80e1973884e632f4a557ac">Canada</option><option value="Burkina Faso" id="5f80e1973884e632f4a557a8">Burkina Faso</option><option value="Brunei" id="5f80e1973884e632f4a557a6">Brunei</option><option value="Cameroon" id="5f80e1973884e632f4a557ab">Cameroon</option><option value="Central African Republic" id="5f80e1973884e632f4a557af">Central African Republic</option><option value="Bulgaria" id="5f80e1973884e632f4a557a7">Bulgaria</option><option value="Burundi" id="5f80e1973884e632f4a557a9">Burundi</option><option value="Chad" id="5f80e1973884e632f4a557b0">Chad</option><option value="China" id="5f80e1973884e632f4a557b2">China</option><option value="Chile" id="5f80e1973884e632f4a557b1">Chile</option><option value="Colombia" id="5f80e1973884e632f4a557b5">Colombia</option><option value="Christmas Island" id="5f80e1973884e632f4a557b3">Christmas Island</option><option value="Cocos (Keeling) Islands" id="5f80e1973884e632f4a557b4">Cocos (Keeling) Islands</option><option value="Comoros" id="5f80e1973884e632f4a557b6">Comoros</option><option value="Costa Rica" id="5f80e1973884e632f4a557ba">Costa Rica</option><option value="Cuba" id="5f80e1973884e632f4a557bd">Cuba</option><option value="Cook Islands" id="5f80e1973884e632f4a557b9">Cook Islands</option><option value="Congo" id="5f80e1973884e632f4a557b7">Congo</option><option value="Congo The Democratic Republic Of The" id="5f80e1973884e632f4a557b8">Congo The Democratic Republic Of The</option><option value="Cote D'Ivoire (Ivory Coast)" id="5f80e1973884e632f4a557bb">Cote D'Ivoire (Ivory Coast)</option><option value="Croatia (Hrvatska)" id="5f80e1973884e632f4a557bc">Croatia (Hrvatska)</option><option value="Djibouti" id="5f80e1973884e632f4a557c1">Djibouti</option><option value="Dominican Republic" id="5f80e1973884e632f4a557c3">Dominican Republic</option><option value="Czech Republic" id="5f80e1973884e632f4a557bf">Czech Republic</option><option value="Denmark" id="5f80e1973884e632f4a557c0">Denmark</option><option value="Cyprus" id="5f80e1973884e632f4a557be">Cyprus</option><option value="Dominica" id="5f80e1973884e632f4a557c2">Dominica</option><option value="Ecuador" id="5f80e1973884e632f4a557c5">Ecuador</option><option value="East Timor" id="5f80e1973884e632f4a557c4">East Timor</option><option value="Egypt" id="5f80e1973884e632f4a557c6">Egypt</option><option value="Eritrea" id="5f80e1973884e632f4a557c9">Eritrea</option><option value="Equatorial Guinea" id="5f80e1973884e632f4a557c8">Equatorial Guinea</option><option value="El Salvador" id="5f80e1973884e632f4a557c7">El Salvador</option><option value="Faroe Islands" id="5f80e1973884e632f4a557cd">Faroe Islands</option><option value="Falkland Islands" id="5f80e1973884e632f4a557cc">Falkland Islands</option><option value="Finland" id="5f80e1973884e632f4a557cf">Finland</option><option value="France" id="5f80e1973884e632f4a557d0">France</option><option value="Ethiopia" id="5f80e1973884e632f4a557cb">Ethiopia</option><option value="Fiji Islands" id="5f80e1973884e632f4a557ce">Fiji Islands</option><option value="Estonia" id="5f80e1973884e632f4a557ca">Estonia</option><option value="Gambia The" id="5f80e1973884e632f4a557d5">Gambia The</option><option value="Georgia" id="5f80e1973884e632f4a557d6">Georgia</option><option value="Gabon" id="5f80e1973884e632f4a557d4">Gabon</option><option value="French Guiana" id="5f80e1973884e632f4a557d1">French Guiana</option><option value="Germany" id="5f80e1973884e632f4a557d7">Germany</option><option value="French Polynesia" id="5f80e1973884e632f4a557d2">French Polynesia</option><option value="French Southern Territories" id="5f80e1973884e632f4a557d3">French Southern Territories</option><option value="Guam" id="5f80e1973884e632f4a557de">Guam</option><option value="Grenada" id="5f80e1973884e632f4a557dc">Grenada</option><option value="Gibraltar" id="5f80e1973884e632f4a557d9">Gibraltar</option><option value="Ghana" id="5f80e1973884e632f4a557d8">Ghana</option><option value="Guadeloupe" id="5f80e1973884e632f4a557dd">Guadeloupe</option><option value="Greece" id="5f80e1973884e632f4a557da">Greece</option><option value="Greenland" id="5f80e1973884e632f4a557db">Greenland</option><option value="Guinea-Bissau" id="5f80e1973884e632f4a557e2">Guinea-Bissau</option><option value="Guyana" id="5f80e1973884e632f4a557e3">Guyana</option><option value="Guatemala" id="5f80e1973884e632f4a557df">Guatemala</option><option value="Heard and McDonald Islands" id="5f80e1973884e632f4a557e5">Heard and McDonald Islands</option><option value="Haiti" id="5f80e1973884e632f4a557e4">Haiti</option><option value="Guernsey and Alderney" id="5f80e1973884e632f4a557e0">Guernsey and Alderney</option><option value="Guinea" id="5f80e1973884e632f4a557e1">Guinea</option><option value="Hungary" id="5f80e1973884e632f4a557e8">Hungary</option><option value="Hong Kong S.A.R." id="5f80e1973884e632f4a557e7">Hong Kong S.A.R.</option><option value="India" id="5f80e1973884e632f4a557ea">India</option><option value="Honduras" id="5f80e1973884e632f4a557e6">Honduras</option><option value="Iceland" id="5f80e1973884e632f4a557e9">Iceland</option><option value="Iran" id="5f80e1973884e632f4a557ec">Iran</option><option value="Indonesia" id="5f80e1973884e632f4a557eb">Indonesia</option><option value="Ireland" id="5f80e1973884e632f4a557ee">Ireland</option><option value="Israel" id="5f80e1973884e632f4a557ef">Israel</option><option value="Iraq" id="5f80e1973884e632f4a557ed">Iraq</option><option value="Japan" id="5f80e1973884e632f4a557f2">Japan</option><option value="Jordan" id="5f80e1973884e632f4a557f4">Jordan</option><option value="Italy" id="5f80e1973884e632f4a557f0">Italy</option><option value="Jamaica" id="5f80e1973884e632f4a557f1">Jamaica</option><option value="Jersey" id="5f80e1973884e632f4a557f3">Jersey</option><option value="Kazakhstan" id="5f80e1973884e632f4a557f5">Kazakhstan</option><option value="Korea North" id="5f80e1973884e632f4a557f8">Korea North</option><option value="Korea South" id="5f80e1973884e632f4a557f9">Korea South</option><option value="Kiribati" id="5f80e1973884e632f4a557f7">Kiribati</option><option value="Kyrgyzstan" id="5f80e1973884e632f4a557fc">Kyrgyzstan</option><option value="Kosovo" id="5f80e1973884e632f4a557fa">Kosovo</option><option value="Laos" id="5f80e1973884e632f4a557fd">Laos</option><option value="Kuwait" id="5f80e1973884e632f4a557fb">Kuwait</option><option value="Kenya" id="5f80e1973884e632f4a557f6">Kenya</option><option value="Latvia" id="5f80e1973884e632f4a557fe">Latvia</option><option value="Lesotho" id="5f80e1973884e632f4a55800">Lesotho</option><option value="Liechtenstein" id="5f80e1973884e632f4a55803">Liechtenstein</option><option value="Libya" id="5f80e1973884e632f4a55802">Libya</option><option value="Lebanon" id="5f80e1973884e632f4a557ff">Lebanon</option><option value="Liberia" id="5f80e1973884e632f4a55801">Liberia</option><option value="Luxembourg" id="5f80e1973884e632f4a55805">Luxembourg</option><option value="Lithuania" id="5f80e1973884e632f4a55804">Lithuania</option><option value="Macedonia" id="5f80e1973884e632f4a55807">Macedonia</option><option value="Macau S.A.R." id="5f80e1973884e632f4a55806">Macau S.A.R.</option><option value="Madagascar" id="5f80e1973884e632f4a55808">Madagascar</option><option value="Mauritania" id="5f80e1973884e632f4a55811">Mauritania</option><option value="Mali" id="5f80e1973884e632f4a5580c">Mali</option><option value="Man (Isle of)" id="5f80e1973884e632f4a5580e">Man (Isle of)</option><option value="Malawi" id="5f80e1973884e632f4a55809">Malawi</option><option value="Malta" id="5f80e1973884e632f4a5580d">Malta</option><option value="Maldives" id="5f80e1973884e632f4a5580b">Maldives</option><option value="Malaysia" id="5f80e1973884e632f4a5580a">Malaysia</option><option value="Martinique" id="5f80e1973884e632f4a55810">Martinique</option><option value="Marshall Islands" id="5f80e1973884e632f4a5580f">Marshall Islands</option><option value="Mexico" id="5f80e1973884e632f4a55814">Mexico</option><option value="Micronesia" id="5f80e1973884e632f4a55815">Micronesia</option><option value="Moldova" id="5f80e1973884e632f4a55816">Moldova</option><option value="Mauritius" id="5f80e1973884e632f4a55812">Mauritius</option><option value="Mayotte" id="5f80e1973884e632f4a55813">Mayotte</option><option value="Montenegro" id="5f80e1973884e632f4a55819">Montenegro</option><option value="Montserrat" id="5f80e1973884e632f4a5581a">Montserrat</option><option value="Monaco" id="5f80e1973884e632f4a55817">Monaco</option><option value="Mongolia" id="5f80e1973884e632f4a55818">Mongolia</option><option value="Myanmar" id="5f80e1973884e632f4a5581d">Myanmar</option><option value="Namibia" id="5f80e1973884e632f4a5581e">Namibia</option><option value="Mozambique" id="5f80e1973884e632f4a5581c">Mozambique</option><option value="Morocco" id="5f80e1973884e632f4a5581b">Morocco</option><option value="Nauru" id="5f80e1973884e632f4a5581f">Nauru</option><option value="Nepal" id="5f80e1973884e632f4a55820">Nepal</option><option value="Netherlands Antilles" id="5f80e1973884e632f4a55821">Netherlands Antilles</option><option value="New Caledonia" id="5f80e1973884e632f4a55823">New Caledonia</option><option value="Nicaragua" id="5f80e1973884e632f4a55825">Nicaragua</option><option value="New Zealand" id="5f80e1973884e632f4a55824">New Zealand</option><option value="Netherlands The" id="5f80e1973884e632f4a55822">Netherlands The</option><option value="Niger" id="5f80e1973884e632f4a55826">Niger</option><option value="Nigeria" id="5f80e1973884e632f4a55827">Nigeria</option><option value="Niue" id="5f80e1973884e632f4a55828">Niue</option><option value="Northern Mariana Islands" id="5f80e1973884e632f4a5582a">Northern Mariana Islands</option><option value="Norfolk Island" id="5f80e1973884e632f4a55829">Norfolk Island</option><option value="Oman" id="5f80e1973884e632f4a5582c">Oman</option><option value="Norway" id="5f80e1973884e632f4a5582b">Norway</option><option value="Pakistan" id="5f80e1973884e632f4a5582d">Pakistan</option><option value="Panama" id="5f80e1973884e632f4a55830">Panama</option><option value="Paraguay" id="5f80e1973884e632f4a55832">Paraguay</option><option value="Romania" id="5f80e1973884e632f4a5583b">Romania</option><option value="Papua new Guinea" id="5f80e1973884e632f4a55831">Papua new Guinea</option><option value="Poland" id="5f80e1973884e632f4a55836">Poland</option><option value="Peru" id="5f80e1973884e632f4a55833">Peru</option><option value="Palau" id="5f80e1973884e632f4a5582e">Palau</option><option value="Pitcairn Island" id="5f80e1973884e632f4a55835">Pitcairn Island</option><option value="Puerto Rico" id="5f80e1973884e632f4a55838">Puerto Rico</option><option value="Qatar" id="5f80e1973884e632f4a55839">Qatar</option><option value="Palestinian Territory Occupied" id="5f80e1973884e632f4a5582f">Palestinian Territory Occupied</option><option value="Philippines" id="5f80e1973884e632f4a55834">Philippines</option><option value="Portugal" id="5f80e1973884e632f4a55837">Portugal</option><option value="Reunion" id="5f80e1973884e632f4a5583a">Reunion</option><option value="Russia" id="5f80e1973884e632f4a5583c">Russia</option><option value="Saint Helena" id="5f80e1973884e632f4a5583e">Saint Helena</option><option value="Rwanda" id="5f80e1973884e632f4a5583d">Rwanda</option><option value="Saint Kitts And Nevis" id="5f80e1973884e632f4a5583f">Saint Kitts And Nevis</option><option value="Saint Lucia" id="5f80e1973884e632f4a55840">Saint Lucia</option><option value="Saint Pierre and Miquelon" id="5f80e1973884e632f4a55841">Saint Pierre and Miquelon</option><option value="Saint Vincent And The Grenadines" id="5f80e1973884e632f4a55842">Saint Vincent And The Grenadines</option><option value="Sao Tome and Principe" id="5f80e1973884e632f4a55847">Sao Tome and Principe</option><option value="Samoa" id="5f80e1973884e632f4a55845">Samoa</option><option value="Saint-Barthelemy" id="5f80e1973884e632f4a55843">Saint-Barthelemy</option><option value="San Marino" id="5f80e1973884e632f4a55846">San Marino</option><option value="Saint-Martin (French part)" id="5f80e1973884e632f4a55844">Saint-Martin (French part)</option><option value="Saudi Arabia" id="5f80e1973884e632f4a55848">Saudi Arabia</option><option value="Serbia" id="5f80e1973884e632f4a5584a">Serbia</option><option value="Senegal" id="5f80e1973884e632f4a55849">Senegal</option><option value="Sierra Leone" id="5f80e1973884e632f4a5584c">Sierra Leone</option><option value="Singapore" id="5f80e1973884e632f4a5584d">Singapore</option><option value="Slovakia" id="5f80e1973884e632f4a5584e">Slovakia</option><option value="Seychelles" id="5f80e1973884e632f4a5584b">Seychelles</option><option value="South Africa" id="5f80e1973884e632f4a55852">South Africa</option><option value="Somalia" id="5f80e1973884e632f4a55851">Somalia</option><option value="Solomon Islands" id="5f80e1973884e632f4a55850">Solomon Islands</option><option value="Slovenia" id="5f80e1973884e632f4a5584f">Slovenia</option><option value="South Georgia" id="5f80e1973884e632f4a55853">South Georgia</option><option value="Svalbard And Jan Mayen Islands" id="5f80e1973884e632f4a55859">Svalbard And Jan Mayen Islands</option><option value="Sweden" id="5f80e1973884e632f4a5585b">Sweden</option><option value="Swaziland" id="5f80e1973884e632f4a5585a">Swaziland</option><option value="Taiwan" id="5f80e1973884e632f4a5585e">Taiwan</option><option value="Thailand" id="5f80e1973884e632f4a55861">Thailand</option><option value="Tajikistan" id="5f80e1973884e632f4a5585f">Tajikistan</option><option value="Suriname" id="5f80e1973884e632f4a55858">Suriname</option><option value="Switzerland" id="5f80e1973884e632f4a5585c">Switzerland</option><option value="Tanzania" id="5f80e1973884e632f4a55860">Tanzania</option><option value="Spain" id="5f80e1973884e632f4a55855">Spain</option><option value="Sri Lanka" id="5f80e1973884e632f4a55856">Sri Lanka</option><option value="South Sudan" id="5f80e1973884e632f4a55854">South Sudan</option><option value="Syria" id="5f80e1973884e632f4a5585d">Syria</option><option value="Sudan" id="5f80e1973884e632f4a55857">Sudan</option><option value="Trinidad And Tobago" id="5f80e1973884e632f4a55865">Trinidad And Tobago</option><option value="Tokelau" id="5f80e1973884e632f4a55863">Tokelau</option><option value="Togo" id="5f80e1973884e632f4a55862">Togo</option><option value="Tonga" id="5f80e1973884e632f4a55864">Tonga</option><option value="Tunisia" id="5f80e1973884e632f4a55866">Tunisia</option><option value="Turkey" id="5f80e1973884e632f4a55867">Turkey</option><option value="Ukraine" id="5f80e1973884e632f4a5586c">Ukraine</option><option value="Uganda" id="5f80e1973884e632f4a5586b">Uganda</option><option value="Turkmenistan" id="5f80e1973884e632f4a55868">Turkmenistan</option><option value="Tuvalu" id="5f80e1973884e632f4a5586a">Tuvalu</option><option value="Turks And Caicos Islands" id="5f80e1973884e632f4a55869">Turks And Caicos Islands</option><option value="United Arab Emirates" id="5f80e1973884e632f4a5586d">United Arab Emirates</option><option value="United Kingdom" id="5f80e1973884e632f4a5586e">United Kingdom</option><option value="Vatican City State (Holy See)" id="5f80e1973884e632f4a55873">Vatican City State (Holy See)</option><option value="United States Minor Outlying Islands" id="5f80e1973884e632f4a5586f">United States Minor Outlying Islands</option><option value="Uzbekistan" id="5f80e1973884e632f4a55871">Uzbekistan</option><option value="Vanuatu" id="5f80e1973884e632f4a55872">Vanuatu</option><option value="Uruguay" id="5f80e1973884e632f4a55870">Uruguay</option><option value="Virgin Islands (US)" id="5f80e1973884e632f4a55877">Virgin Islands (US)</option><option value="Virgin Islands (British)" id="5f80e1973884e632f4a55876">Virgin Islands (British)</option><option value="Vietnam" id="5f80e1973884e632f4a55875">Vietnam</option><option value="Venezuela" id="5f80e1973884e632f4a55874">Venezuela</option><option value="Zambia" id="5f80e1973884e632f4a5587b">Zambia</option><option value="Wallis And Futuna Islands" id="5f80e1973884e632f4a55878">Wallis And Futuna Islands</option><option value="Yemen" id="5f80e1973884e632f4a5587a">Yemen</option><option value="Western Sahara" id="5f80e1973884e632f4a55879">Western Sahara</option><option value="Zimbabwe" id="5f80e1973884e632f4a5587c">Zimbabwe</option></select>
                                            <div class="wizard-form-error"></div>
                                        </div>
                                        <div class="form-group col-md-3">
                                            <select name="province" id="province" class="selectpicker form-control wizard-required" required="required" data-toggle="tooltip" title="" data-original-title="States/Province is required"><option value="">States/Province*</option><option value="Abu Dhabi Emirate">Abu Dhabi Emirate</option><option value="Ajman Emirate">Ajman Emirate</option><option value="Dubai">Dubai</option><option value="Fujairah">Fujairah</option><option value="Ras al-Khaimah">Ras al-Khaimah</option><option value="Sharjah Emirate">Sharjah Emirate</option><option value="Umm al-Quwain">Umm al-Quwain</option></select>
                                            <div class="wizard-form-error"></div>
                                        </div>
                                        <div class="form-group col-md-2">
                                            <input type="text" class="form-control wizard-required" id="city">
                                            <label for="city" class="wizard-form-text-label">City*</label>
                                            <div class="wizard-form-error"></div>
                                        </div>
                                        <div class="form-group col-md-2">
                                            <input type="text" class="form-control wizard-required" id="zipcode">
                                            <label for="zipcode" class="wizard-form-text-label">Zip Code*</label>
                                            <div class="wizard-form-error"></div>
                                        </div>

                                        <div class="form-group col-md-4">
                                            <input type="text" class="form-control wizard-required" id="rel_accHolder">
                                            <label for="rel_accHolder" class="wizard-form-text-label">Relationship To Account Holder*</label>
                                            <div class="wizard-form-error"></div>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <input type="date" class="form-control wizard-required" id="dob">
                                            <label for="dob" class="wizard-form-text-label">Date Of Birth (MM\DD\YYY)*</label>
                                            <div class="wizard-form-error"></div>
                                        </div>

                                    </div>
                                </div>
                                <hr/>
                                <h5>COMPANY DETAIL</h5>

                                <div class="form-group col-md-12">
                                    <input type="text" class="form-control wizard-required" id="comp_name">
                                    <label for="comp_name" class="wizard-form-text-label">Company Name*</label>
                                    <div class="wizard-form-error"></div>
                                </div>


                                <div class="form-group col-md-8">
                                    <input type="text" class="form-control wizard-required" id="comp_address">
                                    <label for="comp_address" class="wizard-form-text-label">Company Address*</label>
                                    <div class="wizard-form-error"></div>
                                </div>
                                <div class="form-group col-md-4">
                                    <input type="text" class="form-control wizard-required" id="aptsuite">
                                    <label for="aptsuite" class="wizard-form-text-label">Apt/Suite*</label>
                                    <div class="wizard-form-error"></div>
                                </div>


                                <div class="form-group col-md-5">
                                    <select name="country" id="country" class="form-control wizard-required" required="required"><option value="">Country*</option><option value="United States" id="5f80e1963884e632f4a55785">United States</option><option value="Afghanistan" id="5f80e1963884e632f4a55786">Afghanistan</option><option value="Aland Islands" id="5f80e1963884e632f4a55787">Aland Islands</option><option value="Albania" id="5f80e1973884e632f4a55788">Albania</option><option value="Angola" id="5f80e1973884e632f4a5578c">Angola</option><option value="Andorra" id="5f80e1973884e632f4a5578b">Andorra</option><option value="Anguilla" id="5f80e1973884e632f4a5578d">Anguilla</option><option value="Antarctica" id="5f80e1973884e632f4a5578e">Antarctica</option><option value="Algeria" id="5f80e1973884e632f4a55789">Algeria</option><option value="American Samoa" id="5f80e1973884e632f4a5578a">American Samoa</option><option value="Argentina" id="5f80e1973884e632f4a55790">Argentina</option><option value="Antigua And Barbuda" id="5f80e1973884e632f4a5578f">Antigua And Barbuda</option><option value="Australia" id="5f80e1973884e632f4a55793">Australia</option><option value="Aruba" id="5f80e1973884e632f4a55792">Aruba</option><option value="Armenia" id="5f80e1973884e632f4a55791">Armenia</option><option value="Azerbaijan" id="5f80e1973884e632f4a55795">Azerbaijan</option><option value="Austria" id="5f80e1973884e632f4a55794">Austria</option><option value="Bahamas The" id="5f80e1973884e632f4a55796">Bahamas The</option><option value="Bangladesh" id="5f80e1973884e632f4a55798">Bangladesh</option><option value="Bahrain" id="5f80e1973884e632f4a55797">Bahrain</option><option value="Belarus" id="5f80e1973884e632f4a5579a">Belarus</option><option value="Barbados" id="5f80e1973884e632f4a55799">Barbados</option><option value="Bouvet Island" id="5f80e1973884e632f4a557a3">Bouvet Island</option><option value="Belgium" id="5f80e1973884e632f4a5579b">Belgium</option><option value="Bolivia" id="5f80e1973884e632f4a557a0">Bolivia</option><option value="Botswana" id="5f80e1973884e632f4a557a2">Botswana</option><option value="Benin" id="5f80e1973884e632f4a5579d">Benin</option><option value="Bhutan" id="5f80e1973884e632f4a5579f">Bhutan</option><option value="Belize" id="5f80e1973884e632f4a5579c">Belize</option><option value="Brazil" id="5f80e1973884e632f4a557a4">Brazil</option><option value="Bermuda" id="5f80e1973884e632f4a5579e">Bermuda</option><option value="Bosnia and Herzegovina" id="5f80e1973884e632f4a557a1">Bosnia and Herzegovina</option><option value="British Indian Ocean Territory" id="5f80e1973884e632f4a557a5">British Indian Ocean Territory</option><option value="Cape Verde" id="5f80e1973884e632f4a557ad">Cape Verde</option><option value="Cambodia" id="5f80e1973884e632f4a557aa">Cambodia</option><option value="Cayman Islands" id="5f80e1973884e632f4a557ae">Cayman Islands</option><option value="Canada" id="5f80e1973884e632f4a557ac">Canada</option><option value="Burkina Faso" id="5f80e1973884e632f4a557a8">Burkina Faso</option><option value="Brunei" id="5f80e1973884e632f4a557a6">Brunei</option><option value="Cameroon" id="5f80e1973884e632f4a557ab">Cameroon</option><option value="Central African Republic" id="5f80e1973884e632f4a557af">Central African Republic</option><option value="Bulgaria" id="5f80e1973884e632f4a557a7">Bulgaria</option><option value="Burundi" id="5f80e1973884e632f4a557a9">Burundi</option><option value="Chad" id="5f80e1973884e632f4a557b0">Chad</option><option value="China" id="5f80e1973884e632f4a557b2">China</option><option value="Chile" id="5f80e1973884e632f4a557b1">Chile</option><option value="Colombia" id="5f80e1973884e632f4a557b5">Colombia</option><option value="Christmas Island" id="5f80e1973884e632f4a557b3">Christmas Island</option><option value="Cocos (Keeling) Islands" id="5f80e1973884e632f4a557b4">Cocos (Keeling) Islands</option><option value="Comoros" id="5f80e1973884e632f4a557b6">Comoros</option><option value="Costa Rica" id="5f80e1973884e632f4a557ba">Costa Rica</option><option value="Cuba" id="5f80e1973884e632f4a557bd">Cuba</option><option value="Cook Islands" id="5f80e1973884e632f4a557b9">Cook Islands</option><option value="Congo" id="5f80e1973884e632f4a557b7">Congo</option><option value="Congo The Democratic Republic Of The" id="5f80e1973884e632f4a557b8">Congo The Democratic Republic Of The</option><option value="Cote D'Ivoire (Ivory Coast)" id="5f80e1973884e632f4a557bb">Cote D'Ivoire (Ivory Coast)</option><option value="Croatia (Hrvatska)" id="5f80e1973884e632f4a557bc">Croatia (Hrvatska)</option><option value="Djibouti" id="5f80e1973884e632f4a557c1">Djibouti</option><option value="Dominican Republic" id="5f80e1973884e632f4a557c3">Dominican Republic</option><option value="Czech Republic" id="5f80e1973884e632f4a557bf">Czech Republic</option><option value="Denmark" id="5f80e1973884e632f4a557c0">Denmark</option><option value="Cyprus" id="5f80e1973884e632f4a557be">Cyprus</option><option value="Dominica" id="5f80e1973884e632f4a557c2">Dominica</option><option value="Ecuador" id="5f80e1973884e632f4a557c5">Ecuador</option><option value="East Timor" id="5f80e1973884e632f4a557c4">East Timor</option><option value="Egypt" id="5f80e1973884e632f4a557c6">Egypt</option><option value="Eritrea" id="5f80e1973884e632f4a557c9">Eritrea</option><option value="Equatorial Guinea" id="5f80e1973884e632f4a557c8">Equatorial Guinea</option><option value="El Salvador" id="5f80e1973884e632f4a557c7">El Salvador</option><option value="Faroe Islands" id="5f80e1973884e632f4a557cd">Faroe Islands</option><option value="Falkland Islands" id="5f80e1973884e632f4a557cc">Falkland Islands</option><option value="Finland" id="5f80e1973884e632f4a557cf">Finland</option><option value="France" id="5f80e1973884e632f4a557d0">France</option><option value="Ethiopia" id="5f80e1973884e632f4a557cb">Ethiopia</option><option value="Fiji Islands" id="5f80e1973884e632f4a557ce">Fiji Islands</option><option value="Estonia" id="5f80e1973884e632f4a557ca">Estonia</option><option value="Gambia The" id="5f80e1973884e632f4a557d5">Gambia The</option><option value="Georgia" id="5f80e1973884e632f4a557d6">Georgia</option><option value="Gabon" id="5f80e1973884e632f4a557d4">Gabon</option><option value="French Guiana" id="5f80e1973884e632f4a557d1">French Guiana</option><option value="Germany" id="5f80e1973884e632f4a557d7">Germany</option><option value="French Polynesia" id="5f80e1973884e632f4a557d2">French Polynesia</option><option value="French Southern Territories" id="5f80e1973884e632f4a557d3">French Southern Territories</option><option value="Guam" id="5f80e1973884e632f4a557de">Guam</option><option value="Grenada" id="5f80e1973884e632f4a557dc">Grenada</option><option value="Gibraltar" id="5f80e1973884e632f4a557d9">Gibraltar</option><option value="Ghana" id="5f80e1973884e632f4a557d8">Ghana</option><option value="Guadeloupe" id="5f80e1973884e632f4a557dd">Guadeloupe</option><option value="Greece" id="5f80e1973884e632f4a557da">Greece</option><option value="Greenland" id="5f80e1973884e632f4a557db">Greenland</option><option value="Guinea-Bissau" id="5f80e1973884e632f4a557e2">Guinea-Bissau</option><option value="Guyana" id="5f80e1973884e632f4a557e3">Guyana</option><option value="Guatemala" id="5f80e1973884e632f4a557df">Guatemala</option><option value="Heard and McDonald Islands" id="5f80e1973884e632f4a557e5">Heard and McDonald Islands</option><option value="Haiti" id="5f80e1973884e632f4a557e4">Haiti</option><option value="Guernsey and Alderney" id="5f80e1973884e632f4a557e0">Guernsey and Alderney</option><option value="Guinea" id="5f80e1973884e632f4a557e1">Guinea</option><option value="Hungary" id="5f80e1973884e632f4a557e8">Hungary</option><option value="Hong Kong S.A.R." id="5f80e1973884e632f4a557e7">Hong Kong S.A.R.</option><option value="India" id="5f80e1973884e632f4a557ea">India</option><option value="Honduras" id="5f80e1973884e632f4a557e6">Honduras</option><option value="Iceland" id="5f80e1973884e632f4a557e9">Iceland</option><option value="Iran" id="5f80e1973884e632f4a557ec">Iran</option><option value="Indonesia" id="5f80e1973884e632f4a557eb">Indonesia</option><option value="Ireland" id="5f80e1973884e632f4a557ee">Ireland</option><option value="Israel" id="5f80e1973884e632f4a557ef">Israel</option><option value="Iraq" id="5f80e1973884e632f4a557ed">Iraq</option><option value="Japan" id="5f80e1973884e632f4a557f2">Japan</option><option value="Jordan" id="5f80e1973884e632f4a557f4">Jordan</option><option value="Italy" id="5f80e1973884e632f4a557f0">Italy</option><option value="Jamaica" id="5f80e1973884e632f4a557f1">Jamaica</option><option value="Jersey" id="5f80e1973884e632f4a557f3">Jersey</option><option value="Kazakhstan" id="5f80e1973884e632f4a557f5">Kazakhstan</option><option value="Korea North" id="5f80e1973884e632f4a557f8">Korea North</option><option value="Korea South" id="5f80e1973884e632f4a557f9">Korea South</option><option value="Kiribati" id="5f80e1973884e632f4a557f7">Kiribati</option><option value="Kyrgyzstan" id="5f80e1973884e632f4a557fc">Kyrgyzstan</option><option value="Kosovo" id="5f80e1973884e632f4a557fa">Kosovo</option><option value="Laos" id="5f80e1973884e632f4a557fd">Laos</option><option value="Kuwait" id="5f80e1973884e632f4a557fb">Kuwait</option><option value="Kenya" id="5f80e1973884e632f4a557f6">Kenya</option><option value="Latvia" id="5f80e1973884e632f4a557fe">Latvia</option><option value="Lesotho" id="5f80e1973884e632f4a55800">Lesotho</option><option value="Liechtenstein" id="5f80e1973884e632f4a55803">Liechtenstein</option><option value="Libya" id="5f80e1973884e632f4a55802">Libya</option><option value="Lebanon" id="5f80e1973884e632f4a557ff">Lebanon</option><option value="Liberia" id="5f80e1973884e632f4a55801">Liberia</option><option value="Luxembourg" id="5f80e1973884e632f4a55805">Luxembourg</option><option value="Lithuania" id="5f80e1973884e632f4a55804">Lithuania</option><option value="Macedonia" id="5f80e1973884e632f4a55807">Macedonia</option><option value="Macau S.A.R." id="5f80e1973884e632f4a55806">Macau S.A.R.</option><option value="Madagascar" id="5f80e1973884e632f4a55808">Madagascar</option><option value="Mauritania" id="5f80e1973884e632f4a55811">Mauritania</option><option value="Mali" id="5f80e1973884e632f4a5580c">Mali</option><option value="Man (Isle of)" id="5f80e1973884e632f4a5580e">Man (Isle of)</option><option value="Malawi" id="5f80e1973884e632f4a55809">Malawi</option><option value="Malta" id="5f80e1973884e632f4a5580d">Malta</option><option value="Maldives" id="5f80e1973884e632f4a5580b">Maldives</option><option value="Malaysia" id="5f80e1973884e632f4a5580a">Malaysia</option><option value="Martinique" id="5f80e1973884e632f4a55810">Martinique</option><option value="Marshall Islands" id="5f80e1973884e632f4a5580f">Marshall Islands</option><option value="Mexico" id="5f80e1973884e632f4a55814">Mexico</option><option value="Micronesia" id="5f80e1973884e632f4a55815">Micronesia</option><option value="Moldova" id="5f80e1973884e632f4a55816">Moldova</option><option value="Mauritius" id="5f80e1973884e632f4a55812">Mauritius</option><option value="Mayotte" id="5f80e1973884e632f4a55813">Mayotte</option><option value="Montenegro" id="5f80e1973884e632f4a55819">Montenegro</option><option value="Montserrat" id="5f80e1973884e632f4a5581a">Montserrat</option><option value="Monaco" id="5f80e1973884e632f4a55817">Monaco</option><option value="Mongolia" id="5f80e1973884e632f4a55818">Mongolia</option><option value="Myanmar" id="5f80e1973884e632f4a5581d">Myanmar</option><option value="Namibia" id="5f80e1973884e632f4a5581e">Namibia</option><option value="Mozambique" id="5f80e1973884e632f4a5581c">Mozambique</option><option value="Morocco" id="5f80e1973884e632f4a5581b">Morocco</option><option value="Nauru" id="5f80e1973884e632f4a5581f">Nauru</option><option value="Nepal" id="5f80e1973884e632f4a55820">Nepal</option><option value="Netherlands Antilles" id="5f80e1973884e632f4a55821">Netherlands Antilles</option><option value="New Caledonia" id="5f80e1973884e632f4a55823">New Caledonia</option><option value="Nicaragua" id="5f80e1973884e632f4a55825">Nicaragua</option><option value="New Zealand" id="5f80e1973884e632f4a55824">New Zealand</option><option value="Netherlands The" id="5f80e1973884e632f4a55822">Netherlands The</option><option value="Niger" id="5f80e1973884e632f4a55826">Niger</option><option value="Nigeria" id="5f80e1973884e632f4a55827">Nigeria</option><option value="Niue" id="5f80e1973884e632f4a55828">Niue</option><option value="Northern Mariana Islands" id="5f80e1973884e632f4a5582a">Northern Mariana Islands</option><option value="Norfolk Island" id="5f80e1973884e632f4a55829">Norfolk Island</option><option value="Oman" id="5f80e1973884e632f4a5582c">Oman</option><option value="Norway" id="5f80e1973884e632f4a5582b">Norway</option><option value="Pakistan" id="5f80e1973884e632f4a5582d">Pakistan</option><option value="Panama" id="5f80e1973884e632f4a55830">Panama</option><option value="Paraguay" id="5f80e1973884e632f4a55832">Paraguay</option><option value="Romania" id="5f80e1973884e632f4a5583b">Romania</option><option value="Papua new Guinea" id="5f80e1973884e632f4a55831">Papua new Guinea</option><option value="Poland" id="5f80e1973884e632f4a55836">Poland</option><option value="Peru" id="5f80e1973884e632f4a55833">Peru</option><option value="Palau" id="5f80e1973884e632f4a5582e">Palau</option><option value="Pitcairn Island" id="5f80e1973884e632f4a55835">Pitcairn Island</option><option value="Puerto Rico" id="5f80e1973884e632f4a55838">Puerto Rico</option><option value="Qatar" id="5f80e1973884e632f4a55839">Qatar</option><option value="Palestinian Territory Occupied" id="5f80e1973884e632f4a5582f">Palestinian Territory Occupied</option><option value="Philippines" id="5f80e1973884e632f4a55834">Philippines</option><option value="Portugal" id="5f80e1973884e632f4a55837">Portugal</option><option value="Reunion" id="5f80e1973884e632f4a5583a">Reunion</option><option value="Russia" id="5f80e1973884e632f4a5583c">Russia</option><option value="Saint Helena" id="5f80e1973884e632f4a5583e">Saint Helena</option><option value="Rwanda" id="5f80e1973884e632f4a5583d">Rwanda</option><option value="Saint Kitts And Nevis" id="5f80e1973884e632f4a5583f">Saint Kitts And Nevis</option><option value="Saint Lucia" id="5f80e1973884e632f4a55840">Saint Lucia</option><option value="Saint Pierre and Miquelon" id="5f80e1973884e632f4a55841">Saint Pierre and Miquelon</option><option value="Saint Vincent And The Grenadines" id="5f80e1973884e632f4a55842">Saint Vincent And The Grenadines</option><option value="Sao Tome and Principe" id="5f80e1973884e632f4a55847">Sao Tome and Principe</option><option value="Samoa" id="5f80e1973884e632f4a55845">Samoa</option><option value="Saint-Barthelemy" id="5f80e1973884e632f4a55843">Saint-Barthelemy</option><option value="San Marino" id="5f80e1973884e632f4a55846">San Marino</option><option value="Saint-Martin (French part)" id="5f80e1973884e632f4a55844">Saint-Martin (French part)</option><option value="Saudi Arabia" id="5f80e1973884e632f4a55848">Saudi Arabia</option><option value="Serbia" id="5f80e1973884e632f4a5584a">Serbia</option><option value="Senegal" id="5f80e1973884e632f4a55849">Senegal</option><option value="Sierra Leone" id="5f80e1973884e632f4a5584c">Sierra Leone</option><option value="Singapore" id="5f80e1973884e632f4a5584d">Singapore</option><option value="Slovakia" id="5f80e1973884e632f4a5584e">Slovakia</option><option value="Seychelles" id="5f80e1973884e632f4a5584b">Seychelles</option><option value="South Africa" id="5f80e1973884e632f4a55852">South Africa</option><option value="Somalia" id="5f80e1973884e632f4a55851">Somalia</option><option value="Solomon Islands" id="5f80e1973884e632f4a55850">Solomon Islands</option><option value="Slovenia" id="5f80e1973884e632f4a5584f">Slovenia</option><option value="South Georgia" id="5f80e1973884e632f4a55853">South Georgia</option><option value="Svalbard And Jan Mayen Islands" id="5f80e1973884e632f4a55859">Svalbard And Jan Mayen Islands</option><option value="Sweden" id="5f80e1973884e632f4a5585b">Sweden</option><option value="Swaziland" id="5f80e1973884e632f4a5585a">Swaziland</option><option value="Taiwan" id="5f80e1973884e632f4a5585e">Taiwan</option><option value="Thailand" id="5f80e1973884e632f4a55861">Thailand</option><option value="Tajikistan" id="5f80e1973884e632f4a5585f">Tajikistan</option><option value="Suriname" id="5f80e1973884e632f4a55858">Suriname</option><option value="Switzerland" id="5f80e1973884e632f4a5585c">Switzerland</option><option value="Tanzania" id="5f80e1973884e632f4a55860">Tanzania</option><option value="Spain" id="5f80e1973884e632f4a55855">Spain</option><option value="Sri Lanka" id="5f80e1973884e632f4a55856">Sri Lanka</option><option value="South Sudan" id="5f80e1973884e632f4a55854">South Sudan</option><option value="Syria" id="5f80e1973884e632f4a5585d">Syria</option><option value="Sudan" id="5f80e1973884e632f4a55857">Sudan</option><option value="Trinidad And Tobago" id="5f80e1973884e632f4a55865">Trinidad And Tobago</option><option value="Tokelau" id="5f80e1973884e632f4a55863">Tokelau</option><option value="Togo" id="5f80e1973884e632f4a55862">Togo</option><option value="Tonga" id="5f80e1973884e632f4a55864">Tonga</option><option value="Tunisia" id="5f80e1973884e632f4a55866">Tunisia</option><option value="Turkey" id="5f80e1973884e632f4a55867">Turkey</option><option value="Ukraine" id="5f80e1973884e632f4a5586c">Ukraine</option><option value="Uganda" id="5f80e1973884e632f4a5586b">Uganda</option><option value="Turkmenistan" id="5f80e1973884e632f4a55868">Turkmenistan</option><option value="Tuvalu" id="5f80e1973884e632f4a5586a">Tuvalu</option><option value="Turks And Caicos Islands" id="5f80e1973884e632f4a55869">Turks And Caicos Islands</option><option value="United Arab Emirates" id="5f80e1973884e632f4a5586d">United Arab Emirates</option><option value="United Kingdom" id="5f80e1973884e632f4a5586e">United Kingdom</option><option value="Vatican City State (Holy See)" id="5f80e1973884e632f4a55873">Vatican City State (Holy See)</option><option value="United States Minor Outlying Islands" id="5f80e1973884e632f4a5586f">United States Minor Outlying Islands</option><option value="Uzbekistan" id="5f80e1973884e632f4a55871">Uzbekistan</option><option value="Vanuatu" id="5f80e1973884e632f4a55872">Vanuatu</option><option value="Uruguay" id="5f80e1973884e632f4a55870">Uruguay</option><option value="Virgin Islands (US)" id="5f80e1973884e632f4a55877">Virgin Islands (US)</option><option value="Virgin Islands (British)" id="5f80e1973884e632f4a55876">Virgin Islands (British)</option><option value="Vietnam" id="5f80e1973884e632f4a55875">Vietnam</option><option value="Venezuela" id="5f80e1973884e632f4a55874">Venezuela</option><option value="Zambia" id="5f80e1973884e632f4a5587b">Zambia</option><option value="Wallis And Futuna Islands" id="5f80e1973884e632f4a55878">Wallis And Futuna Islands</option><option value="Yemen" id="5f80e1973884e632f4a5587a">Yemen</option><option value="Western Sahara" id="5f80e1973884e632f4a55879">Western Sahara</option><option value="Zimbabwe" id="5f80e1973884e632f4a5587c">Zimbabwe</option></select>
                                    <div class="wizard-form-error"></div>
                                </div>
                                <div class="form-group col-md-3">
                                    <select name="province" id="province" class="selectpicker form-control wizard-required" required="required" data-toggle="tooltip" title="" data-original-title="States/Province is required"><option value="">States/Province*</option><option value="Abu Dhabi Emirate">Abu Dhabi Emirate</option><option value="Ajman Emirate">Ajman Emirate</option><option value="Dubai">Dubai</option><option value="Fujairah">Fujairah</option><option value="Ras al-Khaimah">Ras al-Khaimah</option><option value="Sharjah Emirate">Sharjah Emirate</option><option value="Umm al-Quwain">Umm al-Quwain</option></select>
                                    <div class="wizard-form-error"></div>
                                </div>
                                <div class="form-group col-md-2">
                                    <input type="text" class="form-control wizard-required" id="city">
                                    <label for="city" class="wizard-form-text-label">City*</label>
                                    <div class="wizard-form-error"></div>
                                </div>
                                <div class="form-group col-md-2">
                                    <input type="text" class="form-control wizard-required" id="zipcode">
                                    <label for="zipcode" class="wizard-form-text-label">Zip Code*</label>
                                    <div class="wizard-form-error"></div>
                                </div>

                                <div class="form-group col-md-8">
                                    <input type="email" class="form-control wizard-required" id="comp_email">
                                    <label for="comp_email" class="wizard-form-text-label">Company email*</label>
                                    <div class="wizard-form-error"></div>
                                </div>
                                <div class="form-group col-md-4">
                                    <input type="number" class="form-control wizard-required" id="comp_phone">
                                    <label for="comp_phone" class="wizard-form-text-label">Company Phone #*</label>
                                    <div class="wizard-form-error"></div>
                                </div>


                                <div class="form-group col-md-12">
                                    <input class="mailingaddressthree" type="checkbox" name="mailingaddressthree" value="1" onchange="valueChangedthree()"/> Mailing Address (If Different)

                                </div>

                                <div class="form-group col-md-12 mailingdivthree" style="display: none;">
                                    <div class="row">
                                        <div class="form-group col-md-8">
                                            <input type="text" class="form-control wizard-required" id="address">
                                            <label for="address" class="wizard-form-text-label">Address*</label>
                                            <div class="wizard-form-error"></div>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <input type="text" class="form-control wizard-required" id="aptsuite">
                                            <label for="aptsuite" class="wizard-form-text-label">Apt/Suite*</label>
                                            <div class="wizard-form-error"></div>
                                        </div>
                                        <div class="form-group col-md-5">
                                        
                                            <select name="country" id="country" class="form-control wizard-required" required="required"><option value="">Country*</option><option value="United States" id="5f80e1963884e632f4a55785">United States</option><option value="Afghanistan" id="5f80e1963884e632f4a55786">Afghanistan</option><option value="Aland Islands" id="5f80e1963884e632f4a55787">Aland Islands</option><option value="Albania" id="5f80e1973884e632f4a55788">Albania</option><option value="Angola" id="5f80e1973884e632f4a5578c">Angola</option><option value="Andorra" id="5f80e1973884e632f4a5578b">Andorra</option><option value="Anguilla" id="5f80e1973884e632f4a5578d">Anguilla</option><option value="Antarctica" id="5f80e1973884e632f4a5578e">Antarctica</option><option value="Algeria" id="5f80e1973884e632f4a55789">Algeria</option><option value="American Samoa" id="5f80e1973884e632f4a5578a">American Samoa</option><option value="Argentina" id="5f80e1973884e632f4a55790">Argentina</option><option value="Antigua And Barbuda" id="5f80e1973884e632f4a5578f">Antigua And Barbuda</option><option value="Australia" id="5f80e1973884e632f4a55793">Australia</option><option value="Aruba" id="5f80e1973884e632f4a55792">Aruba</option><option value="Armenia" id="5f80e1973884e632f4a55791">Armenia</option><option value="Azerbaijan" id="5f80e1973884e632f4a55795">Azerbaijan</option><option value="Austria" id="5f80e1973884e632f4a55794">Austria</option><option value="Bahamas The" id="5f80e1973884e632f4a55796">Bahamas The</option><option value="Bangladesh" id="5f80e1973884e632f4a55798">Bangladesh</option><option value="Bahrain" id="5f80e1973884e632f4a55797">Bahrain</option><option value="Belarus" id="5f80e1973884e632f4a5579a">Belarus</option><option value="Barbados" id="5f80e1973884e632f4a55799">Barbados</option><option value="Bouvet Island" id="5f80e1973884e632f4a557a3">Bouvet Island</option><option value="Belgium" id="5f80e1973884e632f4a5579b">Belgium</option><option value="Bolivia" id="5f80e1973884e632f4a557a0">Bolivia</option><option value="Botswana" id="5f80e1973884e632f4a557a2">Botswana</option><option value="Benin" id="5f80e1973884e632f4a5579d">Benin</option><option value="Bhutan" id="5f80e1973884e632f4a5579f">Bhutan</option><option value="Belize" id="5f80e1973884e632f4a5579c">Belize</option><option value="Brazil" id="5f80e1973884e632f4a557a4">Brazil</option><option value="Bermuda" id="5f80e1973884e632f4a5579e">Bermuda</option><option value="Bosnia and Herzegovina" id="5f80e1973884e632f4a557a1">Bosnia and Herzegovina</option><option value="British Indian Ocean Territory" id="5f80e1973884e632f4a557a5">British Indian Ocean Territory</option><option value="Cape Verde" id="5f80e1973884e632f4a557ad">Cape Verde</option><option value="Cambodia" id="5f80e1973884e632f4a557aa">Cambodia</option><option value="Cayman Islands" id="5f80e1973884e632f4a557ae">Cayman Islands</option><option value="Canada" id="5f80e1973884e632f4a557ac">Canada</option><option value="Burkina Faso" id="5f80e1973884e632f4a557a8">Burkina Faso</option><option value="Brunei" id="5f80e1973884e632f4a557a6">Brunei</option><option value="Cameroon" id="5f80e1973884e632f4a557ab">Cameroon</option><option value="Central African Republic" id="5f80e1973884e632f4a557af">Central African Republic</option><option value="Bulgaria" id="5f80e1973884e632f4a557a7">Bulgaria</option><option value="Burundi" id="5f80e1973884e632f4a557a9">Burundi</option><option value="Chad" id="5f80e1973884e632f4a557b0">Chad</option><option value="China" id="5f80e1973884e632f4a557b2">China</option><option value="Chile" id="5f80e1973884e632f4a557b1">Chile</option><option value="Colombia" id="5f80e1973884e632f4a557b5">Colombia</option><option value="Christmas Island" id="5f80e1973884e632f4a557b3">Christmas Island</option><option value="Cocos (Keeling) Islands" id="5f80e1973884e632f4a557b4">Cocos (Keeling) Islands</option><option value="Comoros" id="5f80e1973884e632f4a557b6">Comoros</option><option value="Costa Rica" id="5f80e1973884e632f4a557ba">Costa Rica</option><option value="Cuba" id="5f80e1973884e632f4a557bd">Cuba</option><option value="Cook Islands" id="5f80e1973884e632f4a557b9">Cook Islands</option><option value="Congo" id="5f80e1973884e632f4a557b7">Congo</option><option value="Congo The Democratic Republic Of The" id="5f80e1973884e632f4a557b8">Congo The Democratic Republic Of The</option><option value="Cote D'Ivoire (Ivory Coast)" id="5f80e1973884e632f4a557bb">Cote D'Ivoire (Ivory Coast)</option><option value="Croatia (Hrvatska)" id="5f80e1973884e632f4a557bc">Croatia (Hrvatska)</option><option value="Djibouti" id="5f80e1973884e632f4a557c1">Djibouti</option><option value="Dominican Republic" id="5f80e1973884e632f4a557c3">Dominican Republic</option><option value="Czech Republic" id="5f80e1973884e632f4a557bf">Czech Republic</option><option value="Denmark" id="5f80e1973884e632f4a557c0">Denmark</option><option value="Cyprus" id="5f80e1973884e632f4a557be">Cyprus</option><option value="Dominica" id="5f80e1973884e632f4a557c2">Dominica</option><option value="Ecuador" id="5f80e1973884e632f4a557c5">Ecuador</option><option value="East Timor" id="5f80e1973884e632f4a557c4">East Timor</option><option value="Egypt" id="5f80e1973884e632f4a557c6">Egypt</option><option value="Eritrea" id="5f80e1973884e632f4a557c9">Eritrea</option><option value="Equatorial Guinea" id="5f80e1973884e632f4a557c8">Equatorial Guinea</option><option value="El Salvador" id="5f80e1973884e632f4a557c7">El Salvador</option><option value="Faroe Islands" id="5f80e1973884e632f4a557cd">Faroe Islands</option><option value="Falkland Islands" id="5f80e1973884e632f4a557cc">Falkland Islands</option><option value="Finland" id="5f80e1973884e632f4a557cf">Finland</option><option value="France" id="5f80e1973884e632f4a557d0">France</option><option value="Ethiopia" id="5f80e1973884e632f4a557cb">Ethiopia</option><option value="Fiji Islands" id="5f80e1973884e632f4a557ce">Fiji Islands</option><option value="Estonia" id="5f80e1973884e632f4a557ca">Estonia</option><option value="Gambia The" id="5f80e1973884e632f4a557d5">Gambia The</option><option value="Georgia" id="5f80e1973884e632f4a557d6">Georgia</option><option value="Gabon" id="5f80e1973884e632f4a557d4">Gabon</option><option value="French Guiana" id="5f80e1973884e632f4a557d1">French Guiana</option><option value="Germany" id="5f80e1973884e632f4a557d7">Germany</option><option value="French Polynesia" id="5f80e1973884e632f4a557d2">French Polynesia</option><option value="French Southern Territories" id="5f80e1973884e632f4a557d3">French Southern Territories</option><option value="Guam" id="5f80e1973884e632f4a557de">Guam</option><option value="Grenada" id="5f80e1973884e632f4a557dc">Grenada</option><option value="Gibraltar" id="5f80e1973884e632f4a557d9">Gibraltar</option><option value="Ghana" id="5f80e1973884e632f4a557d8">Ghana</option><option value="Guadeloupe" id="5f80e1973884e632f4a557dd">Guadeloupe</option><option value="Greece" id="5f80e1973884e632f4a557da">Greece</option><option value="Greenland" id="5f80e1973884e632f4a557db">Greenland</option><option value="Guinea-Bissau" id="5f80e1973884e632f4a557e2">Guinea-Bissau</option><option value="Guyana" id="5f80e1973884e632f4a557e3">Guyana</option><option value="Guatemala" id="5f80e1973884e632f4a557df">Guatemala</option><option value="Heard and McDonald Islands" id="5f80e1973884e632f4a557e5">Heard and McDonald Islands</option><option value="Haiti" id="5f80e1973884e632f4a557e4">Haiti</option><option value="Guernsey and Alderney" id="5f80e1973884e632f4a557e0">Guernsey and Alderney</option><option value="Guinea" id="5f80e1973884e632f4a557e1">Guinea</option><option value="Hungary" id="5f80e1973884e632f4a557e8">Hungary</option><option value="Hong Kong S.A.R." id="5f80e1973884e632f4a557e7">Hong Kong S.A.R.</option><option value="India" id="5f80e1973884e632f4a557ea">India</option><option value="Honduras" id="5f80e1973884e632f4a557e6">Honduras</option><option value="Iceland" id="5f80e1973884e632f4a557e9">Iceland</option><option value="Iran" id="5f80e1973884e632f4a557ec">Iran</option><option value="Indonesia" id="5f80e1973884e632f4a557eb">Indonesia</option><option value="Ireland" id="5f80e1973884e632f4a557ee">Ireland</option><option value="Israel" id="5f80e1973884e632f4a557ef">Israel</option><option value="Iraq" id="5f80e1973884e632f4a557ed">Iraq</option><option value="Japan" id="5f80e1973884e632f4a557f2">Japan</option><option value="Jordan" id="5f80e1973884e632f4a557f4">Jordan</option><option value="Italy" id="5f80e1973884e632f4a557f0">Italy</option><option value="Jamaica" id="5f80e1973884e632f4a557f1">Jamaica</option><option value="Jersey" id="5f80e1973884e632f4a557f3">Jersey</option><option value="Kazakhstan" id="5f80e1973884e632f4a557f5">Kazakhstan</option><option value="Korea North" id="5f80e1973884e632f4a557f8">Korea North</option><option value="Korea South" id="5f80e1973884e632f4a557f9">Korea South</option><option value="Kiribati" id="5f80e1973884e632f4a557f7">Kiribati</option><option value="Kyrgyzstan" id="5f80e1973884e632f4a557fc">Kyrgyzstan</option><option value="Kosovo" id="5f80e1973884e632f4a557fa">Kosovo</option><option value="Laos" id="5f80e1973884e632f4a557fd">Laos</option><option value="Kuwait" id="5f80e1973884e632f4a557fb">Kuwait</option><option value="Kenya" id="5f80e1973884e632f4a557f6">Kenya</option><option value="Latvia" id="5f80e1973884e632f4a557fe">Latvia</option><option value="Lesotho" id="5f80e1973884e632f4a55800">Lesotho</option><option value="Liechtenstein" id="5f80e1973884e632f4a55803">Liechtenstein</option><option value="Libya" id="5f80e1973884e632f4a55802">Libya</option><option value="Lebanon" id="5f80e1973884e632f4a557ff">Lebanon</option><option value="Liberia" id="5f80e1973884e632f4a55801">Liberia</option><option value="Luxembourg" id="5f80e1973884e632f4a55805">Luxembourg</option><option value="Lithuania" id="5f80e1973884e632f4a55804">Lithuania</option><option value="Macedonia" id="5f80e1973884e632f4a55807">Macedonia</option><option value="Macau S.A.R." id="5f80e1973884e632f4a55806">Macau S.A.R.</option><option value="Madagascar" id="5f80e1973884e632f4a55808">Madagascar</option><option value="Mauritania" id="5f80e1973884e632f4a55811">Mauritania</option><option value="Mali" id="5f80e1973884e632f4a5580c">Mali</option><option value="Man (Isle of)" id="5f80e1973884e632f4a5580e">Man (Isle of)</option><option value="Malawi" id="5f80e1973884e632f4a55809">Malawi</option><option value="Malta" id="5f80e1973884e632f4a5580d">Malta</option><option value="Maldives" id="5f80e1973884e632f4a5580b">Maldives</option><option value="Malaysia" id="5f80e1973884e632f4a5580a">Malaysia</option><option value="Martinique" id="5f80e1973884e632f4a55810">Martinique</option><option value="Marshall Islands" id="5f80e1973884e632f4a5580f">Marshall Islands</option><option value="Mexico" id="5f80e1973884e632f4a55814">Mexico</option><option value="Micronesia" id="5f80e1973884e632f4a55815">Micronesia</option><option value="Moldova" id="5f80e1973884e632f4a55816">Moldova</option><option value="Mauritius" id="5f80e1973884e632f4a55812">Mauritius</option><option value="Mayotte" id="5f80e1973884e632f4a55813">Mayotte</option><option value="Montenegro" id="5f80e1973884e632f4a55819">Montenegro</option><option value="Montserrat" id="5f80e1973884e632f4a5581a">Montserrat</option><option value="Monaco" id="5f80e1973884e632f4a55817">Monaco</option><option value="Mongolia" id="5f80e1973884e632f4a55818">Mongolia</option><option value="Myanmar" id="5f80e1973884e632f4a5581d">Myanmar</option><option value="Namibia" id="5f80e1973884e632f4a5581e">Namibia</option><option value="Mozambique" id="5f80e1973884e632f4a5581c">Mozambique</option><option value="Morocco" id="5f80e1973884e632f4a5581b">Morocco</option><option value="Nauru" id="5f80e1973884e632f4a5581f">Nauru</option><option value="Nepal" id="5f80e1973884e632f4a55820">Nepal</option><option value="Netherlands Antilles" id="5f80e1973884e632f4a55821">Netherlands Antilles</option><option value="New Caledonia" id="5f80e1973884e632f4a55823">New Caledonia</option><option value="Nicaragua" id="5f80e1973884e632f4a55825">Nicaragua</option><option value="New Zealand" id="5f80e1973884e632f4a55824">New Zealand</option><option value="Netherlands The" id="5f80e1973884e632f4a55822">Netherlands The</option><option value="Niger" id="5f80e1973884e632f4a55826">Niger</option><option value="Nigeria" id="5f80e1973884e632f4a55827">Nigeria</option><option value="Niue" id="5f80e1973884e632f4a55828">Niue</option><option value="Northern Mariana Islands" id="5f80e1973884e632f4a5582a">Northern Mariana Islands</option><option value="Norfolk Island" id="5f80e1973884e632f4a55829">Norfolk Island</option><option value="Oman" id="5f80e1973884e632f4a5582c">Oman</option><option value="Norway" id="5f80e1973884e632f4a5582b">Norway</option><option value="Pakistan" id="5f80e1973884e632f4a5582d">Pakistan</option><option value="Panama" id="5f80e1973884e632f4a55830">Panama</option><option value="Paraguay" id="5f80e1973884e632f4a55832">Paraguay</option><option value="Romania" id="5f80e1973884e632f4a5583b">Romania</option><option value="Papua new Guinea" id="5f80e1973884e632f4a55831">Papua new Guinea</option><option value="Poland" id="5f80e1973884e632f4a55836">Poland</option><option value="Peru" id="5f80e1973884e632f4a55833">Peru</option><option value="Palau" id="5f80e1973884e632f4a5582e">Palau</option><option value="Pitcairn Island" id="5f80e1973884e632f4a55835">Pitcairn Island</option><option value="Puerto Rico" id="5f80e1973884e632f4a55838">Puerto Rico</option><option value="Qatar" id="5f80e1973884e632f4a55839">Qatar</option><option value="Palestinian Territory Occupied" id="5f80e1973884e632f4a5582f">Palestinian Territory Occupied</option><option value="Philippines" id="5f80e1973884e632f4a55834">Philippines</option><option value="Portugal" id="5f80e1973884e632f4a55837">Portugal</option><option value="Reunion" id="5f80e1973884e632f4a5583a">Reunion</option><option value="Russia" id="5f80e1973884e632f4a5583c">Russia</option><option value="Saint Helena" id="5f80e1973884e632f4a5583e">Saint Helena</option><option value="Rwanda" id="5f80e1973884e632f4a5583d">Rwanda</option><option value="Saint Kitts And Nevis" id="5f80e1973884e632f4a5583f">Saint Kitts And Nevis</option><option value="Saint Lucia" id="5f80e1973884e632f4a55840">Saint Lucia</option><option value="Saint Pierre and Miquelon" id="5f80e1973884e632f4a55841">Saint Pierre and Miquelon</option><option value="Saint Vincent And The Grenadines" id="5f80e1973884e632f4a55842">Saint Vincent And The Grenadines</option><option value="Sao Tome and Principe" id="5f80e1973884e632f4a55847">Sao Tome and Principe</option><option value="Samoa" id="5f80e1973884e632f4a55845">Samoa</option><option value="Saint-Barthelemy" id="5f80e1973884e632f4a55843">Saint-Barthelemy</option><option value="San Marino" id="5f80e1973884e632f4a55846">San Marino</option><option value="Saint-Martin (French part)" id="5f80e1973884e632f4a55844">Saint-Martin (French part)</option><option value="Saudi Arabia" id="5f80e1973884e632f4a55848">Saudi Arabia</option><option value="Serbia" id="5f80e1973884e632f4a5584a">Serbia</option><option value="Senegal" id="5f80e1973884e632f4a55849">Senegal</option><option value="Sierra Leone" id="5f80e1973884e632f4a5584c">Sierra Leone</option><option value="Singapore" id="5f80e1973884e632f4a5584d">Singapore</option><option value="Slovakia" id="5f80e1973884e632f4a5584e">Slovakia</option><option value="Seychelles" id="5f80e1973884e632f4a5584b">Seychelles</option><option value="South Africa" id="5f80e1973884e632f4a55852">South Africa</option><option value="Somalia" id="5f80e1973884e632f4a55851">Somalia</option><option value="Solomon Islands" id="5f80e1973884e632f4a55850">Solomon Islands</option><option value="Slovenia" id="5f80e1973884e632f4a5584f">Slovenia</option><option value="South Georgia" id="5f80e1973884e632f4a55853">South Georgia</option><option value="Svalbard And Jan Mayen Islands" id="5f80e1973884e632f4a55859">Svalbard And Jan Mayen Islands</option><option value="Sweden" id="5f80e1973884e632f4a5585b">Sweden</option><option value="Swaziland" id="5f80e1973884e632f4a5585a">Swaziland</option><option value="Taiwan" id="5f80e1973884e632f4a5585e">Taiwan</option><option value="Thailand" id="5f80e1973884e632f4a55861">Thailand</option><option value="Tajikistan" id="5f80e1973884e632f4a5585f">Tajikistan</option><option value="Suriname" id="5f80e1973884e632f4a55858">Suriname</option><option value="Switzerland" id="5f80e1973884e632f4a5585c">Switzerland</option><option value="Tanzania" id="5f80e1973884e632f4a55860">Tanzania</option><option value="Spain" id="5f80e1973884e632f4a55855">Spain</option><option value="Sri Lanka" id="5f80e1973884e632f4a55856">Sri Lanka</option><option value="South Sudan" id="5f80e1973884e632f4a55854">South Sudan</option><option value="Syria" id="5f80e1973884e632f4a5585d">Syria</option><option value="Sudan" id="5f80e1973884e632f4a55857">Sudan</option><option value="Trinidad And Tobago" id="5f80e1973884e632f4a55865">Trinidad And Tobago</option><option value="Tokelau" id="5f80e1973884e632f4a55863">Tokelau</option><option value="Togo" id="5f80e1973884e632f4a55862">Togo</option><option value="Tonga" id="5f80e1973884e632f4a55864">Tonga</option><option value="Tunisia" id="5f80e1973884e632f4a55866">Tunisia</option><option value="Turkey" id="5f80e1973884e632f4a55867">Turkey</option><option value="Ukraine" id="5f80e1973884e632f4a5586c">Ukraine</option><option value="Uganda" id="5f80e1973884e632f4a5586b">Uganda</option><option value="Turkmenistan" id="5f80e1973884e632f4a55868">Turkmenistan</option><option value="Tuvalu" id="5f80e1973884e632f4a5586a">Tuvalu</option><option value="Turks And Caicos Islands" id="5f80e1973884e632f4a55869">Turks And Caicos Islands</option><option value="United Arab Emirates" id="5f80e1973884e632f4a5586d">United Arab Emirates</option><option value="United Kingdom" id="5f80e1973884e632f4a5586e">United Kingdom</option><option value="Vatican City State (Holy See)" id="5f80e1973884e632f4a55873">Vatican City State (Holy See)</option><option value="United States Minor Outlying Islands" id="5f80e1973884e632f4a5586f">United States Minor Outlying Islands</option><option value="Uzbekistan" id="5f80e1973884e632f4a55871">Uzbekistan</option><option value="Vanuatu" id="5f80e1973884e632f4a55872">Vanuatu</option><option value="Uruguay" id="5f80e1973884e632f4a55870">Uruguay</option><option value="Virgin Islands (US)" id="5f80e1973884e632f4a55877">Virgin Islands (US)</option><option value="Virgin Islands (British)" id="5f80e1973884e632f4a55876">Virgin Islands (British)</option><option value="Vietnam" id="5f80e1973884e632f4a55875">Vietnam</option><option value="Venezuela" id="5f80e1973884e632f4a55874">Venezuela</option><option value="Zambia" id="5f80e1973884e632f4a5587b">Zambia</option><option value="Wallis And Futuna Islands" id="5f80e1973884e632f4a55878">Wallis And Futuna Islands</option><option value="Yemen" id="5f80e1973884e632f4a5587a">Yemen</option><option value="Western Sahara" id="5f80e1973884e632f4a55879">Western Sahara</option><option value="Zimbabwe" id="5f80e1973884e632f4a5587c">Zimbabwe</option></select>
                                        
                                            <div class="wizard-form-error"></div>
                                        </div>
                                        <div class="form-group col-md-3">
                                            <select name="province" id="province" class="selectpicker form-control wizard-required" required="required" data-toggle="tooltip" title="" data-original-title="States/Province is required"><option value="">States/Province*</option><option value="Abu Dhabi Emirate">Abu Dhabi Emirate</option><option value="Ajman Emirate">Ajman Emirate</option><option value="Dubai">Dubai</option><option value="Fujairah">Fujairah</option><option value="Ras al-Khaimah">Ras al-Khaimah</option><option value="Sharjah Emirate">Sharjah Emirate</option><option value="Umm al-Quwain">Umm al-Quwain</option></select>
                                            <div class="wizard-form-error"></div>
                                        </div>
                                        <div class="form-group col-md-2">
                                            <input type="text" class="form-control wizard-required" id="city">
                                            <label for="city" class="wizard-form-text-label">City*</label>
                                            <div class="wizard-form-error"></div>
                                        </div>
                                        <div class="form-group col-md-2">
                                            <input type="text" class="form-control wizard-required" id="zipcode">
                                            <label for="zipcode" class="wizard-form-text-label">Zip Code*</label>
                                            <div class="wizard-form-error"></div>
                                        </div>
                                    </div>
                                </div> 

                                 <div class="form-group col-md-6">
                                    Gender
                                    <div class="wizard-form-radio">
                                    <input name="radio-name" id="radio1" type="radio">
                                    <label for="radio1">Male</label>
                                    </div>
                                    <div class="wizard-form-radio">
                                    <input name="radio-name" id="radio2" type="radio">
                                    <label for="radio2">Female</label>
                                    </div>
                                </div>
                                <div class="form-group col-md-6">
                                    <input type="text" class="form-control wizard-required" id="zcode">
                                    <label for="zcode" class="wizard-form-text-label">Zip Code*</label>
                                    <div class="wizard-form-error"></div>
                                </div> -->
                                <div class="form-group clearfix col-md-12">
                                    <a href="javascript:;" class="form-wizard-next-btn float-right">Next</a>
                                </div>
                              </div>
                            </fieldset>
                            <fieldset class="wizard-fieldset">
                                <h5>PROFESSIONAL DETAILS</h5>
                                <ul class="nav nav-pills">
                                  <li class="active"><a data-toggle="pill" href="#home" class="fancy-radio active"><i class="fa fa-briefcase" aria-hidden="true"></i>Employeed</a></li>
                                  <li style="margin-left: 20px;"><a data-toggle="pill" href="#menu1" class="fancy-radio">Self Employed</a></li>
                                  <li style="margin-left: 20px;"><a data-toggle="pill" href="#menu2" class="fancy-radio">Retired</a></li>
                                </ul>
                                <div class="tab-content">
                                  <div id="home" class="tab-pane fade in active show">
                                      <div class="row">
                                          <div class="form-group col-md-8">
                                              <input type="text" class="form-control wizard-required" id="employername">
                                              <label for="employername" class="wizard-form-text-label">Employer Name*</label>
                                              <div class="wizard-form-error"></div>
                                          </div>
                                          <div class="form-group col-md-4">
                                              <input type="text" class="form-control wizard-required" id="occupation">
                                              <label for="occupation" class="wizard-form-text-label">Occupation*</label>
                                              <div class="wizard-form-error"></div>
                                          </div>
                                          <div class="form-group col-md-8">
                                            <input type="text" class="form-control wizard-required" id="employerAdd">
                                            <label for="employerAdd" class="wizard-form-text-label">Address Of Employer*</label>
                                            <div class="wizard-form-error"></div>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <input type="text" class="form-control wizard-required" id="yearwithEmployer">
                                            <label for="yearwithEmployer" class="wizard-form-text-label">Years With Employer*</label>
                                            <div class="wizard-form-error"></div>
                                        </div>
  
                                        <div class="form-group col-md-4">
                                          
                                          <select name="country" id="country" class="form-control wizard-required" required="required"><option value="">Country*</option><option value="United States" id="5f80e1963884e632f4a55785">United States</option><option value="Afghanistan" id="5f80e1963884e632f4a55786">Afghanistan</option><option value="Aland Islands" id="5f80e1963884e632f4a55787">Aland Islands</option><option value="Albania" id="5f80e1973884e632f4a55788">Albania</option><option value="Angola" id="5f80e1973884e632f4a5578c">Angola</option><option value="Andorra" id="5f80e1973884e632f4a5578b">Andorra</option><option value="Anguilla" id="5f80e1973884e632f4a5578d">Anguilla</option><option value="Antarctica" id="5f80e1973884e632f4a5578e">Antarctica</option><option value="Algeria" id="5f80e1973884e632f4a55789">Algeria</option><option value="American Samoa" id="5f80e1973884e632f4a5578a">American Samoa</option><option value="Argentina" id="5f80e1973884e632f4a55790">Argentina</option><option value="Antigua And Barbuda" id="5f80e1973884e632f4a5578f">Antigua And Barbuda</option><option value="Australia" id="5f80e1973884e632f4a55793">Australia</option><option value="Aruba" id="5f80e1973884e632f4a55792">Aruba</option><option value="Armenia" id="5f80e1973884e632f4a55791">Armenia</option><option value="Azerbaijan" id="5f80e1973884e632f4a55795">Azerbaijan</option><option value="Austria" id="5f80e1973884e632f4a55794">Austria</option><option value="Bahamas The" id="5f80e1973884e632f4a55796">Bahamas The</option><option value="Bangladesh" id="5f80e1973884e632f4a55798">Bangladesh</option><option value="Bahrain" id="5f80e1973884e632f4a55797">Bahrain</option><option value="Belarus" id="5f80e1973884e632f4a5579a">Belarus</option><option value="Barbados" id="5f80e1973884e632f4a55799">Barbados</option><option value="Bouvet Island" id="5f80e1973884e632f4a557a3">Bouvet Island</option><option value="Belgium" id="5f80e1973884e632f4a5579b">Belgium</option><option value="Bolivia" id="5f80e1973884e632f4a557a0">Bolivia</option><option value="Botswana" id="5f80e1973884e632f4a557a2">Botswana</option><option value="Benin" id="5f80e1973884e632f4a5579d">Benin</option><option value="Bhutan" id="5f80e1973884e632f4a5579f">Bhutan</option><option value="Belize" id="5f80e1973884e632f4a5579c">Belize</option><option value="Brazil" id="5f80e1973884e632f4a557a4">Brazil</option><option value="Bermuda" id="5f80e1973884e632f4a5579e">Bermuda</option><option value="Bosnia and Herzegovina" id="5f80e1973884e632f4a557a1">Bosnia and Herzegovina</option><option value="British Indian Ocean Territory" id="5f80e1973884e632f4a557a5">British Indian Ocean Territory</option><option value="Cape Verde" id="5f80e1973884e632f4a557ad">Cape Verde</option><option value="Cambodia" id="5f80e1973884e632f4a557aa">Cambodia</option><option value="Cayman Islands" id="5f80e1973884e632f4a557ae">Cayman Islands</option><option value="Canada" id="5f80e1973884e632f4a557ac">Canada</option><option value="Burkina Faso" id="5f80e1973884e632f4a557a8">Burkina Faso</option><option value="Brunei" id="5f80e1973884e632f4a557a6">Brunei</option><option value="Cameroon" id="5f80e1973884e632f4a557ab">Cameroon</option><option value="Central African Republic" id="5f80e1973884e632f4a557af">Central African Republic</option><option value="Bulgaria" id="5f80e1973884e632f4a557a7">Bulgaria</option><option value="Burundi" id="5f80e1973884e632f4a557a9">Burundi</option><option value="Chad" id="5f80e1973884e632f4a557b0">Chad</option><option value="China" id="5f80e1973884e632f4a557b2">China</option><option value="Chile" id="5f80e1973884e632f4a557b1">Chile</option><option value="Colombia" id="5f80e1973884e632f4a557b5">Colombia</option><option value="Christmas Island" id="5f80e1973884e632f4a557b3">Christmas Island</option><option value="Cocos (Keeling) Islands" id="5f80e1973884e632f4a557b4">Cocos (Keeling) Islands</option><option value="Comoros" id="5f80e1973884e632f4a557b6">Comoros</option><option value="Costa Rica" id="5f80e1973884e632f4a557ba">Costa Rica</option><option value="Cuba" id="5f80e1973884e632f4a557bd">Cuba</option><option value="Cook Islands" id="5f80e1973884e632f4a557b9">Cook Islands</option><option value="Congo" id="5f80e1973884e632f4a557b7">Congo</option><option value="Congo The Democratic Republic Of The" id="5f80e1973884e632f4a557b8">Congo The Democratic Republic Of The</option><option value="Cote D'Ivoire (Ivory Coast)" id="5f80e1973884e632f4a557bb">Cote D'Ivoire (Ivory Coast)</option><option value="Croatia (Hrvatska)" id="5f80e1973884e632f4a557bc">Croatia (Hrvatska)</option><option value="Djibouti" id="5f80e1973884e632f4a557c1">Djibouti</option><option value="Dominican Republic" id="5f80e1973884e632f4a557c3">Dominican Republic</option><option value="Czech Republic" id="5f80e1973884e632f4a557bf">Czech Republic</option><option value="Denmark" id="5f80e1973884e632f4a557c0">Denmark</option><option value="Cyprus" id="5f80e1973884e632f4a557be">Cyprus</option><option value="Dominica" id="5f80e1973884e632f4a557c2">Dominica</option><option value="Ecuador" id="5f80e1973884e632f4a557c5">Ecuador</option><option value="East Timor" id="5f80e1973884e632f4a557c4">East Timor</option><option value="Egypt" id="5f80e1973884e632f4a557c6">Egypt</option><option value="Eritrea" id="5f80e1973884e632f4a557c9">Eritrea</option><option value="Equatorial Guinea" id="5f80e1973884e632f4a557c8">Equatorial Guinea</option><option value="El Salvador" id="5f80e1973884e632f4a557c7">El Salvador</option><option value="Faroe Islands" id="5f80e1973884e632f4a557cd">Faroe Islands</option><option value="Falkland Islands" id="5f80e1973884e632f4a557cc">Falkland Islands</option><option value="Finland" id="5f80e1973884e632f4a557cf">Finland</option><option value="France" id="5f80e1973884e632f4a557d0">France</option><option value="Ethiopia" id="5f80e1973884e632f4a557cb">Ethiopia</option><option value="Fiji Islands" id="5f80e1973884e632f4a557ce">Fiji Islands</option><option value="Estonia" id="5f80e1973884e632f4a557ca">Estonia</option><option value="Gambia The" id="5f80e1973884e632f4a557d5">Gambia The</option><option value="Georgia" id="5f80e1973884e632f4a557d6">Georgia</option><option value="Gabon" id="5f80e1973884e632f4a557d4">Gabon</option><option value="French Guiana" id="5f80e1973884e632f4a557d1">French Guiana</option><option value="Germany" id="5f80e1973884e632f4a557d7">Germany</option><option value="French Polynesia" id="5f80e1973884e632f4a557d2">French Polynesia</option><option value="French Southern Territories" id="5f80e1973884e632f4a557d3">French Southern Territories</option><option value="Guam" id="5f80e1973884e632f4a557de">Guam</option><option value="Grenada" id="5f80e1973884e632f4a557dc">Grenada</option><option value="Gibraltar" id="5f80e1973884e632f4a557d9">Gibraltar</option><option value="Ghana" id="5f80e1973884e632f4a557d8">Ghana</option><option value="Guadeloupe" id="5f80e1973884e632f4a557dd">Guadeloupe</option><option value="Greece" id="5f80e1973884e632f4a557da">Greece</option><option value="Greenland" id="5f80e1973884e632f4a557db">Greenland</option><option value="Guinea-Bissau" id="5f80e1973884e632f4a557e2">Guinea-Bissau</option><option value="Guyana" id="5f80e1973884e632f4a557e3">Guyana</option><option value="Guatemala" id="5f80e1973884e632f4a557df">Guatemala</option><option value="Heard and McDonald Islands" id="5f80e1973884e632f4a557e5">Heard and McDonald Islands</option><option value="Haiti" id="5f80e1973884e632f4a557e4">Haiti</option><option value="Guernsey and Alderney" id="5f80e1973884e632f4a557e0">Guernsey and Alderney</option><option value="Guinea" id="5f80e1973884e632f4a557e1">Guinea</option><option value="Hungary" id="5f80e1973884e632f4a557e8">Hungary</option><option value="Hong Kong S.A.R." id="5f80e1973884e632f4a557e7">Hong Kong S.A.R.</option><option value="India" id="5f80e1973884e632f4a557ea">India</option><option value="Honduras" id="5f80e1973884e632f4a557e6">Honduras</option><option value="Iceland" id="5f80e1973884e632f4a557e9">Iceland</option><option value="Iran" id="5f80e1973884e632f4a557ec">Iran</option><option value="Indonesia" id="5f80e1973884e632f4a557eb">Indonesia</option><option value="Ireland" id="5f80e1973884e632f4a557ee">Ireland</option><option value="Israel" id="5f80e1973884e632f4a557ef">Israel</option><option value="Iraq" id="5f80e1973884e632f4a557ed">Iraq</option><option value="Japan" id="5f80e1973884e632f4a557f2">Japan</option><option value="Jordan" id="5f80e1973884e632f4a557f4">Jordan</option><option value="Italy" id="5f80e1973884e632f4a557f0">Italy</option><option value="Jamaica" id="5f80e1973884e632f4a557f1">Jamaica</option><option value="Jersey" id="5f80e1973884e632f4a557f3">Jersey</option><option value="Kazakhstan" id="5f80e1973884e632f4a557f5">Kazakhstan</option><option value="Korea North" id="5f80e1973884e632f4a557f8">Korea North</option><option value="Korea South" id="5f80e1973884e632f4a557f9">Korea South</option><option value="Kiribati" id="5f80e1973884e632f4a557f7">Kiribati</option><option value="Kyrgyzstan" id="5f80e1973884e632f4a557fc">Kyrgyzstan</option><option value="Kosovo" id="5f80e1973884e632f4a557fa">Kosovo</option><option value="Laos" id="5f80e1973884e632f4a557fd">Laos</option><option value="Kuwait" id="5f80e1973884e632f4a557fb">Kuwait</option><option value="Kenya" id="5f80e1973884e632f4a557f6">Kenya</option><option value="Latvia" id="5f80e1973884e632f4a557fe">Latvia</option><option value="Lesotho" id="5f80e1973884e632f4a55800">Lesotho</option><option value="Liechtenstein" id="5f80e1973884e632f4a55803">Liechtenstein</option><option value="Libya" id="5f80e1973884e632f4a55802">Libya</option><option value="Lebanon" id="5f80e1973884e632f4a557ff">Lebanon</option><option value="Liberia" id="5f80e1973884e632f4a55801">Liberia</option><option value="Luxembourg" id="5f80e1973884e632f4a55805">Luxembourg</option><option value="Lithuania" id="5f80e1973884e632f4a55804">Lithuania</option><option value="Macedonia" id="5f80e1973884e632f4a55807">Macedonia</option><option value="Macau S.A.R." id="5f80e1973884e632f4a55806">Macau S.A.R.</option><option value="Madagascar" id="5f80e1973884e632f4a55808">Madagascar</option><option value="Mauritania" id="5f80e1973884e632f4a55811">Mauritania</option><option value="Mali" id="5f80e1973884e632f4a5580c">Mali</option><option value="Man (Isle of)" id="5f80e1973884e632f4a5580e">Man (Isle of)</option><option value="Malawi" id="5f80e1973884e632f4a55809">Malawi</option><option value="Malta" id="5f80e1973884e632f4a5580d">Malta</option><option value="Maldives" id="5f80e1973884e632f4a5580b">Maldives</option><option value="Malaysia" id="5f80e1973884e632f4a5580a">Malaysia</option><option value="Martinique" id="5f80e1973884e632f4a55810">Martinique</option><option value="Marshall Islands" id="5f80e1973884e632f4a5580f">Marshall Islands</option><option value="Mexico" id="5f80e1973884e632f4a55814">Mexico</option><option value="Micronesia" id="5f80e1973884e632f4a55815">Micronesia</option><option value="Moldova" id="5f80e1973884e632f4a55816">Moldova</option><option value="Mauritius" id="5f80e1973884e632f4a55812">Mauritius</option><option value="Mayotte" id="5f80e1973884e632f4a55813">Mayotte</option><option value="Montenegro" id="5f80e1973884e632f4a55819">Montenegro</option><option value="Montserrat" id="5f80e1973884e632f4a5581a">Montserrat</option><option value="Monaco" id="5f80e1973884e632f4a55817">Monaco</option><option value="Mongolia" id="5f80e1973884e632f4a55818">Mongolia</option><option value="Myanmar" id="5f80e1973884e632f4a5581d">Myanmar</option><option value="Namibia" id="5f80e1973884e632f4a5581e">Namibia</option><option value="Mozambique" id="5f80e1973884e632f4a5581c">Mozambique</option><option value="Morocco" id="5f80e1973884e632f4a5581b">Morocco</option><option value="Nauru" id="5f80e1973884e632f4a5581f">Nauru</option><option value="Nepal" id="5f80e1973884e632f4a55820">Nepal</option><option value="Netherlands Antilles" id="5f80e1973884e632f4a55821">Netherlands Antilles</option><option value="New Caledonia" id="5f80e1973884e632f4a55823">New Caledonia</option><option value="Nicaragua" id="5f80e1973884e632f4a55825">Nicaragua</option><option value="New Zealand" id="5f80e1973884e632f4a55824">New Zealand</option><option value="Netherlands The" id="5f80e1973884e632f4a55822">Netherlands The</option><option value="Niger" id="5f80e1973884e632f4a55826">Niger</option><option value="Nigeria" id="5f80e1973884e632f4a55827">Nigeria</option><option value="Niue" id="5f80e1973884e632f4a55828">Niue</option><option value="Northern Mariana Islands" id="5f80e1973884e632f4a5582a">Northern Mariana Islands</option><option value="Norfolk Island" id="5f80e1973884e632f4a55829">Norfolk Island</option><option value="Oman" id="5f80e1973884e632f4a5582c">Oman</option><option value="Norway" id="5f80e1973884e632f4a5582b">Norway</option><option value="Pakistan" id="5f80e1973884e632f4a5582d">Pakistan</option><option value="Panama" id="5f80e1973884e632f4a55830">Panama</option><option value="Paraguay" id="5f80e1973884e632f4a55832">Paraguay</option><option value="Romania" id="5f80e1973884e632f4a5583b">Romania</option><option value="Papua new Guinea" id="5f80e1973884e632f4a55831">Papua new Guinea</option><option value="Poland" id="5f80e1973884e632f4a55836">Poland</option><option value="Peru" id="5f80e1973884e632f4a55833">Peru</option><option value="Palau" id="5f80e1973884e632f4a5582e">Palau</option><option value="Pitcairn Island" id="5f80e1973884e632f4a55835">Pitcairn Island</option><option value="Puerto Rico" id="5f80e1973884e632f4a55838">Puerto Rico</option><option value="Qatar" id="5f80e1973884e632f4a55839">Qatar</option><option value="Palestinian Territory Occupied" id="5f80e1973884e632f4a5582f">Palestinian Territory Occupied</option><option value="Philippines" id="5f80e1973884e632f4a55834">Philippines</option><option value="Portugal" id="5f80e1973884e632f4a55837">Portugal</option><option value="Reunion" id="5f80e1973884e632f4a5583a">Reunion</option><option value="Russia" id="5f80e1973884e632f4a5583c">Russia</option><option value="Saint Helena" id="5f80e1973884e632f4a5583e">Saint Helena</option><option value="Rwanda" id="5f80e1973884e632f4a5583d">Rwanda</option><option value="Saint Kitts And Nevis" id="5f80e1973884e632f4a5583f">Saint Kitts And Nevis</option><option value="Saint Lucia" id="5f80e1973884e632f4a55840">Saint Lucia</option><option value="Saint Pierre and Miquelon" id="5f80e1973884e632f4a55841">Saint Pierre and Miquelon</option><option value="Saint Vincent And The Grenadines" id="5f80e1973884e632f4a55842">Saint Vincent And The Grenadines</option><option value="Sao Tome and Principe" id="5f80e1973884e632f4a55847">Sao Tome and Principe</option><option value="Samoa" id="5f80e1973884e632f4a55845">Samoa</option><option value="Saint-Barthelemy" id="5f80e1973884e632f4a55843">Saint-Barthelemy</option><option value="San Marino" id="5f80e1973884e632f4a55846">San Marino</option><option value="Saint-Martin (French part)" id="5f80e1973884e632f4a55844">Saint-Martin (French part)</option><option value="Saudi Arabia" id="5f80e1973884e632f4a55848">Saudi Arabia</option><option value="Serbia" id="5f80e1973884e632f4a5584a">Serbia</option><option value="Senegal" id="5f80e1973884e632f4a55849">Senegal</option><option value="Sierra Leone" id="5f80e1973884e632f4a5584c">Sierra Leone</option><option value="Singapore" id="5f80e1973884e632f4a5584d">Singapore</option><option value="Slovakia" id="5f80e1973884e632f4a5584e">Slovakia</option><option value="Seychelles" id="5f80e1973884e632f4a5584b">Seychelles</option><option value="South Africa" id="5f80e1973884e632f4a55852">South Africa</option><option value="Somalia" id="5f80e1973884e632f4a55851">Somalia</option><option value="Solomon Islands" id="5f80e1973884e632f4a55850">Solomon Islands</option><option value="Slovenia" id="5f80e1973884e632f4a5584f">Slovenia</option><option value="South Georgia" id="5f80e1973884e632f4a55853">South Georgia</option><option value="Svalbard And Jan Mayen Islands" id="5f80e1973884e632f4a55859">Svalbard And Jan Mayen Islands</option><option value="Sweden" id="5f80e1973884e632f4a5585b">Sweden</option><option value="Swaziland" id="5f80e1973884e632f4a5585a">Swaziland</option><option value="Taiwan" id="5f80e1973884e632f4a5585e">Taiwan</option><option value="Thailand" id="5f80e1973884e632f4a55861">Thailand</option><option value="Tajikistan" id="5f80e1973884e632f4a5585f">Tajikistan</option><option value="Suriname" id="5f80e1973884e632f4a55858">Suriname</option><option value="Switzerland" id="5f80e1973884e632f4a5585c">Switzerland</option><option value="Tanzania" id="5f80e1973884e632f4a55860">Tanzania</option><option value="Spain" id="5f80e1973884e632f4a55855">Spain</option><option value="Sri Lanka" id="5f80e1973884e632f4a55856">Sri Lanka</option><option value="South Sudan" id="5f80e1973884e632f4a55854">South Sudan</option><option value="Syria" id="5f80e1973884e632f4a5585d">Syria</option><option value="Sudan" id="5f80e1973884e632f4a55857">Sudan</option><option value="Trinidad And Tobago" id="5f80e1973884e632f4a55865">Trinidad And Tobago</option><option value="Tokelau" id="5f80e1973884e632f4a55863">Tokelau</option><option value="Togo" id="5f80e1973884e632f4a55862">Togo</option><option value="Tonga" id="5f80e1973884e632f4a55864">Tonga</option><option value="Tunisia" id="5f80e1973884e632f4a55866">Tunisia</option><option value="Turkey" id="5f80e1973884e632f4a55867">Turkey</option><option value="Ukraine" id="5f80e1973884e632f4a5586c">Ukraine</option><option value="Uganda" id="5f80e1973884e632f4a5586b">Uganda</option><option value="Turkmenistan" id="5f80e1973884e632f4a55868">Turkmenistan</option><option value="Tuvalu" id="5f80e1973884e632f4a5586a">Tuvalu</option><option value="Turks And Caicos Islands" id="5f80e1973884e632f4a55869">Turks And Caicos Islands</option><option value="United Arab Emirates" id="5f80e1973884e632f4a5586d">United Arab Emirates</option><option value="United Kingdom" id="5f80e1973884e632f4a5586e">United Kingdom</option><option value="Vatican City State (Holy See)" id="5f80e1973884e632f4a55873">Vatican City State (Holy See)</option><option value="United States Minor Outlying Islands" id="5f80e1973884e632f4a5586f">United States Minor Outlying Islands</option><option value="Uzbekistan" id="5f80e1973884e632f4a55871">Uzbekistan</option><option value="Vanuatu" id="5f80e1973884e632f4a55872">Vanuatu</option><option value="Uruguay" id="5f80e1973884e632f4a55870">Uruguay</option><option value="Virgin Islands (US)" id="5f80e1973884e632f4a55877">Virgin Islands (US)</option><option value="Virgin Islands (British)" id="5f80e1973884e632f4a55876">Virgin Islands (British)</option><option value="Vietnam" id="5f80e1973884e632f4a55875">Vietnam</option><option value="Venezuela" id="5f80e1973884e632f4a55874">Venezuela</option><option value="Zambia" id="5f80e1973884e632f4a5587b">Zambia</option><option value="Wallis And Futuna Islands" id="5f80e1973884e632f4a55878">Wallis And Futuna Islands</option><option value="Yemen" id="5f80e1973884e632f4a5587a">Yemen</option><option value="Western Sahara" id="5f80e1973884e632f4a55879">Western Sahara</option><option value="Zimbabwe" id="5f80e1973884e632f4a5587c">Zimbabwe</option></select>
                                      
                                          <div class="wizard-form-error"></div>
                                      </div>
                                      <div class="form-group col-md-4">
                                          <select name="province" id="province" class="selectpicker form-control wizard-required" required="required" data-toggle="tooltip" title="" data-original-title="States/Province is required"><option value="">States/Province*</option><option value="Abu Dhabi Emirate">Abu Dhabi Emirate</option><option value="Ajman Emirate">Ajman Emirate</option><option value="Dubai">Dubai</option><option value="Fujairah">Fujairah</option><option value="Ras al-Khaimah">Ras al-Khaimah</option><option value="Sharjah Emirate">Sharjah Emirate</option><option value="Umm al-Quwain">Umm al-Quwain</option></select>
                                          <div class="wizard-form-error"></div>
                                      </div>
                                      <div class="form-group col-md-4">
                                          <input type="text" class="form-control wizard-required" id="city">
                                          <label for="city" class="wizard-form-text-label">City*</label>
                                          <div class="wizard-form-error"></div>
                                      </div>
  
                                      <div class="form-group col-md-4">
                                        <input type="text" class="form-control wizard-required" id="aptsuiteno">
                                        <label for="aptsuiteno" class="wizard-form-text-label">Apt/Suite No</label>
                                        <div class="wizard-form-error"></div>
                                      </div>
                                      <div class="form-group col-md-4">
                                        <input type="number" class="form-control wizard-required" id="emp_phnumber">
                                        <label for="emp_phnumber" class="wizard-form-text-label">Employers Phone Number*</label>
                                        <div class="wizard-form-error"></div>
                                      </div>
                                      <div class="form-group col-md-3">
                                        <input type="number" class="form-control wizard-required" id="fax">
                                        <label for="fax" class="wizard-form-text-label">Fax</label>
                                        <div class="wizard-form-error"></div>
                                      </div>
  
  
                                      </div>
                                  </div>
                                  <div id="menu1" class="tab-pane fade">
                                    
                                      <div class="row">
                                        <div class="form-group col-md-8">
                                            <input type="text" class="form-control wizard-required" id="businessname">
                                            <label for="businessname" class="wizard-form-text-label">Business Name*</label>
                                            <div class="wizard-form-error"></div>
                                        </div>
                                        <div class="form-group col-md-4">
                                            <input type="text" class="form-control wizard-required" id="comp_type">
                                            <label for="comp_type" class="wizard-form-text-label">Company Type*</label>
                                            <div class="wizard-form-error"></div>
                                        </div>
                                        <div class="form-group col-md-8">
                                          <input type="text" class="form-control wizard-required" id="businessAddress">
                                          <label for="businessAddress" class="wizard-form-text-label">Business Address*</label>
                                          <div class="wizard-form-error"></div>
                                      </div>
                                      <div class="form-group col-md-4">
                                          <input type="text" class="form-control wizard-required" id="yearwithBusiness">
                                          <label for="yearwithBusiness" class="wizard-form-text-label">Years of business*</label>
                                          <div class="wizard-form-error"></div>
                                      </div>
  
                                      <div class="form-group col-md-4">
                                        
                                        <select name="country" id="country" class="form-control wizard-required" required="required"><option value="">Country*</option><option value="United States" id="5f80e1963884e632f4a55785">United States</option><option value="Afghanistan" id="5f80e1963884e632f4a55786">Afghanistan</option><option value="Aland Islands" id="5f80e1963884e632f4a55787">Aland Islands</option><option value="Albania" id="5f80e1973884e632f4a55788">Albania</option><option value="Angola" id="5f80e1973884e632f4a5578c">Angola</option><option value="Andorra" id="5f80e1973884e632f4a5578b">Andorra</option><option value="Anguilla" id="5f80e1973884e632f4a5578d">Anguilla</option><option value="Antarctica" id="5f80e1973884e632f4a5578e">Antarctica</option><option value="Algeria" id="5f80e1973884e632f4a55789">Algeria</option><option value="American Samoa" id="5f80e1973884e632f4a5578a">American Samoa</option><option value="Argentina" id="5f80e1973884e632f4a55790">Argentina</option><option value="Antigua And Barbuda" id="5f80e1973884e632f4a5578f">Antigua And Barbuda</option><option value="Australia" id="5f80e1973884e632f4a55793">Australia</option><option value="Aruba" id="5f80e1973884e632f4a55792">Aruba</option><option value="Armenia" id="5f80e1973884e632f4a55791">Armenia</option><option value="Azerbaijan" id="5f80e1973884e632f4a55795">Azerbaijan</option><option value="Austria" id="5f80e1973884e632f4a55794">Austria</option><option value="Bahamas The" id="5f80e1973884e632f4a55796">Bahamas The</option><option value="Bangladesh" id="5f80e1973884e632f4a55798">Bangladesh</option><option value="Bahrain" id="5f80e1973884e632f4a55797">Bahrain</option><option value="Belarus" id="5f80e1973884e632f4a5579a">Belarus</option><option value="Barbados" id="5f80e1973884e632f4a55799">Barbados</option><option value="Bouvet Island" id="5f80e1973884e632f4a557a3">Bouvet Island</option><option value="Belgium" id="5f80e1973884e632f4a5579b">Belgium</option><option value="Bolivia" id="5f80e1973884e632f4a557a0">Bolivia</option><option value="Botswana" id="5f80e1973884e632f4a557a2">Botswana</option><option value="Benin" id="5f80e1973884e632f4a5579d">Benin</option><option value="Bhutan" id="5f80e1973884e632f4a5579f">Bhutan</option><option value="Belize" id="5f80e1973884e632f4a5579c">Belize</option><option value="Brazil" id="5f80e1973884e632f4a557a4">Brazil</option><option value="Bermuda" id="5f80e1973884e632f4a5579e">Bermuda</option><option value="Bosnia and Herzegovina" id="5f80e1973884e632f4a557a1">Bosnia and Herzegovina</option><option value="British Indian Ocean Territory" id="5f80e1973884e632f4a557a5">British Indian Ocean Territory</option><option value="Cape Verde" id="5f80e1973884e632f4a557ad">Cape Verde</option><option value="Cambodia" id="5f80e1973884e632f4a557aa">Cambodia</option><option value="Cayman Islands" id="5f80e1973884e632f4a557ae">Cayman Islands</option><option value="Canada" id="5f80e1973884e632f4a557ac">Canada</option><option value="Burkina Faso" id="5f80e1973884e632f4a557a8">Burkina Faso</option><option value="Brunei" id="5f80e1973884e632f4a557a6">Brunei</option><option value="Cameroon" id="5f80e1973884e632f4a557ab">Cameroon</option><option value="Central African Republic" id="5f80e1973884e632f4a557af">Central African Republic</option><option value="Bulgaria" id="5f80e1973884e632f4a557a7">Bulgaria</option><option value="Burundi" id="5f80e1973884e632f4a557a9">Burundi</option><option value="Chad" id="5f80e1973884e632f4a557b0">Chad</option><option value="China" id="5f80e1973884e632f4a557b2">China</option><option value="Chile" id="5f80e1973884e632f4a557b1">Chile</option><option value="Colombia" id="5f80e1973884e632f4a557b5">Colombia</option><option value="Christmas Island" id="5f80e1973884e632f4a557b3">Christmas Island</option><option value="Cocos (Keeling) Islands" id="5f80e1973884e632f4a557b4">Cocos (Keeling) Islands</option><option value="Comoros" id="5f80e1973884e632f4a557b6">Comoros</option><option value="Costa Rica" id="5f80e1973884e632f4a557ba">Costa Rica</option><option value="Cuba" id="5f80e1973884e632f4a557bd">Cuba</option><option value="Cook Islands" id="5f80e1973884e632f4a557b9">Cook Islands</option><option value="Congo" id="5f80e1973884e632f4a557b7">Congo</option><option value="Congo The Democratic Republic Of The" id="5f80e1973884e632f4a557b8">Congo The Democratic Republic Of The</option><option value="Cote D'Ivoire (Ivory Coast)" id="5f80e1973884e632f4a557bb">Cote D'Ivoire (Ivory Coast)</option><option value="Croatia (Hrvatska)" id="5f80e1973884e632f4a557bc">Croatia (Hrvatska)</option><option value="Djibouti" id="5f80e1973884e632f4a557c1">Djibouti</option><option value="Dominican Republic" id="5f80e1973884e632f4a557c3">Dominican Republic</option><option value="Czech Republic" id="5f80e1973884e632f4a557bf">Czech Republic</option><option value="Denmark" id="5f80e1973884e632f4a557c0">Denmark</option><option value="Cyprus" id="5f80e1973884e632f4a557be">Cyprus</option><option value="Dominica" id="5f80e1973884e632f4a557c2">Dominica</option><option value="Ecuador" id="5f80e1973884e632f4a557c5">Ecuador</option><option value="East Timor" id="5f80e1973884e632f4a557c4">East Timor</option><option value="Egypt" id="5f80e1973884e632f4a557c6">Egypt</option><option value="Eritrea" id="5f80e1973884e632f4a557c9">Eritrea</option><option value="Equatorial Guinea" id="5f80e1973884e632f4a557c8">Equatorial Guinea</option><option value="El Salvador" id="5f80e1973884e632f4a557c7">El Salvador</option><option value="Faroe Islands" id="5f80e1973884e632f4a557cd">Faroe Islands</option><option value="Falkland Islands" id="5f80e1973884e632f4a557cc">Falkland Islands</option><option value="Finland" id="5f80e1973884e632f4a557cf">Finland</option><option value="France" id="5f80e1973884e632f4a557d0">France</option><option value="Ethiopia" id="5f80e1973884e632f4a557cb">Ethiopia</option><option value="Fiji Islands" id="5f80e1973884e632f4a557ce">Fiji Islands</option><option value="Estonia" id="5f80e1973884e632f4a557ca">Estonia</option><option value="Gambia The" id="5f80e1973884e632f4a557d5">Gambia The</option><option value="Georgia" id="5f80e1973884e632f4a557d6">Georgia</option><option value="Gabon" id="5f80e1973884e632f4a557d4">Gabon</option><option value="French Guiana" id="5f80e1973884e632f4a557d1">French Guiana</option><option value="Germany" id="5f80e1973884e632f4a557d7">Germany</option><option value="French Polynesia" id="5f80e1973884e632f4a557d2">French Polynesia</option><option value="French Southern Territories" id="5f80e1973884e632f4a557d3">French Southern Territories</option><option value="Guam" id="5f80e1973884e632f4a557de">Guam</option><option value="Grenada" id="5f80e1973884e632f4a557dc">Grenada</option><option value="Gibraltar" id="5f80e1973884e632f4a557d9">Gibraltar</option><option value="Ghana" id="5f80e1973884e632f4a557d8">Ghana</option><option value="Guadeloupe" id="5f80e1973884e632f4a557dd">Guadeloupe</option><option value="Greece" id="5f80e1973884e632f4a557da">Greece</option><option value="Greenland" id="5f80e1973884e632f4a557db">Greenland</option><option value="Guinea-Bissau" id="5f80e1973884e632f4a557e2">Guinea-Bissau</option><option value="Guyana" id="5f80e1973884e632f4a557e3">Guyana</option><option value="Guatemala" id="5f80e1973884e632f4a557df">Guatemala</option><option value="Heard and McDonald Islands" id="5f80e1973884e632f4a557e5">Heard and McDonald Islands</option><option value="Haiti" id="5f80e1973884e632f4a557e4">Haiti</option><option value="Guernsey and Alderney" id="5f80e1973884e632f4a557e0">Guernsey and Alderney</option><option value="Guinea" id="5f80e1973884e632f4a557e1">Guinea</option><option value="Hungary" id="5f80e1973884e632f4a557e8">Hungary</option><option value="Hong Kong S.A.R." id="5f80e1973884e632f4a557e7">Hong Kong S.A.R.</option><option value="India" id="5f80e1973884e632f4a557ea">India</option><option value="Honduras" id="5f80e1973884e632f4a557e6">Honduras</option><option value="Iceland" id="5f80e1973884e632f4a557e9">Iceland</option><option value="Iran" id="5f80e1973884e632f4a557ec">Iran</option><option value="Indonesia" id="5f80e1973884e632f4a557eb">Indonesia</option><option value="Ireland" id="5f80e1973884e632f4a557ee">Ireland</option><option value="Israel" id="5f80e1973884e632f4a557ef">Israel</option><option value="Iraq" id="5f80e1973884e632f4a557ed">Iraq</option><option value="Japan" id="5f80e1973884e632f4a557f2">Japan</option><option value="Jordan" id="5f80e1973884e632f4a557f4">Jordan</option><option value="Italy" id="5f80e1973884e632f4a557f0">Italy</option><option value="Jamaica" id="5f80e1973884e632f4a557f1">Jamaica</option><option value="Jersey" id="5f80e1973884e632f4a557f3">Jersey</option><option value="Kazakhstan" id="5f80e1973884e632f4a557f5">Kazakhstan</option><option value="Korea North" id="5f80e1973884e632f4a557f8">Korea North</option><option value="Korea South" id="5f80e1973884e632f4a557f9">Korea South</option><option value="Kiribati" id="5f80e1973884e632f4a557f7">Kiribati</option><option value="Kyrgyzstan" id="5f80e1973884e632f4a557fc">Kyrgyzstan</option><option value="Kosovo" id="5f80e1973884e632f4a557fa">Kosovo</option><option value="Laos" id="5f80e1973884e632f4a557fd">Laos</option><option value="Kuwait" id="5f80e1973884e632f4a557fb">Kuwait</option><option value="Kenya" id="5f80e1973884e632f4a557f6">Kenya</option><option value="Latvia" id="5f80e1973884e632f4a557fe">Latvia</option><option value="Lesotho" id="5f80e1973884e632f4a55800">Lesotho</option><option value="Liechtenstein" id="5f80e1973884e632f4a55803">Liechtenstein</option><option value="Libya" id="5f80e1973884e632f4a55802">Libya</option><option value="Lebanon" id="5f80e1973884e632f4a557ff">Lebanon</option><option value="Liberia" id="5f80e1973884e632f4a55801">Liberia</option><option value="Luxembourg" id="5f80e1973884e632f4a55805">Luxembourg</option><option value="Lithuania" id="5f80e1973884e632f4a55804">Lithuania</option><option value="Macedonia" id="5f80e1973884e632f4a55807">Macedonia</option><option value="Macau S.A.R." id="5f80e1973884e632f4a55806">Macau S.A.R.</option><option value="Madagascar" id="5f80e1973884e632f4a55808">Madagascar</option><option value="Mauritania" id="5f80e1973884e632f4a55811">Mauritania</option><option value="Mali" id="5f80e1973884e632f4a5580c">Mali</option><option value="Man (Isle of)" id="5f80e1973884e632f4a5580e">Man (Isle of)</option><option value="Malawi" id="5f80e1973884e632f4a55809">Malawi</option><option value="Malta" id="5f80e1973884e632f4a5580d">Malta</option><option value="Maldives" id="5f80e1973884e632f4a5580b">Maldives</option><option value="Malaysia" id="5f80e1973884e632f4a5580a">Malaysia</option><option value="Martinique" id="5f80e1973884e632f4a55810">Martinique</option><option value="Marshall Islands" id="5f80e1973884e632f4a5580f">Marshall Islands</option><option value="Mexico" id="5f80e1973884e632f4a55814">Mexico</option><option value="Micronesia" id="5f80e1973884e632f4a55815">Micronesia</option><option value="Moldova" id="5f80e1973884e632f4a55816">Moldova</option><option value="Mauritius" id="5f80e1973884e632f4a55812">Mauritius</option><option value="Mayotte" id="5f80e1973884e632f4a55813">Mayotte</option><option value="Montenegro" id="5f80e1973884e632f4a55819">Montenegro</option><option value="Montserrat" id="5f80e1973884e632f4a5581a">Montserrat</option><option value="Monaco" id="5f80e1973884e632f4a55817">Monaco</option><option value="Mongolia" id="5f80e1973884e632f4a55818">Mongolia</option><option value="Myanmar" id="5f80e1973884e632f4a5581d">Myanmar</option><option value="Namibia" id="5f80e1973884e632f4a5581e">Namibia</option><option value="Mozambique" id="5f80e1973884e632f4a5581c">Mozambique</option><option value="Morocco" id="5f80e1973884e632f4a5581b">Morocco</option><option value="Nauru" id="5f80e1973884e632f4a5581f">Nauru</option><option value="Nepal" id="5f80e1973884e632f4a55820">Nepal</option><option value="Netherlands Antilles" id="5f80e1973884e632f4a55821">Netherlands Antilles</option><option value="New Caledonia" id="5f80e1973884e632f4a55823">New Caledonia</option><option value="Nicaragua" id="5f80e1973884e632f4a55825">Nicaragua</option><option value="New Zealand" id="5f80e1973884e632f4a55824">New Zealand</option><option value="Netherlands The" id="5f80e1973884e632f4a55822">Netherlands The</option><option value="Niger" id="5f80e1973884e632f4a55826">Niger</option><option value="Nigeria" id="5f80e1973884e632f4a55827">Nigeria</option><option value="Niue" id="5f80e1973884e632f4a55828">Niue</option><option value="Northern Mariana Islands" id="5f80e1973884e632f4a5582a">Northern Mariana Islands</option><option value="Norfolk Island" id="5f80e1973884e632f4a55829">Norfolk Island</option><option value="Oman" id="5f80e1973884e632f4a5582c">Oman</option><option value="Norway" id="5f80e1973884e632f4a5582b">Norway</option><option value="Pakistan" id="5f80e1973884e632f4a5582d">Pakistan</option><option value="Panama" id="5f80e1973884e632f4a55830">Panama</option><option value="Paraguay" id="5f80e1973884e632f4a55832">Paraguay</option><option value="Romania" id="5f80e1973884e632f4a5583b">Romania</option><option value="Papua new Guinea" id="5f80e1973884e632f4a55831">Papua new Guinea</option><option value="Poland" id="5f80e1973884e632f4a55836">Poland</option><option value="Peru" id="5f80e1973884e632f4a55833">Peru</option><option value="Palau" id="5f80e1973884e632f4a5582e">Palau</option><option value="Pitcairn Island" id="5f80e1973884e632f4a55835">Pitcairn Island</option><option value="Puerto Rico" id="5f80e1973884e632f4a55838">Puerto Rico</option><option value="Qatar" id="5f80e1973884e632f4a55839">Qatar</option><option value="Palestinian Territory Occupied" id="5f80e1973884e632f4a5582f">Palestinian Territory Occupied</option><option value="Philippines" id="5f80e1973884e632f4a55834">Philippines</option><option value="Portugal" id="5f80e1973884e632f4a55837">Portugal</option><option value="Reunion" id="5f80e1973884e632f4a5583a">Reunion</option><option value="Russia" id="5f80e1973884e632f4a5583c">Russia</option><option value="Saint Helena" id="5f80e1973884e632f4a5583e">Saint Helena</option><option value="Rwanda" id="5f80e1973884e632f4a5583d">Rwanda</option><option value="Saint Kitts And Nevis" id="5f80e1973884e632f4a5583f">Saint Kitts And Nevis</option><option value="Saint Lucia" id="5f80e1973884e632f4a55840">Saint Lucia</option><option value="Saint Pierre and Miquelon" id="5f80e1973884e632f4a55841">Saint Pierre and Miquelon</option><option value="Saint Vincent And The Grenadines" id="5f80e1973884e632f4a55842">Saint Vincent And The Grenadines</option><option value="Sao Tome and Principe" id="5f80e1973884e632f4a55847">Sao Tome and Principe</option><option value="Samoa" id="5f80e1973884e632f4a55845">Samoa</option><option value="Saint-Barthelemy" id="5f80e1973884e632f4a55843">Saint-Barthelemy</option><option value="San Marino" id="5f80e1973884e632f4a55846">San Marino</option><option value="Saint-Martin (French part)" id="5f80e1973884e632f4a55844">Saint-Martin (French part)</option><option value="Saudi Arabia" id="5f80e1973884e632f4a55848">Saudi Arabia</option><option value="Serbia" id="5f80e1973884e632f4a5584a">Serbia</option><option value="Senegal" id="5f80e1973884e632f4a55849">Senegal</option><option value="Sierra Leone" id="5f80e1973884e632f4a5584c">Sierra Leone</option><option value="Singapore" id="5f80e1973884e632f4a5584d">Singapore</option><option value="Slovakia" id="5f80e1973884e632f4a5584e">Slovakia</option><option value="Seychelles" id="5f80e1973884e632f4a5584b">Seychelles</option><option value="South Africa" id="5f80e1973884e632f4a55852">South Africa</option><option value="Somalia" id="5f80e1973884e632f4a55851">Somalia</option><option value="Solomon Islands" id="5f80e1973884e632f4a55850">Solomon Islands</option><option value="Slovenia" id="5f80e1973884e632f4a5584f">Slovenia</option><option value="South Georgia" id="5f80e1973884e632f4a55853">South Georgia</option><option value="Svalbard And Jan Mayen Islands" id="5f80e1973884e632f4a55859">Svalbard And Jan Mayen Islands</option><option value="Sweden" id="5f80e1973884e632f4a5585b">Sweden</option><option value="Swaziland" id="5f80e1973884e632f4a5585a">Swaziland</option><option value="Taiwan" id="5f80e1973884e632f4a5585e">Taiwan</option><option value="Thailand" id="5f80e1973884e632f4a55861">Thailand</option><option value="Tajikistan" id="5f80e1973884e632f4a5585f">Tajikistan</option><option value="Suriname" id="5f80e1973884e632f4a55858">Suriname</option><option value="Switzerland" id="5f80e1973884e632f4a5585c">Switzerland</option><option value="Tanzania" id="5f80e1973884e632f4a55860">Tanzania</option><option value="Spain" id="5f80e1973884e632f4a55855">Spain</option><option value="Sri Lanka" id="5f80e1973884e632f4a55856">Sri Lanka</option><option value="South Sudan" id="5f80e1973884e632f4a55854">South Sudan</option><option value="Syria" id="5f80e1973884e632f4a5585d">Syria</option><option value="Sudan" id="5f80e1973884e632f4a55857">Sudan</option><option value="Trinidad And Tobago" id="5f80e1973884e632f4a55865">Trinidad And Tobago</option><option value="Tokelau" id="5f80e1973884e632f4a55863">Tokelau</option><option value="Togo" id="5f80e1973884e632f4a55862">Togo</option><option value="Tonga" id="5f80e1973884e632f4a55864">Tonga</option><option value="Tunisia" id="5f80e1973884e632f4a55866">Tunisia</option><option value="Turkey" id="5f80e1973884e632f4a55867">Turkey</option><option value="Ukraine" id="5f80e1973884e632f4a5586c">Ukraine</option><option value="Uganda" id="5f80e1973884e632f4a5586b">Uganda</option><option value="Turkmenistan" id="5f80e1973884e632f4a55868">Turkmenistan</option><option value="Tuvalu" id="5f80e1973884e632f4a5586a">Tuvalu</option><option value="Turks And Caicos Islands" id="5f80e1973884e632f4a55869">Turks And Caicos Islands</option><option value="United Arab Emirates" id="5f80e1973884e632f4a5586d">United Arab Emirates</option><option value="United Kingdom" id="5f80e1973884e632f4a5586e">United Kingdom</option><option value="Vatican City State (Holy See)" id="5f80e1973884e632f4a55873">Vatican City State (Holy See)</option><option value="United States Minor Outlying Islands" id="5f80e1973884e632f4a5586f">United States Minor Outlying Islands</option><option value="Uzbekistan" id="5f80e1973884e632f4a55871">Uzbekistan</option><option value="Vanuatu" id="5f80e1973884e632f4a55872">Vanuatu</option><option value="Uruguay" id="5f80e1973884e632f4a55870">Uruguay</option><option value="Virgin Islands (US)" id="5f80e1973884e632f4a55877">Virgin Islands (US)</option><option value="Virgin Islands (British)" id="5f80e1973884e632f4a55876">Virgin Islands (British)</option><option value="Vietnam" id="5f80e1973884e632f4a55875">Vietnam</option><option value="Venezuela" id="5f80e1973884e632f4a55874">Venezuela</option><option value="Zambia" id="5f80e1973884e632f4a5587b">Zambia</option><option value="Wallis And Futuna Islands" id="5f80e1973884e632f4a55878">Wallis And Futuna Islands</option><option value="Yemen" id="5f80e1973884e632f4a5587a">Yemen</option><option value="Western Sahara" id="5f80e1973884e632f4a55879">Western Sahara</option><option value="Zimbabwe" id="5f80e1973884e632f4a5587c">Zimbabwe</option></select>
                                    
                                        <div class="wizard-form-error"></div>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <select name="province" id="province" class="selectpicker form-control wizard-required" required="required" data-toggle="tooltip" title="" data-original-title="States/Province is required"><option value="">States/Province*</option><option value="Abu Dhabi Emirate">Abu Dhabi Emirate</option><option value="Ajman Emirate">Ajman Emirate</option><option value="Dubai">Dubai</option><option value="Fujairah">Fujairah</option><option value="Ras al-Khaimah">Ras al-Khaimah</option><option value="Sharjah Emirate">Sharjah Emirate</option><option value="Umm al-Quwain">Umm al-Quwain</option></select>
                                        <div class="wizard-form-error"></div>
                                    </div>
                                    <div class="form-group col-md-4">
                                        <input type="text" class="form-control wizard-required" id="city">
                                        <label for="city" class="wizard-form-text-label">City*</label>
                                        <div class="wizard-form-error"></div>
                                    </div>
  
                                    <div class="form-group col-md-4">
                                      <input type="text" class="form-control wizard-required" id="aptsuiteno">
                                      <label for="aptsuiteno" class="wizard-form-text-label">Apt/Suite No</label>
                                      <div class="wizard-form-error"></div>
                                    </div>
                                    <div class="form-group col-md-4">
                                      <input type="number" class="form-control wizard-required" id="emp_phnumber">
                                      <label for="emp_phnumber" class="wizard-form-text-label">Business Phone Number*</label>
                                      <div class="wizard-form-error"></div>
                                    </div>
                                    <div class="form-group col-md-3">
                                      <input type="number" class="form-control wizard-required" id="fax">
                                      <label for="fax" class="wizard-form-text-label">Fax</label>
                                      <div class="wizard-form-error"></div>
                                    </div>
  
  
                                    </div>
  
                                  </div>
                                  <div id="menu2" class="tab-pane fade">
                                    
                                  </div>
                                </div>
                                <div class="form-group clearfix">
                                  <a href="javascript:;" class="form-wizard-previous-btn float-left">Previous</a>
                                  <a href="javascript:;" class="form-wizard-next-btn float-right">Next</a>
                                </div>
                            </fieldset>
                            <fieldset class="wizard-fieldset">
                                <form method="post" class="stepbox-3" style="display: block;">
                                  <h2 class="fs-title">ID Information</h2>
  
                                  <div class="maincontent">
                                      <!-- <h4>CUSTOMER AGREEMENT</h4> -->
                                      <h4>IMPORTANT INFORMATION ABOUT PROCEDURES FOR OPENING A NEW ACCOUNT USA PATRIOT ACT
                                          INFORMATION
                                      </h4>
                                      <p>Important information. To help the government fight the funding of terrorism and
                                          money‐laundering
                                          activities, Federal law requires that Velocity Clearing LLC ("Velocity") verify your
                                          identity by
                                          obtaining your name, date of birth, address, and a government‐issued identification
                                          number
                                          before opening your account. In certain circumstances, Velocity may obtain and
                                          verify this
                                          information with respect to any person(s) authorized to effect transactions in an
                                          account. For
                                          certain entities, such as trusts, estates, corporations, partnerships or other
                                          organizations,
                                          identifying documentation is also required. Your account may be restricted and/or
                                          closed if
                                          Velocity cannot verify this information. Velocity will not be responsible for any
                                          losses or
                                          damages (including, but not limited to, lost opportunities) resulting from any
                                          failure to
                                          provide this information or from any restriction placed upon, or closing of your
                                          account.</p>
                                      <div class="personal-formm">
                                          <div class="row">
                                              <div class="form-group">
                                                  <div class="form-inline">
                                                      <div class="col-md-12">
  
                                                          <div class="row">
                                                              <div class="col-sm-3">
                                                                  <label>Tax ID (SS# / EIN)<span class="text-danger">
                                                                          *</span></label>
                                                                  <input type="text" name="three_tax_id" value="{{@Auth::user()->idInformation->tax_id}}" id="three_tax_id" class="form-control" data-toggle="tooltip" data-placement="top">
                                                              </div>
  
                                                              <div class="col-sm-3">
                                                                  <input type="hidden" name="id" value="607d551b77135856ffbfe26e">
                                                                  <label>Date of Birth (MM\DD\YYYY) <span class="text-danger">
                                                                          *</span></label>
  
                                                                  <input type="text" name="three_date_of_birth" value="{{@Auth::user()->idInformation->date_of_birth}}" id="three_date_of_birth" class="form-control" data-toggle="tooltip" data-placement="top">
  
                                                              </div>
  
                                                              <div class="col-sm-3">
                                                                  <label>Country of Tax Residence 
                                                                    <span class="text-danger">*</span>
                                                                  </label>
                                                                  <select name="three_country" id="three_country" class="form-control" data-toggle="tooltip" data-placement="top">
                                                                    <option value>Select Country</option>
                                                                    @foreach ($countries as $item)
                                                                    <option value="{{$item->id}}" {{@Auth::user()->idInformation->tax_country_id == $item->id?'selected':''}}>{{$item->name}}</option>
                                                                    @endforeach
                                                                  </select>
                                                              </div>
  
                                                              <div class="col-md-3">
                                                                  <label for="taxResidanceState">State 
                                                                    <span class="text-danger">*</span>
                                                                  </label>
                                                                  <select name="three_state" id="three_state" class="form-control" data-toggle="tooltip" data-placement="top">
                                                                    <option value>Select States</option>
                                                                    @isset(Auth::user()->idInformation)
                                                                        @php
                                                                            $states =\App\Models\State::where('country_id',@Auth::user()->idInformation->tax_country_id)->get();
                                                                        @endphp
                                                                        @foreach ($states as $item)
                                                                        <option value="{{$item->id}}" {{@Auth::user()->idInformation->tax_state_id == $item->id?'selected':''}}>{{$item->name}}</option>
                                                                        @endforeach
                                                                    @endisset
                                                                  </select>
                                                              </div>
                                                          </div>
                                                          <br>
                                                          <div class="row">
                                                              <h4> Valid Government Issued Photo ID </h4>
                                                          </div>
                                                          <div class="row">
                                                              <div class="col-sm-3">
                                                                  <label>ID Type <span class="text-danger"> *</span></label>
                                                                  <select name="three_id_type" id="three_id_type" class="form-control" data-toggle="tooltip" data-placement="top">
                                                                    <option value="Driver License" {{@Auth::user()->idInformation->id_type == "Driver License"?'selected':''}}>Driver License</option>
                                                                    <option value="Passport" {{@Auth::user()->idInformation->id_type == "Passport"?'selected':''}}>Passport</option>
                                                                    <option value="Local ID" {{@Auth::user()->idInformation->id_type == "Local ID"?'selected':''}}>Local ID</option>
                                                                  </select>
                                                              </div>
  
                                                              <div class="col-sm-3">
                                                                  <div id="select_change_option">
                                                                      <label for="nameOfId" class="nameOfId">Name of ID
                                                                      </label>
                                                                      <input type="text" name="three_name_of_id" value="{{@Auth::user()->idInformation->name_of_id}}" id="three_name_of_id" class="form-control" data-toggle="tooltip" data-placement="top">
                                                                  </div>
                                                              </div>
  
                                                              <div class="col-sm-3">
                                                                  <label>ID Number <span class="text-danger"> *</span></label>
                                                                  <input type="text" name="three_id_number" value="{{@Auth::user()->idInformation->id_number}}" id="three_id_number" class="form-control" data-toggle="tooltip" data-placement="top">
                                                              </div>
  
  
                                                              <div class="col-sm-3">
                                                                  <label>Country of Issuance <span class="text-danger">
                                                                          *</span></label>
                                                                  <select name="three_country_residence" id="three_country_residence" class="form-control" data-toggle="tooltip" data-placement="top">
                                                                    <option value>Select Country</option>
                                                                    @foreach ($countries as $item)
                                                                    <option value="{{$item->id}}" {{@Auth::user()->idInformation->identity_country_id == $item->id?'selected':''}}>{{$item->name}}</option>
                                                                    @endforeach
                                                                  </select>
                                                              </div>
                                                              <div class="col-md-3">
                                                                  <label for="residenceState">State <span class="text-danger">
                                                                          *</span></label>
                                                                  <select name="three_state_residence" id="three_state_residence" class="form-control" data-toggle="tooltip" data-placement="top">
                                                                    <option value>Select States</option>
                                                                    @isset(Auth::user()->idInformation)
                                                                        @php
                                                                            $states =\App\Models\State::where('country_id',@Auth::user()->idInformation->identity_country_id)->get();
                                                                        @endphp
                                                                        @foreach ($states as $item)
                                                                        <option value="{{$item->id}}" {{@Auth::user()->idInformation->identity_state_id == $item->id?'selected':''}}>{{$item->name}}</option>
                                                                        @endforeach
                                                                    @endisset
                                                                  </select>
                                                              </div>
                                                              <div class="col-sm-3">
                                                                  <label>Issue Date (MM\DD\YYYY) <span class="text-danger">
                                                                          *</span></label>
                                                                  <input type="text" name="three_issue_date" value="{{@Auth::user()->idInformation->issue_date}}" id="three_issue_date" class="form-control" data-toggle="tooltip" data-placement="top">
                                                              </div>
                                                              <div class="col-sm-3">
                                                                  <label>Expiration (MM\DD\YYYY) <span class="text-danger">
                                                                          *</span></label>
                                                                  <input type="text" name="three_expiration" value="{{@Auth::user()->idInformation->expiration_date}}" id="three_expiration" class="form-control" data-toggle="tooltip" data-placement="top">
                                                              </div>
                                                              
                                                          </div>
  
  
  
                                                      </div>
                                                  </div>
                                              </div>
                                          </div>
                                      </div>
  
  
  
  
                                  </div>
                                
                                  <div class="form-group clearfix">
                                    <a href="javascript:;" class="form-wizard-previous-btn float-left ">Previous</a>
                                    <a href="javascript:;" class="form-wizard-next-btn float-left " id="btnStepThree">Next
                                        <span class="loader" style="display: none">
                                            <div class="spinner-border text-light" role="status" style="width: 1rem;height: 1rem;">
                                                <span class="sr-only">Loading...</span>
                                            </div>
                                        </span>
                                    </a>

                                  </div>
                                </form>
                              </fieldset>
                              <fieldset class="wizard-fieldset">
                                <form method="post" class="stepbox-4" style="display: block;">
                                  <input type="hidden" name="id" value="607d551b77135856ffbfe26e">
                                  <h2 class="fs-title">Income Details</h2>
                                  <div class="maincontent">
                                      <p>Financial situation and needs, Liquidity Considerations and Tax Status.</p>
                                      <div class="property-form anual_incomemainclm">
                                          <div class="row">
                                              <div class="form-group">
                                                  <div class="form-inline" style="align-items: flex-start;">
  
                                                      <div class="col-sm-12 text-left">
                                                          <p><b>Annual Income</b> includes income form sources such as
                                                              employment,
                                                              alimony, social security, investment, income etc.</p>
                                                      </div>
                                                      <div class="col-sm-12 income-error"></div>
  
                                                      <div class="col-sm-6 col-md-4 annualincomeclm text-left">
                                                          <h4>Annual Income</h4>
  
                                                          <div class="label-icon ">
                                                              <label class="radioholder">
  
                                                                  <input type="radio" name="annualIncome" id="annualIncome" value="$25,000 and under" data-value="25000"><i class="fa fa-usd" aria-hidden="true"></i> $25,000 and
                                                                  under
                                                                  <span class="radiomark"></span>
                                                              </label>
  
                                                          </div>
  
                                                          <div class="label-icon radiocurrent">
                                                              <label class="radioholder">
                                                                  <input type="radio" name="annualIncome" id="annualIncome" data-value="25001" value="$25,001 - $50,000" checked=""><i class="fa fa-usd" aria-hidden="true"></i> $25,001 -
                                                                  $50,000
                                                                  <span class="radiomark"></span>
                                                              </label>
                                                          </div>
                                                          <div class="label-icon ">
                                                              <label class="radioholder">
                                                                  <input type="radio" name="annualIncome" id="annualIncome" value="$50,001 - $100,000" data-value="50001"><i class="fa fa-usd" aria-hidden="true"></i> $50,001 -
                                                                  $100,000
                                                                  <span class="radiomark"></span>
                                                              </label>
                                                          </div>
  
                                                          <div class="label-icon ">
                                                              <label class="radioholder">
                                                                  <input type="radio" name="annualIncome" id="annualIncome" value="$100,001 - $250000" data-value="100001"><i class="fa fa-usd" aria-hidden="true"></i> $100,001 -
                                                                  $250000
                                                                  <span class="radiomark"></span>
                                                              </label>
                                                          </div>
                                                          <div class="label-icon ">
                                                              <label class="radioholder">
                                                                  <input type="radio" name="annualIncome" id="annualIncome" value="$250,001 ‐ $500,000" data-value="250001"><i class="fa fa-usd" aria-hidden="true"></i> $250,001 ‐
                                                                  $500,000
                                                                  <span class="radiomark"></span>
                                                              </label>
                                                          </div>
                                                          <div class="label-icon ">
                                                              <label class="radioholder">
                                                                  <input type="radio" name="annualIncome" id="annualIncome" value="over $500,000" class="validate" required="required" data-value="500000">
                                                                  <i class="fa fa-usd" aria-hidden="true"></i>over $500,000
                                                                  <span class="radiomark"></span>
                                                              </label>
                                                          </div>
                                                          <div class="annualIncome-error"></div>
                                                      </div>
  
                                                      <div class="col-sm-6 col-md-4 annualincomeclm text-left">
                                                          <h4>Net Worth (Excluding Residence)</h4>
                                                          <div class="label-icon ">
                                                              <label class="radioholder">
                                                                  <input type="radio" name="netWorth" id="netWorth" value="$25,000 and Under" data-value="25000"><i class="fa fa-usd" aria-hidden="true"></i> $25,000 and
                                                                  Under
                                                                  <span class="radiomark"></span>
                                                              </label>
                                                          </div>
                                                          <div class="label-icon radiocurrent">
                                                              <label class="radioholder">
                                                                  <input type="radio" name="netWorth" id="netWorth" value="$25,001 - $50,000" data-value="25001" checked="">
                                                                  <i class="fa fa-usd" aria-hidden="true"></i>$25,001 -
                                                                  $50,000
                                                                  <span class="radiomark"></span>
                                                              </label>
                                                          </div>
                                                          <div class="label-icon ">
                                                              <label class="radioholder">
                                                                  <input type="radio" name="netWorth" id="netWorth" value="$50,001 - $200,000" data-value="50001"><i class="fa fa-usd" aria-hidden="true"></i> $50,001 -
                                                                  $200,000
                                                                  <span class="radiomark"></span>
                                                              </label>
                                                          </div>
                                                          <div class="label-icon ">
                                                              <label class="radioholder">
                                                                  <input type="radio" name="netWorth" id="netWorth" value="$200,001 - $500,000" data-value="200001"><i class="fa fa-usd" aria-hidden="true"></i> $200,001 -
                                                                  $500,000
                                                                  <span class="radiomark"></span>
                                                              </label>
                                                          </div>
                                                          <div class="label-icon ">
                                                              <label class="radioholder">
                                                                  <input type="radio" name="netWorth" id="netWorth" value="$500,001 - $1,000,000" data-value="500001"><i class="fa fa-usd" aria-hidden="true"></i> $500,001 -
                                                                  $1,000,000
                                                                  <span class="radiomark"></span>
                                                              </label>
                                                          </div>
                                                          <div class="label-icon ">
                                                              <label class="radioholder">
                                                                  <input type="radio" name="netWorth" id="netWorth" value="over $1,000,000" data-value="1000000"><i class="fa fa-usd" aria-hidden="true"></i> over
                                                                  $1,000,000
                                                                  <span class="radiomark"></span>
                                                              </label>
                                                          </div>
                                                          <div class="netWorth-error"></div>
  
                                                      </div>
  
                                                      <div class="col-sm-6 col-md-4 text-left annualincomeclm">
                                                          <h4>Liquid Net Worth (Must be less than Net Worth)</h4>
  
                                                          <div class="label-icon ">
                                                              <label class="radioholder">
                                                                  <input type="radio" name="liquidNetWorth" id="liquidNetWorth" value="$25,000 and Under" data-value="25000"><i class="fa fa-usd" aria-hidden="true"></i> $25,000 and
                                                                  Under
                                                                  <span class="radiomark"></span>
                                                              </label>
                                                          </div>
                                                          <div class="label-icon radiocurrent">
                                                              <label class="radioholder">
                                                                  <input type="radio" name="liquidNetWorth" id="liquidNetWorth" value="$25,001 - $50,000" data-value="25001" checked=""><i class="fa fa-usd" aria-hidden="true"></i> $25,001 -
                                                                  $50,000
                                                                  <span class="radiomark"></span>
                                                              </label>
                                                          </div>
                                                          <div class="label-icon ">
                                                              <label class="radioholder">
                                                                  <input type="radio" name="liquidNetWorth" id="liquidNetWorth" value="$50,001 - $200,000" data-value="50001"><i class="fa fa-usd" aria-hidden="true"></i> $50,001 -
                                                                  $200,000
                                                                  <span class="radiomark"></span>
                                                              </label>
                                                          </div>
                                                          <div class="label-icon ">
                                                              <label class="radioholder">
                                                                  <input type="radio" name="liquidNetWorth" id="liquidNetWorth" value="$200,001 - $500,000" data-value="200001"><i class="fa fa-usd" aria-hidden="true"></i> $200,001 -
                                                                  $500,000
                                                                  <span class="radiomark"></span>
                                                              </label>
                                                          </div>
                                                          <div class="label-icon ">
                                                              <label class="radioholder">
                                                                  <input type="radio" name="liquidNetWorth" id="liquidNetWorth" value="$500,001 - $1,000,000" data-value="500001">
                                                                  <i class="fa fa-usd" aria-hidden="true"></i>$500,001 -
                                                                  $1,000,000
                                                                  <span class="radiomark"></span>
                                                              </label>
                                                          </div>
                                                          <div class="label-icon ">
                                                              <label class="radioholder">
                                                                  <input type="radio" name="liquidNetWorth" id="liquidNetWorth" value="over $1,000,000" data-value="1000000"><i class="fa fa-usd" aria-hidden="true"></i> over
                                                                  $1,000,000
                                                                  <span class="radiomark"></span>
                                                              </label>
                                                          </div>
                                                          <div class="liquidNetWorth-error"></div>
                                                      </div>
  
                                                      <div class="col-sm-6 col-md-4 text-left annualincomeclm">
                                                          <h4>TAX RATE (Highest Marginal)</h4>
                                                          <div class="label-icon ">
                                                              <label class="radioholder">
                                                                  <input type="radio" name="taxRate" id="taxRate" value="0-5"><i class="fa fa-usd" aria-hidden="true"></i> 0 - 5
                                                                  <span class="radiomark"></span>
                                                              </label>
                                                          </div>
                                                          <div class="label-icon radiocurrent">
                                                              <label class="radioholder">
                                                                  <input type="radio" name="taxRate" id="taxRate" value="16-25" checked=""><i class="fa fa-usd" aria-hidden="true"></i> 16 - 25
                                                                  <span class="radiomark"></span>
                                                              </label>
                                                          </div>
                                                          <div class="label-icon ">
                                                              <label class="radioholder">
                                                                  <input type="radio" name="taxRate" id="taxRate" value="26-30"><i class="fa fa-usd" aria-hidden="true"></i> 26 - 30
                                                                  <span class="radiomark"></span>
                                                              </label>
                                                          </div>
                                                          <div class="label-icon ">
                                                              <label class="radioholder">
                                                                  <input type="radio" name="taxRate" id="taxRate" value="31-35"><i class="fa fa-usd" aria-hidden="true"></i> 31 - 35
                                                                  <span class="radiomark"></span>
                                                              </label>
                                                          </div>
                                                          <div class="label-icon ">
                                                              <label class="radioholder">
                                                                  <input type="radio" name="taxRate" id="taxRate" value="over 35"><i class="fa fa-usd" aria-hidden="true"></i> over 35
                                                                  <span class="radiomark"></span>
                                                              </label>
                                                          </div>
                                                          <div class="taxRate-error"></div>
                                                      </div>
  
  
                                                  </div>
  
  
  
                                              </div>
                                          </div>
                                      </div>
  
                                  </div>
  
                                  <!-- <input type="button" name="previous" class="previous action-button-previous" value="Previous">
                                  <input type="button" name="next" id="stepbox4-btn" class="next action-button" value="Next" onclick="stepBox4()"> -->
                                  <div class="form-group clearfix">
                                    <a href="javascript:;" class="form-wizard-previous-btn float-left">Previous</a>
                                    <a href="javascript:;" class="form-wizard-next-btn float-left">Next</a>
                                  </div>
                              </form>
                              </fieldset>
                              <fieldset class="wizard-fieldset">
                                <form method="post" class="stepbox-5 was-validated" style="display: block;" novalidate="novalidate">
                                  <h2 class="fs-title">Funding Details</h2>
  
                                  <div class="maincontent">
                                      <h4 style="border-bottom: 1px solid #E5E5E5; padding-bottom: 20px;">I am funding this
                                          account with
                                          (check all that apply)</h4>
                                      <div class="property-form">
  
                                          <div class="row">
                                              <div class="col-sm-6 text-left fundingclm">
  
                                                  <div class="custom-control custom-checkbox">
                                                      <label for="incomeAccount" class="labelholder">
                                                          <input type="checkbox" name="incomeAccount" id="incomeAccount" value="1">Income
                                                          <span class="checkmark"></span>
                                                          <input type="hidden" name="id" value="607d551b77135856ffbfe26e">
                                                      </label>
  
                                                  </div>
                                                  <div class="custom-control custom-checkbox">
  
  
                                                      <label for="retireSaving" class="labelholder">
                                                          <input type="checkbox" name="retireSaving" id="retireSaving" value="1">Pension or
                                                          retirement savings
                                                          <span class="checkmark"></span>
                                                      </label>
                                                  </div>
                                                  <div class="custom-control custom-checkbox">
                                                      <label for="gift" class="labelholder">
                                                          <input type="checkbox" name="gift" id="gift" value="1" required="" aria-required="true">Gift
                                                          <span class="checkmark"></span>
                                                      </label>
                                                  </div>
                                                  <div class="custom-control custom-checkbox">
                                                      <label for="businessProp" class="labelholder">
                                                          <input type="checkbox" name="businessProp" id="businessProp" value="1">Sale of
                                                          business or property
                                                          <span class="checkmark"></span>
                                                      </label>
                                                  </div>
  
                                              </div>
                                              <div class="col-sm-6 text-left fundingclm">
  
                                                  <div class="custom-control custom-checkbox">
                                                      <label for="inheritence" class="labelholder">
                                                          <input type="checkbox" name="inheritence" id="inheritence" value="1">Inheritance
                                                          <span class="checkmark"></span>
                                                      </label>
  
                                                  </div>
                                                  <div class="custom-control custom-checkbox">
                                                      <label for="secuBnefit" class="labelholder">
                                                          <input type="checkbox" name="secuBnefit" id="secuBnefit" value="1">Social Security benefit
                                                          <span class="checkmark"></span>
                                                      </label>
                                                  </div>
  
                                                  <div class="custom-control custom-checkbox">
                                                      <label for="otherCheckbox" class="labelholder">
                                                          <input type="checkbox" name="otherCheckbox" id="otherCheckbox" value="1">Other
                                                          <span class="checkmark"></span>
                                                      </label>
                                                      <input type="text" name="otherInput" id="otherInput" class="form-control validate valid" style="margin-top: 10px;" value="" disabled="" aria-invalid="false">
                                                  </div>
  
  
  
                                              </div>
  
                                          </div>
                                          <div class="row">
                                              <div class="col-sm-12">
                                                  <h5 style="text-align: left;">Funding Details </h5>
                                              </div>
                                          </div>
                                          <div class="row">
  
                                              <div class="col-sm-6 text-left">
                                                  <div class="form-group">
                                                      <label>Name of the bank you will be funding your account from <span class="text-danger"> *</span></label>
                                                      <input type="text" name="bankFunding" id="bankFunding" class="form-control validate valid" pattern="^[A-Za-z -]+$" required="required" value="gdsf" data-toggle="tooltip" title="" onkeypress="return /[A-Za-z -]/i.test(event.key)" data-original-title="Bank is required" aria-required="true" aria-invalid="false">
                                                  </div>
                                              </div>
  
                                              <div class="col-sm-6">
                                                  <div class="form-group">
                                                      <label>ABA / SWIFT <span class="text-danger"> *</span></label>
                                                      <input type="password" id="abaSwift" name="abaSwift" class="form-control validate valid" required="required" value="Waqar123" data-toggle="tooltip" title="" data-original-title="ABA / SWIFT is required" aria-required="true" aria-invalid="false">
                                                  </div>
                                              </div>
                                          </div>
                                          <div class="row">
                                              <div class="col-sm-12">
                                                  <div class="form-group">
                                                      <label>Account Number <span class="text-danger"> *</span></label>
                                                      <input type="password" id="accountNumber" name="accountNumber" class="form-control validate valid" required="required" onkeyup="if(this.value.length==20) console.log(this.value.length); return false;" value="dsfsad" data-toggle="tooltip" title="" data-original-title="Account Number is required" aria-required="true" aria-invalid="false">
                                                  </div>
                                              </div>
  
                                          </div>
  
                                      </div>
                                  </div>
  
                                  <!-- <input type="button" name="previous" class="previous action-button-previous" value="Previous">
                                  <input type="button" name="next" id="stepbox5-btn" class="next action-button" value="Next" onclick="stepBox5()"> -->
                                  <div class="form-group clearfix">
                                    <a href="javascript:;" class="form-wizard-previous-btn float-left">Previous</a>
                                    <a href="javascript:;" class="form-wizard-next-btn float-left">Next</a>
                                  </div>
                                </form>
                              </fieldset>
                              <fieldset class="wizard-fieldset">
                                <form method="post" class="stepbox-6 was-validated" novalidate="novalidate" style="display: block;">
                                  <h2 class="fs-title">Risk Acceptance</h2>
  
                                  <div class="maincontent">
                                      <div class="property-form">
                                          <div class="row">
                                              <div class="form-inline">
                                                  <div class="col-sm-12 text-left">
                                                      <div class="questionrow">
                                                          <p>Please answer the following questions about your investment
                                                              objectives.</p>
                                                          <div class="radio-custom">
                                                              <label class="radioholder" style="color: #fff; margin: 0;">
                                                                  <input type="radio" name="accountRisk" value="Active or Day Trading" checked="">
                                                                  <span class="tname" checked="checked">Active or Day
                                                                      Trading</span>
                                                                  <span class="radiomark"></span>
                                                              </label>
                                                              <input type="hidden" name="id" value="607d551b77135856ffbfe26e">
                                                          </div>
                                                          <div class="radio-custom">
                                                              <label class="radioholder" style="color: #fff; margin: 0;">
                                                                  <input type="radio" name="accountRisk" value="Short Term Trading"><span class="tname">Short Term Trading</span>
                                                                  <span class="radiomark"></span>
                                                              </label>
                                                          </div>
                                                          <br>
                                                      </div>
                                                  </div>
  
  
  
  
                                                  <div class="utility-coonections">
  
  
                                                      <div class="row" style="margin: 0px;">
                                                          <div class="col-md-12 text-left">
                                                              <h4> Investment Risk Tolerance </h4>
                                                              <p>Please select the degree of risk you are willing to take with
                                                                  the assets
                                                                  in this account</p>
                                                              <div class="tolerance-error"></div>
                                                              <div class="row">
                                                                  <div class="col-md-1">
                                                                      <label for="conservative" class="labelholder">
                                                                          <input type="checkbox" name="conservative" id="conservative" class="form-control" value="1">
                                                                          <span class="checkmark"></span>
                                                                      </label>
                                                                  </div>
                                                                  <div class="col-md-3" style="padding-left: 0px; left: 0; margin-top: 20px;">
                                                                      <h6>Conservative</h6>
                                                                      
                                                                  </div>
                                                                  <div class="col-md-8" style="padding-left: 0px; left: 0; margin-top: 5px;">
                                                                      <p>I want to preserve my initial principal in this
                                                                          account, with
                                                                          minimal risk, even if that means this
                                                                          account does not generate significantincome or
                                                                          returns and may
                                                                          not keep pacewith inflation.</p>
                                                                  </div>
  
  
                                                              </div><!-- // Row // -->
  
                                                              <div class="row">
                                                                  <div class="col-md-1">
                                                                      <label for="moderatelyConservative" class="labelholder">
                                                                          <input type="checkbox" name="moderatelyConservative" id="moderatelyConservative" class="form-control validate valid" value="1" aria-invalid="false">
                                                                          <span class="checkmark"></span>
                                                                      </label>
                                                                  </div>
                                                                  <div class="col-md-3" style="padding-left: 0px; left: 0; margin-top: 20px;">
                                                                      <h6>Moderately Conservative</h6>
                                                                      
                                                                  </div>
                                                                  <div class="col-md-8" style="padding-left: 0px; left: 0; margin-top: 5px;">
                                                                      <p>I am willing to acceptlowrisk tomy initial principal,
                                                                          including
                                                                          low volatility,to seek a modestlevel
                                                                          of portfolio returns.</p>
                                                                  </div>
  
  
                                                              </div><!-- // Row // -->
  
                                                              <div class="row">
                                                                  <div class="col-md-1">
                                                                      <label for="moderate" class="labelholder">
                                                                          <input type="checkbox" name="moderate" id="moderate" class="form-control validate valid" value="1" aria-invalid="false">
                                                                          <span class="checkmark"></span>
                                                                      </label>
                                                                  </div>
                                                                  <div class="col-md-3" style="padding-left: 0px; left: 0; margin-top: 20px;">
                                                                      <h6>Moderate</h6>
                                                                      
                                                                  </div>
                                                                  <div class="col-md-8" style="padding-left: 0px; left: 0; margin-top: 5px;">
                                                                      <p>I am willing to accept some risk to my initial
                                                                          principal and
                                                                          tolerate some volatility to seek higher
                                                                          returns, and understand I could lose a portion of
                                                                          the money
                                                                          invested.</p>
                                                                  </div>
  
  
                                                              </div><!-- // Row // -->
  
                                                              <div class="row">
                                                                  <div class="col-md-1">
                                                                      <label for="moderatelyAggressive" class="labelholder">
                                                                          <input type="checkbox" name="moderatelyAggressive" id="moderatelyAggressive" class="form-control validate valid" value="1" aria-invalid="false">
                                                                          <span class="checkmark"></span>
                                                                      </label>
                                                                  </div>
                                                                  <div class="col-md-3" style="padding-left: 0px; left: 0; margin-top: 20px;">
                                                                      <h6>Moderately Aggressive</h6>
                                                                      
                                                                  </div>
                                                                  <div class="col-md-8" style="padding-left: 0px; left: 0; margin-top: 5px;">
                                                                      <p>I am willing to accept high risk to my initial
                                                                          principal,
                                                                          including high volatility, to seek high returns
                                                                          over time and understand I could lose a substantial
                                                                          amount of
                                                                          the money invested.</p>
                                                                  </div>
  
  
                                                              </div><!-- // Row // -->
                                                              <div class="row">
                                                                  <div class="col-md-1">
                                                                      <label for="significantRisk" class="labelholder">
                                                                          <input type="checkbox" name="significantRisk" id="significantRisk" class="form-control validate valid" value="1" checked="" aria-invalid="false">
                                                                          <span class="checkmark"></span>
                                                                      </label>
                                                                  </div>
                                                                  <div class="col-md-3" style="padding-left: 0px; left: 0; margin-top: 20px;">
                                                                      <h6>Significant Risk </h6>
                                                                      
                                                                  </div>
                                                                  <div class="col-md-8" style="padding-left: 0px; left: 0; margin-top: 5px;">
                                                                      <p>I am willing to accept maximum risk to my initial
                                                                          principal to
                                                                          aggressively seek maximum returns,
                                                                          and I understand I could lose most, or all, of
                                                                          themoney
                                                                          invested.</p>
                                                                  </div>
  
  
                                                              </div><!-- // Row // -->
  
                                                              <p style="text-align: justify;"> We consider day trading to be a
                                                                  high‐risk
                                                                  trading strategy. Our clients must have a 'significant risk'
                                                                  tolerance
                                                                  to employ such a strategy. Please
                                                                  ensure that you have read and understand the accompanying
                                                                  Day Trading
                                                                  Risk Disclosure Statement before submitting your new account
                                                                  documentation. It is in your best interest to carefully
                                                                  consider whether
                                                                  or not you have a significant risk tolerance before
                                                                  proceeding with this
                                                                  form.
                                                              </p>
  
                                                          </div>
                                                      </div>
  
                                                  </div>
  
                                              </div>
                                          </div>
                                      </div>
  
  
                                  </div>
  
                                  <!-- <input type="button" name="previous" class="previous action-button-previous" value="Previous">
                                  <input type="button" name="next" id="stepbox6-btn" class="next action-button" value="Next" onclick="stepBox6()"> -->
                                  <div class="form-group clearfix">
                                    <a href="javascript:;" class="form-wizard-previous-btn float-left">Previous</a>
                                    <a href="javascript:;" class="form-wizard-next-btn float-left">Next</a>
                                  </div>
                                </form>
                              </fieldset>
                            <fieldset class="wizard-fieldset">
                              <form method="post" class="stepbox-7" style="display: block;">

                                <div class="maincontent " id="msform">
                                  <div class="property-form">
                                    <div class="row">
                                        <div class="form-inline">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <h3>Financial Situation and Needs, Liquidity Considerations and Tax
                                                        Status</h3>
                                                </div>
                                            </div>


                                            <div class="row" style="width: 100%;margin: 0px;padding: -1px 15px;">
                                                <div class="col-md-4 annualincomeclm text-left">
                                                    <br>
                                                    <h6 style="margin-left: 10px;">ANNUAL EXPENSES</h6>
                                                    <div class="label-icon ">
                                                        <label class="radioholder">
                                                            <input type="hidden" name="id" value="607d551b77135856ffbfe26e">
                                                            <input type="radio" name="annualExpense" id="annualExpense" value="$25,000 and under" data-value="25000">
                                                            <i class="fa fa-usd" aria-hidden="true"></i> $25,000 and
                                                            under
                                                            <span class="radiomark"></span>
                                                        </label>
                                                    </div>
                                                    <div class="label-icon radiocurrent">
                                                        <label class="radioholder">
                                                            <input type="radio" name="annualExpense" id="annualExpense" value="$25,001 - $50,000" data-value="25001" checked=""><i class="fa fa-usd" aria-hidden="true"></i> $25,001 -
                                                            $50,000
                                                            <span class="radiomark"></span>
                                                        </label>
                                                    </div>
                                                    <div class="label-icon ">
                                                        <label class="radioholder">
                                                            <input type="radio" name="annualExpense" id="annualExpense" value="$50,001 ‐ 100,000" data-value="50001"><i class="fa fa-usd" aria-hidden="true"></i> $50,001 ‐
                                                            100,000
                                                            <span class="radiomark"></span>
                                                        </label>
                                                    </div>

                                                    <div class="label-icon ">
                                                        <label class="radioholder">
                                                            <input type="radio" name="annualExpense" id="annualExpense" value="$100,001 ‐250,000" data-value="100001"><i class="fa fa-usd" aria-hidden="true" ==""></i> $100,001
                                                            ‐250,000
                                                            <span class="radiomark"></span>
                                                        </label>
                                                    </div>
                                                    <div class="label-icon ">
                                                        <label class="radioholder">
                                                            <input type="radio" name="annualExpense" id="annualExpense" value="$250,001 ‐500,000" data-value="250001"><i class="fa fa-usd" aria-hidden="true"></i> $250,001
                                                            ‐500,000
                                                            <span class="radiomark"></span>
                                                        </label>
                                                    </div>
                                                    <div class="label-icon ">
                                                        <label class="radioholder">
                                                            <input type="radio" name="annualExpense" id="annualExpense" value="Over $500,000" data-value="500000"><i class="fa fa-usd" aria-hidden="true"></i> Over $500,000
                                                            <span class="radiomark"></span>
                                                        </label>
                                                    </div>

                                                    <div class="annualExpense-error"></div>

                                                </div>
                                                <div class="col-md-4 text-left annualincomeclm">
                                                    <br>
                                                    <h6 style="margin-left: 10px;">SPECIAL EXPENSES</h6>
                                                    <div class="label-icon ">
                                                        <label class="radioholder">
                                                            <input type="radio" name="specialExpense" id="specialExpense" value="$25,000 and under">
                                                            <i class="fa fa-usd" aria-hidden="true"></i> $25,000 and
                                                            under
                                                            <span class="radiomark"></span>
                                                        </label>
                                                    </div>
                                                    <div class="label-icon radiocurrent">
                                                        <label class="radioholder">
                                                            <input type="radio" name="specialExpense" id="specialExpense" value="$25,001 - $50,000" checked=""><i class="fa fa-usd" aria-hidden="true"></i> $25,001 -
                                                            $50,000
                                                            <span class="radiomark"></span>
                                                        </label>
                                                    </div>
                                                    <div class="label-icon ">
                                                        <label class="radioholder">
                                                            <input type="radio" name="specialExpense" id="specialExpense" value="$50,001 ‐ 100,000">
                                                            <i class="fa fa-usd" aria-hidden="true"></i> $50,001 ‐
                                                            100,000
                                                            <span class="radiomark"></span>
                                                        </label>
                                                    </div>
                                                    <div class="label-icon ">
                                                        <label class="radioholder">
                                                            <input type="radio" name="specialExpense" id="specialExpense" value="$100,001 ‐250,000">
                                                            <i class="fa fa-usd" aria-hidden="true"></i> $100,001
                                                            ‐250,000
                                                            <span class="radiomark"></span>
                                                        </label>
                                                    </div>
                                                    <div class="label-icon ">
                                                        <label class="radioholder">
                                                            <input type="radio" name="specialExpense" id="specialExpense" value="$250,001 ‐500,000">
                                                            <i class="fa fa-usd" aria-hidden="true"></i> $250,001
                                                            ‐500,000
                                                            <span class="radiomark"></span>
                                                        </label>
                                                    </div>
                                                    <div class="label-icon ">
                                                        <label class="radioholder">
                                                            <input type="radio" name="specialExpense" id="specialExpense" value="Over $500,000">
                                                            <i class="fa fa-usd" aria-hidden="true"></i> Over $500,000
                                                            <span class="radiomark"></span>
                                                        </label>
                                                    </div>
                                                    <div class="specialExpense-error"></div>
                                                </div>

                                                <div class="col-md-4 text-left liquidi-need annualincomeclm">
                                                    <br>
                                                    <h6 style="margin-left: 10px;">LIQUIDITY NEEDS</h6>
                                                    <div class="label-icon ">
                                                        <label class="radioholder">
                                                            <input type="radio" name="liquidityExpense" id="liquidityExpense" value="Very important">
                                                            Very important
                                                            <span class="radiomark"></span>
                                                        </label>
                                                    </div>
                                                    <div class="label-icon radiocurrent">
                                                        <label class="radioholder">
                                                            <input type="radio" name="liquidityExpense" id="liquidityExpense" value="Important" checked="">
                                                            Important
                                                            <span class="radiomark"></span>
                                                        </label>
                                                    </div>
                                                    <div class="label-icon ">
                                                        <label class="radioholder">
                                                            <input type="radio" name="liquidityExpense" id="liquidityExpense" value="Some what important">Some
                                                            what important
                                                            <span class="radiomark"></span>
                                                        </label>
                                                    </div>
                                                    <div class="label-icon ">
                                                        <label class="radioholder">
                                                            <input type="radio" name="liquidityExpense" id="liquidityExpense" value="Does not matter">
                                                            Does not matter
                                                            <span class="radiomark"></span>
                                                        </label>
                                                    </div>
                                                    <div class="liquidityExpense-error"></div>
                                                </div>
                                                <br>

                                            </div>



                                            <div class="col-md-12">
                                                <br>
                                                <div class="form-group">
                                                    <h3>The expected period you plan to achieve your financial goal(s)
                                                    </h3>
                                                </div>
                                            </div>



                                            <div class="row" style="width: 100%;margin: 0px;padding: -1px 15px;">
                                                <div class="col-sm-4 text-left expected-per annualincomeclm">
                                                    <br>
                                                    <div class="label-icon ">
                                                        <label class="radioholder">
                                                            <input type="radio" name="financialGoal" id="financialGoal" value="Under 1 year">
                                                            Under
                                                            1 year
                                                            <span class="radiomark"></span>
                                                        </label>
                                                    </div>
                                                    <div class="label-icon radiocurrent">
                                                        <label class="radioholder">
                                                            <input type="radio" name="financialGoal" id="financialGoal" value="1 - 2" checked="">
                                                            1 - 2
                                                            <span class="radiomark"></span>
                                                        </label>
                                                    </div>
                                                    <div class="label-icon ">
                                                        <label class="radioholder">
                                                            <input type="radio" name="financialGoal" id="financialGoal" value="3 - 5">
                                                            3 - 5
                                                            <span class="radiomark"></span>
                                                        </label>
                                                    </div>
                                                    <div class="label-icon ">
                                                        <label class="radioholder">
                                                            <input type="radio" name="financialGoal" id="financialGoal" value="6 - 10">6
                                                            - 10
                                                            <span class="radiomark"></span>
                                                        </label>
                                                    </div>
                                                    <div class="label-icon ">
                                                        <label class="radioholder">
                                                            <input type="radio" name="financialGoal" id="financialGoal" value="11 - 20">
                                                            11 - 20
                                                            <span class="radiomark"></span>
                                                        </label>
                                                    </div>
                                                    <div class="label-icon ">
                                                        <label class="radioholder">
                                                            <input type="radio" name="financialGoal" id="financialGoal" value="Over 20">
                                                            Over 20
                                                            <span class="radiomark"></span>
                                                        </label>
                                                    </div>
                                                    <div class="financialGoal-error"></div>
                                                </div>
                                                <div class="col-md-8"></div>
                                            </div>
                                        </div>
                                    </div>
                                  </div>
                                </div>

                                <!-- <input type="button" name="previous" class="previous action-button-previous" value="Previous">
                                <input type="button" name="next" id="stepbox7-btn" class="next action-button" value="Next" onclick="stepBox7()"> -->
                                <div class="form-group clearfix">
                                    <a href="javascript:;" class="form-wizard-previous-btn float-left">Previous</a>
                                    <a href="javascript:;" class="form-wizard-next-btn float-right">Next</a>
                                </div>
                            </form>
                            </fieldset>
                            <fieldset class="wizard-fieldset">
                              <form method="post" class="stepbox-8 was-validated" style="display: block;">
                                <h2 class="fs-title">Investment Expeirence</h2>
                                <div class="maincontent">

                                    <div class="row" style="margin: 0px;margin-bottom: 20px;">
                                        <div class="col-md-12 invest-tptxt" style="background-color: #dedede;">
                                            <p>We are collecting the information below to better understand your investment
                                                experience.
                                                We recognize your responses may change over time as you work with us. Please
                                                check the
                                                boxes that best describe your investment experience to date.</p>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-sm-12">
                                            <table class="table">
                                                <thead>
                                                    <tr>
                                                        <th>Investment</th>
                                                        <th>Year(s) Of Experience</th>
                                                        <th>Knowledge</th>
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    <tr>
                                                        <td colspan="3">
                                                            <div class="stockExpertise-error"></div>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div class="custom-checkbox">
                                                                <label for="investStock" class="labelholder">
                                                                    <input type="checkbox" name="investStock" id="investStock" value="1"> Stocks
                                                                    <span class="checkmark"></span>
                                                                    <input type="hidden" name="id" value="607d551b77135856ffbfe26e">
                                                                </label>

                                                            </div>

                                                        </td>
                                                        <td>
                                                            <label class="radioholder">
                                                                <input type="radio" name="stockExp" id="stockExp" value="0"> 0
                                                                <span class="radiomark"></span>
                                                            </label>
                                                            <label class="radioholder ">
                                                                <input type="radio" name="stockExp" id="stockExp" value="1-5"> 1-5
                                                                <span class="radiomark"></span>
                                                            </label>
                                                            <label class="radioholder ">
                                                                <input type="radio" name="stockExp" id="stockExp" value="5+"> 5+
                                                                <span class="radiomark"></span>

                                                            </label>
                                                        </td>
                                                        <td>
                                                            <label class="radioholder">
                                                                <input type="radio" name="stockExpertise" id="stockExpertise" value="None">
                                                                None
                                                                <span class="radiomark"></span>
                                                            </label>
                                                            <label class="radioholder">
                                                                <input type="radio" name="stockExpertise" id="stockExpertise" value="Limited">
                                                                Limited
                                                                <span class="radiomark"></span>
                                                            </label>
                                                            <label class="radioholder">
                                                                <input type="radio" name="stockExpertise" id="stockExpertise" value="Good">
                                                                Good
                                                                <span class="radiomark"></span>
                                                            </label>
                                                            <label class="radioholder">
                                                                <input type="radio" name="stockExpertise" id="stockExpertise" value="Extensive">
                                                                Extensive
                                                                <span class="radiomark"></span>
                                                            </label>
                                                        </td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div class="custom-checkbox">
                                                                <label for="investBond" class="labelholder">
                                                                    <input type="checkbox" name="investBond" id="investBond" value="1">
                                                                    Fixed Income
                                                                    <span class="checkmark"></span>
                                                                </label>
                                                            </div>

                                                        </td>
                                                        <td>
                                                            <label class="radioholder">
                                                                <input type="radio" name="bondExp" id="bondExp" value="0"> 0
                                                                <span class="radiomark"></span>
                                                            </label>
                                                            <label class="radioholder ">
                                                                <input type="radio" name="bondExp" id="bondExp" value="1-5"> 1-5
                                                                <span class="radiomark"></span>
                                                            </label>
                                                            <label class="radioholder ">
                                                                <input type="radio" name="bondExp" id="bondExp" value="5+"> 5+
                                                                <span class="radiomark"></span>

                                                            </label>
                                                        </td>
                                                        <td>
                                                            <label class="radioholder">
                                                                <input type="radio" name="bondExpertise" id="bondExpertise" value="None">
                                                                None
                                                                <span class="radiomark"></span>
                                                            </label>
                                                            <label class="radioholder">
                                                                <input type="radio" name="bondExpertise" id="bondExpertise" value="Limited">
                                                                Limited
                                                                <span class="radiomark"></span>
                                                            </label>
                                                            <label class="radioholder">
                                                                <input type="radio" name="bondExpertise" id="bondExpertise" value="Good">
                                                                Good
                                                                <span class="radiomark"></span>
                                                            </label>
                                                            <label class="radioholder">
                                                                <input type="radio" name="bondExpertise" id="bondExpertise" value="Extensive">
                                                                Extensive
                                                                <span class="radiomark"></span>
                                                            </label></td>
                                                    </tr>
                                                    <tr>
                                                        <td>
                                                            <div class="custom-checkbox">
                                                                <label for="investOptions" class="labelholder">
                                                                    <input type="checkbox" value="1" name="investOptions" id="investOptions">
                                                                    Options
                                                                    <span class="checkmark"></span>
                                                                </label>
                                                            </div>

                                                        </td>
                                                        <td> <label class="radioholder">
                                                                <input type="radio" name="optionsExp" id="optionsExp" value="0" disabled=""> 0
                                                                <span class="radiomark"></span>
                                                            </label>
                                                            <label class="radioholder ">
                                                                <input type="radio" name="optionsExp" id="optionsExp" value="1-5" disabled=""> 1-5
                                                                <span class="radiomark"></span>
                                                            </label>
                                                            <label class="radioholder ">
                                                                <input type="radio" name="optionsExp" id="optionsExp" value="5+" disabled=""> 5+
                                                                <span class="radiomark"></span>

                                                            </label></td>
                                                        <td> <label class="radioholder">
                                                                <input type="radio" name="optionsExpertise" id="optionsExpertise" value="None" disabled="">
                                                                None
                                                                <span class="radiomark"></span>
                                                            </label>
                                                            <label class="radioholder">
                                                                <input type="radio" name="optionsExpertise" id="optionsExpertise" value="Limited" disabled="">
                                                                Limited
                                                                <span class="radiomark"></span>
                                                            </label>
                                                            <label class="radioholder">
                                                                <input type="radio" name="optionsExpertise" id="optionsExpertise" value="Good" disabled="">
                                                                Good
                                                                <span class="radiomark"></span>
                                                            </label>
                                                            <label class="radioholder">
                                                                <input type="radio" name="optionsExpertise" id="optionsExpertise" value="Extensive" disabled="">
                                                                Extensive
                                                                <span class="radiomark"></span>
                                                            </label></td>
                                                    </tr>

                                                    <tr>
                                                        <td>

                                                            <div class="custom-checkbox">
                                                                <label for="investFuture" class="labelholder">
                                                                    <input type="checkbox" value="1" name="investFuture" id="investFuture">
                                                                    Futures
                                                                    <span class="checkmark"></span>
                                                                </label>
                                                            </div>

                                                        </td>
                                                        <td>
                                                            <label class="radioholder">
                                                                <input type="radio" name="futureExp" id="futureExp" value="0" disabled="">
                                                                0
                                                                <span class="radiomark"></span>
                                                            </label>
                                                            <label class="radioholder ">
                                                                <input type="radio" name="futureExp" id="futureExp" value="1-5" disabled=""> 1-5
                                                                <span class="radiomark"></span>
                                                            </label>
                                                            <label class="radioholder ">
                                                                <input type="radio" name="futureExp" id="futureExp" value="5+" disabled=""> 5+
                                                                <span class="radiomark"></span>

                                                            </label>

                                                        </td>
                                                        <td>
                                                            <label class="radioholder">
                                                                <input type="radio" name="futureExpertise" id="futureExpertise" value="None" disabled="">
                                                                None
                                                                <span class="radiomark"></span>
                                                            </label>
                                                            <label class="radioholder">
                                                                <input type="radio" name="futureExpertise" id="futureExpertise" value="Limited" disabled="">
                                                                Limited
                                                                <span class="radiomark"></span>
                                                            </label>
                                                            <label class="radioholder">
                                                                <input type="radio" name="futureExpertise" id="futureExpertise" value="Good" disabled="">
                                                                Good
                                                                <span class="radiomark"></span>
                                                            </label>
                                                            <label class="radioholder">
                                                                <input type="radio" name="futureExpertise" id="futureExpertise" value="Extensive" disabled="">
                                                                Extensive
                                                                <span class="radiomark"></span>
                                                            </label>
                                                        </td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                                <!-- <input type="button" name="previous" class="previous action-button-previous" value="Previous">
                                <input type="button" name="next" id="stepbox8-btn" class="next action-button" value="Next" onclick="stepBox8()"> -->
                                <div class="form-group clearfix">
                                    <a href="javascript:;" class="form-wizard-previous-btn float-left">Previous</a>
                                    <a href="javascript:;" class="form-wizard-next-btn float-right">Next</a>
                                </div>
                              </form>
                            </fieldset>
                            <fieldset class=" wizard-fieldset">
                              <form method="post" class="stepbox-9 was-validated" enctype="multipart/form-data" novalidate="novalidate" style="display: block;">
                                <h2 class="fs-title">Identification Proof Upload</h2>
                                <div class="maincontent">
                                    <div class="property-form">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <br>
                                                <p>
                                                    Government issued ID. If Driver's License is used and the address is not
                                                    the same as
                                                    on the application please provide a utility bill with your name and
                                                    address. You can
                                                    email a copy to accountopening@guardiantrading.com
                                                </p>
                                                <p>
                                                    <b>Please upload a copy of the Applicant’s Government issued ID in jpeg
                                                        format (Front
                                                        and Back)</b>
                                                </p>
                                                <div class="form-row">
                                                    <div class="form-group col-md-5">
                                                        <input type="file" name="photo[]" class="form-control show-for-sr validate valid" id="photo1" accept=".jpg, .jpg" aria-invalid="false">
                                                        <input type="hidden" name="id" value="607d551b77135856ffbfe26e">

                                                        <div class="quote-imgs-thumbs quote-imgs-thumbs--hidden" id="img_preview" aria-live="polite"></div>

                                                        <span class="text-danger"></span>
                                                    </div>

                                                    <div class="form-group col-md-5">
                                                        <input type="file" name="photo[]" class="form-control show-for-sr validate valid" id="photo2" accept=".jpg, .jpg" aria-invalid="false">


                                                        <div class="quote-imgs-thumbs quote-imgs-thumbs--hidden" id="img_preview" aria-live="polite"></div>

                                                        <span class="text-danger"></span>
                                                    </div>

                                                    <div class="form-group col-md-5">
                                                        <input type="file" name="photo[]" class="form-control show-for-sr validate valid" id="photo3" accept=".jpg, .jpg" aria-invalid="false">

                                                        <div class="quote-imgs-thumbs quote-imgs-thumbs--hidden" id="img_preview" aria-live="polite"></div>

                                                        <span class="text-danger"></span>
                                                    </div>

                                                    <div class="form-group col-md-5">
                                                        <input type="file" name="photo[]" class="form-control show-for-sr validate valid" id="photo4" accept=".jpg, .jpg" aria-invalid="false">

                                                        <div class="quote-imgs-thumbs quote-imgs-thumbs--hidden" id="img_preview" aria-live="polite"></div>

                                                        <span class="text-danger"></span>
                                                    </div>

                                                    <div class="form-group col-md-5" style="margin-top:0px;">
                                                        <button type="button" class="btn btn-link btn-md image-hint" style="font-size: 15px;"> Image Hints and Tips </button>
                                                    </div>
                                                    
                                                </div>
                                                
                                                    <p>
                                                        In addition to your ID, please submit Article of Incorporation, 
                                                        Certificate of Incorporation, Articles of Organization and Certification of Beneficial Ownership. 
                                                        (If foreign please submit W8 BEN-E)
                                                    </p>
                                                    <p>
                                                        <b>Please upload your in jpeg format)</b>
                                                    </p>
                                                    <div class="form-row">

                                                        <div class="form-group col-md-5">
                                                            <input type="file" name="photo[]" class="form-control show-for-sr validate valid" id="corporate_photo1" accept=".jpg, .jpg" aria-invalid="false">
    
                                                            <div class="quote-imgs-thumbs quote-imgs-thumbs--hidden" id="img_preview" aria-live="polite"></div>
    
                                                            <span class="text-danger"></span>
                                                        </div>
    
                                                        <div class="form-group col-md-5">
                                                            <input type="file" name="photo[]" class="form-control show-for-sr validate valid" id="corporate_photo2" accept=".jpg, .jpg" aria-invalid="false">
    
    
                                                            <div class="quote-imgs-thumbs quote-imgs-thumbs--hidden" id="img_preview" aria-live="polite"></div>
    
                                                            <span class="text-danger"></span>
                                                        </div>
    
                                                        <div class="form-group col-md-5">
                                                            <input type="file" name="photo[]" class="form-control show-for-sr validate valid" id="corporate_photo3" accept=".jpg, .jpg" aria-invalid="false">
    
                                                            <div class="quote-imgs-thumbs quote-imgs-thumbs--hidden" id="img_preview" aria-live="polite"></div>
    
                                                            <span class="text-danger"></span>
                                                        </div>
    
                                                        <div class="form-group col-md-5">
                                                            <input type="file" name="photo[]" class="form-control show-for-sr validate valid" id="corporate_photo4" accept=".jpg, .jpg" aria-invalid="false">
    
                                                            <div class="quote-imgs-thumbs quote-imgs-thumbs--hidden" id="img_preview" aria-live="polite"></div>
    
                                                            <span class="text-danger"></span>
                                                        </div>
    
                                                        <div class="form-group col-md-5">
                                                            <button type="button" class="btn btn-link btn-md image-hint" style="font-size: 15px;"> Image Hints and Tips </button>
                                                        </div>

                                                    </div>
                                                
                                                <div class="form-row">
                                                    <div class="photo-error"></div>
                                                    <br>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <input type="hidden" name="registration_type" id="registration_type" value="Corporate">
                                <!-- <input type="button" name="previous" class="previous action-button-previous" value="Previous">
                                <input type="button" name="next" id="stepbox9-btn" class="next action-button" value="Next" onclick="stepBox9()"> -->
                                <div class="form-group clearfix">
                                    <a href="javascript:;" class="form-wizard-previous-btn float-left">Previous</a>
                                    <a href="javascript:;" class="form-wizard-next-btn float-right">Next</a>
                                </div>
                              </form>
                            </fieldset>
                            <fieldset class="wizard-fieldset">
                              <form method="post" class="stepbox-10 was-validated" novalidate="novalidate" style="display: block;">
                                <h2 class="fs-title">Disclosures</h2>

                                <div class="maincontent">
                                    <div class="utility-coonections">

                                        <div class="row" style="margin: 0px;">
                                            <div class="col-sm-12 text-left">
                                                <p>Borrowing Money to Buy Securities (Buying "on Margin") - (Please read
                                                    carefully)</p>
                                                <p>You chose to have a "margin loan account" (customarily known as "margin
                                                    account") by
                                                    checking the boxes below. To help you decide whether a margin loan
                                                    account is right
                                                    for you. Please read the information below and the client agreement.</p>
                                            </div>
                                        </div>

                                        <input type="hidden" name="id" value="607d551b77135856ffbfe26e">
                                        <div class="row" style="margin: 0px;">
                                            <div class="col-sm-12 text-left">
                                                <br>
                                                <p>I want the ability to borrow funds in my account. I have read the client
                                                    agreement
                                                    and disclosures understand my right and obligation under it <span class="text-danger"> *</span>
                                                    </p><div class="optRadio-error"></div>
                                                <p></p>
                                            </div>
                                            <div class="col-sm-12 text-left ynoption">
                                                <label class="radioholder">
                                                    <input type="radio" name="optRadio" class="optRadio" id="optRadio" value="1">Yes
                                                    <span class="radiomark"></span>
                                                </label>

                                                <label class="radioholder">
                                                    <input type="radio" name="optRadio" class="optRadio" id="optRadio" value="0" checked="">No, (I disagree)
                                                    <span class="radiomark"></span>
                                                </label>

                                                <br>
                                                <br>
                                            </div>
                                        </div>


                                        <div class="row" style="margin: 0px;">
                                            <div class="col-md-12 text-left">
                                                <p>Pattern day trading accounts are only offered to an account that
                                                    maintains balance
                                                    greater than $25,000. Guardian Trading requires an intial deposit of at
                                                    least
                                                    $30,000.
                                                    <br>
                                                    What is the initial deposit that you will fund your account with? <span class="text-danger"> *</span>
                                                </p>
                                                <input type="number" name="borrowInput" class="form-control validate valid" min="25000" value="" required="required" data-toggle="tooltip" title="" data-original-title="Deposit amount must be greater than or equal to $25000" aria-required="true" aria-invalid="false"><label id="borrowInput-error" class="error" for="borrowInput" style="display: none;"></label>
                                            </div>
                                        </div>


                                    </div>

                                    <div class="form-inline" style="align-items: flex-start;">
                                        <div class="col-md-12 text-left">
                                            <p>Do you maintain any other Brokerage accounts? <span class="text-danger">
                                                    *</span>
                                                </p><div class="brokerAccount-error"></div>
                                            <p></p>

                                        </div>
                                        <div class="col-md-12 text-left ynoption">
                                            <label class="radioholder">
                                                <input type="radio" name="brokerAccount" id="brokerAccount" class="brokerAccount" value="1">Yes
                                                <span class="radiomark"></span>
                                            </label>
                                            <label class="radioholder">
                                                <input type="radio" name="brokerAccount" id="brokerAccount" class="brokerAccount" value="0">No
                                                <span class="radiomark"></span>
                                            </label>

                                            <br>
                                            <br>
                                        </div>
                                        <div class="row" style="margin: 0px;" id="brokerAccount_div">

                                            <div class="col-sm-12 text-left">
                                                <div class="form-group">
                                                    <label for="brokerAccountTotal"> Number Of Accounts (total)
                                                        <span class="text-danger"> *</span></label>
                                                    <input type="number" class="form-control validate valid" name="brokerAccountTotal" required="required" id="brokerAccountTotal" value="" data-toggle="tooltip" title="" onkeydown="if(this.value.length==20  &amp;&amp; event.keyCode!=8 || event.keyCode == 189 || event.keyCode == 69)  return false;" data-original-title="Number Of Accounts is required" aria-required="true" aria-invalid="false"><label id="brokerAccountTotal-error" class="error" for="brokerAccountTotal" style="display: none;"></label>
                                                </div>
                                            </div>

                                        </div>
                                    </div>


                                    <div class="form-inline" style="align-items: flex-start;">
                                        <div class="col-md-12 text-left">
                                            <p>Do you already maintain an account at either Velocity Clearing LLC in which
                                                you have
                                                control, beneficial interest, or trading authority? <span class="text-danger"> *</span>
                                                </p><div class="beneficial-error"></div>
                                            <p></p>
                                        </div>
                                        <div class="col-md-12 text-left ynoption">
                                            <label class="radioholder">
                                                <input type="radio" name="beneficial" id="beneficial" class="beneficial" value="1">Yes
                                                <span class="radiomark"></span>
                                            </label>
                                            <label class="radioholder">
                                                <input type="radio" name="beneficial" id="beneficial" class="beneficial" value="0">No
                                                <span class="radiomark"></span>
                                            </label>

                                            <br>
                                            <br>
                                        </div>
                                        <div class="row" style="margin: 0px;" id="beneficial_div">
                                            <div class="col-sm-6 text-left">
                                                <div class="form-group">
                                                    <label for="beneficialAccountNumber"> Account Number <span class="text-danger">
                                                            *</span></label>
                                                    <input type="text" class="form-control validate valid" name="beneficialAccountNumber" required="required" id="beneficialAccountNumber" value="" data-toggle="tooltip" title="" data-original-title="Account Number is required" aria-required="true" aria-invalid="false"><label id="beneficialAccountNumber-error" class="error" for="beneficialAccountNumber" style="display: none;"></label>

                                                </div>
                                            </div>
                                            <div class="col-sm-6 text-left">
                                                <div class="form-group">

                                                    <label for="beneficialAccountName"> Account Name <span class="text-danger">
                                                            *</span></label>
                                                    <input type="text" class="form-control validate valid" name="beneficialAccountName" required="required" id="beneficialAccountName" value="" data-toggle="tooltip" title="" data-original-title="Account Name is required" aria-required="true" aria-invalid="false"><label id="beneficialAccountName-error" class="error" for="beneficialAccountName" style="display: none;"></label>

                                                </div>

                                            </div>

                                        </div>
                                    </div>


                                    <div class="form-inline" style="align-items: flex-start;">
                                        <div class="col-sm-12 text-left">
                                            <p>Do you have a relationship with an entity that already maintains an account
                                                at Velocity
                                                Clearing LLC, such as employee, officer, shareholder, member, partner or
                                                owner? <span class="text-danger"> *</span>
                                                </p><div class="shareHolder-error"></div>
                                            <p></p>
                                        </div>
                                        <div class="col-sm-12 text-left ynoption">
                                            <label class="radioholder">
                                                <input type="radio" name="shareHolder" id="shareHolder" class="shareHolder" value="1">Yes
                                                <span class="radiomark"></span>
                                            </label>

                                            <label class="radioholder">
                                                <input type="radio" name="shareHolder" id="shareHolder" class="shareHolder" value="0">No
                                                <span class="radiomark"></span>
                                            </label>

                                            <br>
                                        </div>

                                        <div class="row" style="margin: 0px;" id="shareHolder_div">

                                            <div class="col-sm-6 text-left">
                                                <div class="form-group">
                                                    <label for="shareHolderAccountNumber"> Account Number <span class="text-danger">
                                                            *</span></label>
                                                    <input type="number" class="form-control validate valid" name="shareHolderAccountNumber" required="required" id="shareHolderAccountNumber" value="" data-toggle="tooltip" title="" onkeydown="if(this.value.length==20  &amp;&amp; event.keyCode!=8 || event.keyCode == 189 || event.keyCode == 69)  return false;" data-original-title="Account number is required" aria-required="true" aria-invalid="false"><label id="shareHolderAccountNumber-error" class="error" for="shareHolderAccountNumber" style="display: none;"></label>
                                                </div>
                                            </div>

                                            <div class="col-sm-6 text-left">
                                                <div class="form-group">
                                                    <label for="shareHolderAccountName"> Account Name <span class="text-danger">
                                                            *</span></label>
                                                    <input type="text" class="form-control validate valid" name="shareHolderAccountName" required="required" id="shareHolderAccountName" value="" data-toggle="tooltip" title="" onkeypress="return /[A-Za-z -]/i.test(event.key)" data-original-title="Account name is required" aria-required="true" aria-invalid="false"><label id="shareHolderAccountName-error" class="error" for="shareHolderAccountName" style="display: none;"></label>
                                                </div>
                                            </div>

                                            <div class="col-sm-6 text-left">
                                                <div class="form-group">
                                                    <label for="shareHolderAccountNumber"> Relationship <span class="text-danger">
                                                            *</span></label>
                                                    <input type="text" class="form-control validate valid" name="shareHolderRelationship" pattern="^[A-Za-z -]+$" required="required" id="shareHolderRelationship" value="" data-toggle="tooltip" title="" onkeypress="return /[A-Za-z -]/i.test(event.key)" data-original-title="Relationship is required" aria-required="true" aria-invalid="false"><label id="shareHolderRelationship-error" class="error" for="shareHolderRelationship" style="display: none;"></label>
                                                </div>
                                            </div>

                                        </div>

                                    </div>


                                    <div class="form-inline" style="align-items: flex-start;">
                                        <div class="col-md-12 text-left">
                                            <br>
                                            <p>Are either you or an immediate family member an officer, director or at least
                                                10%
                                                shareholder in a publicly traded company? <span class="text-danger">
                                                    *</span>
                                                </p><div class="immediate-error"></div>
                                            <p></p>
                                        </div>
                                        <div class="col-md-12 text-left ynoption">
                                            <label class="radioholder">
                                                <input type="radio" name="immediate" id="immediate" class="immediate" value="1">Yes
                                                <span class="radiomark"></span>
                                            </label>

                                            <label class="radioholder">
                                                <input type="radio" name="immediate" id="immediate" class="immediate" value="0">No
                                                <span class="radiomark"></span>
                                            </label>

                                            <br>
                                        </div>

                                        <div class="row" style="margin: 0px;" id="immediate_div">

                                            <div class="col-sm-6 text-left">
                                                <div class="form-group">
                                                    <label for="immediateCompanyName"> Company Name <span class="text-danger">
                                                            *</span></label>
                                                    <input type="text" class="form-control validate valid" name="immediateCompanyName" pattern="^[A-Za-z -]+$" required="required" id="immediateCompanyName" value="" data-toggle="tooltip" title="" onkeypress="return /[A-Za-z -]/i.test(event.key)" data-original-title="Company Name is required" aria-required="true" aria-invalid="false"><label id="immediateCompanyName-error" class="error" for="immediateCompanyName" style="display: none;"></label>
                                                </div>
                                            </div>

                                            <div class="col-sm-6 text-left">
                                                <div class="form-group">
                                                    <label for="immediateCompanyaddress"> Company Address <span class="text-danger">
                                                            *</span></label>
                                                    <input type="text" class="form-control validate valid" name="immediateCompanyaddress" required="required" id="immediateCompanyaddress" value="" data-toggle="tooltip" title="" data-original-title="Company address is required" aria-required="true" aria-invalid="false"><label id="immediateCompanyaddress-error" class="error" for="immediateCompanyaddress" style="display: none;"></label>
                                                </div>
                                            </div>
                                            <div class="col-sm-6 text-left">
                                                <div class="form-group">
                                                    <label for="immediateRelationshipwithentity"> Relationship with entity
                                                        <span class="text-danger"> *</span></label>
                                                    <input type="text" class="form-control validate valid" name="immediateRelationshipwithentity" required="required" id="immediateRelationshipwithentity" value="" data-toggle="tooltip" title="" data-original-title="Relationship with entity is required" aria-required="true" aria-invalid="false"><label id="immediateRelationshipwithentity-error" class="error" for="immediateRelationshipwithentity" style="display: none;"></label>
                                                </div>
                                            </div>
                                        </div>

                                    </div>


                                    <div class="row" style="margin: 0px;">
                                        <div class="col-md-12 text-left">
                                            <br>
                                            <p>Are either you or an immediate family member employed by FINRA, a registered
                                                broker
                                                dealer or a securities exchange? <span class="text-danger"> *</span>
                                                </p><div class="securities-error"></div>
                                            <p></p>
                                        </div>
                                        <div class="col-md-12 text-left ynoption">

                                            <label class="radioholder">
                                                <input type="radio" name="securities" id="securities" class="securities" value="1">Yes
                                                <span class="radiomark"></span>
                                            </label>
                                            <label class="radioholder">
                                                <input type="radio" name="securities" id="securities" class="securities" value="0">No
                                                <span class="radiomark"></span>
                                            </label>

                                            <br>
                                        </div>
                                    </div>
                                    <div class="row" style="margin: 0px;" id="securities_div">

                                        <div class="col-sm-6 text-left">
                                            <div class="form-group">
                                                <label for="securitiesFirm"> Name of the Firm Or Exchange <span class="text-danger">
                                                        *</span></label>
                                                <input type="text" class="form-control validate valid" name="securitiesFirm" pattern="^[A-Za-z -]+$" required="required" id="securitiesFirm" value="" data-toggle="tooltip" title="" onkeypress="return /[A-Za-z -]/i.test(event.key)" data-original-title="Firm Or Exchange is required" aria-required="true" aria-invalid="false"><label id="securitiesFirm-error" class="error" for="securitiesFirm" style="display: none;"></label>
                                            </div>
                                        </div>

                                        <div class="col-sm-6 text-left">
                                            <div class="form-group">
                                                <label for="securitiesFirmAddress"> Firm Address <span class="text-danger">
                                                        *</span></label>
                                                <input type="text" class="form-control validate valid" name="securitiesFirmAddress" required="required" id="securitiesFirmAddress" value="" data-toggle="tooltip" title="" data-original-title="Firm address is required" aria-required="true" aria-invalid="false"><label id="securitiesFirmAddress-error" class="error" for="securitiesFirmAddress" style="display: none;"></label>
                                            </div>
                                        </div>

                                        <div class="col-sm-6 text-left">
                                            <div class="form-group">
                                                <label for="securitiesPermissionAccount"> Permission to open the account
                                                    <span class="text-danger"> *</span></label>
                                                <input type="file" class="form-control show-for-sr validate valid" accept=".jpg, .jpg, .pdf" name="securitiesPermissionAccount" required="required" id="securitiesPermissionAccount" value="" data-toggle="tooltip" title="" data-original-title="Permission Account is required" aria-required="true" aria-invalid="false"><label id="securitiesPermissionAccount-error" class="error" for="securitiesPermissionAccount" style="display: none;"></label>
                                            </div>
                                        </div>

                                    </div>



                                    <div class="row" style="margin: 0px;">
                                        <div class="col-md-12 text-left">
                                            <br>
                                            <p>Are you a senior officer at a bank, savings and loan institution, investment
                                                company,
                                                investment advisory firm, or other financial institution? <span class="text-danger">
                                                    *</span>
                                                </p><div class="institution-error"></div>
                                            <p></p>
                                        </div>
                                        <div class="col-md-12 text-left ynoption">
                                            <label class="radioholder">
                                                <input type="radio" name="institution" id="institution" class="institution" value="1">Yes
                                                <span class="radiomark"></span>
                                            </label>
                                            <label class="radioholder">
                                                <input type="radio" name="institution" id="institution" class="institution" value="0">No
                                                <span class="radiomark"></span>
                                            </label>

                                            <br>
                                            <br>
                                        </div>
                                    </div>

                                    <div class="row" style="margin: 0px;" id="institution_div">
                                        <div class="col-sm-6 text-left">
                                            <div class="form-group">
                                                <label for="institutionFirm"> Name of the Firm <span class="text-danger">
                                                        *</span></label>
                                                <input type="text" class="form-control validate valid" name="institutionFirm" pattern="^[A-Za-z -]+$" required="required" id="institutionFirm" value="" data-toggle="tooltip" title="" onkeypress="return /[A-Za-z -]/i.test(event.key)" data-original-title="Firm is required" aria-required="true" aria-invalid="false"><label id="institutionFirm-error" class="error" for="institutionFirm" style="display: none;"></label>
                                            </div>
                                        </div>

                                        <div class="col-sm-6 text-left">
                                            <div class="form-group">
                                                <label for="institutionFirmAddress"> Firm Address <span class="text-danger">
                                                        *</span></label>
                                                <input type="text" class="form-control validate valid" name="institutionFirmAddress" required="required" id="institutionFirmAddress" value="" data-toggle="tooltip" title="" data-original-title="Firm address is required" aria-required="true" aria-invalid="false"><label id="institutionFirmAddress-error" class="error" for="institutionFirmAddress" style="display: none;"></label>
                                            </div>
                                        </div>

                                        <div class="col-sm-6 text-left">
                                            <div class="form-group">
                                                <label for="institutionPositioninFirm"> Position in Firm <span class="text-danger">
                                                        *</span></label>
                                                <input type="text" class="form-control validate valid" name="institutionPositioninFirm" required="required" id="institutionPositioninFirm" value="" data-toggle="tooltip" pattern="^[A-Za-z -]+$" onkeypress="return /[A-Za-z -]/i.test(event.key)" title="" data-original-title="Position in Firm is required" aria-required="true" aria-invalid="false"><label id="institutionPositioninFirm-error" class="error" for="institutionPositioninFirm" style="display: none;"></label>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row" style="margin: 0px;">
                                        <div class="col-md-12" style="background-color: #dedede;">
                                            <br>
                                            <p><b>Tax Withholding Certifications - Choose One</b></p>




                                            <label class="radioholder">
                                                <input type="radio" onclick="show1();" name="taxWitholding" value="U.S. Person" checked="">
                                                <b>U.S. Person:</b> under penalty of perjury, I certify that:
                                                <span class="radiomark"></span>
                                                
                                                <script>
                                                    $(function () {
                                                        document.getElementById('undershow1').style.display =
                                                            'block';
                                                    });
                                                </script>
                                                
                                                <ul style="list-style: none; display: block;" id="undershow1">
                                                    <li>
                                                        <label class="radioholder"> 1.<input type="radio" onclick="show3();" name="underUs" value="1" checked="">
                                                            am a U.S. citizen or a U.S. Resident Alien or other U.S. Person
                                                            and the
                                                            Social Security Number or Taxpayer identification Number
                                                            provided in this
                                                            application is correct
                                                            <span class="radiomark"></span>

                                                        </label>
                                                        
                                                    </li>
                                                    <li>
                                                        <label class="radioholder">
                                                            2.<input type="radio" onclick="show2();" name="underUs" value="2"> I am
                                                            not subject to backup
                                                            withholding because

                                                            <span class="radiomark"></span>
                                                            
                                                        </label>
                                                    </li>

                                                </ul>
                                                <ul style="list-style:none;margin-left: 30px;margin-bottom:0px;display:none;" id="undershow2">
                                                    <li>
                                                        <label class="radioholder">a. <input type="radio" name="underUs2" value="a"> I am
                                                            exempt from backup
                                                            withholding or
                                                            <span class="radiomark"></span>
                                                        </label>
                                                    </li>

                                                    <li>
                                                        <label class="radioholder">b. <input type="radio" name="underUs2" value="b"> I
                                                            have not been notified
                                                            by the Internal Revenue Services (IRS) that I am subject to
                                                            backup
                                                            withholding as a result of failure to report all interest or
                                                            dividends or
                                                            <span class="radiomark"></span>
                                                        </label>
                                                    </li>
                                                    <li><label class="radioholder">c.
                                                            <input type="radio" name="underUs2" value="c"> The IRS has
                                                            notified me
                                                            that I am no longer subject to backup withholding.
                                                            <span class="radiomark"></span>
                                                        </label></li>

                                                </ul>


                                            </label>
                                            <label class="radioholder">
                                                <input type="radio" name="taxWitholding" onclick="show4();" value="Certification Instruction">
                                                <b>Certification Instruction:</b> I cannot certify that I am not subject to
                                                backup
                                                withholding, meaning that I have been notified by the IRS that I am
                                                currently subject to
                                                backup withholding because I have failed to report all interest and
                                                dividends on my tax
                                                return.
                                                <span class="radiomark"></span>
                                            </label>
                                            <label class="radioholder">
                                                <input type="radio" name="taxWitholding" onclick="show4();" value="Non Residence Alien">
                                                <b>Non Resident
                                                    Alien:</b> I certify that I am not a U.S. resident alien or other U.S.
                                                person for
                                                U.S. tax purpose and I am submitting the applicable Form W-8 with this
                                                application to
                                                certify my foreign status and if applicable, claim tax treaty benefits.
                                                <span class="radiomark"></span>
                                            </label>
                                            <br>
                                        </div>
                                    </div>
                                </div>
                                <br>
                                <!-- <input type="button" name="previous" class="previous action-button-previous" value="Previous">
                                <input type="button" id="stepbox10-btn" class="next action-button" value="Next" onclick="stepBox10()"> -->
                                <div class="form-group clearfix">
                                    <a href="javascript:;" class="form-wizard-previous-btn float-left">Previous</a>
                                    <a href="javascript:;" class="form-wizard-next-btn float-right">Next</a>
                                </div>
                            </form>
                            </fieldset>
                            <fieldset class="wizard-fieldset">
                              <form method="post" class="stepbox-11" style="display: block;">
                                <h2 class="fs-title">Disclosures &amp; Signatures</h2>

                                <div class="maincontent">
                                  <div class="utility-coonections">
                                    <div class="row clientagreerow" style="margin: 0px;margin-top: 20px;">
                                        <div class="col-md-12 text-left pdfagreement">
                                            <p> Please select the disclosures below and the check the box noting you
                                                have read and
                                                understood these disclosures.</p>
                                            <div class="row">
                                                <input type="hidden" name="id" value="6079764bc907937d54f47014">
                                                <div class="col-md-5">
                                                    <label class="labelholder" style="padding-left:0px">
                                                        Account Terms &amp; Conditions <span class="text-danger"> *</span>
                                                    </label>
                                                </div>
                                                <div class="col-md-1 viewholder-txt"><a onclick="showFileModal(this, 0);" href="javascript:;"> View </a>
                                                </div>
                                                <div class="col-md-1">
                                                    <label for="accountTerms" class="labelholder">
                                                        <input type="checkbox" name="accountTerms" id="accountTerms" class="form-control validate" value="1" required="required" data-toggle="tooltip" title="" data-original-title="Account Terms &amp; Conditions is required">
                                                        <span class="checkmark"></span>
                                                    </label>
                                                </div>

                                                <div class="col-md-4 viewholder-txt">
                                                    <p>I provide my consent </p>
                                                </div>

                                            </div><!-- // Row // -->

                                            <div class="row">

                                                <div class="col-md-5">
                                                    <label class="labelholder" style="padding-left:0px">Day Trading Risk
                                                        Disclosure
                                                        <span class="text-danger"> *</span>
                                                    </label>
                                                </div><!-- // Col // -->
                                                <div class="col-md-1 viewholder-txt"><a onclick="showFileModal(this, 1);" href="javascript:;"> View </a>
                                                </div><!-- // Col // -->
                                                <div class="col-md-1">
                                                    <label for="riskDis" class="labelholder">
                                                        <input type="checkbox" class="form-control validate" name="riskDis" id="riskDis" required="required" value="1" data-toggle="tooltip" title="" data-original-title="Day Trading Risk Disclosure is required">
                                                        <span class="checkmark"></span>
                                                    </label>
                                                </div>
                                                <div class="col-md-4 viewholder-txt">
                                                    <p> I provide my consent</p>
                                                </div><!-- // Col // -->

                                            </div><!-- // Row // -->

                                            <div class="row">

                                                <div class="col-md-5">
                                                    <label class="labelholder" style="padding-left:0px">Penny Stocks
                                                        Disclosure
                                                        <span class="text-danger"> *</span> </label>
                                                </div>
                                                <div class="col-md-1 viewholder-txt"><a onclick="showFileModal(this, 2);" href="javascript:;"> View </a>
                                                </div>
                                                <div class="col-md-1">
                                                    <label for="pennyStocks" class="labelholder">
                                                        <input type="checkbox" class="form-control validate" name="pennyStocks" id="pennyStocks" required="required" value="1" data-toggle="tooltip" title="" data-original-title="Penny Stocks Disclosure is required">
                                                        <span class="checkmark"></span>
                                                </label></div>
                                                <div class="col-md-4 viewholder-txt">
                                                    <p> I provide my consent </p>
                                                </div>
                                            </div><!-- // Row // -->

                                            <div class="row">

                                                <div class="col-md-5">
                                                    <label class="labelholder" style="padding-left:0px">Electronic
                                                        Access &amp; Trading
                                                        Agreement <span class="text-danger"> *</span> </label>
                                                </div><!-- // Col // -->
                                                <div class="col-md-1 viewholder-txt"><a onclick="showFileModal(this, 3);" href="javascript:;"> View </a>
                                                </div><!-- // Col // -->
                                                <div class="col-md-1">
                                                    <label for="electronicAccess" class="labelholder">
                                                        <input type="checkbox" class="form-control validate" name="electronicAccess" id="electronicAccess" required="required" data-toggle="tooltip" title="" value="1" data-original-title="Electronic Access &amp; Trading Agreement is required">
                                                        <span class="checkmark"></span>
                                                    </label>
                                                </div><!-- // Col // -->
                                                <div class="col-md-4 viewholder-txt">
                                                    <p>I provide my consent</p>
                                                </div><!-- // Col // -->
                                            </div><!-- // Row // -->

                                            <div class="row">

                                                <div class="col-md-5">
                                                    <label class="labelholder" style="padding-left:0px">Margin
                                                        Disclosure Statement
                                                        <span class="text-danger"> *</span> </label>
                                                </div>
                                                <div class="col-md-1 viewholder-txt"> <a onclick="showFileModal(this, 4);" href="javascript:;"> View </a>
                                                </div>
                                                <div class="col-md-1">
                                                    <label for="marginDisclosure" class="labelholder">
                                                        <input type="checkbox" name="marginDisclosure" class="form-control validate" required="required" id="marginDisclosure" value="1" data-toggle="tooltip" title="" data-original-title="Margin Disclosure Statement is required">
                                                        <span class="checkmark"></span>
                                                    </label>
                                                </div>
                                                <div class="col-md-4 viewholder-txt">
                                                    <p>I provide my consent</p>
                                                </div>
                                            </div><!-- // Row // -->

                                            <div class="row">

                                                <div class="col-md-5">
                                                    <label class="labelholder" style="padding-left:0px">W-9
                                                        Certification <span class="text-danger"> *</span> </label>
                                                </div>
                                                <div class="col-md-1 viewholder-txt"> <a onclick="showFileModal(this, 5);" href="javascript:;"> View </a>
                                                </div>
                                                <div class="col-md-1">
                                                    <label for="w9_Certification" class="labelholder">
                                                        <input type="checkbox" name="w9_Certification" class="form-control validate" required="required" id="w9_Certification" value="1" data-toggle="tooltip" title="" data-original-title="W-9 Certification is required">
                                                        <span class="checkmark"></span>
                                                    </label>
                                                </div>
                                                <div class="col-md-4 viewholder-txt">
                                                    <p>I provide my consent</p>
                                                </div>
                                            </div><!-- // Row // -->

                                            <div class="row">

                                                <div class="col-md-5">
                                                    <label class="labelholder" style="padding-left:0px">Stock Locate
                                                        Agreement <span class="text-danger"> *</span> </label>
                                                </div>
                                                <div class="col-md-1 viewholder-txt"> <a onclick="showFileModal(this, 6);" href="javascript:;"> View </a>
                                                </div>
                                                <div class="col-md-1">
                                                    <label for="stock_Locate" class="labelholder">
                                                        <input type="checkbox" name="stock_Locate" class="form-control validate" required="required" id="stock_Locate" value="1" data-toggle="tooltip" title="" data-original-title="Understanding Online Trading Risk Disclosure is required">
                                                        <span class="checkmark"></span>
                                                    </label>
                                                </div>
                                                <div class="col-md-4 viewholder-txt">
                                                    <p>I provide my consent</p>
                                                </div>
                                            </div><!-- // Row // -->

                                            <div class="row">
                                                <div class="col-md-5">
                                                    <label class="labelholder" style="padding-left:0px"> Margin
                                                        Agreement <span class="text-danger"> *</span> </label>
                                                </div>
                                                <div class="col-md-1 viewholder-txt"> <a onclick="showFileModal(this, 7);" href="javascript:;"> View </a>
                                                </div>
                                                <div class="col-md-1">
                                                    <label for="marginDisc" class="labelholder">
                                                        <input type="checkbox" class="form-control validate" name="marginDisc" required="required" id="marginDisc" value="1" data-toggle="tooltip" title="" data-original-title="Margin Disclosure is required">
                                                        <span class="checkmark"></span>
                                                    </label>
                                                </div>
                                                <div class="col-md-4 viewholder-txt">
                                                    <p> I provide my consent</p>
                                                </div>
                                            </div><!-- // Row // -->


                                            <div class="lastbox-step-7">
                                                <p>Consent for mail delivery of statements and confirms otherwise they
                                                    will be
                                                    delivered electronically </p>
                                                <h6>Additional charges will apply if you do NOT check the below box for
                                                    electronic
                                                    delivery of statements, confirmations and tax documents</h6>
                                                <label for="confirmedElectronic" class="labelholder" style="padding-left:35px">
                                                    <input type="checkbox" class="form-control" name="confirmedElectronic" id="confirmedElectronic" value="1" checked="">
                                                    Please check this box if you wish only to receive communications
                                                    electronically,
                                                    including trade confirmations, prospectuses, account statements,
                                                    proxy
                                                    materials, tax‐related documents, and marketing and sales documents.
                                                    If you do
                                                    not check this box, all such Communications will be delivered to you
                                                    by standard
                                                    mail.
                                                    <span class="checkmark"></span>

                                                </label>

                                                <br>
                                                <div style="border:solid 1px black;"></div>
                                                <br>
                                                <p>By signing below, I/We attest to the accuracy of the information
                                                    provided on this
                                                    form. I/We acknowledge that we have received, read and agree to the
                                                    terms and
                                                    conditions contained in the attached Account Agreement, including
                                                    the
                                                    arbitration clause. By executing this agreement, I/We agree to be
                                                    bound by the
                                                    terms and conditions contained here in. </p>
                                                <div class="row">
                                                    <div class="col-md-4">
                                                        <div class="signature-error"></div>
                                                        <h6> ACCOUNT OWNER(S):<small>Signature</small> </h6>
                                                        <p>
                                                            <button type="button" id="signature" class="btn btn-primary"> Signature
                                                            </button></p>
                                                        <br>
                                                        <img id="signatureName" src="">
                                                    </div>


                                                </div>
                                                <br>
                                            </div>
                                        </div>
                                        <input type="hidden" name="isDraft" value="0">
                                    </div>

                                  </div>
                              </div>

                              <input type="button" name="previous" class="previous action-button-previous" value="Previous">
                              <input type="button" id="stepbox11-btn" class="next action-button" value="Submit">

                          </form>
                            </fieldset>
                        </div>
                          
                        </div>
                      </div>
                    </div>
                  </section>


            </div>
        </div>
        <!--Main Content End-->
        <!--Footer-->
        <div class="container-fluid">
            <div class="row footerbg">
                <div class="container">
                    <div class="row footerlinks">
                        <div class="col-md-2">
                            <h2>QUICK LINK</h2>
                            <ul>
                                <li><a href="#">About Us</a></li>
                                <li><a href="#">Pricing</a></li>
                                <li><a href="#">Contact Us</a></li>
                                <li><a href="#">Platforms</a></li>
                            </ul>
                        </div>
                        <div class="col-md-2">
                            <h2>LEGAL</h2>
                            <ul>
                                <li><a href="#">Privacy Policy</a></li>
                                <li><a href="#">Frequently Asked Questions</a></li>
                            </ul>
                        </div>
                        <div class="col-md-2">
                            <h2>SERVICES</h2>
                            <ul>
                                <li><a href="#">Trading</a></li>
                                <li><a href="#">Clearing</a></li>
                                <li><a href="#">Locates</a></li>
                                <li><a href="#">Lending</a></li>
                            </ul>
                        </div>
                        <div class="col-md-3">
                            <h2>CONTACT</h2>
                            <ul>
                                <li><a href=""><i class="fa fa-phone" data-name="phone-alt"></i> 844-963-1512</a></li>
                                <li><a href="mailto:info@guardiantrading.com"><i class="fa fa-envelope" data-name="envelope"></i>info@guardiantrading.com</a></li>
                                <li><a href="https://www.google.com/search?ei=XN1mXuqaEYP5tAakwLvACQ&amp;q=1301+ROUTE+36+STE+109%2C+HAZLET%2C+NJ+07730&amp;oq=1301+ROUTE+36+STE+109%2C+HAZLET%2C+NJ+07730&amp;gs_l=psy-ab.3..33i299.14462.14462..15117...0.0..0.153.356.1j2......0....1..gws-wiz.9uZ6bVXpDLE&amp;ved=0ahUKEwjquMW50Y7oAhWDPM0KHSTgDpgQ4dUDCAs&amp;uact=5" target="_blank"><i class="fa fa-map-marker" data-name="map-marker-alt"></i> 1301
                                        ROUTE 36 STE 109, HAZLET, NJ 07730</a></li>
                            </ul>
                        </div>
                        <div class="col-md-3">
                            <h2>SUBSCRIBE</h2>
                        </div>
                    </div><!-- // Row // -->
                </div><!-- // Container  // -->
            </div><!-- // Row // -->
        </div>
        <!--Footer End-->
        <div class="modal fade" id="file_modal" tabindex="-1" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
            <div class="modal-dialog" role="document" style="max-width: 1000px !important;">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title"></h5>
    
                    </div>
                    <div class="modal-body">
                        <p>ASEES</p>
                        <input type="hidden" class="el_id">
                        <iframe style="width:100%; height:500px;" frameborder="0"></iframe>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
                    </div>
                </div>
            </div>
        </div>
        <script src="{{asset('js/application-script.js')}}"></script>
        <script>
      jQuery(document).ready(function () {
  // click on next button
  jQuery(".form-wizard-next-btn").click(function () {
    var parentFieldset = jQuery(this).parents(".wizard-fieldset");
    var currentActiveStep = jQuery(this)
      .parents(".form-wizard")
      .find(".form-wizard-steps .active");
    var next = jQuery(this);
    var nextWizardStep = true;
    parentFieldset.find(".wizard-required").each(function () {
      var thisValue = jQuery(this).val();

      if (thisValue == "") {
        jQuery(this).siblings(".wizard-form-error").slideDown();
        nextWizardStep = false;
      } else {
        jQuery(this).siblings(".wizard-form-error").slideUp();
      }
    });
    if (nextWizardStep) {
      next.parents(".wizard-fieldset").removeClass("show", "400");
      currentActiveStep
        .removeClass("active")
        .addClass("activated")
        .next()
        .addClass("active", "400");
      next
        .parents(".wizard-fieldset")
        .next(".wizard-fieldset")
        .addClass("show", "400");
      jQuery(document)
        .find(".wizard-fieldset")
        .each(function () {
          if (jQuery(this).hasClass("show")) {
            var formAtrr = jQuery(this).attr("data-tab-content");
            jQuery(document)
              .find(".form-wizard-steps .form-wizard-step-item")
              .each(function () {
                if (jQuery(this).attr("data-attr") == formAtrr) {
                  jQuery(this).addClass("active");
                  var innerWidth = jQuery(this).innerWidth();
                  var position = jQuery(this).position();
                  jQuery(document)
                    .find(".form-wizard-step-move")
                    .css({ left: position.left, width: innerWidth });
                } else {
                  jQuery(this).removeClass("active");
                }
              });
          }
        });
    }
  });
  //click on previous button
  jQuery(".form-wizard-previous-btn").click(function () {
    var counter = parseInt(jQuery(".wizard-counter").text());
    var prev = jQuery(this);
    var currentActiveStep = jQuery(this)
      .parents(".form-wizard")
      .find(".form-wizard-steps .active");
    prev.parents(".wizard-fieldset").removeClass("show", "400");
    prev
      .parents(".wizard-fieldset")
      .prev(".wizard-fieldset")
      .addClass("show", "400");
    currentActiveStep
      .removeClass("active")
      .prev()
      .removeClass("activated")
      .addClass("active", "400");
    jQuery(document)
      .find(".wizard-fieldset")
      .each(function () {
        if (jQuery(this).hasClass("show")) {
          var formAtrr = jQuery(this).attr("data-tab-content");
          jQuery(document)
            .find(".form-wizard-steps .form-wizard-step-item")
            .each(function () {
              if (jQuery(this).attr("data-attr") == formAtrr) {
                jQuery(this).addClass("active");
                var innerWidth = jQuery(this).innerWidth();
                var position = jQuery(this).position();
                jQuery(document)
                  .find(".form-wizard-step-move")
                  .css({ left: position.left, width: innerWidth });
              } else {
                jQuery(this).removeClass("active");
              }
            });
        }
      });
  });
  //click on form submit button
  jQuery(document).on("click", ".form-wizard .form-wizard-submit", function () {
    var parentFieldset = jQuery(this).parents(".wizard-fieldset");
    var currentActiveStep = jQuery(this)
      .parents(".form-wizard")
      .find(".form-wizard-steps .active");
    parentFieldset.find(".wizard-required").each(function () {
      var thisValue = jQuery(this).val();
      if (thisValue == "") {
        jQuery(this).siblings(".wizard-form-error").slideDown();
      } else {
        jQuery(this).siblings(".wizard-form-error").slideUp();
      }
    });
  });
  // focus on input field check empty or not
  jQuery(".form-control")
    .on("focus", function () {
      var tmpThis = jQuery(this).val();
      if (tmpThis == "") {
        jQuery(this).parent().addClass("focus-input");
      } else if (tmpThis != "") {
        jQuery(this).parent().addClass("focus-input");
      }
    })
    .on("blur", function () {
      var tmpThis = jQuery(this).val();
      if (tmpThis == "") {
        jQuery(this).parent().removeClass("focus-input");
        jQuery(this).siblings(".wizard-form-error").slideDown("3000");
      } else if (tmpThis != "") {
        jQuery(this).parent().addClass("focus-input");
        jQuery(this).siblings(".wizard-form-error").slideUp("3000");
      }
    });
});
function showFileModal(el, index) {
    let element = $(el);
    let modal = $('#file_modal');
    modal.find('.modal-title').text(checkBoxes[index].title);
    modal.find('.el_id').val(checkBoxes[index].key);
    let file_path = link + checkBoxes[index].file;
    if (checkBoxes[index].file == '') {
        file_path = null;
    }
    modal.find('iframe').attr('src', file_path);
    modal.attr('dismissed', false)
    modal.modal('show');
}
function valueChanged()
        {
            if($('.mailingaddress').is(":checked"))   
                $(".mailingdiv").show();
            else
                $(".mailingdiv").hide();
        }
    </script>
    <script type="text/javascript">
        function valueChangedthree()
        {
            if($('.mailingaddressthree').is(":checked"))   
                $(".mailingdivthree").show();
            else
                $(".mailingdivthree").hide();
        }
    </script>
    <script type="text/javascript">
        function valueChangedtwo()
        {
            if($('.trusted_contact').is(":checked"))   
                $(".trusteddiv").show();
            else
                $(".trusteddiv").hide();
        }
    </script>
    </body>

    
</html>
