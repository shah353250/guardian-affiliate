@extends('layouts.customer.login')

@section('content')
<section class="wpb_loader">
	<div class="loader"></div>
</section>

<div class="container-fluid" style="background-color: #000">
	<div class="container">
		<br /><br /><br />
		<!-- Rigister form -->
		<div class="row justify-content-center">
			<div class="col-md-8">
				<form method="POST" id="ajaxLoginSubmit" action="{{ route('customer.login-submit') }}">
					@csrf
					<div class="reg_form">
						<div class="col-md-12 logoholder mb-3">
							<img src="{{ asset('assets_customer/img/logo1.png')}}" alt="Guardian" />
						</div>
						<h3 class="text-center"><b>User Sign In</b></h3>
						<p class="text-center">
							Don't have an account? <a href="{{route('customer.register')}}">Sign up...</a>
						</p>
						<hr />
						<input id="email" type="email" class="form-control " placeholder="Email" name="email" value="{{ old('email') }}" autocomplete="email" autofocus>
						<hr />
						<input id="password" type="password" class="form-control" placeholder="Password" name="password">
						<hr />
						<button type="submit" class="btn btn-primary" id="loginBtn">
							{{ __('Login') }}
							<span class="loader" style="display: none">
								<div class="spinner-border text-light" role="status" style="width: 1rem;height: 1rem;">
									<span class="sr-only">Loading...</span>
								</div>
							</span>
						</button>
						<div class="mt-3">
							<a href="{{route('forgot-password')}}">Forgot Password?</a>
						</div>
					</div>
				</form>
			</div>
			
		</div>
	</div>
</div>
@endsection

@push('scripts')
	<script>
		$('#ajaxLoginSubmit').on('submit',function(e){
			e.preventDefault()
			formData = new FormData(this)
			submitbutton = document.querySelector('#loginBtn');
			$.ajax({
				url: "{{route('customer.login-submit')}}",
				type: "POST",
				data:  formData,
				contentType: false,
				cache: false,
				processData:false,
				beforeSend : function()
				{
					submitbutton.setAttribute("disabled","disabled");
					submitbutton.children[0].style.display = 'inline';
				},
				success: function(data)
				{
					// console.log(data);
					if($.isEmptyObject(data.error)){
						if(!$.isEmptyObject(data.url))
						{
							window.location.href = data.url;
						}
						if(!$.isEmptyObject(data.success))
						{
							window.location.href = data.url;
						}
						// $(redirect).tab('show')
					}else{
						error(data.error.join("<br>"))
					}
				},
				error: function(e) 
				{
					console.log(e);
				},
				complete:function(){
					submitbutton.removeAttribute("disabled");
					submitbutton.children[0].style.display = 'none';
				}      
			});
		});
	</script>
@endpush