@extends('layouts.customer.app')

@section('content')
<style>
    .invalid-field {
        border-bottom: 2px red solid !important;
    }

    .form-control {
        font-weight: 300;
        height: auto !important;
        padding: 15px;
        color: #000000;
        background-color: #fff;
        /* border: none; */
        box-shadow: 0 0 2px #00000073;
        border-radius: 0px;
    }

    .form-control:valid {
        background-image: none !important;
    }

    .card-header {
        background-color: white;
        border-bottom: none;
    }

    .custom-control {
        padding-left: 15px;
    }

    .card.mt-2 {
        border: 0 none;
        border-radius: 0px;
        box-shadow: 0 0 15px 1px rgb(0 0 0 / 40%);
        padding: 5px 30px;
        box-sizing: border-box;
    }

    .card-body {
        padding: 0px;
    }

    .card-header {
        padding: .75rem 0rem;
    }

    .custom-control-label::before {
        background-color: #eee;
    }

    .custom-checkbox .custom-control-label::before {
        border-radius: 0px;
    }

    .custom-control-input:focus~.custom-control-label::before {
        box-shadow: none;
    }

    label.custom-control-label-cursor {
        cursor: pointer;
        padding-left: 20px;
    }

    .custom-control-label::after,
    .custom-control-label::before {
        width: 25px;
        height: 25px;
        top: 0;
        left: -15px;
    }

    .boxed label {
        display: inline-block;
        border: solid 2px #ccc;
        transition: all 0.3s;
        color: #888;
        cursor: pointer;
        width: 100px;
        line-height: normal;
        padding: 10px 0;
        text-align: center;
    }

    .boxed input[type="radio"] {
        display: none;
    }

    .boxed input[type="radio"]:checked+label {
        border: solid 2px #4290CA;
        background: #4290CA;
        color: white;
    }

    .radiobox_label {
        display: inline !important;
        position: relative;
        padding-left: 35px;
        margin-bottom: 12px;
        cursor: pointer;
        font-size: 18px;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        width: 100%;
    }

    .radiobox_label input {
        position: absolute;
        opacity: 1;
        cursor: pointer;
        height: 100%;
        width: 30px;
        left: 0;
    }

    .checkbox_form_control {
        width: 25px;
        height: 25px !important;
        margin: 15px auto;
    }
</style>
@php
    $phone_lenght               = 30;
    $zip_length                 = 15;
    $city_length                = 50;
    $name_length               = 30;
    $company_name_length        = 30;
    $address_length             = 250;
    $suite_length               = 30;

    $dept_number_length         = 3; // Year with Employer
    $account_length             = 40;
    $default_length             = 30;
    $swift_length               = 50;
    $bank_name_length           = 100;
    $occupation_length          = 50;
@endphp
<div class="form-wizard container-fluid">
    <div class="form-wizard-header ">
        <ul class="list-unstyled form-wizard-steps clearfix">
            <li class="active"><span>1</span>
                <p class="form-name">PERSONAL DETAILS</p>
            </li>
            <li><span>2</span>
                <p class="form-name">PROFESSIONAL DETAILS</p>
            </li>
            <li><span>3</span>
                <p class="form-name">ID INFORMATION</p>
            </li>
            <li><span>4</span>
                <p class="form-name">INCOME DETAILS</p>
            </li>
            <li><span>5</span>
                <p class="form-name">FUNDING DETAILS</p>
            </li>
            <li><span>6</span>
                <p class="form-name">RISK ACCEPTANCE</p>
            </li>
            <li><span>7</span>
                <p class="form-name">FINANCIAL SITUATION</p>
            </li>
            <li><span>8</span>
                <p class="form-name">INVESTMENT EXPEIRENCE</p>
            </li>
            <li><span>9</span>
                <p class="form-name">IDENTIFICATION PROOF UPLOAD</p>
            </li>
            <li><span>10</span>
                <p class="form-name">DISCLOSURES</p>
            </li>
            <li><span>11</span>
                <p class="form-name">SIGNATURES</p>
            </li>
        </ul>
    </div>
</div>
<div class="container mt-4 ">
    <div class="form-wizard" style="display: none">
        <div class="form-wizard-header">
            <ul class="nav nav-tabs list-unstyled form-wizard-steps clearfix" id="myTab" role="tablist">
                <li class="nav-item" role="presentation">
                    <span>1</span><a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Personal Details</a>
                </li>
                <li class="nav-item" role="presentation">
                    <span>2</span><a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Professional Details</a>
                </li>
                <li class="nav-item" role="presentation">
                    <span>3</span><a class="nav-link" id="idinformation-tab" data-toggle="tab" href="#idinformation" role="tab" aria-controls="idinformation" aria-selected="false">ID Information</a>
                </li>
                <li class="nav-item" role="presentation">
                    <span>4</span><a class="nav-link" id="income_detail-tab" data-toggle="tab" href="#income_detail" role="tab" aria-controls="income_detail" aria-selected="false">Income Detail</a>
                </li>
                <li class="nav-item" role="presentation">
                    <span>5</span><a class="nav-link" id="funding_detail-tab" data-toggle="tab" href="#funding_detail" role="tab" aria-controls="funding_detail" aria-selected="false">Funding Detail</a>
                </li>
                <li class="nav-item" role="presentation">
                    <span>6</span><a class="nav-link" id="risk_acceptance-tab" data-toggle="tab" href="#risk_acceptance" role="tab" aria-controls="risk_acceptance" aria-selected="false">Risk Acceptance</a>
                </li>

                <li class="nav-item" role="presentation">
                    <span>7</span><a class="nav-link" id="financial_situation-tab" data-toggle="tab" href="#financial_situation" role="tab" aria-controls="financial_situation" aria-selected="false">Financial Situation</a>
                </li>
                <li class="nav-item" role="presentation">
                    <span>8</span><a class="nav-link" id="investment_experience-tab" data-toggle="tab" href="#investment_experience" role="tab" aria-controls="investment_experience" aria-selected="false">Investment Experience</a>
                </li>
                <li class="nav-item" role="presentation">
                    <span>9</span><a class="nav-link" id="identification_proof-tab" data-toggle="tab" href="#identification_proof" role="tab" aria-controls="identification_proof" aria-selected="false">Identification Proof</a>
                </li>
                <li class="nav-item" role="presentation">
                    <span>10</span><a class="nav-link" id="disclouser-tab" data-toggle="tab" href="#disclouser" role="tab" aria-controls="disclouser" aria-selected="false">Disclosure</a>
                </li>
                <li class="nav-item" role="presentation">
                    <span>11</span><a class="nav-link" id="signature-tab" data-toggle="tab" href="#signature" role="tab" aria-controls="signature" aria-selected="false">Signature</a>
                </li>
                <li class="nav-item" role="presentation" style="display:none">
                    <span>12</span><a class="nav-link" id="thankyou-tab" data-toggle="tab" href="#thankyou" role="tab" aria-controls="signature" aria-selected="false">Thanky You</a>
                </li>
            </ul>
        </div>
    </div>


    <div class="tab-content mb-5" id="myTabContent">
        <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
            <div class="card mt-2">
                <div class="card-header">
                    <h4 class="fs-title" style="margin:0px;">Personal Details</h4>
                </div>
                <div class="card-body">
                    <form action="" method="post" class="row" id="steponeForm">
                        @csrf
                        <div class="col-md-5">
                            <label for="">First Name<span class="text-danger">*</span></label>
                            <input type="text" name="first_name" id="first_name" value="{{@Auth::user()->personalDetail->first_name}}" class="form-control" data-toggle="tooltip" data-placement="top" maxlength="{{$name_length}}" onkeydown="return alphaOnly(event);">
                        </div>
                        <div class="col-md-5">
                            <label for="">Last Name<span class="text-danger">*</span></label>
                            <input type="text" name="last_name" value="{{@Auth::user()->personalDetail->last_name}}" id="last_name" class="form-control" data-toggle="tooltip" data-placement="top" maxlength="{{$name_length}}" onkeydown="return alphaOnly(event);">
                        </div>
                        <div class="col-md-8">
                            <label for="">Address<span class="text-danger">*</span></label>
                            <input type="text" name="address" value="{{@Auth::user()->personalDetail->address}}" id="address" class="form-control" data-toggle="tooltip" data-placement="top" maxlength="{{$address_length}}">
                        </div>

                        <div class="col-md-4">
                            <label for="">Apt/Suite</label>
                            <input type="text" name="apt_suite" value="{{@Auth::user()->personalDetail->apt_suite}}" id="apt_suite" class="form-control" data-toggle="tooltip" data-placement="top" maxlength="{{$suite_length}}">
                        </div>
                        <div class="col-md-5">
                            <label for="">Country<span class="text-danger">*</span></label>
                            <select name="country" id="country" class="form-control" data-toggle="tooltip" data-placement="top">
                                <option value>Select Country</option>
                                @foreach ($countries as $item)
                                <option value="{{$item->id}}" {{@Auth::user()->personalDetail->country_id == $item->id?'selected':''}}>{{$item->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-3">
                            <label for="">States/Province<span class="text-danger">*</span></label>
                            <select name="state" id="state" class="form-control" data-toggle="tooltip" data-placement="top">
                                <option value>Select States</option>
                                @isset(Auth::user()->personalDetail)
                                @php
                                $states =\App\Models\State::where('country_id',@Auth::user()->personalDetail->country_id)->get();
                                @endphp
                                @foreach ($states as $item)
                                <option value="{{$item->id}}" {{@Auth::user()->personalDetail->state_id == $item->id?'selected':''}}>{{$item->name}}</option>
                                @endforeach
                                @endisset
                            </select>
                        </div>

                        <div class="col-md-2">
                            <label for="">City<span class="text-danger">*</span></label>
                            <input type="text" name="city" value="{{@Auth::user()->personalDetail->city}}" id="city" class="form-control" data-toggle="tooltip" data-placement="top" maxlength="{{$city_length}}">
                        </div>
                        <div class="col-md-2">
                            <label for="">Zip Code<span class="text-danger">*</span></label>
                            <input type="text" name="zip_code" id="zip_code" value="{{@Auth::user()->personalDetail->zip_code}}" class="form-control" data-toggle="tooltip" data-placement="top" maxlength="{{$zip_length}}">
                        </div>
                        <div class="col-12 my-3">
                            <div class="custom-control custom-checkbox mr-sm-2">
                                <input type="checkbox" name="mailing" class="custom-control-input" value="1" id="mailing" {{@Auth::user()->personalDetail->is_mailing_address ==1?"checked":""}}>
                                <label class="custom-control-label custom-control-label-cursor" for="mailing">Mailing Address (If Different)</label>
                            </div>
                        </div>
                        <div id="mailing-div" class="col-12" style="{{@Auth::user()->personalDetail->is_mailing_address == 0?'display: none':''}}">
                            <div class="row">
                                <div class="col-md-8">
                                    <label for="">Address<span class="text-danger">*</span></label>
                                    <input type="text" name="mailing_address" value="{{@Auth::user()->mailing->address}}" id="mailing_address" class="form-control" data-toggle="tooltip" data-placement="top" maxlength="{{$address_length}}">
                                </div>

                                <div class="col-md-4">
                                    <label for="">Apt/Suite</label>
                                    <input type="text" name="mailing_apt_suite" value="{{@Auth::user()->mailing->apt_suite}}" id="mailing_apt_suite" class="form-control" maxlength="{{$suite_length}}">
                                </div>
                                <div class="col-md-5">
                                    <label for="">Country<span class="text-danger">*</span></label>
                                    <select name="mailing_country" id="mailing_country" class="form-control" data-toggle="tooltip" data-placement="top">
                                        <option value>Select Country</option>
                                        @foreach ($countries as $item)
                                        <option value="{{$item->id}}" {{@Auth::user()->mailing->country_id == $item->id?'selected':''}}>{{$item->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <label for="">States/Province<span class="text-danger">*</span></label>
                                    <select name="mailing_state" id="mailing_state" class="form-control" data-toggle="tooltip" data-placement="top">
                                        <option value>Select States</option>
                                        @isset(Auth::user()->mailing)
                                        @php
                                        $states =\App\Models\State::where('country_id',@Auth::user()->mailing->country_id)->get();
                                        @endphp
                                        @foreach ($states as $item)
                                        <option value="{{$item->id}}" {{@Auth::user()->mailing->state_id == $item->id?'selected':''}}>{{$item->name}}</option>
                                        @endforeach
                                        @endisset
                                    </select>
                                </div>

                                <div class="col-md-2">
                                    <label for="">City<span class="text-danger">*</span></label>
                                    <input type="text" name="mailing_city" value="{{@Auth::user()->mailing->city}}" id="mailing_city" class="form-control" data-toggle="tooltip" data-placement="top" maxlength="{{$city_length}}">
                                </div>
                                <div class="col-md-2">
                                    <label for="">Zip Code<span class="text-danger">*</span></label>
                                    <input type="text" name="mailing_zip_code" value="{{@Auth::user()->mailing->zip_code}}" id="mailing_zip_code" class="form-control" data-toggle="tooltip" data-placement="top" maxlength="{{$zip_length}}">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <label for="">Phone Number<span class="text-danger">*</span></label>
                            <input type="text" name="phone_number" value="{{@Auth::user()->personalDetail->phone_number}}" id="phone_number" class="form-control" data-toggle="tooltip" data-placement="top" maxlength="{{$phone_lenght}}" onkeyup="return phoneNumberValidation(this);">
                            <span style="color: #888;font-size: 14px;">Phone numbers are checked for validity in the country that your are applying</span>
                        </div>
                        <div class="col-md-6">
                            <label for="">Number of Dependents<span class="text-danger">*</span></label>
                            <input type="text" name="number_of_dependents" value="{{@Auth::user()->personalDetail->number_of_dependents}}" id="number_of_dependents" class="form-control" data-toggle="tooltip" data-placement="top" maxlength="{{$dept_number_length}}" onkeydown="return enforceFormat(event);">
                        </div>

                        <div class="col-12 my-3 boxed">
                            <span style="color: #888;">Are You:</span> <span class="text-danger">*</span>
                            <div class="form-check form-check-inline">
                                <input type="radio" id="inlineRadio1" name="marital_status" value="single" {{@Auth::user()->personalDetail->marital_status == "single" || Auth::user()->personalDetail == null ?"checked":""}}>
                                <label for="inlineRadio1">Single</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input type="radio" id="inlineRadio2" name="marital_status" value="married" {{@Auth::user()->personalDetail->marital_status == "married" ?"checked":""}}>
                                <label for="inlineRadio2">Married</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="marital_status" id="inlineRadio3" value="divorced" {{@Auth::user()->personalDetail->marital_status == "divorced" ?"checked":""}}>
                                <label class="form-check-label" for="inlineRadio3">Divorced</label>
                            </div>
                            <div class="form-check form-check-inline">
                                <input class="form-check-input" type="radio" name="marital_status" id="inlineRadio4" value="widowed" {{@Auth::user()->personalDetail->marital_status == "widowed" ?"checked":""}}>
                                <label class="form-check-label" for="inlineRadio4">Widowed</label>
                            </div>
                        </div>
                        <div class="col-12 my-3">
                            <div class="custom-control custom-checkbox mr-sm-2">
                                <input type="checkbox" name="contact_person" class="custom-control-input" id="contact_person" {{@Auth::user()->personalDetail->is_trusted_contact_person ==1?"checked":""}}>
                                <label class="custom-control-label custom-control-label-cursor" for="contact_person">Would you like to add a Trusted Contact Person</label>
                            </div>
                        </div>
                        <div id="contact-person-div" class="col-12" style="{{@Auth::user()->personalDetail->is_trusted_contact_person ==0?"display: none":""}}">
                            <div class="row">
                                <div class="col-md-4">
                                    <label for="">Name<span class="text-danger">*</span></label>
                                    <input type="text" name="contact_person_name" value="{{@Auth::user()->contact_person->name}}" id="contact_person_name" class="form-control" data-toggle="tooltip" data-placement="top" maxlength="{{$name_length}}" onkeydown="return alphaOnly(event);">
                                </div>
                                <div class="col-md-4">
                                    <label for="">Phone Number<span class="text-danger">*</span></label>
                                    <input type="text" name="contact_person_phone" value="{{@Auth::user()->contact_person->phone}}" id="contact_person_phone" class="form-control" data-toggle="tooltip" data-placement="top" maxlength="{{$phone_lenght}}" onkeyup="return phoneNumberValidation(this);">
                                </div>
                                <div class="col-md-4">
                                    <label for="">Email Address<span class="text-danger">*</span></label>
                                    <input type="text" name="contact_person_email" value="{{@Auth::user()->contact_person->email}}" id="contact_person_email" class="form-control" data-toggle="tooltip" data-placement="top" onkeyup="return ValidateEmail(this,'contact_person_email_validation_msg');">
                                    <small id="contact_person_email_validation_msg" class=""></small>
                                </div>
                                <div class="col-md-12">
                                    <label for="">Street Address<span class="text-danger">*</span></label>
                                    <input type="text" name="contact_person_address" value="{{@Auth::user()->contact_person->street_address}}" id="contact_person_address" class="form-control" data-toggle="tooltip" data-placement="top" maxlength="{{$address_length}}">
                                </div>
                                <div class="col-md-5">
                                    <label for="">Country<span class="text-danger">*</span></label>
                                    <select name="contact_person_country" id="contact_person_country" class="form-control" data-toggle="tooltip" data-placement="top">
                                        <option value>Select Country</option>
                                        @foreach ($countries as $item)
                                        <option value="{{$item->id}}" {{@Auth::user()->contact_person->country_id == $item->id?'selected':''}}>{{$item->name}}</option>
                                        @endforeach
                                    </select>

                                </div>
                                <div class="col-md-3">
                                    <label for="">States/Province<span class="text-danger">*</span></label>
                                    <select name="contact_person_state" id="contact_person_state" class="form-control" data-toggle="tooltip" data-placement="top">
                                        <option value>Select States</option>
                                        @isset(Auth::user()->personalDetail)
                                        @php
                                        $states =\App\Models\State::where('country_id',@Auth::user()->contact_person->country_id)->get();
                                        @endphp
                                        @foreach ($states as $item)
                                        <option value="{{$item->id}}" {{@Auth::user()->contact_person->state_id == $item->id?'selected':''}}>{{$item->name}}</option>
                                        @endforeach
                                        @endisset
                                    </select>
                                </div>

                                <div class="col-md-2">
                                    <label for="">City<span class="text-danger">*</span></label>
                                    <input type="text" name="contact_person_city" value="{{@Auth::user()->contact_person->city}}" id="contact_person_city" class="form-control" data-toggle="tooltip" data-placement="top" maxlength="{{$city_length}}">
                                </div>
                                <div class="col-md-2">
                                    <label for="">Zip Code<span class="text-danger">*</span></label>
                                    <input type="text" name="contact_person_zip_code" value="{{@Auth::user()->contact_person->zip_code}}" id="contact_person_zip_code" class="form-control" data-toggle="tooltip" data-placement="top" maxlength="{{$zip_length}}">
                                </div>

                                <div class="col-md-4">
                                    <label for="">Relationship to Account Holder<span class="text-danger">*</span></label>
                                    <input type="text" name="contact_person_relation_account_holder" value="{{@Auth::user()->contact_person->relation_holder}}" id="contact_person_relation_account_holder" class="form-control" data-toggle="tooltip" data-placement="top" maxlength="{{$default_length}}">
                                </div>
                                <div class="col-md-4">
                                    <label for="">Date of Birth<span class="text-danger">*</span></label>
                                    <input type="date" name="date_of_birth" id="date_of_birth" value="{{@Auth::user()->contact_person->date_of_birth}}" class="form-control" data-toggle="tooltip" data-placement="top">
                                </div>
                            </div>
                        </div>
                        @if (Auth::user()->registration_type == "Corporate" || Auth::user()->registration_type == "Limited Liability Company" || Auth::user()->registration_type == "Limited Partnership")
                        <div class="col-12">
                            <hr>
                            <h4 class="fs-title" style="margin:0px;">Company Detail</h4>
                        </div>
                        <div class="col-12">
                            <div class="row">
                                <div class="col-md-12">
                                    <label for="">Company Name<span class="text-danger">*</span></label>
                                    <input type="text" name="company_name" value="{{@Auth::user()->companyDetail->company_name}}" id="company_name" class="form-control" data-toggle="tooltip" data-placement="top" maxlength="{{$company_name_length}}">
                                </div>
                                <div class="col-md-12">
                                    <label for="">Company Address<span class="text-danger">*</span></label>
                                    <input type="text" name="company_address" value="{{@Auth::user()->companyDetail->company_address}}" id="company_address" class="form-control" data-toggle="tooltip" data-placement="top" maxlength="{{$address_length}}">
                                </div>
                                <div class="col-md-12">
                                    <label for="">Company Apt/Suite<span class="text-danger">*</span></label>
                                    <input type="text" name="company_apt_suite" value="{{@Auth::user()->companyDetail->apt_suite}}" id="company_apt_suite" class="form-control" data-toggle="tooltip" data-placement="top" maxlength="{{$suite_length}}">
                                </div>
                                <div class="col-md-12">
                                    <label for="">Country<span class="text-danger">*</span></label>
                                    <select name="company_country" id="company_country" class="form-control" data-toggle="tooltip" data-placement="top">
                                        <option value>Select Country</option>
                                        @foreach ($countries as $item)
                                        <option value="{{$item->id}}" {{@Auth::user()->companyDetail->country_id == $item->id?'selected':''}}>{{$item->name}}</option>
                                        @endforeach
                                    </select>

                                </div>
                                <div class="col-md-12">
                                    <label for="">States/Province<span class="text-danger">*</span></label>
                                    <select name="company_state" id="company_state" class="form-control" data-toggle="tooltip" data-placement="top">
                                        <option value>Select States</option>
                                        @isset(Auth::user()->companyDetail)
                                        @php
                                        $states =\App\Models\State::where('country_id',@Auth::user()->companyDetail->country_id)->get();
                                        @endphp
                                        @foreach ($states as $item)
                                        <option value="{{$item->id}}" {{@Auth::user()->companyDetail->state_id == $item->id?'selected':''}}>{{$item->name}}</option>
                                        @endforeach
                                        @endisset
                                    </select>
                                </div>

                                <div class="col-md-12">
                                    <label for="">City<span class="text-danger">*</span></label>
                                    <input type="text" name="company_city" value="{{@Auth::user()->companyDetail->city}}" id="company_city" class="form-control" data-toggle="tooltip" data-placement="top" maxlength="{{$city_length}}">
                                </div>
                                <div class="col-md-12">
                                    <label for="">Zip Code<span class="text-danger">*</span></label>
                                    <input type="text" name="company_zip_code" value="{{@Auth::user()->companyDetail->zip_code}}" id="company_zip_code" class="form-control" data-toggle="tooltip" data-placement="top" maxlength="{{$zip_length}}">
                                </div>
                                <div class="col-md-12">
                                    <label for="">Company Email<span class="text-danger">*</span></label>
                                    <input type="text" name="company_email" value="{{@Auth::user()->companyDetail->company_email}}" id="company_email" class="form-control" data-toggle="tooltip" data-placement="top" onkeyup="ValidateEmail(this,'company_email_validation_msg');">
                                    <small id="company_email_validation_msg" class=""></small>
                                </div>
                                <div class="col-md-12">
                                    <label for="">Company Phone#<span class="text-danger">*</span></label>
                                    <input type="text" name="company_phone" value="{{@Auth::user()->companyDetail->company_phone}}" id="company_phone" class="form-control" data-toggle="tooltip" data-placement="top" maxlength="{{$phone_lenght}}" onkeyup="return phoneNumberValidation(this);">
                                </div>
                            </div>
                        </div>
                        @endif
                        <div class="col-12">
                            <button class="btn rounded mt-2 form-wizard-next-btn" type="button" id="btnStepOne">Next
                                <span class="loader" style="display: none">
                                    <div class="spinner-border text-light" role="status" style="width: 1rem;height: 1rem;">
                                        <span class="sr-only">Loading...</span>
                                    </div>
                                </span>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
            <div class="card mt-2">
                <div class="card-header">
                    <h4>Professional Details</h4>
                </div>
                <div class="card-body">
                    <form action="" method="post" class="row" id="steptwoForm">
                        @csrf
                        <input type="hidden" name="second_form_selected_value" id="second_form_selected_value" value="employed">
                        <div class="text-left col-sm-12">
                            <h4>Are You Currently</h4>
                        </div>

                        <div class="text-left col-md-12">
                            <div class="label-icon " style="margin-left: 0;">
                                <label class="radioholder {{ (Auth::user()->professionalDetail == null)? "radiocurrent":((@Auth::user()->professionalDetail->employment_status == "employed")? "radiocurrent":"") }}">
                                    <input type="radio" name="employee" class="employee" value="employed"  {{ (Auth::user()->professionalDetail == null)? "checked":((@Auth::user()->professionalDetail->employment_status == "employed")? "checked":"") }}>
                                    <i class="fa fa-briefcase" aria-hidden="true"></i> Employed
                                    <span class="radiomark"></span>
                                </label>
                                <input type="hidden" name="id" value="6086963bae86990094156fba">
                            </div>
                            
                            <div class="label-icon">
                                <label class="radioholder {{ (Auth::user()->professionalDetail == null)? "radiocurrent":((@Auth::user()->professionalDetail->employment_status == "self employed")? "radiocurrent":"") }}">
                                    <input type="radio" name="employee" class="employee" value="self employed" {{ (Auth::user()->professionalDetail == null)? "":((@Auth::user()->professionalDetail->employment_status == "self employed")? "checked":"") }}> 
                                    <i class="fa fa-user" aria-hidden="true"></i> Self Employed
                                    <span class="radiomark"></span>
                                </label>
                            </div>
                            
                            <div class="label-icon">
                                <label class="radioholder {{ (Auth::user()->professionalDetail == null)? "radiocurrent":((@Auth::user()->professionalDetail->employment_status == "retired")? "radiocurrent":"") }}">
                                    <input type="radio" name="employee" class="employee" value="retired" {{ (Auth::user()->professionalDetail == null)? "":((@Auth::user()->professionalDetail->employment_status == "retired")? "checked":"") }}> 
                                    <i class="fa fa-user-circle-o"></i> Retired
                                    <span class="radiomark"></span>
                                </label>
                            </div>
                        </div>
                        <div class="col-md-12 work_fields">
                            <div class="row">
                                <div class="col-md-8">
                                    <label for="employee_name">Employee Name*</label>
                                    <input type="text" name="employee_name" value="{{@Auth::user()->professionalDetail->name}}" id="employee_name" class="form-control" data-toggle="tooltip" data-placement="top" maxlength="{{$name_length}}" onkeydown="return alphaOnly(event);">
                                </div>
                                <div class="col-md-4">
                                    <label for="occupation">Occupation*</label>
                                    <input type="text" name="occupation" id="occupation" value="{{@Auth::user()->professionalDetail->occupation}}" class="form-control" data-toggle="tooltip" data-placement="top" maxlength="{{$occupation_length}}">
                                </div>
                                <div class="col-md-8">
                                    <label for="employee_address">Address Of Employer*</label>
                                    <input type="text" name="employee_address" value="{{@Auth::user()->professionalDetail->address}}" id="employee_address" class="form-control" data-toggle="tooltip" data-placement="top" maxlength="{{$address_length}}">
                                </div>
                                <div class="col-md-4">
                                    <label for="year_of_employment">Year with Employer*</label>
                                    <input type="text" name="year_of_employment" value="{{@Auth::user()->professionalDetail->year_of_employment}}" id="year_of_employment" class="form-control" data-toggle="tooltip" data-placement="top" maxlength="{{$dept_number_length}}" onkeydown="return enforceFormat(event);">
                                </div>
                                <div class="col-md-4">
                                    <label for="employee_country">Country*</label>
                                    <select name="employee_country" id="employee_country" class="form-control" data-toggle="tooltip" data-placement="top">
                                        <option value>Select Country</option>
                                        @foreach ($countries as $item)
                                        <option value="{{$item->id}}" {{@Auth::user()->professionalDetail->country_id == $item->id?'selected':''}}>{{$item->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <label for="employee_state">State*</label>
                                    <select name="employee_state" id="employee_state" class="form-control" data-toggle="tooltip" data-placement="top">
                                        <option value>Select States</option>
                                        @isset(Auth::user()->professionalDetail)
                                        @php
                                        $states =\App\Models\State::where('country_id',@Auth::user()->professionalDetail->country_id)->get();
                                        @endphp
                                        @foreach ($states as $item)
                                        <option value="{{$item->id}}" {{@Auth::user()->professionalDetail->state_id == $item->id?'selected':''}}>{{$item->name}}</option>
                                        @endforeach
                                        @endisset
                                    </select>
                                </div>
                                <div class="col-md-4">
                                    <label for="employee_city">City*</label>
                                    <input type="text" name="employee_city" id="employee_city" value="{{@Auth::user()->professionalDetail->city}}" class="form-control" data-toggle="tooltip" data-placement="top" maxlength="{{$city_length}}">
                                </div>
                                <div class="col-md-4">
                                    <label for="employee_apt_suite">Apt/Suit No*</label>
                                    <input type="text" name="employee_apt_suite" value="{{@Auth::user()->professionalDetail->apt_suite}}" id="employee_apt_suite" class="form-control" data-toggle="tooltip" data-placement="top" maxlength="{{$suite_length}}">
                                </div>
                                <div class="col-md-4">
                                    <label for="employee_phone_no">Employer Phone No*</label>
                                    <input type="text" name="employee_phone_no" value="{{@Auth::user()->professionalDetail->phone}}" id="employee_phone_no" class="form-control" data-toggle="tooltip" data-placement="top" maxlength="{{$phone_lenght}}" onkeyup="return phoneNumberValidation(this);">
                                </div>
                                <div class="col-md-3">
                                    <label for="employee_fax">Fax</label>
                                    <input type="text" name="employee_fax" id="employee_fax" value="{{@Auth::user()->professionalDetail->fax}}" class="form-control" data-toggle="tooltip" data-placement="top" maxlength="{{$phone_lenght}}" onkeyup="return phoneNumberValidation(this);">
                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <button type="button" class="btn mt-5 form-wizard-previous-btn" onclick="gotoPreviousTab(1)">Previous</button>
                            <button class="btn rounded mt-5 form-wizard-next-btn" type="button" id="btnStepTwo">Next
                                <span class="loader" style="display: none">
                                    <div class="spinner-border text-light" role="status" style="width: 1rem;height: 1rem;">
                                        <span class="sr-only">Loading...</span>
                                    </div>
                                </span>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="tab-pane fade" id="idinformation" role="tabpanel" aria-labelledby="idinformation-tab">
            <div class="card mt-2">
                <div class="card-header">
                    <h2 class="fs-title">ID Information</h2>
                </div>
                <div class="card-body maincontent">
                    <div>
                        <h4>IMPORTANT INFORMATION ABOUT PROCEDURES FOR OPENING A NEW ACCOUNT USA PATRIOT ACT INFORMATION</h4>
                        <p>Important information. To help the government fight the funding of terrorism and money‐laundering activities, Federal law requires that Velocity Clearing LLC (“Velocity”) verify your identity by obtaining your name, date of birth, address, and a government‐issued identification number before opening your account. In certain circumstances, Velocity may obtain and verify this information with respect to any person(s) authorized to effect transactions in an account. For certain entities, such as trusts, estates, corporations, partnerships or other organizations, identifying documentation is also required. Your account may be restricted and/or closed if Velocity cannot verify this information. Velocity will not be responsible for any losses or damages (including, but not limited to, lost opportunities) resulting from any failure to provide this information or from any restriction placed upon, or closing of your account.</p>
                    </div>
                    <form action="" method="post" class="row stepbox-3" id="stepthreeForm">
                        @csrf
                        <div class="col-md-3">
                            <label for="">TAX ID (SS#/EN)<span class="text-danger">*</span></label>
                            <input type="password" maxlength="{{$default_length}}" name="three_tax_id" value="{{@Auth::user()->idInformation->tax_id}}" id="three_tax_id" class="form-control" data-toggle="tooltip" data-placement="top">
                        </div>
                        <div class="col-md-3">
                            <label for="">Date of Birth<span class="text-danger">*</span></label>
                            <input type="date" name="three_date_of_birth" value="{{@Auth::user()->idInformation->date_of_birth}}" id="three_date_of_birth" class="form-control" data-toggle="tooltip" data-placement="top">
                        </div>
                        <div class="col-md-3">
                            <label for="">Country of Tax Residence<span class="text-danger">*</span></label>
                            <select name="three_country" id="three_country" class="form-control" data-toggle="tooltip" data-placement="top">
                                <option value>Select Country</option>
                                @foreach ($countries as $item)
                                <option value="{{$item->id}}" {{@Auth::user()->idInformation->tax_country_id == $item->id?'selected':''}}>{{$item->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-3">
                            <label for="">State<span class="text-danger">*</span></label>
                            <select name="three_state" id="three_state" class="form-control" data-toggle="tooltip" data-placement="top">
                                <option value>Select States</option>
                                @isset(Auth::user()->idInformation)
                                @php
                                $states =\App\Models\State::where('country_id',@Auth::user()->idInformation->tax_country_id)->get();
                                @endphp
                                @foreach ($states as $item)
                                <option value="{{$item->id}}" {{@Auth::user()->idInformation->tax_state_id == $item->id?'selected':''}}>{{$item->name}}</option>
                                @endforeach
                                @endisset
                            </select>
                        </div>
                        <div class="col-12">
                            <h4 class="my-3">Valid Government Issued Photo ID</h4>
                        </div>
                        <div class="col-md-3">
                            <label for="">ID Type<span class="text-danger">*</span></label>
                            <select name="three_id_type" id="three_id_type" class="form-control" data-toggle="tooltip" data-placement="top">
                                <option value="Driver License" {{@Auth::user()->idInformation->id_type == "Driver License"?'selected':''}}>Driver License</option>
                                <option value="Passport" {{@Auth::user()->idInformation->id_type == "Passport"?'selected':''}}>Passport</option>
                                <option value="Local ID" {{@Auth::user()->idInformation->id_type == "Local ID"?'selected':''}}>Local ID</option>
                            </select>
                        </div>
                        <div class="col-md-3">
                            <label for="">Name of ID<span class="text-danger">*</span></label>
                            <input type="text" name="three_name_of_id" value="{{@Auth::user()->idInformation->name_of_id}}"  maxlength="{{$name_length}}" id="three_name_of_id" class="form-control" data-toggle="tooltip" data-placement="top" {{((@Auth::user()->idInformation->id_type == "Passport")||(@Auth::user()->idInformation->id_type == "Driver License"))?'disabled':''}}>
                        </div>
                        <div class="col-md-3">
                            <label for="">ID Number<span class="text-danger">*</span></label>
                            <input type="text" name="three_id_number" value="{{@Auth::user()->idInformation->id_number}}" pattern="^[A-Za-z -]+$" onkeyup="return phoneNumberValidation(this);" id="three_id_number" maxlength="{{$default_length}}" class="form-control" data-toggle="tooltip" data-placement="top">
                        </div>
                        <div class="col-md-3">
                            <label for="">Country of Issuance<span class="text-danger">*</span></label>
                            <select name="three_country_residence" id="three_country_residence" class="form-control" data-toggle="tooltip" data-placement="top">
                                <option value>Select Country</option>
                                @foreach ($countries as $item)
                                <option value="{{$item->id}}" {{@Auth::user()->idInformation->identity_country_id == $item->id?'selected':''}}>{{$item->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        <div class="col-md-3">
                            <label for="">State<span class="text-danger">*</span></label>
                            <select name="three_state_residence" id="three_state_residence" class="form-control" data-toggle="tooltip" data-placement="top">
                                <option value>Select States</option>
                                @isset(Auth::user()->idInformation)
                                @php
                                $states =\App\Models\State::where('country_id',@Auth::user()->idInformation->identity_country_id)->get();
                                @endphp
                                @foreach ($states as $item)
                                <option value="{{$item->id}}" {{@Auth::user()->idInformation->identity_state_id == $item->id?'selected':''}}>{{$item->name}}</option>
                                @endforeach
                                @endisset
                            </select>
                        </div>
                        <div class="col-md-3">
                            <label for="">Issue Date (MM\DD\YYYY)<span class="text-danger">*</span></label>
                            <input type="date" name="three_issue_date" value="{{@Auth::user()->idInformation->issue_date}}" id="three_issue_date" class="form-control" data-toggle="tooltip" data-placement="top">
                        </div>
                        <div class="col-md-3">
                            <label for="">Expiration (MM\DD\YYYY)<span class="text-danger">*</span></label>
                            <input type="date" name="three_expiration" value="{{@Auth::user()->idInformation->expiration_date}}" id="three_expiration" class="form-control" data-toggle="tooltip" data-placement="top" onchange="return ValidExpiry();">
                        </div>
                        <div class="col-12">
                            <button type="button" class="form-wizard-previous-btn float-left mt-5" onclick="gotoPreviousTab(2)">Previous</button>
                            <button class="form-wizard-next-btn float-left mt-5" type="button" id="btnStepThree">Next
                                <span class="loader" style="display: none">
                                    <div class="spinner-border text-light" role="status" style="width: 1rem;height: 1rem;">
                                        <span class="sr-only">Loading...</span>
                                    </div>
                                </span>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="tab-pane fade" id="income_detail" role="tabpanel" aria-labelledby="income_detail-tab">
            <div class="card mt-2" style=" border: 0 none; border-radius: 0px; box-shadow: 0 0 15px 1px rgb(0 0 0 / 40%); padding: 20px 30px; box-sizing: border-box;">

                <div class="col-md-12">
                    <h2 class="fs-title">Income Details</h2>
                    <p>Financial situation and needs, Liquidity Considerations and Tax Status.</p>
                    <p><strong>Annual Income</strong> includes income form sources such as employment, alimony, social security, investment, income etc.</p>
                </div>
                <div class="card-body">
                    <form action="" method="post" class="row" id="stepfourForm">
                        @csrf
                        <div class="col-sm-6 col-md-4 text-left annualincomeclm">
                            <h4>Annual Income</h4>


                            <div class="label-icon ">
                                <label class="radioholder">
                                    <input type="radio" name="annual_income" id="annual_income" value="$25,000 and under" {{@Auth::user()->incomeDetail->annual_income== "$25,000 and under"? "checked":""}}><i class="fa fa-usd" aria-hidden="true"></i> $25,000 and under
                                    <span class="radiomark"></span>
                                </label>
                            </div>

                            <div class="label-icon radiocurrent">
                                <label class="radioholder">
                                    <input type="radio" name="annual_income" id="annual_income" value="$25,001 - $50,000" {{@Auth::user()->incomeDetail->annual_income== "$25,001 - $50,000"? "checked":""}}><i class="fa fa-usd" aria-hidden="true"></i> $25,001 - $50,000
                                    <span class="radiomark"></span>
                                </label>
                            </div>

                            <div class="label-icon ">
                                <label class="radioholder">
                                    <input type="radio" name="annual_income" id="annual_income" value="$50,001 - $100,000" {{@Auth::user()->incomeDetail->annual_income== "$50,001 - $100,000"? "checked":""}}><i class="fa fa-usd" aria-hidden="true"></i> $50,001 - $100,000
                                    <span class="radiomark"></span>
                                </label>
                            </div>

                            <div class="label-icon">
                                <label class="radioholder">
                                    <input type="radio" name="annual_income" id="annual_income" value="$100,001 - $250,000" {{@Auth::user()->incomeDetail->annual_income== "$100,001 - $250,000"? "checked":""}}><i class="fa fa-usd" aria-hidden="true"></i> $100,001 - $250000
                                    <span class="radiomark"></span>
                                </label>
                            </div>
                            <div class="label-icon">
                                <label class="radioholder">
                                    <input type="radio" name="annual_income" id="annual_income" value="$250,001 ‐ $500,000" {{@Auth::user()->incomeDetail->annual_income== "$250,001 ‐ $500,000"? "checked":""}}><i class="fa fa-usd" aria-hidden="true"></i> $250,001 ‐ $500,000
                                    <span class="radiomark"></span>
                                </label>
                            </div>
                            <div class="label-icon">
                                <label class="radioholder">
                                    <input type="radio" name="annual_income" id="annual_income" value="over $500,000" data-value="500000" {{@Auth::user()->incomeDetail->annual_income== "over $500,000"? "checked":""}}> <i class="fa fa-usd" aria-hidden="true"></i>over $500,000
                                    <span class="radiomark"></span>
                                </label>
                            </div>
                        </div>

                        <div class="col-sm-6 col-md-4 text-left annualincomeclm">
                            <h4>Net Worth (Excluding Residence)</h4>
                            <div class="label-icon ">
                                <label class="radioholder">
                                    <input type="radio" name="net_worth" id="net_worth" value="$25,000 and under" {{@Auth::user()->incomeDetail->net_worth== "$25,000 and under"? "checked":""}}><i class="fa fa-usd" aria-hidden="true"></i> $25,000 and under
                                    <span class="radiomark"></span>
                                </label>
                            </div>

                            <div class="label-icon ">
                                <label class="radioholder">
                                    <input type="radio" name="net_worth" id="net_worth" value="$25,001 - $50,000" {{@Auth::user()->incomeDetail->net_worth== "$25,001 - $50,000"? "checked":""}}><i class="fa fa-usd" aria-hidden="true"></i> $25,001 - $50,000
                                    <span class="radiomark"></span>
                                </label>
                            </div>

                            <div class="label-icon ">
                                <label class="radioholder">
                                    <input type="radio" name="net_worth" id="net_worth" value="$50,001 - $100,000" {{@Auth::user()->incomeDetail->net_worth== "$50,001 - $100,000"? "checked":""}}> <i class="fa fa-usd" aria-hidden="true"></i>$50,001 - $100,000
                                    <span class="radiomark"></span>
                                </label>
                            </div>

                            <div class="label-icon ">
                                <label class="radioholder">
                                    <input type="radio" name="net_worth" id="net_worth" value="$100,001 - $250,000" {{@Auth::user()->incomeDetail->net_worth== "$100,001 - $250,000"? "checked":""}}><i class="fa fa-usd" aria-hidden="true"></i> $100,001 - $250000
                                    <span class="radiomark"></span>
                                </label>
                            </div>
                            <div class="label-icon ">
                                <label class="radioholder">
                                    <input type="radio" name="net_worth" id="net_worth" value="$250,001 ‐ $500,000" {{@Auth::user()->incomeDetail->net_worth== "$250,001 ‐ $500,000"? "checked":""}}> <i class="fa fa-usd" aria-hidden="true"></i>$250,001 ‐ $500,000
                                    <span class="radiomark"></span>
                                </label>
                            </div>
                            <div class="label-icon ">
                                <label class="radioholder">
                                    <input type="radio" name="net_worth" id="net_worth" value="over $500,000" data-value="500000" {{@Auth::user()->incomeDetail->net_worth== "over $500,000"? "checked":""}}><i class="fa fa-usd" aria-hidden="true"></i> over $500,000
                                    <span class="radiomark"></span>
                                </label>
                            </div>
                        </div>

                        <div class="col-sm-6 col-md-4 text-left annualincomeclm">
                            <h4>Liquid Net Worth (Must be less than Net Worth)</h4>


                            <div class="label-icon ">
                                <label class="radioholder">
                                    <input type="radio" name="liquid_net_worth" id="liquid_net_worth" value="$25,000 and under" {{@Auth::user()->incomeDetail->liquid_net_worth== "$25,000 and under"? "checked":""}}><i class="fa fa-usd" aria-hidden="true"></i> $25,000 and under
                                    <span class="radiomark"></span>
                                </label>
                            </div>

                            <div class="label-icon ">
                                <label class="radioholder">
                                    <input type="radio" name="liquid_net_worth" id="liquid_net_worth" value="$25,001 - $50,000" {{@Auth::user()->incomeDetail->liquid_net_worth== "$25,001 - $50,000"? "checked":""}}><i class="fa fa-usd" aria-hidden="true"></i> $25,001 - $50,000
                                    <span class="radiomark"></span>
                                </label>
                            </div>

                            <div class="label-icon ">
                                <label class="radioholder">
                                    <input type="radio" name="liquid_net_worth" id="liquid_net_worth" value="$50,001 - $100,000" {{@Auth::user()->incomeDetail->liquid_net_worth== "$50,001 - $100,000"? "checked":""}}><i class="fa fa-usd" aria-hidden="true"></i> $50,001 - $100,000
                                    <span class="radiomark"></span>
                                </label>
                            </div>

                            <div class="label-icon ">
                                <label class="radioholder">
                                    <input type="radio" name="liquid_net_worth" id="liquid_net_worth" value="$100,001 - $250,000" {{@Auth::user()->incomeDetail->liquid_net_worth== "$100,001 - $250,000"? "checked":""}}><i class="fa fa-usd" aria-hidden="true"></i> $100,001 - $250000
                                    <span class="radiomark"></span>
                                </label>
                            </div>
                            <div class="label-icon ">
                                <label class="radioholder">
                                    <input type="radio" name="liquid_net_worth" id="liquid_net_worth" value="$250,001 ‐ $500,000" {{@Auth::user()->incomeDetail->liquid_net_worth== "$250,001 ‐ $500,000"? "checked":""}}> <i class="fa fa-usd" aria-hidden="true"></i>$250,001 ‐ $500,000
                                    <span class="radiomark"></span>
                                </label>
                            </div>
                            <div class="label-icon ">
                                <label class="radioholder">
                                    <input type="radio" name="liquid_net_worth" id="liquid_net_worth" value="over $500,000" data-value="500000" {{@Auth::user()->incomeDetail->liquid_net_worth== "over $500,000"? "checked":""}}><i class="fa fa-usd" aria-hidden="true"></i> over $500,000
                                    <span class="radiomark"></span>
                                </label>
                            </div>
                        </div>

                        <div class="col-sm-6 col-md-4 text-left annualincomeclm">
                            <h4>TAX RATE (Highest Marginal)</h4>


                            <div class="label-icon ">
                                <label class="radioholder">
                                    <input type="radio" name="tax_rate" id="tax_rate" value="0 - 5" {{@Auth::user()->incomeDetail->tax_rate == "0 - 5"? "checked":""}}> 0 - 5<i class="fa fa-usd" aria-hidden="true"></i>
                                    <span class="radiomark"></span>
                                </label>
                            </div>

                            <div class="label-icon radiocurrent">
                                <label class="radioholder">
                                    <input type="radio" name="tax_rate" id="tax_rate" value="16 - 25" {{@Auth::user()->incomeDetail->tax_rate == "16 - 25"? "checked":""}}> 16 - 25<i class="fa fa-usd" aria-hidden="true"></i>
                                    <span class="radiomark"></span>
                                </label>
                            </div>

                            <div class="label-icon ">
                                <label class="radioholder">
                                    <input type="radio" name="tax_rate" id="tax_rate" value="26 - 30" {{@Auth::user()->incomeDetail->tax_rate == "26 - 30"? "checked":""}}> 26 - 30<i class="fa fa-usd" aria-hidden="true"></i>
                                    <span class="radiomark"></span>
                                </label>
                            </div>

                            <div class="label-icon ">
                                <label class="radioholder">
                                    <input type="radio" name="tax_rate" id="tax_rate" value="31 - 35" {{@Auth::user()->incomeDetail->tax_rate == "31 - 35"? "checked":""}}> 31 - 35<i class="fa fa-usd" aria-hidden="true"></i>
                                    <span class="radiomark"></span>
                                </label>
                            </div>
                            <div class="label-icon ">
                                <label class="radioholder">
                                    <input type="radio" name="tax_rate" id="tax_rate" value="over 35" {{@Auth::user()->incomeDetail->tax_rate == "over 35"? "checked":""}}> over 35<i class="fa fa-usd" aria-hidden="true"></i>
                                    <span class="radiomark"></span>
                                </label>
                            </div>

                        </div>
                        <div class="col-12">
                            <button type="button" class="btn mt-5 form-wizard-previous-btn float-left" onclick="gotoPreviousTab(3)">Previous</button>
                            <button class="btn rounded mt-5 form-wizard-next-btn float-left" type="button" id="btnStepFour">Next
                                <span class="loader" style="display: none">
                                    <div class="spinner-border text-light" role="status" style="width: 1rem;height: 1rem;">
                                        <span class="sr-only">Loading...</span>
                                    </div>
                                </span>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="tab-pane fade wizard-fieldset" id="funding_detail" role="tabpanel" aria-labelledby="funding_detail-tab">
            <div class="card mt-2 maincontent" style="border: 0 none; border-radius: 0px; box-shadow: 0 0 15px 1px rgb(0 0 0 / 40%); box-sizing: border-box; padding-top:20px;">
                <div class="col-md-12">
                    <h2 class="fs-title">Funding Details</h2>
                    <h4 style="border-bottom: 1px solid #E5E5E5; padding-bottom: 20px;">I am funding this account with (check all that apply)</h4>
                </div>
                <div class="card-body text-left fundingclm">
                    <form action="" method="post" class="row" id="stepfiveForm">
                        @csrf
                        <div class="col-md-6">
                            <div class="form-check custom-control custom-checkbox">
                                <label class="form-check-label labelholder" for="funding_income">
                                    <input class="form-check-input" type="checkbox" value="1" {{@Auth::user()->fundDetail->is_income == 1?'checked':''}} name="funding_income" id="funding_income">Income
                                    <span class="checkmark"></span>


                                </label>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-check custom-control custom-checkbox">
                                <label class="form-check-label labelholder" for="funding_inheritance">
                                    <input class="form-check-input" type="checkbox" value="1" {{@Auth::user()->fundDetail->is_inheritance == 1?'checked':''}} name="funding_inheritance" id="funding_inheritance">Inheritance
                                    <span class="checkmark"></span>


                                </label>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-check custom-control custom-checkbox">
                                <label class="form-check-label labelholder" for="funding_saving">
                                    <input class="form-check-input" type="checkbox" value="1" {{@Auth::user()->fundDetail->is_pension == 1?'checked':''}} name="funding_saving" id="funding_saving">Pension or retirement savings
                                    <span class="checkmark"></span>


                                </label>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-check custom-control custom-checkbox">
                                <label class="form-check-label labelholder" for="funding_social">
                                    <input class="form-check-input" type="checkbox" value="1" name="funding_social" {{@Auth::user()->fundDetail->is_social_security == 1?'checked':''}} id="funding_social">Social Security Benefit
                                    <span class="checkmark"></span>


                                </label>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-check custom-control custom-checkbox">
                                <label class="form-check-label labelholder" for="funding_benefit">
                                    <input class="form-check-input" type="checkbox" value="1" {{@Auth::user()->fundDetail->is_gift == 1?'checked':''}} name="funding_benefit" id="funding_benefit"> Gift
                                    <span class="checkmark"></span>


                                </label>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-check custom-control custom-checkbox">
                                <label class="form-check-label labelholder" for="funding_other">
                                    <input class="form-check-input" type="checkbox" value="1" {{@Auth::user()->fundDetail->is_other == 1?'checked':''}} name="funding_other" id="funding_other">Other
                                    <span class="checkmark"></span>


                                </label>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <div class="form-check custom-control custom-checkbox">
                                <label class="form-check-label labelholder" for="funding_sales">
                                    <input class="form-check-input" type="checkbox" {{@Auth::user()->fundDetail->is_sale_of_business == 1?'checked':''}} value="1" id="funding_sales">Sales of business and property
                                    <span class="checkmark"></span>


                                </label>
                            </div>
                        </div>

                        <div class="col-md-6">
                            <input type="text" {{@Auth::user()->fundDetail->is_other == 0 || @Auth::user()->fundDetail == null?'disabled':''}} name="funding_other_input" value="{{@Auth::user()->fundDetail->other_income}}" id="funding_other_input" class="form-control" data-toggle="tooltip" data-placement="top">
                        </div>

                        <div class="col-md-12" style="padding-top: 20px;">
                            <h5>Funding Details</h5>
                        </div>

                        <div class="col-md-6">
                            <label for="funding_bank_name">Name of the bank you will be funding your account from *</label>
                            <input type="text" name="funding_bank_name" value="{{@Auth::user()->fundDetail->bank_name}}" id="funding_bank_name" class="form-control" data-toggle="tooltip" data-placement="top" maxlength="{{$bank_name_length}}">
                        </div>

                        <div class="col-md-6">
                            <label for="aba_or_swift">ABA / SWIFT *</label>
                            <input type="text" name="aba_or_swift" id="aba_or_swift" value="{{@Auth::user()->fundDetail->aba_or_swift}}" class="form-control" data-toggle="tooltip" data-placement="top" maxlength="{{$swift_length}}" onkeydown="return alphaNumeric(event);">
                        </div>

                        <div class="col-md-12">
                            <label for="account_number">Account No *</label>
                            <input type="text" name="account_number" id="account_number" value="{{@Auth::user()->fundDetail->account_number}}" class="form-control" data-toggle="tooltip" data-placement="top" maxlength="{{$account_length}}" onkeydown="return alphaNumeric(event);">
                        </div>

                        <div class="col-12 form-group clearfix btn-top-mrg">
                            <button type="button" class="btn mt-2 form-wizard-previous-btn" onclick="gotoPreviousTab(4)">Previous</button>
                            <button class="btn rounded mt-2 form-wizard-next-btn" type="button" id="btnStepFive">Next
                                <span class="loader" style="display: none">
                                    <div class="spinner-border text-light" role="status" style="width: 1rem;height: 1rem;">
                                        <span class="sr-only">Loading...</span>
                                    </div>
                                </span>
                            </button>
                        </div>

                    </form>
                </div>
            </div>
        </div>

        <div class="tab-pane fade" id="risk_acceptance" role="tabpanel" aria-labelledby="risk_acceptance-tab">
            <div class="card mt-2">
                <div class="card-header">
                    <h4>Risk Acceptance</h4>
                </div>
                <div class="card-body">
                    <p>Please answer the following questions about your investment objectives.</p>
                    <form action="" method="post" class="row" id="stepsixForm">
                        @csrf
                        <div class="col-md-12 questionrow">
                            <div class="radio-custom">
                                <label class="radioholder" style="color: #fff;margin: 0;">
                                    <input type="radio" name="accountRisk" value="Active or Day Trading" {{@Auth::user()->riskAcceptance->account_risk== "Active or Day Trading"? "checked":""}}>
                                    <span class="tname" checked="checked">Active or Day Trading</span>
                                    <span class="radiomark"></span>
                                </label>
                            </div>
                            <div class="radio-custom">
                                <label class="radioholder" style="color: #fff; margin: 0;">
                                    <input type="radio" name="accountRisk" value="Short Term Trading" {{@Auth::user()->riskAcceptance->account_risk== "Short Term Trading"? "checked":""}}>
                                    <span class="tname">Short Term Trading</span>
                                    <span class="radiomark"></span>
                                </label>
                            </div>
                        </div>
                        <div class="utility-coonections">
                            <div class="row" style="margin: 0px;">
                                <div class="col-md-12 text-left">
                                    <h4>Investment Risk Tolerance </h4>
                                    <p>Please select the degree of risk you are willing to take with the assets in this account</p>
                                    <div class="conservative-error" style="color:red"></div>
                                    <div class="row">
                                        <div class="col-md-1 col-sm-1">
                                            <label for="conservative" class="labelholder">
                                                <input type="checkbox" name="conservative" id="conservative" value="1" {{@Auth::user()->riskAcceptance->is_conservative== "1"? "checked":""}}>
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>
                                        <div class="col-md-3 col-sm-11" style="padding-left: 0px; left: 0; margin-top: 20px;">
                                            <h6>Conservative</h6>
                                        </div>
                                        <div class="col-md-8 col-sm-12" style="left: 0; margin-top: 5px;">
                                            <p>I want to preserve my initial principal in this account, with minimal risk, even if that means this account does not generate significantincome or returns and may not keep pacewith inflation.</p>
                                        </div>
                                    </div><!-- // Row // -->

                                    <div class="row">
                                        <div class="col-md-1 col-sm-1">
                                            <label for="moderatelyConservative" class="labelholder">
                                                <input type="checkbox" name="moderatelyConservative" id="moderatelyConservative" value="1" {{@Auth::user()->riskAcceptance->is_moderately_conservative== "1"? "checked":""}}>
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>
                                        <div class="col-md-3 col-sm-11" style="padding-left: 0px; left: 0; margin-top: 20px;">
                                            <h6>Moderately Conservative</h6>

                                        </div>
                                        <div class="col-md-8 col-sm-12" style="left: 0; margin-top: 5px;">
                                            <p>I am willing to acceptlowrisk tomy initial principal, including low volatility,to seek a modestlevel of portfolio returns.</p>
                                        </div>


                                    </div><!-- // Row // -->

                                    <div class="row">
                                        <div class="col-md-1 col-sm-1">
                                            <label for="moderate" class="labelholder">
                                                <input type="checkbox" name="moderate" id="moderate" value="1" {{@Auth::user()->riskAcceptance->is_moderate== "1"? "checked":""}}>
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>
                                        <div class="col-md-3 col-sm-11" style="padding-left: 0px; left: 0; margin-top: 20px;">
                                            <h6>Moderate</h6>

                                        </div>
                                        <div class="col-md-8 col-sm-12" style="left: 0; margin-top: 5px;">
                                            <p>I am willing to accept some risk to my initial principal and tolerate some volatility to seek higher returns, and understand I could lose a portion of the money invested.</p>
                                        </div>


                                    </div><!-- // Row // -->

                                    <div class="row">
                                        <div class="col-md-1 col-sm-1">
                                            <label for="moderatelyAggressive" class="labelholder">
                                                <input type="checkbox" name="moderatelyAggressive" id="moderatelyAggressive" value="1" {{@Auth::user()->riskAcceptance->is_moderately_aggressive== "1"? "checked":""}}>
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>
                                        <div class="col-md-3 col-sm-11" style="padding-left: 0px; left: 0; margin-top: 20px;">
                                            <h6>Moderately Aggressive</h6>

                                        </div>
                                        <div class="col-md-8 col-sm-12" style="left: 0; margin-top: 5px;">
                                            <p>I am willing to accept high risk to my initial
                                                principal,
                                                including high volatility, to seek high returns
                                                over time and understand I could lose a substantial
                                                amount of
                                                the money invested.</p>
                                        </div>


                                    </div><!-- // Row // -->
                                    <div class="row">
                                        <div class="col-md-1 col-sm-1">
                                            <label for="significantRisk" class="labelholder">
                                                <input type="checkbox" name="significantRisk" id="significantRisk" value="1" {{@Auth::user()->riskAcceptance->is_significant_risk== "1"? "checked":""}}>
                                                <span class="checkmark"></span>
                                            </label>
                                        </div>
                                        <div class="col-md-3 col-sm-11" style="padding-left: 0px; left: 0; margin-top: 20px;">
                                            <h6>Significant Risk </h6>

                                        </div>
                                        <div class="col-md-8 col-sm-12" style="left: 0; margin-top: 5px;">
                                            <p>I am willing to accept maximum risk to my initial
                                                principal to
                                                aggressively seek maximum returns,
                                                and I understand I could lose most, or all, of
                                                themoney
                                                invested.</p>
                                        </div>


                                    </div><!-- // Row // -->

                                    <p style="text-align: justify;"> We consider day trading to be a
                                        high‐risk
                                        trading strategy. Our clients must have a 'significant risk'
                                        tolerance
                                        to employ such a strategy. Please
                                        ensure that you have read and understand the accompanying
                                        Day Trading
                                        Risk Disclosure Statement before submitting your new account
                                        documentation. It is in your best interest to carefully
                                        consider whether
                                        or not you have a significant risk tolerance before
                                        proceeding with this
                                        form.
                                    </p>

                                </div>
                            </div>
                        </div>
                        <div class="col-12">
                            <button type="button" class="btn mt-2 form-wizard-previous-btn" onclick="gotoPreviousTab(5)">Previous</button>
                            <button class="btn rounded mt-2 form-wizard-next-btn" type="button" id="btnStepSix">Next
                                <span class="loader" style="display: none">
                                    <div class="spinner-border text-light" role="status" style="width: 1rem;height: 1rem;">
                                        <span class="sr-only">Loading...</span>
                                    </div>
                                </span>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="tab-pane fade" id="financial_situation" role="tabpanel" aria-labelledby="financial_situation-tab">
            <div class="card mt-2 property-form" style="border: 0 none; border-radius: 0px; box-shadow: 0 0 15px 1px rgb(0 0 0 / 40%); padding: 20px 30px; box-sizing: border-box;">
                <div class="col-md-12" style="padding-top: 20px; padding-bottom: 20px;">
                    <h3>Financial Situation and Needs, Liquidity Considerations and Tax Status</h3>
                </div>
                <div class="card-body annualincomeclm ">
                    <form action="" method="post" class="row" id="stepsevenForm">
                        @csrf
                        <div class="col-sm-6 col-md-4 text-left">
                            <h6 style="margin-left: 10px;">ANNUAL EXPENSES</h6>
                            <div class="label-icon ">
                                <label class="radioholder">
                                    <input type="radio" name="annual_expenses" id="annual_expenses" value="$25,000 and under" {{@Auth::user()->financialSituation->annual_expenses == "$25,000 and under"? "checked":""}}><i class="fa fa-usd" aria-hidden="true"></i> $25,000 and under
                                    <span class="radiomark"></span>
                                </label>
                            </div>

                            <div class="label-icon ">
                                <label class="radioholder">
                                    <input type="radio" name="annual_expenses" id="annual_expenses" value="$25,001 - $50,000" {{@Auth::user()->financialSituation->annual_expenses == "$25,001 - $50,000"? "checked":""}}><i class="fa fa-usd" aria-hidden="true"></i> $25,001 - $50,000
                                    <span class="radiomark"></span>
                                </label>
                            </div>

                            <div class="label-icon ">
                                <label class="radioholder">
                                    <input type="radio" name="annual_expenses" id="annual_expenses" value="$50,001 - $100,000" {{@Auth::user()->financialSituation->annual_expenses == "$50,001 - $100,000"? "checked":""}}><i class="fa fa-usd" aria-hidden="true"></i> $50,001 - $100,000
                                    <span class="radiomark"></span>
                                </label>
                            </div>

                            <div class="label-icon ">
                                <label class="radioholder">
                                    <input type="radio" name="annual_expenses" id="annual_expenses" value="$100,001 - $250,000" {{@Auth::user()->financialSituation->annual_expenses == "$100,001 - $250,000"? "checked":""}}><i class="fa fa-usd" aria-hidden="true"></i> $100,001 - $250000
                                    <span class="radiomark"></span>
                                </label>
                            </div>
                            <div class="label-icon ">
                                <label class="radioholder">
                                    <input type="radio" name="annual_expenses" id="annual_expenses" value="$250,001 ‐ $500,000" {{@Auth::user()->financialSituation->annual_expenses == "$250,001 ‐ $500,000"? "checked":""}}><i class="fa fa-usd" aria-hidden="true"></i> $250,001 ‐ $500,000
                                    <span class="radiomark"></span>
                                </label>
                            </div>
                            <div class="label-icon ">
                                <label class="radioholder">
                                    <input type="radio" name="annual_expenses" id="annual_expenses" value="over $500,000" data-value="500000" {{@Auth::user()->financialSituation->annual_expenses == "over $500,000"? "checked":""}}><i class="fa fa-usd" aria-hidden="true"></i> over $500,000
                                    <span class="radiomark"></span>
                                </label>
                            </div>
                        </div>

                        <div class="col-sm-6 col-md-4 text-left">
                            <h6 style="margin-left: 10px;">SPECIAL EXPENSES</h6>


                            <div class="label-icon ">
                                <label class="radioholder">
                                    <input type="radio" name="special_expenses" id="special_expenses" value="$25,000 and under" {{@Auth::user()->financialSituation->special_expenses == "$25,000 and under"? "checked":""}}><i class="fa fa-usd" aria-hidden="true"></i> $25,000 and under
                                    <span class="radiomark"></span>
                                </label>
                            </div>

                            <div class="label-icon ">
                                <label class="radioholder">
                                    <input checked="" type="radio" name="special_expenses" id="special_expenses" value="$25,001 - $50,000" {{@Auth::user()->financialSituation->special_expenses == "$25,001 - $50,000"? "checked":""}}><i class="fa fa-usd" aria-hidden="true"></i> $25,001 - $50,000
                                    <span class="radiomark"></span>
                                </label>
                            </div>

                            <div class="label-icon ">
                                <label class="radioholder">
                                    <input type="radio" name="special_expenses" id="special_expenses" value="$50,001 - $100,000" {{@Auth::user()->financialSituation->special_expenses == "$50,001 - $100,000"? "checked":""}}><i class="fa fa-usd" aria-hidden="true"></i> $50,001 - $100,000
                                    <span class="radiomark"></span>
                                </label>
                            </div>

                            <div class="label-icon ">
                                <label class="radioholder">
                                    <input type="radio" name="special_expenses" id="special_expenses" value="$100,001 - $250,000" {{@Auth::user()->financialSituation->special_expenses == "$100,001 - $250,000"? "checked":""}}><i class="fa fa-usd" aria-hidden="true"></i> $100,001 - $250000
                                    <span class="radiomark"></span>
                                </label>
                            </div>
                            <div class="label-icon ">
                                <label class="radioholder">
                                    <input type="radio" name="special_expenses" id="special_expenses" value="$250,001 ‐ $500,000" {{@Auth::user()->financialSituation->special_expenses == "$250,001 ‐ $500,000"? "checked":""}}><i class="fa fa-usd" aria-hidden="true"></i> $250,001 ‐ $500,000
                                    <span class="radiomark"></span>
                                </label>
                            </div>
                            <div class="label-icon ">
                                <label class="radioholder">
                                    <input type="radio" name="special_expenses" id="special_expenses" value="over $500,000" data-value="500000" {{@Auth::user()->financialSituation->special_expenses == "over $500,000"? "checked":""}}><i class="fa fa-usd" aria-hidden="true"></i> over $500,000
                                    <span class="radiomark"></span>
                                </label>
                            </div>
                        </div>

                        <div class="col-sm-6 col-md-4 text-left">
                            <h6 style="margin-left: 10px;">LIQUIDITY NEEDS</h6>


                            <div class="label-icon">
                                <label class="radioholder">
                                    <input type="radio" name="liquidity_need" id="liquidity_need" value="very important" {{@Auth::user()->financialSituation->liquidity_needs == "very important"? "checked":""}}> Very Important
                                    <span class="radiomark"></span>
                                </label>
                            </div>

                            <div class="label-icon">
                                <label class="radioholder">
                                    <input type="radio" name="liquidity_need" id="liquidity_need" value="important" {{@Auth::user()->financialSituation->liquidity_needs == "important"? "checked":""}}> Important
                                    <span class="radiomark"></span>
                                </label>
                            </div>

                            <div class="label-icon">
                                <label class="radioholder">
                                    <input checked="" type="radio" name="liquidity_need" id="liquidity_need" value="Some what important" {{@Auth::user()->financialSituation->liquidity_needs == "Some what important"? "checked":""}}> Some what Important
                                    <span class="radiomark"></span>
                                </label>
                            </div>

                            <div class="label-icon">
                                <label class="radioholder">
                                    <input type="radio" name="liquidity_need" id="liquidity_need" value="does not matter" {{@Auth::user()->financialSituation->liquidity_needs == "does not matter"? "checked":""}}> Does not Matter
                                    <span class="radiomark"></span>
                                </label>
                            </div>
                        </div>
                        <div class="col-md-12" style="padding-top: 40px; padding-bottom: 30px;padding-left: 25px;padding-right: 25px;">
                            <h3>The expected period you plan to achieve your financial goal(s)</h3>
                        </div>

                        <div class="col-sm-6 col-md-4 text-left">



                            <div class="label-icon">
                                <label class="radioholder">
                                    <input type="radio" name="financial_goal_plan" id="financial_goal_plan" value="under 1 year" {{@Auth::user()->financialSituation->financial_goal_period == "under 1 year"? "checked":""}}> Under 1 year
                                    <span class="radiomark"></span>
                                </label>
                            </div>

                            <div class="label-icon">
                                <label class="radioholder">
                                    <input type="radio" name="financial_goal_plan" id="financial_goal_plan" value="1 - 2" {{@Auth::user()->financialSituation->financial_goal_period == "1 - 2"? "checked":""}}> 1 - 2
                                    <span class="radiomark"></span>
                                </label>
                            </div>

                            <div class="label-icon">
                                <label class="radioholder">
                                    <input type="radio" name="financial_goal_plan" id="financial_goal_plan" value="3 - 5" {{@Auth::user()->financialSituation->financial_goal_period == "3 - 5"? "checked":""}}> 3 - 5
                                    <span class="radiomark"></span>
                                </label>
                            </div>

                            <div class="label-icon">
                                <label class="radioholder">
                                    <input type="radio" name="financial_goal_plan" id="financial_goal_plan" value="6 - 10" {{@Auth::user()->financialSituation->financial_goal_period == "6 - 10"? "checked":""}}> 6 - 10
                                    <span class="radiomark"></span>
                                </label>
                            </div>
                            <div class="label-icon">
                                <label class="radioholder">
                                    <input type="radio" name="financial_goal_plan" id="financial_goal_plan" value="11 - 20" {{@Auth::user()->financialSituation->financial_goal_period == "11 - 20"? "checked":""}}> 11 - 20
                                    <span class="radiomark"></span>
                                </label>
                            </div>
                            <div class="label-icon">
                                <label class="radioholder">
                                    <input type="radio" name="financial_goal_plan" id="financial_goal_plan" value="Over 20" {{@Auth::user()->financialSituation->financial_goal_period == "Over 20"? "checked":""}}> Over 20
                                    <span class="radiomark"></span>
                                </label>
                            </div>
                            <div class="financial_goal_plan-error" style="color:red"></div>
                        </div>
                        <div class="col-12 btn-top-mrg">
                            <button type="button" class="btn mt-2 form-wizard-previous-btn" onclick="gotoPreviousTab(6)">Previous</button>
                            <button class="btn rounded mt-2 form-wizard-next-btn" type="button" id="btnStepSeven">Next
                                <span class="loader" style="display: none">
                                    <div class="spinner-border text-light" role="status" style="width: 1rem;height: 1rem;">
                                        <span class="sr-only">Loading...</span>
                                    </div>
                                </span>
                            </button>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="tab-pane fade" id="investment_experience" role="tabpanel" aria-labelledby="investment_experience-tab">
            <div class="card mt-2">
                <div class="card-header">
                    <h4>Investment Experience</h4>
                </div>
                <div class="col-md-12">
                    <p>We are collecting the information below to better understand your investment experience. We recognize your responses may change over time as you work with us. Please check the boxes that best describe your investment experience to date.</p>
                </div>
                <div class="card-body">
                    <form action="" method="post" class="row" id="stepeightForm">
                        @csrf
                        <div class="col-12">
                            <table class="table">
                                <thead>
                                    <tr>
                                        <th>Investment</th>
                                        <th>Year(s) Of Experience</th>
                                        <th>Knowledge</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td colspan="3">
                                            <div class="investStock-error stockExpertise-error" style="color:red"></div>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>
                                            <label for="investStock" class="labelholder">
                                                <input type="checkbox" name="investStock" id="investStock" value="1" {{@Auth::user()->investmentExperience->is_stocks == 1 ?'checked':''}} onchange="changeThis(this)">
                                                <span class="checkmark"></span>
                                                <span>Stocks</span>
                                            </label>

                                        </td>
                                        <td>
                                            <label class="radioholder">
                                                <input type="radio" name="stockExp" id="stockExp" value="0" {{@Auth::user()->investmentExperience->stock_year_experience == '0' ?'checked':''}} {{@Auth::user()->investmentExperience->is_stocks == 1 ?'':'disabled'}}> 0
                                                <span class="radiomark"></span>
                                            </label>
                                            <label class="radioholder ">
                                                <input type="radio" name="stockExp" id="stockExp1" value="1-5" {{@Auth::user()->investmentExperience->stock_year_experience == '1-5' ?'checked':''}} {{@Auth::user()->investmentExperience->is_stocks == 1 ?'':'disabled'}}> 1-5
                                                <span class="radiomark"></span>
                                            </label>
                                            <label class="radioholder ">
                                                <input type="radio" name="stockExp" id="stockExp2" value="5+" {{@Auth::user()->investmentExperience->stock_year_experience == '5+' ?'checked':''}} {{@Auth::user()->investmentExperience->is_stocks == 1 ?'':'disabled'}}> 5+
                                                <span class="radiomark"></span>
                                            </label>
                                        </td>
                                        <td>
                                            <label class="radioholder">
                                                <input type="radio" name="stockExpertise" id="stockExpertise" value="None" {{@Auth::user()->investmentExperience->stock_knowledge == 'None' ?'checked':''}} {{@Auth::user()->investmentExperience->is_stocks == 1 ?'':'disabled'}}>
                                                None
                                                <span class="radiomark"></span>
                                            </label>
                                            <label class="radioholder">
                                                <input type="radio" name="stockExpertise" id="stockExpertise1" value="Limited" {{@Auth::user()->investmentExperience->stock_knowledge == 'Limited' ?'checked':''}} {{@Auth::user()->investmentExperience->is_stocks == 1 ?'':'disabled'}}>
                                                Limited
                                                <span class="radiomark"></span>
                                            </label>
                                            <label class="radioholder">
                                                <input type="radio" name="stockExpertise" id="stockExpertise2" {{@Auth::user()->investmentExperience->stock_knowledge == 'Good' ?'checked':''}} value="Good" {{@Auth::user()->investmentExperience->is_stocks == 1 ?'':'disabled'}}>
                                                Good
                                                <span class="radiomark"></span>
                                            </label>
                                            <label class="radioholder">
                                                <input type="radio" name="stockExpertise" id="stockExpertise3" value="Extensive" {{@Auth::user()->investmentExperience->stock_knowledge == 'Extensive' ?'checked':''}} {{@Auth::user()->investmentExperience->is_stocks == 1 ?'':'disabled'}}>
                                                Extensive
                                                <span class="radiomark"></span>
                                            </label>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                            <label for="investBond" class="labelholder">
                                                <input type="checkbox" name="investBond" id="investBond" {{@Auth::user()->investmentExperience->is_fixed_income == 1 ?'checked':''}} value="1" onchange="changeThis(this)">
                                                <span class="checkmark"></span>
                                                <span>Fixed Income</span>
                                            </label>
                                        </td>
                                        <td>
                                            <label class="radioholder">
                                                <input type="radio" name="bondExp" id="bondExp" value="0" {{@Auth::user()->investmentExperience->fixed_income_year_experience == '0' ?'checked':''}} {{@Auth::user()->investmentExperience->is_fixed_income == 1 ?'':'disabled'}}> 0
                                                <span class="radiomark"></span>
                                            </label>
                                            <label class="radioholder ">
                                                <input type="radio" name="bondExp" id="bondExp1" value="1-5" {{@Auth::user()->investmentExperience->fixed_income_year_experience == '1-5' ?'checked':''}} {{@Auth::user()->investmentExperience->is_fixed_income == 1 ?'':'disabled'}}> 1-5
                                                <span class="radiomark"></span>
                                            </label>
                                            <label class="radioholder ">
                                                <input type="radio" name="bondExp" id="bondExp2" value="5+" {{@Auth::user()->investmentExperience->fixed_income_year_experience == '5+' ?'checked':''}} {{@Auth::user()->investmentExperience->is_fixed_income == 1 ?'':'disabled'}}> 5+
                                                <span class="radiomark"></span>

                                            </label>
                                        </td>
                                        <td>
                                            <label class="radioholder">
                                                <input type="radio" name="bondExpertise" id="bondExpertise" value="None" {{@Auth::user()->investmentExperience->fixed_income_knowledge == 'None' ?'checked':''}} {{@Auth::user()->investmentExperience->is_fixed_income == 1 ?'':'disabled'}}>
                                                None
                                                <span class="radiomark"></span>
                                            </label>
                                            <label class="radioholder">
                                                <input type="radio" name="bondExpertise" id="bondExpertise1" value="Limited" {{@Auth::user()->investmentExperience->fixed_income_knowledge == 'Limited' ?'checked':''}} {{@Auth::user()->investmentExperience->is_fixed_income == 1 ?'':'disabled'}}>
                                                Limited
                                                <span class="radiomark"></span>
                                            </label>
                                            <label class="radioholder">
                                                <input type="radio" name="bondExpertise" id="bondExpertise2" value="Good" {{@Auth::user()->investmentExperience->fixed_income_knowledge == 'Good' ?'checked':''}} {{@Auth::user()->investmentExperience->is_fixed_income == 1 ?'':'disabled'}}>
                                                Good
                                                <span class="radiomark"></span>
                                            </label>
                                            <label class="radioholder">
                                                <input type="radio" name="bondExpertise" id="bondExpertise3" value="Extensive" {{@Auth::user()->investmentExperience->fixed_income_knowledge == 'Extensive' ?'checked':''}} {{@Auth::user()->investmentExperience->is_fixed_income == 1 ?'':'disabled'}}>
                                                Extensive
                                                <span class="radiomark"></span>
                                            </label>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                            <label for="investOptions" class="labelholder">
                                                <input type="checkbox" name="investOptions" id="investOptions" value="1" {{@Auth::user()->investmentExperience->is_options == 1 ?'checked':''}} onchange="changeThis(this)">
                                                <span class="checkmark"></span>
                                                <span>Options</span>
                                            </label>
                                        </td>
                                        <td>
                                            <label class="radioholder">
                                                <input type="radio" name="optionsExp" id="optionsExp" value="0" {{@Auth::user()->investmentExperience->options_year_experience == '0' ?'checked':''}} {{@Auth::user()->investmentExperience->is_options == 1 ?'':'disabled'}}> 0
                                                <span class="radiomark"></span>
                                            </label>
                                            <label class="radioholder ">
                                                <input type="radio" name="optionsExp" id="optionsExp1" value="1-5" {{@Auth::user()->investmentExperience->options_year_experience == '1-5' ?'checked':''}} {{@Auth::user()->investmentExperience->is_options == 1 ?'':'disabled'}}> 1-5
                                                <span class="radiomark"></span>
                                            </label>
                                            <label class="radioholder ">
                                                <input type="radio" name="optionsExp" id="optionsExp2" value="5+" {{@Auth::user()->investmentExperience->options_year_experience == '5+' ?'checked':''}} {{@Auth::user()->investmentExperience->is_options == 1 ?'':'disabled'}}> 5+
                                                <span class="radiomark"></span>

                                            </label>
                                        </td>
                                        <td>
                                            <label class="radioholder">
                                                <input type="radio" name="optionsExpertise" id="optionsExpertise" {{@Auth::user()->investmentExperience->options_knowledge == 'None' ?'checked':''}} value="None" {{@Auth::user()->investmentExperience->is_options == 1 ?'':'disabled'}}>
                                                None
                                                <span class="radiomark"></span>
                                            </label>
                                            <label class="radioholder">
                                                <input type="radio" name="optionsExpertise" id="optionsExpertise1" {{@Auth::user()->investmentExperience->options_knowledge == 'Limited' ?'checked':''}} value="Limited" {{@Auth::user()->investmentExperience->is_options == 1 ?'':'disabled'}}>
                                                Limited
                                                <span class="radiomark"></span>
                                            </label>
                                            <label class="radioholder">
                                                <input type="radio" name="optionsExpertise" id="optionsExpertise2" {{@Auth::user()->investmentExperience->options_knowledge == 'Good' ?'checked':''}} value="Good" {{@Auth::user()->investmentExperience->is_options == 1 ?'':'disabled'}}>
                                                Good
                                                <span class="radiomark"></span>
                                            </label>
                                            <label class="radioholder">
                                                <input type="radio" name="optionsExpertise" id="optionsExpertise3" value="Extensive" {{@Auth::user()->investmentExperience->options_knowledge == 'Extensive' ?'checked':''}} {{@Auth::user()->investmentExperience->is_options == 1 ?'':'disabled'}}>
                                                Extensive
                                                <span class="radiomark"></span>
                                            </label>
                                        </td>
                                    </tr>

                                    <tr>
                                        <td>
                                            <!-- <label for="investFuture">
                                                    <input type="checkbox" value="1" name="investFuture" id="investFuture">
                                                    Futures
                                                </label> -->
                                            <label for="investFuture" class="labelholder">
                                                <input type="checkbox" name="investFuture" id="investFuture" value="1" {{@Auth::user()->investmentExperience->is_futures == 1 ?'checked':''}} onchange="changeThis(this)">
                                                <span class="checkmark"></span>
                                                <span>Futures</span>
                                            </label>
                                        </td>
                                        <td>
                                            <label class="radioholder">
                                                <input type="radio" name="futureExp" id="futureExp" value="0" {{@Auth::user()->investmentExperience->futures_year_experience == '0' ?'checked':''}} {{@Auth::user()->investmentExperience->is_futures == 1 ?'':'disabled'}}>
                                                0
                                                <span class="radiomark"></span>
                                            </label>
                                            <label class="radioholder ">
                                                <input type="radio" name="futureExp" id="futureExp1" value="1-5" {{@Auth::user()->investmentExperience->futures_year_experience == '1-5' ?'checked':''}} {{@Auth::user()->investmentExperience->is_futures == 1 ?'':'disabled'}}> 1-5
                                                <span class="radiomark"></span>
                                            </label>
                                            <label class="radioholder ">
                                                <input type="radio" name="futureExp" id="futureExp2" value="5+" {{@Auth::user()->investmentExperience->futures_year_experience == '5+' ?'checked':''}}  {{@Auth::user()->investmentExperience->is_futures == 1 ?'':'disabled'}}> 5+
                                                <span class="radiomark"></span>

                                            </label>

                                        </td>
                                        <td>
                                            <label class="radioholder">
                                                <input type="radio" name="futureExpertise" id="futureExpertise" value="None" {{@Auth::user()->investmentExperience->futures_knowledge == 'None' ?'checked':''}} {{@Auth::user()->investmentExperience->is_futures == 1 ?'':'disabled'}}>
                                                None
                                                <span class="radiomark"></span>
                                            </label>
                                            <label class="radioholder">
                                                <input type="radio" name="futureExpertise" id="futureExpertise1" value="Limited" {{@Auth::user()->investmentExperience->futures_knowledge == 'Limited' ?'checked':''}} {{@Auth::user()->investmentExperience->is_futures == 1 ?'':'disabled'}}>
                                                Limited
                                                <span class="radiomark"></span>
                                            </label>
                                            <label class="radioholder">
                                                <input type="radio" name="futureExpertise" id="futureExpertise2" value="Good" {{@Auth::user()->investmentExperience->futures_knowledge == 'Good' ?'checked':''}} {{@Auth::user()->investmentExperience->is_futures == 1 ?'':'disabled'}}>
                                                Good
                                                <span class="radiomark"></span>
                                            </label>
                                            <label class="radioholder">
                                                <input type="radio" name="futureExpertise" id="futureExpertise3" value="Extensive" {{@Auth::user()->investmentExperience->futures_knowledge == 'Extensive' ?'checked':''}} {{@Auth::user()->investmentExperience->is_futures == 1 ?'':'disabled'}}>
                                                Extensive
                                                <span class="radiomark"></span>
                                            </label>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>

                        <div class="col-12">
                            <button type="button" class="btn mt-2 form-wizard-previous-btn" onclick="gotoPreviousTab(7)">Previous</button>
                            <button class="btn rounded mt-2 form-wizard-next-btn" type="button" id="btnStepEight">Next
                                <span class="loader" style="display: none">
                                    <div class="spinner-border text-light" role="status" style="width: 1rem;height: 1rem;">
                                        <span class="sr-only">Loading...</span>
                                    </div>
                                </span>
                            </button>
                        </div>

                    </form>
                </div>
            </div>
        </div>

        <div class="tab-pane fade" id="identification_proof" role="tabpanel" aria-labelledby="identification_proof-tab">
            <div class="card mt-2">
                <div class="card-header">
                    <h4>Identification Proof</h4>
                </div>
                <div class="col-md-12">
                    <p>Government issued ID. If Driver's License is used and the address is not the same as on the application please provide a utility bill with your name and address. You can email a copy to accountopening@guardiantrading.com</p>
                    <p><b>Please upload a copy of the Applicant’s Government issued ID in jpeg format (Front and Back)</b></p>
                </div>
                <div class="card-body">
                    <form action="" method="post" class="row" id="stepnineForm">
                        @csrf
                        <div class="col-6">
                            <input type="file" name="first_photo_front" class="form-control " id="first_photo_front" accept=".jpeg, .jpg">
                        </div>
                        <div class="col-6">
                            <input type="file" name="first_photo_back" class="form-control " id="first_photo_back" accept=".jpeg, .jpg">
                        </div>
                        <div class="col-6">
                            <input type="file" name="second_photo_front" class="form-control " id="second_photo_front" accept=".jpeg, .jpg">
                        </div>
                        <div class="col-6">
                            <input type="file" name="second_photo_back" class="form-control " id="second_photo_back" accept=".jpeg, .jpg">
                        </div>

                        <div class="col-6">
                            <button type="button" class="btn btn-link modal_btn_custom" data-modal-name="identification_proof" style="font-size: 15px;"> Image Hints and Tips </button>
                        </div>

                        @if (Auth::user()->registration_type == "Corporate")


                        <div class="col-md-12">
                            <p>In addition to your ID, please submit Article of Incorporation, Certificate of Incorporation, Articles of Organization and Certification of Beneficial Ownership. (If foreign please submit W8 BEN-E)</p>
                            <p><b>Please upload your in jpeg format)</b></p>
                        </div>

                        <div class="col-6">
                            <input type="file" name="third_photo_front" class="form-control " id="third_photo_front" accept=".jpeg, .jpg">
                        </div>
                        <div class="col-6">
                            <input type="file" name="third_photo_back" class="form-control " id="third_photo_back" accept=".jpeg, .jpg">
                        </div>
                        <div class="col-6">
                            <input type="file" name="fourth_photo_front" class="form-control " id="fourth_photo_front" accept=".jpeg, .jpg">
                        </div>
                        <div class="col-6">
                            <input type="file" name="fourth_photo_back" class="form-control " id="fourth_photo_back" accept=".jpeg, .jpg">
                        </div>

                        <div class="col-6">
                            <button type="button" class="btn btn-link " style="font-size: 15px;"> Image Hints and Tips </button>
                        </div>
                        @endif
                        <div class="col-12 photo1-error" style="color:red">

                        </div>

                        <div class="col-12">
                            <button type="button" class="btn mt-2 form-wizard-previous-btn" onclick="gotoPreviousTab(8)">Previous</button>
                            <button class="btn rounded mt-2 form-wizard-next-btn" type="button" id="btnStepNine">Next
                                <span class="loader" style="display: none">
                                    <div class="spinner-border text-light" role="status" style="width: 1rem;height: 1rem;">
                                        <span class="sr-only">Loading...</span>
                                    </div>
                                </span>
                            </button>
                        </div>

                    </form>
                </div>
            </div>
        </div>

        <div class="tab-pane fade" id="disclouser" role="tabpanel" aria-labelledby="disclouser-tab">
            <div class="card mt-2">
                <div class="card-header">
                    <h2 class="fs-title">Disclosures</h2>
                </div>
                <div class="col-md-12 col-sm-12 text-left">
                    <p>Borrowing Money to Buy Securities (Buying "on Margin") - (Please read carefully)</p>
                    <p>You chose to have a "margin loan account" (customarily known as "margin account") by checking the boxes below. To help you decide whether a margin loan account is right for you. Please read the information below and the client agreement.</p>
                </div>
                <div class="card-body" style="padding: 1.25rem;">
                    <form action="" method="post" class="row" id="steptenForm">
                        @csrf

                        <div class="col-sm-12 text-left">
                            <br>
                            <p>I want the ability to borrow funds in my account. I have read the client agreement and disclosures understand my right and obligation under it *</p>
                        </div>

                        <div class="col-sm-12 text-left ">
                            <label class="radiobox_label">
                                <input type="radio" name="agreement_opt" class="optRadio" value="1" {{@Auth::user()->disclosure->agreement == 1?'checked':''}}>Yes
                            </label>

                            <label class="radiobox_label">
                                <input type="radio" name="agreement_opt" class="optRadio" value="0" {{@Auth::user()->disclosure->agreement == 0 || @Auth::user()->disclosure ==null?'checked':''}}>No, (I disagree)
                            </label>
                            <p class="text-danger agreement_opt-error"></p>
                            <br>
                        </div>

                        <div class="col-sm-12 ">
                            <br>
                            <p>Pattern day trading accounts are only offered to an account that maintains balance greater than $25,000. Guardian Trading requires an intial deposit of at least $30,000.
                                What is the initial deposit that you will fund your account with? *</p>
                            <input type="number" name="initial_deposit" id="initial_deposit" value="{{@Auth::user()->disclosure->initial_deposit}}" class="form-control" data-toggle="tooltip" data-placement="top">
                        </div>

                        <div class="col-sm-12 ">
                            <p>Do you maintain any other Brokerage accounts? *</p>
                            <label class=" radiobox_label">
                                <input type="radio" name="brokage_account" data-div=".brokage_div" {{@Auth::user()->disclosure->brokage_account == 1?'checked':''}} class="radio_div_display" value="1">Yes
                            </label>

                            <label class=" radiobox_label">
                                <input type="radio" name="brokage_account" data-div=".brokage_div" class="radio_div_display" value="0" {{@Auth::user()->disclosure->brokage_account == 0 || @Auth::user()->disclosure ==null?'checked':''}}>No
                            </label>
                        </div>
                        @php
                        $brokage_account_data = json_decode(@Auth::user()->disclosure->brokage_account_data);
                        @endphp
                        <div class="col-6 brokage_div" style="{{@Auth::user()->disclosure->brokage_account == 1?'':'display: none'}}">
                            <label>Number Of Accounts (total)*</label>
                            <input type="number" name="brokage_account_total" id="brokage_account_total" value="{{@$brokage_account_data->brokage_account_total}}" data-toggle="tooltip" data-placement="top" class="form-control">
                        </div>

                        <div class="col-sm-12 ">
                            <p>Do you already maintain an account at either Velocity Clearing LLC in which you have control, beneficial interest, or trading authority? *</p>
                            <label class=" radiobox_label">
                                <input type="radio" name="beneficial" data-div=".beneficial_div" class="radio_div_display" value="1" {{@Auth::user()->disclosure->beneficial == 1?'checked':''}}>Yes
                            </label>

                            <label class="radiobox_label">
                                <input type="radio" name="beneficial" data-div=".beneficial_div" class="radio_div_display" value="0" {{@Auth::user()->disclosure->beneficial == 0 || @Auth::user()->disclosure ==null?'checked':''}}>No
                            </label>
                        </div>
                        @php
                        $beneficial_data = json_decode(@Auth::user()->disclosure->beneficial_data);
                        @endphp
                        <div class="col-6 beneficial_div" style="{{@Auth::user()->disclosure->beneficial == 1?'':'display: none'}}">
                            <label>Account Number*</label>
                            <input type="text" name="beneficial_account_no" value="{{@$beneficial_data->beneficial_account_no}}" id="beneficial_account_no" class="form-control" data-toggle="tooltip" data-placement="top" maxlength="{{$account_length}}" onkeydown="return alphaNumeric(event);">
                        </div>
                        <div class="col-6 beneficial_div" style="{{@Auth::user()->disclosure->beneficial == 1?'':'display: none'}}">
                            <label>Accounts Name*</label>
                            <input type="text" name="beneficial_account_name" value="{{@$beneficial_data->beneficial_account_name}}" id="beneficial_account_name" class="form-control" data-toggle="tooltip" data-placement="top" maxlength="{{$default_length}}">
                        </div>

                        <div class="col-sm-12 ">
                            <p>Do you have a relationship with an entity that already maintains an account at Velocity Clearing LLC, such as employee, officer, shareholder, member, partner or owner? *</p>
                            <label class=" radiobox_label">
                                <input type="radio" name="shareholder" data-div=".shareholder_div" class="radio_div_display" value="1" {{@Auth::user()->disclosure->shareholder == 1?'checked':''}}>Yes
                            </label>

                            <label class=" radiobox_label">
                                <input type="radio" name="shareholder" data-div=".shareholder_div" class="radio_div_display" value="0" {{@Auth::user()->disclosure->shareholder == 0 || @Auth::user()->disclosure ==null?'checked':''}} >No
                            </label>
                        </div>
                        @php
                        $shareholder_data = json_decode(@Auth::user()->disclosure->shareholder_data);
                        @endphp
                        <div class="col-6 shareholder_div" style="{{@Auth::user()->disclosure->shareholder == 1?'':'display: none'}}">
                            <label>Accounts Number*</label>
                            <input type="text" name="shareholder_number" id="shareholder_number" value="{{@$shareholder_data->shareholder_number}}" class="form-control" data-toggle="tooltip" data-placement="top" maxlength="{{$account_length}}" onkeydown="return alphaNumeric(event);">
                        </div>
                        <div class="col-6 shareholder_div" style="{{@Auth::user()->disclosure->shareholder == 1?'':'display: none'}}">
                            <label>Accounts Name*</label>
                            <input type="text" name="shareholder_name" id="shareholder_name" value="{{@$shareholder_data->shareholder_name}}" class="form-control" data-toggle="tooltip" data-placement="top" maxlength="{{$default_length}}">
                        </div>
                        <div class="col-6 shareholder_div" style="{{@Auth::user()->disclosure->shareholder == 1?'':'display: none'}}">
                            <label>Relationship*</label>
                            <input type="text" name="shareholder_relation" id="shareholder_relation" value="{{@$shareholder_data->shareholder_relation}}" class="form-control" data-toggle="tooltip" data-placement="top" maxlength="{{$default_length}}">
                        </div>

                        <div class="col-sm-12 ">
                            <p>Are either you or an immediate family member an officer, director or at least 10% shareholder in a publicly traded company? *</p>
                            <label class=" radiobox_label">
                                <input type="radio" name="immediate" data-div=".immediate_div" class="radio_div_display" value="1" {{@Auth::user()->disclosure->immediate == 1?'checked':''}}>Yes
                            </label>

                            <label class=" radiobox_label">
                                <input type="radio" name="immediate" data-div=".immediate_div" class="radio_div_display" value="0" {{@Auth::user()->disclosure->immediate == 0 || @Auth::user()->disclosure ==null?'checked':''}}>No
                            </label>
                        </div>
                        @php
                        $immediate_data = json_decode(@Auth::user()->disclosure->immediate_data);
                        @endphp
                        <div class="col-6 immediate_div" style="{{@Auth::user()->disclosure->immediate == 1?'':'display: none'}}">
                            <label>Company Name*</label>
                            <input type="text" name="immediate_company_name" value="{{@$immediate_data->immediate_company_name}}" id="immediate_company_name" class="form-control" data-toggle="tooltip" data-placement="top" maxlength="{{$company_name_length}}">
                        </div>
                        <div class="col-6 immediate_div" style="{{@Auth::user()->disclosure->immediate == 1?'':'display: none'}}">
                            <label>Company Address*</label>
                            <input type="text" name="immediate_company_address" value="{{@$immediate_data->immediate_company_address}}" id="immediate_company_address" class="form-control" data-toggle="tooltip" data-placement="top" maxlength="{{$address_length}}">
                        </div>
                        <div class="col-6 immediate_div" style="{{@Auth::user()->disclosure->immediate == 1?'':'display: none'}}">
                            <label>Relationship with entity*</label>
                            <input type="text" name="immediate_relation" value="{{@$immediate_data->immediate_relation}}" id="immediate_relation" class="form-control" data-toggle="tooltip" data-placement="top" maxlength="{{$default_length}}">
                        </div>

                        <div class="col-sm-12 ">
                            <p>Are either you or an immediate family member employed by FINRA, a registered broker dealer or a securities exchange? *</p>
                            <label class=" radiobox_label">
                                <input type="radio" name="securities" data-div=".securities_div" class="radio_div_display" value="1" {{@Auth::user()->disclosure->securities == 1?'checked':''}}>Yes
                            </label>

                            <label class=" radiobox_label">
                                <input type="radio" name="securities" data-div=".securities_div" class="radio_div_display" value="0" {{@Auth::user()->disclosure->securities == 0 || @Auth::user()->disclosure ==null?'checked':''}}>No
                            </label>
                        </div>
                        @php
                        $securities_data = json_decode(@Auth::user()->disclosure->securities_data);
                        @endphp
                        <div class="col-6 securities_div" style="{{@Auth::user()->disclosure->securities == 1?'':'display: none'}}">
                            <label>Name of the Firm Or Exchange*</label>
                            <input type="text" name="securities_firm_name" value="{{@$securities_data->securities_firm_name}}" id="securities_firm_name" class="form-control" data-toggle="tooltip" data-placement="top" maxlength="{{$name_length}}">
                        </div>
                        <div class="col-6 securities_div" style="{{@Auth::user()->disclosure->securities == 1?'':'display: none'}}">
                            <label>Firm Address*</label>
                            <input type="text" name="securities_firm_address" value="{{@$securities_data->securities_firm_address}}" id="securities_firm_address" class="form-control" data-toggle="tooltip" data-placement="top" maxlength="{{$address_length}}">
                        </div>
                        <div class="col-6 securities_div" style="{{@Auth::user()->disclosure->securities == 1?'':'display: none'}}">
                            <label>Permission to open the account*</label>
                            <input type="file" name="securities_permission"  accept=".jpg, .jpeg, .pdf" id="securities_permission" class="form-control" data-toggle="tooltip" data-placement="top">
                        </div>

                        <div class="col-sm-12 ">
                            <p>Are you a senior officer at a bank, savings and loan institution, investment company, investment advisory firm, or other financial institution? *</p>
                            <label class=" radiobox_label">
                                <input type="radio" name="disclosure_institution" data-div=".institution_div" class="radio_div_display" value="1" {{@Auth::user()->disclosure->institution == 1?'checked':''}}>Yes
                            </label>

                            <label class=" radiobox_label">
                                <input type="radio" name="disclosure_institution" data-div=".institution_div" class="radio_div_display" value="0" {{@Auth::user()->disclosure->institution == 0 || @Auth::user()->disclosure ==null?'checked':''}}>No
                            </label>
                        </div>
                        @php
                        $institution_data = json_decode(@Auth::user()->disclosure->institution_data);
                        @endphp
                        <div class="col-6 institution_div" style="{{@Auth::user()->disclosure->institution == 1?'':'display: none'}}">
                            <label>Name of the Firm*</label>
                            <input type="text" name="disclosure_institution_firm_name" value="{{@$institution_data->disclosure_institution_firm_name}}" id="disclosure_institution_firm_name" class="form-control" data-toggle="tooltip" data-placement="top" maxlength="{{$name_length}}">
                        </div>
                        <div class="col-6 institution_div" style="{{@Auth::user()->disclosure->institution == 1?'':'display: none'}}">
                            <label>Firm Address*</label>
                            <input type="text" name="disclosure_institution_firm_address" value="{{@$institution_data->disclosure_institution_firm_address}}"  id="disclosure_institution_firm_address" class="form-control" data-toggle="tooltip" data-placement="top" maxlength="{{$address_length}}">
                        </div>
                        <div class="col-6 institution_div" style="{{@Auth::user()->disclosure->institution == 1?'':'display: none'}}">
                            <label>Position in Firm*</label>
                            <input type="text" name="disclosure_institution_firm_position" value="{{@$institution_data->disclosure_institution_firm_position}}"  id="disclosure_institution_firm_position" class="form-control" data-toggle="tooltip" data-placement="top" maxlength="{{$default_length}}">
                        </div>

                        <div class="col-md-12 mt-3" style="background-color: #dedede;">
                            <br>
                            <p><b>Tax Withholding Certifications - Choose One</b></p>

                            <label class="radioholder">
                                <input type="radio" onclick="show1();" name="taxWitholding" value="U.S. Person" {{@Auth::user()->disclosure->tax_with_holding == 'U.S. Person' || @Auth::user()->disclosure ==null?'checked':''}}>
                                <b>U.S. Person:</b> under penalty of perjury, I certify that:
                                <span class="radiomark"></span>



                                <ul style="list-style: none; {{@Auth::user()->disclosure->tax_with_holding != 'U.S. Person'?'display: none;':'display: block;'}}" id="undershow1">
                                    <li>
                                        <label class="radioholder"> 1.<input type="radio" onclick="show3();" name="underUs" value="1" {{@Auth::user()->disclosure->under_us == '1' || @Auth::user()->disclosure ==null?'checked':''}}>
                                            I am a U.S. citizen or a U.S. Resident Alien or other U.S. Person
                                            and the
                                            Social Security Number or Taxpayer identification Number
                                            provided in this
                                            application is correct
                                            <span class="radiomark"></span>
                                        </label>
                                    </li>
                                    <li>
                                        <label class="radioholder">
                                            2.<input type="radio" onclick="show2();" name="underUs" value="2" {{@Auth::user()->disclosure->under_us == '2'?'checked':''}}> I am
                                            not subject to backup
                                            withholding because

                                            <span class="radiomark"></span>

                                        </label>
                                    </li>

                                </ul>
                                <ul style="list-style:none;margin-left: 30px;margin-bottom:0px;{{@Auth::user()->disclosure->under_us == '2'?'':'display:none;'}} " id="undershow2">
                                    <li>
                                        <label class="radioholder">a. <input type="radio" name="underUs2" value="a" {{@Auth::user()->disclosure->under_us_detail == 'a'?'checked':''}}> I am
                                            exempt from backup
                                            withholding or
                                            <span class="radiomark"></span>
                                        </label>
                                    </li>

                                    <li>
                                        <label class="radioholder">b. <input type="radio" name="underUs2" value="b" {{@Auth::user()->disclosure->under_us_detail == 'b'?'checked':''}}> I
                                            have not been notified
                                            by the Internal Revenue Services (IRS) that I am subject to
                                            backup
                                            withholding as a result of failure to report all interest or
                                            dividends or
                                            <span class="radiomark"></span>
                                        </label>
                                    </li>
                                    <li><label class="radioholder">c.
                                            <input type="radio" name="underUs2" value="c"> The IRS has
                                            notified me
                                            that I am no longer subject to backup withholding.
                                            <span class="radiomark"></span>
                                        </label></li>

                                </ul>


                            </label>
                            <label class="radioholder">
                                <input type="radio" name="taxWitholding" {{@Auth::user()->disclosure->tax_with_holding == 'Certification Instruction'?'checked':''}} onclick="show4();" value="Certification Instruction">
                                <b>Certification Instruction:</b> I cannot certify that I am not subject to
                                backup
                                withholding, meaning that I have been notified by the IRS that I am
                                currently subject to
                                backup withholding because I have failed to report all interest and
                                dividends on my tax
                                return.
                                <span class="radiomark"></span>
                            </label>
                            <label class="radioholder">
                                <input type="radio" name="taxWitholding" {{@Auth::user()->disclosure->tax_with_holding == 'Non Residence Alien'?'checked':''}} onclick="show4();" value="Non Residence Alien">
                                <b>Non Resident
                                    Alien:</b> I certify that I am not a U.S. resident alien or other U.S.
                                person for
                                U.S. tax purpose and I am submitting the applicable Form W-8 with this
                                application to
                                certify my foreign status and if applicable, claim tax treaty benefits.
                                <span class="radiomark"></span>
                            </label>
                            <br>
                        </div>


                        <div class="col-12">
                            <button type="button" class="btn mt-5 form-wizard-previous-btn" onclick="gotoPreviousTab(9)">Previous</button>
                            <button class="btn rounded mt-5 form-wizard-next-btn" type="button" id="btnStepTen">Next
                                <span class="loader" style="display: none">
                                    <div class="spinner-border text-light" role="status" style="width: 1rem;height: 1rem;">
                                        <span class="sr-only">Loading...</span>
                                    </div>
                                </span>
                            </button>
                        </div>

                    </form>
                </div>
            </div>
        </div>

        <div class="tab-pane fade" id="signature" role="tabpanel" aria-labelledby="signature-tab">
            <form action="" method="post" class="row" id="stepelevenForm">
                @csrf
                <h2 class="fs-title">Disclosures &amp; Signatures</h2>

                <div class="maincontent">
                    <div class="utility-coonections">
                        <div class="row clientagreerow" style="margin: 0px;margin-top: 20px;">
                            <div class="col-md-12 text-left pdfagreement">
                                <p> Please select the disclosures below and the check the box noting you
                                    have read and
                                    understood these disclosures.</p>
                                <div class="row">
                                    <input type="hidden" name="id" value="6079764bc907937d54f47014">
                                    <div class="col-md-5">
                                        <label class="labelholder" style="padding-left:0px">
                                            Account Terms &amp; Conditions <span class="text-danger"> *</span>
                                        </label>
                                    </div>
                                    <div class="col-md-1 viewholder-txt"><a onclick="showFileModal(this, 0);" href="javascript:;"> View </a>
                                    </div>
                                    <div class="col-md-1">
                                        <input type="checkbox" {{ (@Auth::user()->signature->terms_and_condition == 1)?'checked':'' }} name="accountTerms" id="accountTerms" class="form-control checkbox_form_control" value="1" required="required" data-toggle="tooltip" title="" data-original-title="Account Terms &amp; Conditions is required">
                                    </div>

                                    <div class="col-md-4 viewholder-txt">
                                        <p>I provide my consent </p>
                                    </div>

                                </div><!-- // Row // -->

                                <div class="row">

                                    <div class="col-md-5">
                                        <label class="labelholder" style="padding-left:0px">Day Trading Risk
                                            Disclosure
                                            <span class="text-danger"> *</span>
                                        </label>
                                    </div><!-- // Col // -->
                                    <div class="col-md-1 viewholder-txt"><a onclick="showFileModal(this, 1);" href="javascript:;"> View </a>
                                    </div><!-- // Col // -->
                                    <div class="col-md-1">
                                        <input type="checkbox" {{ (@Auth::user()->signature->risk_disclosure == 1)?'checked':'' }} class="form-control checkbox_form_control validate" name="riskDis" id="riskDis" required="required" value="1" data-toggle="tooltip" title="" data-original-title="Day Trading Risk Disclosure is required">
                                    </div>
                                    <div class="col-md-4 viewholder-txt">
                                        <p> I provide my consent</p>
                                    </div><!-- // Col // -->

                                </div><!-- // Row // -->

                                <div class="row">

                                    <div class="col-md-5">
                                        <label class="labelholder" style="padding-left:0px">Penny Stocks
                                            Disclosure
                                            <span class="text-danger"> *</span> </label>
                                    </div>
                                    <div class="col-md-1 viewholder-txt"><a onclick="showFileModal(this, 2);" href="javascript:;"> View </a>
                                    </div>
                                    <div class="col-md-1">

                                        <input type="checkbox" {{ (@Auth::user()->signature->stocks_disclosure == 1)?'checked':'' }} class="form-control checkbox_form_control validate" name="pennyStocks" id="pennyStocks" required="required" value="1" data-toggle="tooltip" title="" data-original-title="Penny Stocks Disclosure is required">


                                    </div>
                                    <div class="col-md-4 viewholder-txt">
                                        <p> I provide my consent </p>
                                    </div>
                                </div><!-- // Row // -->

                                <div class="row">

                                    <div class="col-md-5">
                                        <label class="labelholder" style="padding-left:0px">Electronic
                                            Access &amp; Trading
                                            Agreement <span class="text-danger"> *</span> </label>
                                    </div><!-- // Col // -->
                                    <div class="col-md-1 viewholder-txt"><a onclick="showFileModal(this, 3);" href="javascript:;"> View </a>
                                    </div><!-- // Col // -->
                                    <div class="col-md-1">

                                        <input type="checkbox" {{ (@Auth::user()->signature->trading_Agreement == 1)?'checked':'' }} class="form-control checkbox_form_control validate" name="electronicAccess" id="electronicAccess" required="required" data-toggle="tooltip" title="" value="1" data-original-title="Electronic Access &amp; Trading Agreement is required">


                                    </div><!-- // Col // -->
                                    <div class="col-md-4 viewholder-txt">
                                        <p>I provide my consent</p>
                                    </div><!-- // Col // -->
                                </div><!-- // Row // -->

                                <div class="row">

                                    <div class="col-md-5">
                                        <label class="labelholder" style="padding-left:0px">Margin
                                            Disclosure Statement
                                            <span class="text-danger"> *</span> </label>
                                    </div>
                                    <div class="col-md-1 viewholder-txt"> <a onclick="showFileModal(this, 4);" href="javascript:;"> View </a>
                                    </div>
                                    <div class="col-md-1">
                                        <input type="checkbox" {{ (@Auth::user()->signature->disclosure_statement == 1)?'checked':'' }} name="marginDisclosure" class="checkbox_form_control form-control validate" required="required" id="marginDisclosure" value="1" data-toggle="tooltip" title="" data-original-title="Margin Disclosure Statement is required">
                                    </div>
                                    <div class="col-md-4 viewholder-txt">
                                        <p>I provide my consent</p>
                                    </div>
                                </div><!-- // Row // -->

                                <div class="row">

                                    <div class="col-md-5">
                                        <label class="labelholder" style="padding-left:0px">W-9
                                            Certification <span class="text-danger"> *</span> </label>
                                    </div>
                                    <div class="col-md-1 viewholder-txt"> <a onclick="showFileModal(this, 5);" href="javascript:;"> View </a>
                                    </div>
                                    <div class="col-md-1">

                                        <input type="checkbox" {{ (@Auth::user()->signature->w9_certification == 1)?'checked':'' }} name="w9_Certification" class="checkbox_form_control form-control validate" required="required" id="w9_Certification" value="1" data-toggle="tooltip" title="" data-original-title="W-9 Certification is required">


                                    </div>
                                    <div class="col-md-4 viewholder-txt">
                                        <p>I provide my consent</p>
                                    </div>
                                </div><!-- // Row // -->

                                <div class="row">

                                    <div class="col-md-5">
                                        <label class="labelholder" style="padding-left:0px">Stock Locate
                                            Agreement <span class="text-danger"> *</span> </label>
                                    </div>
                                    <div class="col-md-1 viewholder-txt"> <a onclick="showFileModal(this, 6);" href="javascript:;"> View </a>
                                    </div>
                                    <div class="col-md-1">

                                        <input type="checkbox" {{ (@Auth::user()->signature->locate_agreement == 1)?'checked':'' }} name="stock_Locate" class="checkbox_form_control form-control validate" required="required" id="stock_Locate" value="1" data-toggle="tooltip" title="" data-original-title="Understanding Online Trading Risk Disclosure is required">

                                    </div>
                                    <div class="col-md-4 viewholder-txt">
                                        <p>I provide my consent</p>
                                    </div>
                                </div><!-- // Row // -->

                                <div class="row">
                                    <div class="col-md-5">
                                        <label class="labelholder" style="padding-left:0px"> Margin
                                            Agreement <span class="text-danger"> *</span> </label>
                                    </div>
                                    <div class="col-md-1 viewholder-txt"> <a onclick="showFileModal(this, 7);" href="javascript:;"> View </a>
                                    </div>
                                    <div class="col-md-1">

                                        <input type="checkbox" {{ (@Auth::user()->signature->margin_agreement == 1)?'checked':'' }} class="checkbox_form_control form-control validate" name="marginDisc" required="required" id="marginDisc" value="1" data-toggle="tooltip" title="" data-original-title="Margin Disclosure is required">

                                    </div>
                                    <div class="col-md-4 viewholder-txt">
                                        <p> I provide my consent</p>
                                    </div>
                                </div><!-- // Row // -->


                                <div class="lastbox-step-7">
                                    <p>Consent for mail delivery of statements and confirms otherwise they
                                        will be
                                        delivered electronically </p>
                                    <h6>Additional charges will apply if you do NOT check the below box for
                                        electronic
                                        delivery of statements, confirmations and tax documents</h6>
                                    <label for="confirmedElectronic" class="labelholder" style="padding-left:35px">
                                        <input type="checkbox" class="form-control" {{ (@Auth::user()->signature->confirm_electronic == 1)?'checked':'' }} name="confirmedElectronic" id="confirmedElectronic" value="1">
                                        Please check this box if you wish only to receive communications
                                        electronically,
                                        including trade confirmations, prospectuses, account statements,
                                        proxy
                                        materials, tax‐related documents, and marketing and sales documents.
                                        If you do
                                        not check this box, all such Communications will be delivered to you
                                        by standard
                                        mail.
                                        <span class="checkmark"></span>

                                    </label>

                                    <br>
                                    <div style="border:solid 1px black;"></div>
                                    <br>
                                    <p>By signing below, I/We attest to the accuracy of the information
                                        provided on this
                                        form. I/We acknowledge that we have received, read and agree to the
                                        terms and
                                        conditions contained in the attached Account Agreement, including
                                        the
                                        arbitration clause. By executing this agreement, I/We agree to be
                                        bound by the
                                        terms and conditions contained here in. </p>
                                    <br>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div id="signatureContainer"></div>
                                <label id="clear">Clear</label>
                                <input type="hidden" name="signature_val" id="signature-val">
                            </div>
                            <div class="signature-val-error" style="color:red"></div>
                            <input type="hidden" name="isDraft" value="0">

                        </div>

                    </div>
                </div>
                <div class="col-12">
                    <button type="button" class="btn mt-2 form-wizard-previous-btn" onclick="gotoPreviousTab(10)">Previous</button>
                    <button class="btn rounded mt-2 form-wizard-next-btn" type="button" id="btnStepEleven">Next
                        <span class="loader" style="display: none">
                            <div class="spinner-border text-light" role="status" style="width: 1rem;height: 1rem;">
                                <span class="sr-only">Loading...</span>
                            </div>
                        </span>
                    </button>
                </div>
            </form>
        </div>

        <div class="tab-pane fade" id="thankyou" role="tabpanel" aria-labelledby="thankyou-tab">
            <div class="card mt-2">
                <div class="card-header"></div>
                <div class="card-body maincontent">                    
                    <div class="thankyou-div">    
                        <h1>Thank You!</h1>
                    </div><div class="application-submit-div">    
                        <p>Application has been submitted</p>
                    </div>                    
                </div>
            </div>
        </div>

    </div>
</div>
<div id="myModal" class="modal fade" tabindex="-1">
    <div class="modal-dialog" style="max-width: 1300px !important;">
        <div class="modal-content" style="height:1000px">
            <div class="modal-header">
                <h5 class="modal-title"></h5>
                <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
                <iframe style="width:100%; height:100%;" id="iframe" scrolling="auto" frameborder="0" src=""></iframe>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
                <button type="button" class="btn btn-primary">Save</button>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footer')
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script src="{{asset('assets/admin/myplugin/signature/js/jquery.signature.js')}}"></script>

<script>
    // three_id_type
    
    $("#three_id_type").change(function(e) {
        e.preventDefault();
        _this = $(this);
        let val = _this.val();

        if (val == "Passport" || val == "Driver License") {
            $("#three_name_of_id").prop('disabled', true);
        } else if (val == "Local ID") {
            $("#three_name_of_id").prop('disabled', false);
        }


    });

    $(function() {
        signatureContainer.signature('draw', '<?= @Auth::user()->signature->signature_image ?>');
    });
    $(document).ready(function() {
        $("#three_id_type").trigger("change");
        $(".modal_btn_custom").click(function() {
            var selected = $(this).data('modal-name');
            if(selected == 'identification_proof')
            {
                $('#iframe').attr('src','{{asset("pdf/PassportVERIFICATIONGuideline.pdf#zoom=30")}}')
            }
            if(selected == 'acc_term_condition')
            {
                $('#iframe').attr('src','{{asset("pdf/AccountTermsConditions.pdf#zoom=30")}}')
            }
            if(selected == 'day_trading_risk')
            {
                $('#iframe').attr('src','{{asset("pdf/DayTradingRiskDisclosure.pdf#zoom=30")}}')
            }
            if(selected == 'penny_disclosure')
            {
                $('#iframe').attr('src','{{asset("pdf/PennyStockRiskDisclosure.pdf#zoom=30")}}')
            }
            if(selected == 'electronics_access')
            {
                $('#iframe').attr('src','{{asset("pdf/ElectronicAccessTradingAgreement.pdf#zoom=30")}}')
            }
            if(selected == 'margin_disclosure_agreement')
            {
                $('#iframe').attr('src','{{asset("pdf/MarginDisclosureStatement.pdf#zoom=30")}}')
            }
            if(selected == 'w_certification')
            {
                $('#iframe').attr('src','{{asset("pdf/W9Certification.pdf#zoom=30")}}')
            }
            if(selected == 'stock_locate')
            {
                $('#iframe').attr('src','{{asset("pdf/Stockslocateagreement.pdf#zoom=30")}}')
            }
            if(selected == 'margin_agreement')
            {
                $('#iframe').attr('src','{{asset("pdf/MarginAgreement.pdf#zoom=30")}}')
            }
            $("#myModal").modal('show');
        });
    });
</script>
@endsection