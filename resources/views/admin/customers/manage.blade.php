@extends('layouts.app')

@section('content')
<div class="card">
    <div class="card-header">
        <h3 class="card-title">DataTable with minimal features & hover style</h3>
    </div>
    <div class="card-body">
        <table id="example1" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th>S.No</th>
                    <th>User Name</th>
                    <th>UserEmail</th>
                    <th>Date</th>
                    <th>Status</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @php ($count = 1)
                @foreach( $data as $v)
                <tr>
                    <td>{{ $count++ }}</td>
                    <td>{{ @$v->personalDetail->first_name.' '.@$v->personalDetail->last_name }}</td>
                    <td>{{ $v->email }}</td>
                    <td>{{@date("m/d/Y",strtotime($v->created_at)) }}</td>
                    <td>
                        @if (@$v->status == 0)
                            <span class="badge badge-primary">draft</span>
                        @elseif(@$v->status == 1)
                            <span class="badge badge-info">pending</span>
                        @elseif(@$v->status == 3)
                            <span class="badge badge-success">accept</span>
                        @elseif(@$v->status == 4)
                            <span class="badge badge-danger">reject</span>
                        @endif
                    </td>
                    <td>
                        <a href="{{route('admin.user.details', $v->id)}}">
                            <button type="button" class="btn btn-sm btn-dark waves-effect waves-light" data-toggle="tooltip" data-placement="top" title="View Application">
                                <i class="fa fa-eye"></i>
                            </button>
                        </a>
                        @if ($v->signature !=null)
                            <a href="{{route('CustomerPDF',$v->id)}}" class="btn btn-sm btn-dark waves-effect waves-light" target="_blank">
                                <i class="fas fa-file-pdf"></i>
                            </a>
                        @endif
                        
                        @if (Request::segment(3) == 1)
                            <button type="button" class="btn btn-sm btn-success waves-effect waves-light changeStatusBtn" data-id="{{$v->id}}" data-value="accept" type="button"  data-toggle="tooltip" data-placement="top" title="Accept Application">
                                <i class="fa fa-check"></i> 
                            </button>
                            <button type="button" class="btn btn-sm btn-danger waves-effect waves-light changeStatusBtn" data-id="{{$v->id}}" data-value="reject" type="button" data-toggle="tooltip" data-placement="top" title="Reject Application">
                                <i class="far fa-window-close"></i> 
                            </button>
                        @endif
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
<div class="modal fade" id="StatusModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Are You Sure?</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
            <p id="message">Please confirm to accept this request?</p>
            <form action="" class="row" id="changeStatusForm">
                <input type="hidden" name="status" id="application-status">
                <input type="hidden" name="user_id" id="user_id">
                <div class="form-group col-12">
                    <label for="">Add Signature</label>
                    <br>
                    <div id="signatureContainer"></div>
                    <br>
                    <span id="clear" class="btn btn-light mt-2" style="cursor: pointer">Clear</span>
                    <input type="hidden" name="signature_val" id="signature-val">
                </div>
            </form>
          
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancel</button>
          <button type="button" class="btn btn-primary" id="btnSubmitApplication">Save changes</button>
        </div>
      </div>
    </div>
  </div>

@endsection
@section('footer')
<script>
    $(function() {
        $("#example1").DataTable({
            "responsive": true,
            "lengthChange": false,
            "autoWidth": false,
            "buttons": ["csv", "excel", "pdf", "print", "colvis"]
        }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
        $('[data-toggle="tooltip"]').tooltip()

        $('.changeStatusBtn').on('click',function(){
            $('#message').html(`Please confirm to <b>${$(this).attr('data-value')}</b> this request?`);
            $('#application-status').val($(this).attr('data-value'));
            $('#user_id').val($(this).attr('data-id'));
            $('#btnSubmitApplication').text("Yes! "+$(this).attr('data-value'));
            $('#StatusModal').modal('show')
        })
        var signatureContainer = $('#signatureContainer').signature();
        $('#clear').on('click',function() {
            signatureContainer.signature('clear');
        });
        $('#btnSubmitApplication').on('click',function(){
            submitbutton = $(this)
            if (signatureContainer.signature('isEmpty')) {
                return alert("Signature is required");
            }
            document.querySelector('#signature-val').value = $('#signatureContainer').signature('toJSON');
            $.ajax({
                url: '{{route('admin.change_application_status')}}',
                type: "POST",
                data:  new FormData(document.querySelector('#changeStatusForm')),
                contentType: false,
                cache: false,
                processData:false,
                beforeSend : function()
                {
                    submitbutton.prop("disabled",true);
                    // submitbutton.children[0].style.display = 'inline';
                },
                success: function(data)
                {
                    // console.log(data);
                    if($.isEmptyObject(data.error)){
                        // console.log(data.status)
                        window.location.reload();
                    }else{
                        // console.log(data.error);
                        // printErrorMessage(data.error,formId);
                    }
                },
                error: function(e) 
                {
                    console.log(e);
                },
                complete:function(){
                    submitbutton.prop("disabled",false);
                    // submitbutton.children[0].style.display = 'none';
                }      
            });
        });
    });
</script>
@endsection