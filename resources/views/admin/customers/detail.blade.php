@extends('layouts.app')

@section('content')
<div class="card" style="margin-bottom:0px;background-color: transparent;border: none !important;outline: none;
box-shadow: none !important;">
    
  {{-- <h5 class="mt-4 mb-2">Bootstrap Accordion & Carousel</h5> --}}

  <div class="row">
    <div class="col-md-12">
      <div class="card" style="margin-bottom:0px;background-color: transparent;border: none !important;outline: none;
      box-shadow: none !important;">
        
        <!-- /.card-header -->
        <div class="card-body" style="padding: 0px;">
          <!-- we are adding the accordion ID so Bootstrap's collapse plugin detects it -->
          <div id="accordion">
            <div class="card ">
              <div class="card-header" style="padding: 20px 40px;
              color: black !important;">
                <h4 class="card-title w-100">
                  <a class="d-block w-100" data-toggle="collapse" href="#collapseOne" style="    color: black;font-weight: 700;
                  font-size: 16px;">
                    Personal Detail
                    <i class="fa fa-plus"></i>
                  </a>
                  
                </h4>
              </div>
              <div id="collapseOne" class="collapse" data-parent="#accordion">
                <div class="card-body">
                  <table class="table acc-tblcutom">
                    <tbody>
                      <tr>
                        <td><strong>First Name</strong></td>
                        <td>{{@$data->personalDetail->first_name}}</td>

                      </tr>
                      <tr>
                        <td><strong>Last Name</strong></td>
                        <td>{{@$data->personalDetail->last_name}}</td>
                      </tr>
                      <tr>
                        <td><strong>Address</strong></td>
                        <td>{{@$data->personalDetail->address}}</td>
                      </tr>
                      <tr>
                        <td><strong>Apt/Suite</strong></td>
                        <td>{{@$data->personalDetail->apt_suite}}</td>
                      </tr>
                      <tr>
                        <td><strong>Country</strong></td>
                        <td>
                          @foreach ($countries as $item)
                          {{@$data->personalDetail->country_id == $item->id?$item->name:''}}
                          @endforeach
                        </td>
                      </tr>
                      <tr>
                        <td><strong>States/Province</strong></td>
                        <td>
                          @foreach ($states as $item)
                          {{@$data->personalDetail->state_id == $item->id?$item->name:''}}
                          @endforeach
                        </td>
                      </tr>
                      <tr>
                        <td><strong>City</strong></td>
                        <td>{{@$data->personalDetail->city}}</td>
                      </tr>
                      <tr>
                        <td><strong>Zip Code</strong></td>
                        <td>{{@$data->personalDetail->zip_code}}</td>
                      </tr>
                      

                      <tr>
                        <td><strong>Are You</strong></td>
                        <td>{{@$data->personalDetail->marital_status}}</td>
                      </tr>

                      <tr>
                        <td><strong>Telephone</strong></td>
                        <td>{{@$data->personalDetail->phone_number}}</td>
                      </tr>

                      <tr>
                        <td><strong>No. of Dependents</strong></td>
                        <td>{{@$data->personalDetail->number_of_dependents}}</td>
                      </tr>
                      
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
            <div class="card card-danger">
              <div class="card-header">
                <h4 class="card-title w-100">
                  <a class="d-block w-100" data-toggle="collapse" href="#collapseTwo">
                    Professional Details
                  </a>
                </h4>
              </div>
              <div id="collapseTwo" class="collapse" data-parent="#accordion">
                <div class="card-body">
                  Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid.
                  3
                  wolf moon officia aute, non cupidatat skateboard dolor brunch. Food truck quinoa nesciunt
                  laborum
                  eiusmod. Brunch 3 wolf moon tempor, sunt aliqua put a bird on it squid single-origin coffee
                  nulla
                  assumenda shoreditch et. Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred
                  nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft
                  beer
                  farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus
                  labore sustainable VHS.
                </div>
              </div>
            </div>
           
          </div>
        </div>
        <!-- /.card-body -->
      </div>
      <!-- /.card -->
    </div>
    
  </div>

</div>


@endsection
@section('footer')
<script>
    $(function() {
        $("#example1").DataTable({
            "responsive": true,
            "lengthChange": false,
            "autoWidth": false,
            "buttons": ["csv", "excel", "pdf", "print", "colvis"]
        }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');

    });
</script>
@endsection