@extends('layouts.app')
    
@section('content')
<div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="card">
            @if (Session::has('message'))
            <div class="alert alert-success" role="alert">
                Email Credentials Updated Successfully
                    </div>
            @endif
            
            @section('page_head',"Email Credentials")
          <!-- /.card-header -->
         <div class="col-md-6">
           <div class="card card-primary mt-4 mb-4">
          
          <!-- /.card-header -->
          <!-- form start -->
          <form role="form" class="form-horizontal" action="{{route('admin.email_credentials.post')}}" method="post">
            @csrf
            <div class="card-body">
              <div class="form-group row">
                <label for="smtp_host" class="col-sm-3 col-form-label">SMTP Host</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control validate" id="smtp_host" name="smtp_host" placeholder="Please Enter SMTP Host" value="{{@$setting->host}}" required="">
                </div>
              </div>

              <div class="form-group row">
                <label for="smtp_host" class="col-sm-3 col-form-label">SMTP Port</label>
                <div class="col-sm-9">
                  <input type="text" class="form-control validate" id="smtp_port" name="smtp_port" placeholder="Please Enter SMTP Port" value="{{@$setting->port}}" required="">
                </div>
              </div>

               <div class="form-group row">
                <label for="email" class="col-sm-3 col-form-label">Enter Email</label>
                <div class="col-sm-9">
                  <input type="email" class="form-control validate" id="email" name="email" placeholder="Email" value="{{@$setting->email}}" pattern="[^@]+@[^@]+\.[a-zA-Z]{2,}" required="">
                </div>
              </div>

                  <div class="form-group row">
                <label for="password" class="col-sm-3 col-form-label">Enter Password</label>
                <div class="col-sm-9">
                 <input type="password" class="form-control validate" id="password" name="password" placeholder="password" value="{{@$setting->password}}" required="">
                </div>
              </div>

            <div class="form-group">
                <label></label>
            </div>

            </div>
            <!-- /.card-body -->
            <div class="card-footer text-center">
              <button type="submit" class="btn btn-block btn-primary btn-sm">Submit</button>
            </div>
          </form>
        </div>
         </div>
          <!-- /.card-body -->
        </div>
        <!-- /.card -->
      </div>
    </div><!-- // Row // -->
  </div>
@endsection
