@extends('layouts.app')
    
@section('content')
<div class="container-fluid card py-3">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                @section('page_head',"Accepted E-mail Template")
                @if (Session::has('message'))
                    <div class="alert alert-success" role="alert">
                        Template Updated Successfully
                    </div>
                @endif
              <div class="card-body">
                <form role="form" class="form-horizontal" action="{{route('admin.accept_email_template.post')}}" method="post">
                    @csrf
                      <div class="form-group">
                        <h3 class="mt-4 mb-4">Subject</h3>
                          <input id="subject" name="subject" class="form-control" value="{{@$template->subject}}" required >
                        </div> 
                        <div class="form-group">
                            <h3 class="mt-4 mb-4">Message sent to user (Template)</h3>
                            <textarea id="compose-textarea" required class="form-control" rows="10" name="compose_email" cols="10">{{@$template->content}}</textarea>
                        </div>
                        <p>Note: <strong>@name</strong> denotes username</p>
                    <!-- /.card-body -->
                    <div class="card-footer text-center">
                      <button type="submit" class="btn btn-block btn-primary btn-sm">Submit</button>
                    </div>
                </form>
              </div>
            </div>
        </div>
</div>
@endsection
@section('footer')
    <script>
        $(function(){
            $('#compose-textarea').summernote({
                height:300
            });
        });
    </script>
@endsection