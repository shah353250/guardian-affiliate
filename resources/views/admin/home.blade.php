@extends('layouts.app')
    
@section('content')
    <div class="row">
        <div class="col-12 col-sm-6 col-md-3">
            <a class="cLink" href="/admin/accepted-application">
                <div class="info-box">
                    <span class="info-box-icon bg-info elevation-1"><i class="fas fa-check"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Accepted</span>
                        <span class="info-box-number">1</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
            </a>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-12 col-sm-6 col-md-3">
            <a class="cLink" href="/admin/rejected-application">
                <div class="info-box mb-3">
                    <span class="info-box-icon bg-danger elevation-1"><i class="fas fa-ban"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Rejected</span>
                        <span class="info-box-number">0</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
            </a>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->

        <!-- fix for small devices only -->
        <div class="clearfix hidden-md-up"></div>

        <div class="col-12 col-sm-6 col-md-3">
            <a class="cLink" href="/admin/pending-application">
                <div class="info-box mb-3">
                    <span class="info-box-icon bg-success elevation-1"><i class="far fa-clock"></i></span>

                    <div class="info-box-content">
                        <span class="info-box-text">Pending</span>
                        <span class="info-box-number">2</span>
                    </div>
                    <!-- /.info-box-content -->
                </div>
            </a>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
        <div class="col-12 col-sm-6 col-md-3">
            <a class="cLink" href="/admin/draft-application">
                <div class="info-box mb-3">
                    <span class="info-box-icon bg-warning elevation-1" style="color: #fff !important;"><i class="fas fa-pen"></i></span>
                    <div class="info-box-content">
                        <span class="info-box-text">In Draft</span>
                        <span class="info-box-number">1</span>
                    </div>

                    <!-- /.info-box-content -->
                </div>
            </a>
            <!-- /.info-box -->
        </div>
        <!-- /.col -->
    </div>
    <!-- /.row -->

    <div class="row">
        <div class="col-12 col-sm-12">
            <div class="card card-primary card-outline card-outline-tabs">
                <div class="card-header p-0 border-bottom-0">
                    <ul class="nav nav-tabs customTabs" id="custom-tabs-four-tab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="custom-tabs-four-home-tab" data-toggle="pill" href="#custom-tabs-four-home" role="tab" aria-controls="custom-tabs-four-home" aria-selected="true">All</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="custom-tabs-four-profile-tab" data-toggle="pill" href="#custom-tabs-four-profile" role="tab" aria-controls="custom-tabs-four-profile" aria-selected="false">Pending</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="custom-tabs-four-messages-tab" data-toggle="pill" href="#custom-tabs-four-messages" role="tab" aria-controls="custom-tabs-four-messages" aria-selected="false">Draft</a>
                        </li>

                    </ul>

                </div>
                <div class="card-body customTabsBody">
                    <div class="tab-content" id="custom-tabs-four-tabContent">
                        <div class="tab-pane fade active show" id="custom-tabs-four-home" role="tabpanel" aria-labelledby="custom-tabs-four-home-tab">
                            <div class="row">
                                <div class="col-12">
                                    <div class="card">
                                        <div class="card-header">
                                            <!-- <h1>qwerty</h1> -->
                                        </div>
                                        <!-- /.card-header -->
                                        <div class="card-body table-responsive p-0">
                                            <div id="allApplicationTable_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer"><div class="row"><div class="col-sm-12 col-md-6"><div class="dataTables_length" id="allApplicationTable_length"><label>Show <select name="allApplicationTable_length" aria-controls="allApplicationTable" class="custom-select custom-select-sm form-control form-control-sm"><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option></select> entries</label></div></div><div class="col-sm-12 col-md-6"><div id="allApplicationTable_filter" class="dataTables_filter"><label>Search:<input type="search" class="form-control form-control-sm" placeholder="" aria-controls="allApplicationTable"></label></div></div></div><div class="row"><div class="col-sm-12"><table id="allApplicationTable" class="table table-head-fixed text-nowrap dataTable no-footer" role="grid" aria-describedby="allApplicationTable_info">
                                                <thead>
                                                    <tr role="row"><th class="sorting_asc" tabindex="0" aria-controls="allApplicationTable" rowspan="1" colspan="1" aria-sort="ascending" aria-label="User Name: activate to sort column descending" style="width: 223px;">User Name</th><th class="sorting" tabindex="0" aria-controls="allApplicationTable" rowspan="1" colspan="1" aria-label="User Email: activate to sort column ascending" style="width: 345px;">User Email</th><th class="sorting" tabindex="0" aria-controls="allApplicationTable" rowspan="1" colspan="1" aria-label="Date: activate to sort column ascending" style="width: 132px;">Date</th><th class="sorting" tabindex="0" aria-controls="allApplicationTable" rowspan="1" colspan="1" aria-label="Status: activate to sort column ascending" style="width: 111px;">Status</th><th class="sorting" tabindex="0" aria-controls="allApplicationTable" rowspan="1" colspan="1" aria-label="TrueId: activate to sort column ascending" style="width: 113px;">TrueId</th><th class="sorting" tabindex="0" aria-controls="allApplicationTable" rowspan="1" colspan="1" aria-label="Bridger: activate to sort column ascending" style="width: 125px;">Bridger</th><th class="text-center sorting" tabindex="0" aria-controls="allApplicationTable" rowspan="1" colspan="1" aria-label="Action: activate to sort column ascending" style="width: 250px;">Action</th></tr>
                                                </thead>
                                                <tbody>
                                                    
                                                        
                                                    
                                                        
                                                    
                                                        
                                                    
                                                        
                                                    


                                                <tr role="row" class="odd">
                                                            <td class="sorting_1"> DONALD HENSLEY </td>
                                                            <td> donald.r.hensley@gmail.com </td>
                                                            <td> 05/11/2021 </td>
                                                            <!-- 
                                                                isApprove
                                                                isDraft
                                                             -->
                                                            
                                                            <td>
                                                                <span class="badge badge-success">Pending</span>
                                                            </td>
                                                            <td>
                                                                <button onclick="window.location='trueId-response/609acc49c29a4b45b1d0c049'" data-toggle="tooltip" title="" class="btn btn-success btn-sm" data-original-title="TrueId"><i class="far fa-eye"></i></button>  
                                                            </td>
                                                            <td>
                                                                <button onclick="window.location='bridger-response/609acc49c29a4b45b1d0c049'" data-toggle="tooltip" title="" class="btn btn-success btn-sm" data-original-title="BridgerId"><i class="far fa-eye"></i></button>  
                                                            </td>
                                                            <td class="custom-icons">
                                                                <button onclick="window.location='application-details/dashboard/609acc49c29a4b45b1d0c049'" data-toggle="tooltip" title="" class="btn btn-success btn-sm" data-original-title="View Application"><i class="far fa-eye"></i></button>
                                                                
                                                                <button id="609acc49c29a4b45b1d0c049" data-toggle="tooltip" title="" class="btn btn-warning btn-sm status_change" data-original-title="Status Change">
                                                                    <i class="fas fa-exchange-alt">
                                                                    </i></button>
                                                                <button id="609acc49c29a4b45b1d0c049" data-toggle="tooltip" title="" class="btn btn-danger btn-sm rejected" data-original-title="Reject Application">
                                                                    <i class="far fa-window-close">
                                                                    </i></button>
                                                                <button id="609acc49c29a4b45b1d0c049" data-toggle="tooltip" title="" class="btn btn-info btn-sm accepted" data-original-title="Accept Application"> <i class="far fa-check-square"></i>
                                                                </button>
                                                                
                                                            </td>
                                                            
                                                        </tr><tr role="row" class="even">
                                                            <td class="sorting_1"> John Harrill </td>
                                                            <td> jess@allyrealtyfl.com </td>
                                                            <td> 05/10/2021 </td>
                                                            <!-- 
                                                                isApprove
                                                                isDraft
                                                             -->
                                                            
                                                            <td>
                                                                <span class="badge badge-warning">Draft</span>
                                                            </td>
                                                            <td></td>
                                                            <td></td>
                                                            <td class="custom-icons">
                                                                <button id="60998d22c28cf0742059e535" data-toggle="tooltip" title="" class="btn btn-warning btn-sm draft" data-original-title="Draft Application Mail"> <i class="far fa-envelope draft"></i>
                                                                </button>
                                                                <button onclick="window.location='application-details/dashboard/60998d22c28cf0742059e535'" data-toggle="tooltip" title="" class="btn btn-success btn-sm" data-original-title="View Application"><i class="far fa-eye"></i></button>
                                                            </td>
                                                            
                                                        </tr><tr role="row" class="odd">
                                                            <td class="sorting_1"> John Smith </td>
                                                            <td> jessthegoat5@gmail.com </td>
                                                            <td> 05/11/2021 </td>
                                                            <!-- 
                                                                isApprove
                                                                isDraft
                                                             -->
                                                            
                                                            <td><span class="badge badge-info">Accepted</span></td>
                                                            <td>
                                                                <button onclick="window.location='trueId-response/609a803471bab8036301af46'" data-toggle="tooltip" title="" class="btn btn-success btn-sm" data-original-title="TrueId"><i class="far fa-eye"></i></button>  
                                                            </td>
                                                            <td>
                                                                <button onclick="window.location='bridger-response/609a803471bab8036301af46'" data-toggle="tooltip" title="" class="btn btn-success btn-sm" data-original-title="BridgerId"><i class="far fa-eye"></i></button>  
                                                            </td>
                                                            <td class="custom-icons">
                                                                <button onclick="window.location='application-details/dashboard/609a803471bab8036301af46'" data-toggle="tooltip" title="" class="btn btn-success btn-sm" data-original-title="View Application"><i class="far fa-eye"></i>
                                                                </button>
                                                            </td>
                                                            
                                                        </tr><tr role="row" class="even">
                                                            <td class="sorting_1"> Nicholas lewis </td>
                                                            <td> nick.ml@yahoo.com </td>
                                                            <td> 05/11/2021 </td>
                                                            <!-- 
                                                                isApprove
                                                                isDraft
                                                             -->
                                                            
                                                            <td>
                                                                <span class="badge badge-success">Pending</span>
                                                            </td>
                                                            <td>
                                                                <button onclick="window.location='trueId-response/609ac752c29a4b45b1d0c047'" data-toggle="tooltip" title="" class="btn btn-success btn-sm" data-original-title="TrueId"><i class="far fa-eye"></i></button>  
                                                            </td>
                                                            <td>
                                                                <button onclick="window.location='bridger-response/609ac752c29a4b45b1d0c047'" data-toggle="tooltip" title="" class="btn btn-success btn-sm" data-original-title="BridgerId"><i class="far fa-eye"></i></button>  
                                                            </td>
                                                            <td class="custom-icons">
                                                                <button onclick="window.location='application-details/dashboard/609ac752c29a4b45b1d0c047'" data-toggle="tooltip" title="" class="btn btn-success btn-sm" data-original-title="View Application"><i class="far fa-eye"></i></button>
                                                                
                                                                <button id="609ac752c29a4b45b1d0c047" data-toggle="tooltip" title="" class="btn btn-warning btn-sm status_change" data-original-title="Status Change">
                                                                    <i class="fas fa-exchange-alt">
                                                                    </i></button>
                                                                <button id="609ac752c29a4b45b1d0c047" data-toggle="tooltip" title="" class="btn btn-danger btn-sm rejected" data-original-title="Reject Application">
                                                                    <i class="far fa-window-close">
                                                                    </i></button>
                                                                <button id="609ac752c29a4b45b1d0c047" data-toggle="tooltip" title="" class="btn btn-info btn-sm accepted" data-original-title="Accept Application"> <i class="far fa-check-square"></i>
                                                                </button>
                                                                
                                                            </td>
                                                            
                                                        </tr></tbody>
                                            </table></div></div><div class="row"><div class="col-sm-12 col-md-5"><div class="dataTables_info" id="allApplicationTable_info" role="status" aria-live="polite">Showing 1 to 4 of 4 entries</div></div><div class="col-sm-12 col-md-7"><div class="dataTables_paginate paging_simple_numbers" id="allApplicationTable_paginate"><ul class="pagination"><li class="paginate_button page-item previous disabled" id="allApplicationTable_previous"><a href="#" aria-controls="allApplicationTable" data-dt-idx="0" tabindex="0" class="page-link">Previous</a></li><li class="paginate_button page-item active"><a href="#" aria-controls="allApplicationTable" data-dt-idx="1" tabindex="0" class="page-link">1</a></li><li class="paginate_button page-item next disabled" id="allApplicationTable_next"><a href="#" aria-controls="allApplicationTable" data-dt-idx="2" tabindex="0" class="page-link">Next</a></li></ul></div></div></div></div>
                                        </div>
                                        <!-- /.card-body -->
                                    </div>
                                    <!-- /.card -->
                                </div>
                            </div>
                            <!-- /.row -->
                        </div>
                        <div class="tab-pane fade" id="custom-tabs-four-profile" role="tabpanel" aria-labelledby="custom-tabs-four-profile-tab">
                            <div class="row">
                                <div class="col-12">
                                    <div class="card">

                                        <!-- /.card-header -->
                                        <div class="card-body table-responsive p-0">
                                            <div id="pendingApplicationTable_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer"><div class="row"><div class="col-sm-12 col-md-6"><div class="dataTables_length" id="pendingApplicationTable_length"><label>Show <select name="pendingApplicationTable_length" aria-controls="pendingApplicationTable" class="custom-select custom-select-sm form-control form-control-sm"><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option></select> entries</label></div></div><div class="col-sm-12 col-md-6"><div id="pendingApplicationTable_filter" class="dataTables_filter"><label>Search:<input type="search" class="form-control form-control-sm" placeholder="" aria-controls="pendingApplicationTable"></label></div></div></div><div class="row"><div class="col-sm-12"><table id="pendingApplicationTable" class="table table-head-fixed text-nowrap dataTable no-footer" role="grid" aria-describedby="pendingApplicationTable_info">
                                                <thead>
                                                    <tr role="row"><th class="sorting_asc" tabindex="0" aria-controls="pendingApplicationTable" rowspan="1" colspan="1" aria-sort="ascending" aria-label="User Name: activate to sort column descending" style="width: 0px;">User Name</th><th class="sorting" tabindex="0" aria-controls="pendingApplicationTable" rowspan="1" colspan="1" aria-label="User Email: activate to sort column ascending" style="width: 0px;">User Email</th><th class="sorting" tabindex="0" aria-controls="pendingApplicationTable" rowspan="1" colspan="1" aria-label="Date: activate to sort column ascending" style="width: 0px;">Date</th><th class="sorting" tabindex="0" aria-controls="pendingApplicationTable" rowspan="1" colspan="1" aria-label="Status: activate to sort column ascending" style="width: 0px;">Status</th><th class="sorting" tabindex="0" aria-controls="pendingApplicationTable" rowspan="1" colspan="1" aria-label="TrueId: activate to sort column ascending" style="width: 0px;">TrueId</th><th class="sorting" tabindex="0" aria-controls="pendingApplicationTable" rowspan="1" colspan="1" aria-label="Bridger: activate to sort column ascending" style="width: 0px;">Bridger</th><th class="text-center sorting" tabindex="0" aria-controls="pendingApplicationTable" rowspan="1" colspan="1" aria-label="Action: activate to sort column ascending" style="width: 0px;">Action</th></tr>
                                                </thead>
                                                <tbody>
                                                    
                                                    

                                                    
                                                    

                                                    


                                                <tr role="row" class="odd">
                                                        <td class="sorting_1">DONALD HENSLEY
                                                        </td>
                                                        <td>donald.r.hensley@gmail.com</td>
                                                        <td>05/11/2021
                                                        </td>
                                                        <td>
                                                            <span class="badge badge-success">Pending</span>
                                                        </td>
                                                        <td>
                                                            <button onclick="window.location='trueId-response/609acc49c29a4b45b1d0c049'" data-toggle="tooltip" title="" class="btn btn-success btn-sm" data-original-title="TrueId"><i class="far fa-eye"></i></button>  
                                                        </td>
                                                        <td>
                                                            <button onclick="window.location='bridger-response/609acc49c29a4b45b1d0c049'" data-toggle="tooltip" title="" class="btn btn-success btn-sm" data-original-title="BridgerId"><i class="far fa-eye"></i></button>  
                                                        </td>
                                                        <td class="custom-icons">
                                                            <button onclick="window.location='application-details/dashboard/609acc49c29a4b45b1d0c049'" data-toggle="tooltip" title="" class="btn btn-success btn-sm" data-original-title="View Application"><i class="far fa-eye"></i></button>
                                                            
                                                            <button id="609acc49c29a4b45b1d0c049" data-toggle="tooltip" title="" class="btn btn-warning btn-sm status_change" data-original-title="Status Change">
                                                                <i class="fas fa-exchange-alt">
                                                                </i></button>
                                                            <button id="609acc49c29a4b45b1d0c049" data-toggle="tooltip" title="" class="btn btn-danger btn-sm rejected" data-original-title="Reject Application">
                                                                <i class="far fa-window-close">
                                                                </i></button>
                                                            <button id="609acc49c29a4b45b1d0c049" data-toggle="tooltip" title="" class="btn btn-info btn-sm accepted" data-original-title="Accept Application"> <i class="far fa-check-square"></i>
                                                            </button>
                                                        </td>
                                                        
                                                    </tr><tr role="row" class="even">
                                                        <td class="sorting_1">Nicholas lewis
                                                        </td>
                                                        <td>nick.ml@yahoo.com</td>
                                                        <td>05/11/2021
                                                        </td>
                                                        <td>
                                                            <span class="badge badge-success">Pending</span>
                                                        </td>
                                                        <td>
                                                            <button onclick="window.location='trueId-response/609ac752c29a4b45b1d0c047'" data-toggle="tooltip" title="" class="btn btn-success btn-sm" data-original-title="TrueId"><i class="far fa-eye"></i></button>  
                                                        </td>
                                                        <td>
                                                            <button onclick="window.location='bridger-response/609ac752c29a4b45b1d0c047'" data-toggle="tooltip" title="" class="btn btn-success btn-sm" data-original-title="BridgerId"><i class="far fa-eye"></i></button>  
                                                        </td>
                                                        <td class="custom-icons">
                                                            <button onclick="window.location='application-details/dashboard/609ac752c29a4b45b1d0c047'" data-toggle="tooltip" title="" class="btn btn-success btn-sm" data-original-title="View Application"><i class="far fa-eye"></i></button>
                                                            
                                                            <button id="609ac752c29a4b45b1d0c047" data-toggle="tooltip" title="" class="btn btn-warning btn-sm status_change" data-original-title="Status Change">
                                                                <i class="fas fa-exchange-alt">
                                                                </i></button>
                                                            <button id="609ac752c29a4b45b1d0c047" data-toggle="tooltip" title="" class="btn btn-danger btn-sm rejected" data-original-title="Reject Application">
                                                                <i class="far fa-window-close">
                                                                </i></button>
                                                            <button id="609ac752c29a4b45b1d0c047" data-toggle="tooltip" title="" class="btn btn-info btn-sm accepted" data-original-title="Accept Application"> <i class="far fa-check-square"></i>
                                                            </button>
                                                        </td>
                                                        
                                                    </tr></tbody>
                                            </table></div></div><div class="row"><div class="col-sm-12 col-md-5"><div class="dataTables_info" id="pendingApplicationTable_info" role="status" aria-live="polite">Showing 1 to 2 of 2 entries</div></div><div class="col-sm-12 col-md-7"><div class="dataTables_paginate paging_simple_numbers" id="pendingApplicationTable_paginate"><ul class="pagination"><li class="paginate_button page-item previous disabled" id="pendingApplicationTable_previous"><a href="#" aria-controls="pendingApplicationTable" data-dt-idx="0" tabindex="0" class="page-link">Previous</a></li><li class="paginate_button page-item active"><a href="#" aria-controls="pendingApplicationTable" data-dt-idx="1" tabindex="0" class="page-link">1</a></li><li class="paginate_button page-item next disabled" id="pendingApplicationTable_next"><a href="#" aria-controls="pendingApplicationTable" data-dt-idx="2" tabindex="0" class="page-link">Next</a></li></ul></div></div></div></div>
                                        </div>
                                        <!-- /.card-body -->
                                    </div>
                                    <!-- /.card -->
                                </div>
                            </div>
                            <!-- /.row -->
                        </div>
                        <div class="tab-pane fade" id="custom-tabs-four-messages" role="tabpanel" aria-labelledby="custom-tabs-four-messages-tab">
                            <div class="row">
                                <div class="col-12">
                                    <div class="card">

                                        <!-- /.card-header -->
                                        <div class="card-body table-responsive p-0">
                                            <div id="draftApplicationTable_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer"><div class="row"><div class="col-sm-12 col-md-6"><div class="dataTables_length" id="draftApplicationTable_length"><label>Show <select name="draftApplicationTable_length" aria-controls="draftApplicationTable" class="custom-select custom-select-sm form-control form-control-sm"><option value="10">10</option><option value="25">25</option><option value="50">50</option><option value="100">100</option></select> entries</label></div></div><div class="col-sm-12 col-md-6"><div id="draftApplicationTable_filter" class="dataTables_filter"><label>Search:<input type="search" class="form-control form-control-sm" placeholder="" aria-controls="draftApplicationTable"></label></div></div></div><div class="row"><div class="col-sm-12"><table id="draftApplicationTable" class="table table-head-fixed text-nowrap dataTable no-footer" role="grid" aria-describedby="draftApplicationTable_info">
                                                <thead>
                                                    <tr role="row"><th class="sorting_asc" tabindex="0" aria-controls="draftApplicationTable" rowspan="1" colspan="1" aria-sort="ascending" aria-label="User Name: activate to sort column descending" style="width: 0px;">User Name</th><th class="sorting" tabindex="0" aria-controls="draftApplicationTable" rowspan="1" colspan="1" aria-label="User Email: activate to sort column ascending" style="width: 0px;">User Email</th><th class="sorting" tabindex="0" aria-controls="draftApplicationTable" rowspan="1" colspan="1" aria-label="Date: activate to sort column ascending" style="width: 0px;">Date</th><th class="sorting" tabindex="0" aria-controls="draftApplicationTable" rowspan="1" colspan="1" aria-label="Status: activate to sort column ascending" style="width: 0px;">Status</th><th class="text-center sorting" tabindex="0" aria-controls="draftApplicationTable" rowspan="1" colspan="1" aria-label="Action: activate to sort column ascending" style="width: 0px;">Action</th></tr>
                                                </thead>
                                                <tbody>
                                                    
                                                    

                                                    


                                                <tr role="row" class="odd">
                                                        <td class="sorting_1">John Harrill
                                                        </td>
                                                        <td>jess@allyrealtyfl.com</td>
                                                        <td>05/10/2021
                                                        </td>
                                                        <td>
                                                            <span class="badge badge-warning">Draft</span>
                                                        </td>
                                                        <td class="custom-icons">
                                                            <button id="60998d22c28cf0742059e535" data-toggle="tooltip" title="" class="btn btn-warning btn-sm draft" data-original-title="Draft Application Mail"> <i class="far fa-envelope draft"></i>
                                                            </button>
                                                            <button onclick="window.location='application-details/dashboard/60998d22c28cf0742059e535'" data-toggle="tooltip" title="" class="btn btn-success btn-sm" data-original-title="View Application"><i class="far fa-eye"></i></button>
                                                        </td>
                                                    </tr></tbody>
                                            </table></div></div><div class="row"><div class="col-sm-12 col-md-5"><div class="dataTables_info" id="draftApplicationTable_info" role="status" aria-live="polite">Showing 1 to 1 of 1 entries</div></div><div class="col-sm-12 col-md-7"><div class="dataTables_paginate paging_simple_numbers" id="draftApplicationTable_paginate"><ul class="pagination"><li class="paginate_button page-item previous disabled" id="draftApplicationTable_previous"><a href="#" aria-controls="draftApplicationTable" data-dt-idx="0" tabindex="0" class="page-link">Previous</a></li><li class="paginate_button page-item active"><a href="#" aria-controls="draftApplicationTable" data-dt-idx="1" tabindex="0" class="page-link">1</a></li><li class="paginate_button page-item next disabled" id="draftApplicationTable_next"><a href="#" aria-controls="draftApplicationTable" data-dt-idx="2" tabindex="0" class="page-link">Next</a></li></ul></div></div></div></div>
                                        </div>
                                        <!-- /.card-body -->
                                    </div>
                                    <!-- /.card -->
                                </div>
                            </div>
                            <!-- /.row -->
                        </div>

                    </div>
                </div>
                <!-- /.card -->
            </div>
        </div>
    </div>
@endsection
