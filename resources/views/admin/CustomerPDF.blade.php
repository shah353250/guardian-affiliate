<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <style>
        .page-break {
            page-break-after: always;
        }
        .kbw-signature { width: 400px; height: 200px; }
    </style>

</head>

<body>
    <div>
    <h1 style=" text-align: center;">NEW ACCOUNT FORM AND AGREEMENT</h1>
    <table id="" class="" style="border-collapse: collapse;width: 100%;">
        
        <tbody>
            {{-- <tr>
                <td style="border: 1px solid #ddd;padding: 8px;">Name</td>
                <td style="border: 1px solid #ddd;padding: 8px;">{{ @$User->personalDetail->first_name." ".@$User->personalDetail->last_name }}
            </td>
            </tr> --}}
            <tr>
                <td style="border: 1px solid #ddd;padding: 8px;">Email</td>
                <td style="border: 1px solid #ddd;padding: 8px;">{{ @$User->email }}</td>
            </tr>
            <tr>
                <td style="border: 1px solid #ddd;padding: 8px;">Registration Type</td>
                <td style="border: 1px solid #ddd;padding: 8px;">{{ @$User->registration_type }}</td>
            </tr>
            <tr>
                <td style="border: 1px solid #ddd;padding: 8px;">Product You Want</td>
                <td style="border: 1px solid #ddd;padding: 8px;">{{ @$User->product_you_want }}</td>
            </tr>
            <tr>
                <td style="border: 1px solid #ddd;padding: 8px;">Hear About Us</td>
                <td style="border: 1px solid #ddd;padding: 8px;">{{ @$User->hear_about_us }}</td>
            </tr>

            {{-- registration_type --}}
        </tbody>

    </table>
    </div>
    <h2 style=" text-align: left;">Personal Information</h2>
    <table id="" class="" style="border-collapse: collapse;width: 100%;">
        
        <tbody>
            <tr>
                <td style="border: 1px solid #ddd;padding: 8px;">Name</td>
                <td style="border: 1px solid #ddd;padding: 8px;">
                    {{ @$User->personalDetail->first_name." ".@$User->personalDetail->last_name }}</td>
            </tr>
            <tr>
                <td style="border: 1px solid #ddd;padding: 8px;">Address</td>
                <td style="border: 1px solid #ddd;padding: 8px;">{{ @$User->personalDetail->address }}</td>
            </tr>
            <tr>
                <td style="border: 1px solid #ddd;padding: 8px;">Apt Suite</td>
                <td style="border: 1px solid #ddd;padding: 8px;">{{ @$User->personalDetail->apt_suite }}</td>
            </tr>
            <tr>
                <td style="border: 1px solid #ddd;padding: 8px;">Country</td>
                <td style="border: 1px solid #ddd;padding: 8px;">{{ @$User->personalDetail->country->name }}</td>
            </tr>
            <tr>
                <td style="border: 1px solid #ddd;padding: 8px;">State</td>
                <td style="border: 1px solid #ddd;padding: 8px;">{{ @$User->personalDetail->state->name  }}</td>
            </tr>

            <tr>
                <td style="border: 1px solid #ddd;padding: 8px;">City</td>
                <td style="border: 1px solid #ddd;padding: 8px;">{{ @$User->personalDetail->city  }}</td>
            </tr>
            <tr>
                <td style="border: 1px solid #ddd;padding: 8px;">Zip Code</td>
                <td style="border: 1px solid #ddd;padding: 8px;">{{ @$User->personalDetail->zip_code  }}</td>
            </tr>
            <tr>
                <td style="border: 1px solid #ddd;padding: 8px;">Phone Number</td>
                <td style="border: 1px solid #ddd;padding: 8px;">{{ @$User->personalDetail->phone_number  }}</td>
            </tr>

            <tr>
                <td style="border: 1px solid #ddd;padding: 8px;">Number Of Dependents</td>
                <td style="border: 1px solid #ddd;padding: 8px;">{{ @$User->personalDetail->number_of_dependents  }}
                </td>
            </tr>

        </tbody>

    </table>
    {{-- Mailing Address (If different from home address) --}}

    @if (isset($User->personalDetail->is_mailing_address))
    @if (@$User->personalDetail->is_mailing_address != 0)
    <div style="page-break-before: auto">
        <h5 style=" text-align: left;">Mailing Address (If different from home address)</h5>
        <table id="" class="" style="border-collapse: collapse;width: 100%;">
            
            <tbody>


                <tr>
                    <td style="border: 1px solid #ddd;padding: 8px;">Apt Suite</td>
                    <td style="border: 1px solid #ddd;padding: 8px;">{{ @$User->mailing->apt_suite }}</td>
                </tr>
                <tr>
                    <td style="border: 1px solid #ddd;padding: 8px;">Address</td>
                    <td style="border: 1px solid #ddd;padding: 8px;">{{ @$User->mailing->address }}</td>
                </tr>
                <tr>
                    <td style="border: 1px solid #ddd;padding: 8px;">Country</td>
                    <td style="border: 1px solid #ddd;padding: 8px;">{{ @$User->mailing->country->name }}</td>
                </tr>
                <tr>
                    <td style="border: 1px solid #ddd;padding: 8px;">State</td>
                    <td style="border: 1px solid #ddd;padding: 8px;">{{ @$User->mailing->state->name  }}</td>
                </tr>

                <tr>
                    <td style="border: 1px solid #ddd;padding: 8px;">City</td>
                    <td style="border: 1px solid #ddd;padding: 8px;">{{ @$User->mailing->city  }}</td>
                </tr>
                <tr>
                    <td style="border: 1px solid #ddd;padding: 8px;">Zip Code</td>
                    <td style="border: 1px solid #ddd;padding: 8px;">{{ @$User->mailing->zip_code  }}</td>
                </tr>


            </tbody>

        </table>
    </div>
    @endif
    @endif

    @if (isset($User->personalDetail->is_trusted_contact_person))
    @if (@$User->personalDetail->is_trusted_contact_person != 0)
    <div style="page-break-before: always">
        <h5 style=" text-align: left;">Trusted Contact Person</h5>
        <table id="" class="" style="border-collapse: collapse;width: 100%;">
            
            <tbody>
                <tr>
                    <td style="border: 1px solid #ddd;padding: 8px;">Name</td>
                    <td style="border: 1px solid #ddd;padding: 8px;">{{ @$User->contact_person->name }}</td>
                </tr>
                <tr>
                    <td style="border: 1px solid #ddd;padding: 8px;">Phone Number</td>
                    <td style="border: 1px solid #ddd;padding: 8px;">{{ @$User->contact_person->phone }}</td>
                </tr>
                <tr>
                    <td style="border: 1px solid #ddd;padding: 8px;">Email</td>
                    <td style="border: 1px solid #ddd;padding: 8px;">{{ @$User->contact_person->email }}</td>
                </tr>
                <tr>
                    <td style="border: 1px solid #ddd;padding: 8px;">Street Address</td>
                    <td style="border: 1px solid #ddd;padding: 8px;">{{ @$User->contact_person->street_address }}</td>
                </tr>
                <tr>
                    <td style="border: 1px solid #ddd;padding: 8px;">Country</td>
                    <td style="border: 1px solid #ddd;padding: 8px;">{{ @$User->contact_person->country->name }}</td>
                </tr>
                <tr>
                    <td style="border: 1px solid #ddd;padding: 8px;">State</td>
                    <td style="border: 1px solid #ddd;padding: 8px;">{{ @$User->contact_person->state->name  }}</td>
                </tr>

                <tr>
                    <td style="border: 1px solid #ddd;padding: 8px;">City</td>
                    <td style="border: 1px solid #ddd;padding: 8px;">{{ @$User->contact_person->city  }}</td>
                </tr>
                <tr>
                    <td style="border: 1px solid #ddd;padding: 8px;">Zip Code</td>
                    <td style="border: 1px solid #ddd;padding: 8px;">{{ @$User->contact_person->zip_code  }}</td>
                </tr>
                <tr>
                    <td style="border: 1px solid #ddd;padding: 8px;">Relation Holder</td>
                    <td style="border: 1px solid #ddd;padding: 8px;">{{ @$User->contact_person->relation_holder  }}</td>
                </tr>

                <tr>
                    <td style="border: 1px solid #ddd;padding: 8px;">Date Of Birth</td>
                    <td style="border: 1px solid #ddd;padding: 8px;">
                        {{ @date_format(date_create($User->contact_person->date_of_birth),"jS M,Y")  }}</td>
                </tr>

            </tbody>

        </table>
    </div>
    @endif
    @endif

    @if ($User->companyDetail != null)
    <div style="page-break-after: auto">
        <h5 style=" text-align: left;">Company Details</h5>
        <table id="" class="" style="border-collapse: collapse;width: 100%;">
            
            <tbody>
                <tr>
                    <td style="border: 1px solid #ddd;padding: 8px;">Company Name</td>
                    <td style="border: 1px solid #ddd;padding: 8px;">{{ @$User->companyDetail->company_name }}</td>
                </tr>
                <tr>
                    <td style="border: 1px solid #ddd;padding: 8px;">Company Phone</td>
                    <td style="border: 1px solid #ddd;padding: 8px;">{{ @$User->companyDetail->company_phone }}</td>
                </tr>
                <tr>
                    <td style="border: 1px solid #ddd;padding: 8px;">Company Email</td>
                    <td style="border: 1px solid #ddd;padding: 8px;">{{ @$User->companyDetail->company_email }}</td>
                </tr>
                <tr>
                    <td style="border: 1px solid #ddd;padding: 8px;">Apt Suite</td>
                    <td style="border: 1px solid #ddd;padding: 8px;">{{ @$User->companyDetail->apt_suite }}</td>
                </tr>
                <tr>
                    <td style="border: 1px solid #ddd;padding: 8px;">Company Address</td>
                    <td style="border: 1px solid #ddd;padding: 8px;">{{ @$User->companyDetail->company_address }}</td>
                </tr>
                <tr>
                    <td style="border: 1px solid #ddd;padding: 8px;">Country</td>
                    <td style="border: 1px solid #ddd;padding: 8px;">{{ @$User->companyDetail->country->name }}</td>
                </tr>
                <tr>
                    <td style="border: 1px solid #ddd;padding: 8px;">State</td>
                    <td style="border: 1px solid #ddd;padding: 8px;">{{ @$User->companyDetail->state->name  }}</td>
                </tr>

                <tr>
                    <td style="border: 1px solid #ddd;padding: 8px;">City</td>
                    <td style="border: 1px solid #ddd;padding: 8px;">{{ @$User->companyDetail->city  }}</td>
                </tr>
                <tr>
                    <td style="border: 1px solid #ddd;padding: 8px;">Zip Code</td>
                    <td style="border: 1px solid #ddd;padding: 8px;">{{ @$User->companyDetail->zip_code  }}</td>
                </tr>


            </tbody>

        </table>
    </div>
    @endif

    <!-- Professional Details -->
    <div style="page-break-before: always">
        <h2 style=" text-align: center;">Professional Details Information</h2>

        @if ($User->professionalDetail->employment_status == "employed" || $User->professionalDetail->employment_status == "self employed")
            <table id="" class="" style="border-collapse: collapse;width: 100%;">
                 
                <tbody>
                    
                    <tr>
                        <td style="border: 1px solid #ddd;padding: 8px;">Employment Status</td>
                        <td style="border: 1px solid #ddd;padding: 8px;">{{ ucwords(@$User->professionalDetail->employment_status) }}</td>
                    </tr>
                    <tr>
                        <td style="border: 1px solid #ddd;padding: 8px;"> {{($User->professionalDetail->employment_status=="employed")? "Employee Name":"Business Name" }} </td>
                        <td style="border: 1px solid #ddd;padding: 8px;">{{ @$User->professionalDetail->name }}</td>
                    </tr>
                    <tr>
                        <td style="border: 1px solid #ddd;padding: 8px;">{{($User->professionalDetail->employment_status=="employed")? "Address Of Employer":"Business Address" }}</td>
                        <td style="border: 1px solid #ddd;padding: 8px;">{{ @$User->professionalDetail->address }}</td>
                    </tr>
                    <tr>
                        <td style="border: 1px solid #ddd;padding: 8px;">Apt Suite</td>
                        <td style="border: 1px solid #ddd;padding: 8px;">{{ @$User->professionalDetail->apt_suite }}</td>
                    </tr>
                    <tr>
                        <td style="border: 1px solid #ddd;padding: 8px;">Country</td>
                        <td style="border: 1px solid #ddd;padding: 8px;">{{ @$User->professionalDetail->country->name }}</td>
                    </tr>
                    <tr>
                        <td style="border: 1px solid #ddd;padding: 8px;">State</td>
                        <td style="border: 1px solid #ddd;padding: 8px;">{{ @$User->professionalDetail->state->name  }}</td>
                    </tr>

                    <tr>
                        <td style="border: 1px solid #ddd;padding: 8px;">City</td>
                        <td style="border: 1px solid #ddd;padding: 8px;">{{ @$User->professionalDetail->city  }}</td>
                    </tr>
                    <tr>
                        <td style="border: 1px solid #ddd;padding: 8px;">{{($User->professionalDetail->employment_status=="employed")? "Employee Phone Number":"Business Phone Number" }}</td>
                        <td style="border: 1px solid #ddd;padding: 8px;">{{ @$User->professionalDetail->phone  }}</td>
                    </tr>
                    <tr>
                        <td style="border: 1px solid #ddd;padding: 8px;">Fax Number</td>
                        <td style="border: 1px solid #ddd;padding: 8px;">{{ @$User->professionalDetail->fax  }}</td>
                    </tr>

                    <tr>
                        <td style="border: 1px solid #ddd;padding: 8px;">{{($User->professionalDetail->employment_status=="employed")? "Year Of Employment":"Years of buisness" }}</td>
                        <td style="border: 1px solid #ddd;padding: 8px;">{{ @$User->professionalDetail->year_of_employment }}
                        </td>
                    </tr>
                    <tr>
                        <td style="border: 1px solid #ddd;padding: 8px;">{{($User->professionalDetail->employment_status=="employed")? "Occupation":"Company Type" }}</td>
                        <td style="border: 1px solid #ddd;padding: 8px;">{{ ucwords(@$User->professionalDetail->occupation) }}
                        </td>
                    </tr>

                </tbody>

            </table>
        @elseif($User->professionalDetail->employment_status == "retired")
            <table id="" class="" style="border-collapse: collapse;width: 100%;">
                
                <tbody>
                    
                    <tr>
                        <td style="border: 1px solid #ddd;padding: 8px;">Employment Status</td>
                        <td style="border: 1px solid #ddd;padding: 8px;">{{ ucwords(@$User->professionalDetail->employment_status) }}</td>
                    </tr>
                
            </table>
        @endif

    </div>

    <div style="page-break-before: always">
    <h2 style="text-align: center">IMPORTANT INFORMATION ABOUT PROCEDURES FOR OPENING A NEW ACCOUNT</h2>
    <h5>USA PATRIOT ACT INFORMATION</h5>

    
    
        <table id="" class="" style="border-collapse: collapse;width: 100%;">
            
            <tbody>
                {{-- <tr>
                    <td style="border: 1px solid #ddd;padding: 8px;">Name</td>
                    <td style="border: 1px solid #ddd;padding: 8px;">{{ @$User->personalDetail->first_name." ".@$User->personalDetail->last_name }}
                </td>
                </tr> --}}
                <tr>
                    <td style="border: 1px solid #ddd;padding: 8px;">TAX ID (SS#/EN)</td>
                    <td style="border: 1px solid #ddd;padding: 8px;">{{ @$User->idInformation->tax_id }}</td>
                </tr>
                <tr>
                    <td style="border: 1px solid #ddd;padding: 8px;">Date of Birth</td>
                    <td style="border: 1px solid #ddd;padding: 8px;">{{ @date_format(date_create(@$User->idInformation->date_of_birth),"jS M,Y") }}</td>
                </tr>
                <tr>
                    <td style="border: 1px solid #ddd;padding: 8px;">Country of Tax Residence</td>
                    <td style="border: 1px solid #ddd;padding: 8px;">{{ @$User->idInformation->country->name   }}</td>
                </tr>

                <tr>
                    <td style="border: 1px solid #ddd;padding: 8px;">State of Tax Residence</td>
                    <td style="border: 1px solid #ddd;padding: 8px;">{{ @$User->idInformation->state->name }}</td>
                </tr>
                <tr>
                    <td style="border: 1px solid #ddd;padding: 8px;">ID Type</td>
                    <td style="border: 1px solid #ddd;padding: 8px;">{{ @$User->idInformation->id_type }}</td>
                </tr>
                {{-- <tr>
                    <td style="border: 1px solid #ddd;padding: 8px;">name_of_id</td>
                    <td style="border: 1px solid #ddd;padding: 8px;">{{ @$User->idInformation->name_of_id }}</td>
                </tr> --}}

                {{-- <tr>
                    <td style="border: 1px solid #ddd;padding: 8px;">id_number</td>
                    <td style="border: 1px solid #ddd;padding: 8px;">{{ @$User->idInformation->id_number }}</td>
                </tr> --}}

                {{-- <tr>
                    <td style="border: 1px solid #ddd;padding: 8px;">identity_country_id </td>
                    <td style="border: 1px solid #ddd;padding: 8px;">{{ @$User->idInformation->identity_country_id  }}</td>
                </tr>

                <tr>
                    <td style="border: 1px solid #ddd;padding: 8px;">identity_state_id  </td>
                    <td style="border: 1px solid #ddd;padding: 8px;">{{ @$User->idInformation->identity_state_id   }}</td>
                </tr>

                <tr>
                    <td style="border: 1px solid #ddd;padding: 8px;">issue_date</td>
                    <td style="border: 1px solid #ddd;padding: 8px;">{{ @$User->idInformation->issue_date }}</td>
                </tr> --}}

                <tr>
                    <td style="border: 1px solid #ddd;padding: 8px;">Expiration Date</td>
                    <td style="border: 1px solid #ddd;padding: 8px;">{{ @date_format(date_create(@$User->idInformation->expiration_date),"jS M,Y") }}</td>
                </tr>
    
                
            </tbody>
    
        </table>
        </div>


        <div style="page-break-before: always">
            <h2 style="text-align: center">Income Details</h2>
            <table id="" class="" style="border-collapse: collapse;width: 100%;">
                
                <tbody>
                   
                    <tr>
                        <td style="border: 1px solid #ddd;padding: 8px;">Annual Income</td>
                        <td style="border: 1px solid #ddd;padding: 8px;">{{ @$User->incomeDetail->annual_income }}</td>
                    </tr>
                   
                    <tr>
                        <td style="border: 1px solid #ddd;padding: 8px;">Net Worth</td>
                        <td style="border: 1px solid #ddd;padding: 8px;">{{ @$User->incomeDetail->net_worth}}</td>
                    </tr>
    
                    <tr>
                        <td style="border: 1px solid #ddd;padding: 8px;">Liquid Net Worth</td>
                        <td style="border: 1px solid #ddd;padding: 8px;">{{ @$User->incomeDetail->liquid_net_worth }}</td>
                    </tr>
                    <tr>
                        <td style="border: 1px solid #ddd;padding: 8px;">Tax Rate</td>
                        <td style="border: 1px solid #ddd;padding: 8px;">{{ @$User->incomeDetail->tax_rate }}</td>
                    </tr>
                    
                </tbody>
        
            </table>
        </div>

        <div style="page-break-before: always">
            <h2 style="text-align: center">Initial Funding</h2>
            <table>
                <tbody>
                    <tr>
                        <td style="padding-right : 20px;"><input type="checkbox"  {{($User->fundDetail->is_income==1)? "checked":""}}></td>
                        <td >Income</td>
                    </tr>
                    <tr>
                        <td style="padding-right: 10px;"><input type="checkbox"  {{($User->fundDetail->is_pension==1)? "checked":""}}></td>
                        <td>Pension or retirement savings</td>
                    </tr>
                    <tr>
                        <td style="padding-right: 10px;"><input type="checkbox"  {{($User->fundDetail->is_gift==1)? "checked":""}}></td>
                        <td>Gift</td>
                    </tr>
                    <tr>
                        <td style="padding-right: 10px;"><input type="checkbox"  {{($User->fundDetail->is_sale_of_business==1)? "checked":""}}></td>
                        <td>Sale of business and property</td>
                    </tr>
                    <tr>
                        <td style="padding-right: 10px;"><input type="checkbox"  {{($User->fundDetail->is_inheritance==1)? "checked":""}}></td>
                        <td>Inheritance</td>
                    </tr>
                    <tr>
                        <td style="padding-right: 10px;"><input type="checkbox"  {{($User->fundDetail->is_social_security==1)? "checked":""}}></td>
                        <td>Social Security benefits</td>
                    </tr>
                    <tr>
                        <td style="padding-right: 10px;"><input type="checkbox"  {{($User->fundDetail->is_other==1)? "checked":""}}></td>
                        <td>Other: <strong>{{@$User->fundDetail->other_income}}</strong></td>
                    </tr>
                    
                </tbody>
            </table>
        </div>

        <div style="page-break-before: always">
            <h2 style="text-align: center">Investment Risk Tolerance</h2>
            <table>
                <tbody>
                    <tr>
                        <td style="padding-right : 20px;"><input type="checkbox"  {{($User->riskAcceptance->account_risk=="Active or Day Trading")? "checked":""}}></td>
                        <td >Active or Day Trading</td>
                    </tr>
                    <tr>
                        <td style="padding-right : 20px;"><input type="checkbox"  {{($User->riskAcceptance->account_risk=="Short Term Trading")? "checked":""}}></td>
                        <td >Short Term Trading</td>
                    </tr>
                </tbody>
            </table>
            <hr>

            <table>
                <tbody>
                    <tr>
                        <td style="padding-right : 20px;"><input type="checkbox"  {{($User->riskAcceptance->is_conservative==1)? "checked":""}}></td>
                        <td >Conservative</td>
                        <td>I want to preserve my initial principal in this account, with minimal risk, even if that means this account does not generate significantincome or returns and may not keep pacewith inflation.</td>
                    </tr>
                    <tr>
                        <td style="padding-right : 20px;"><input type="checkbox"  {{($User->riskAcceptance->is_moderately_conservative==1)? "checked":""}}></td>
                        <td >Moderately Conservative</td>
                        <td>I am willing to acceptlowrisk tomy initial principal, including low volatility,to seek a modestlevel of portfolio returns.</td>
                    </tr>
                    <tr>
                        <td style="padding-right : 20px;"><input type="checkbox"  {{($User->riskAcceptance->is_moderate==1)? "checked":""}}></td>
                        <td >Moderate</td>
                        <td>I am willing to accept some risk to my initial principal and tolerate some volatility to seek higher returns, and understand I could lose a portion of the money invested.</td>
                    </tr>
                    <tr>
                        <td style="padding-right : 20px;"><input type="checkbox"  {{($User->riskAcceptance->is_moderately_aggressive==1)? "checked":""}}></td>
                        <td >Moderately Aggressive</td>
                        <td>I am willing to accept high risk to my initial principal, including high volatility, to seek high returns over time and understand I could lose a substantial amount of the money invested.</td>
                    </tr>
                    <tr>
                        <td style="padding-right : 20px;"><input type="checkbox"  {{($User->riskAcceptance->is_significant_risk==1)? "checked":""}}></td>
                        <td >Significant Risk</td>
                        <td>I am willing to accept maximum risk to my initial principal to aggressively seek maximum returns, and I understand I could lose most, or all, of themoney invested.</td>
                    </tr>
                </tbody>
            </table>
            <hr>
        </div>

        <div style="page-break-before: always">
            <h2 style="text-align: center">Financial Situation and Needs, Liquidity Considerations and Tax Status</h2>
            <table id="" class="" style="border-collapse: collapse;width: 100%;">
                
                <tbody>
                   
                    <tr>
                        <td style="border: 1px solid #ddd;padding: 8px;">Annual Expenses</td>
                        <td style="border: 1px solid #ddd;padding: 8px;">{{ @$User->financialSituation->annual_expenses }}</td>
                    </tr>
                   
                    <tr>
                        <td style="border: 1px solid #ddd;padding: 8px;">Special Expenses</td>
                        <td style="border: 1px solid #ddd;padding: 8px;">{{ @$User->financialSituation->special_expenses}}</td>
                    </tr>
    
                    <tr>
                        <td style="border: 1px solid #ddd;padding: 8px;">Liquidity Needs</td>
                        <td style="border: 1px solid #ddd;padding: 8px;">{{ @$User->financialSituation->liquidity_needs }}</td>
                    </tr>
                    <tr>
                        <td style="border: 1px solid #ddd;padding: 8px;">Financial Goal Period</td>
                        <td style="border: 1px solid #ddd;padding: 8px;">{{ @$User->financialSituation->financial_goal_period }}</td>
                    </tr>
                    
                </tbody>
        
            </table>
        </div>


        <div style="page-break-before: always">
            <h2 style="text-align:center">Investment Experience</h2>
            <table style="width: 100%">
                <thead>
                    <tr>
                        <th >Investment</th>
                        <th >Year(s) Of Experience</th>
                        <th>Knowledge</th>
                    </tr>
                </thead>
                <tbody>
                    
                    <tr>
                        <td style="border: 1px solid #ddd; padding : 10px;">
                            <div >
                                <input style="display: inline-block" type="checkbox" {{@$User->investmentExperience->is_stocks == 1 ?'checked':''}}>
                                
                                <span style="display: inline-block">Stocks</span>
                            </div>

                        </td>
                        <td style="border: 1px solid #ddd;padding : 10px;">
                           
                                <input type="radio" {{@$User->investmentExperience->stock_year_experience == '0' ?'checked':''}} > 0 <br>
                                
                                <input type="radio" {{@$User->investmentExperience->stock_year_experience == '1-5' ?'checked':''}} > 1-5 <br>
                                
                                <input type="radio" {{@$User->investmentExperience->stock_year_experience == '5+' ?'checked':''}}> 5+ <br>
                               
                        </td>
                        <td style="border: 1px solid #ddd;padding : 10px;">
                                <input type="radio" {{@$User->investmentExperience->stock_knowledge == 'None' ?'checked':''}}>
                                None <br>
                                <input type="radio" {{@$User->investmentExperience->stock_knowledge == 'Limited' ?'checked':''}}>
                                Limited <br>
                                <input type="radio"  {{@$User->investmentExperience->stock_knowledge == 'Good' ?'checked':''}}>
                                Good <br>
                                <input type="radio"  {{@$User->investmentExperience->stock_knowledge == 'Extensive' ?'checked':''}}>
                                Extensive <br>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: 1px solid #ddd;padding : 10px;">
                                <input style="display: inline-block" type="checkbox" name="investBond" id="investBond" {{@$User->investmentExperience->is_fixed_income == 1 ?'checked':''}}>
                                
                                <span style="display: inline-block">Fixed Income</span>
                        </td>
                        <td style="border: 1px solid #ddd;padding : 10px;">
                                <input type="radio"  {{@$User->investmentExperience->fixed_income_year_experience == '0' ?'checked':''}} > 0 <br>
                                
                                <input type="radio" {{@$User->investmentExperience->fixed_income_year_experience == '1-5' ?'checked':''}}> 1-5 <br>
                                
                                <input type="radio" {{@$User->investmentExperience->fixed_income_year_experience == '5+' ?'checked':''}} > 5+ <br>
                                
                        </td>
                        <td style="border: 1px solid #ddd;padding : 10px;">
                                <input type="radio" {{@$User->investmentExperience->fixed_income_knowledge == 'None' ?'checked':''}} >
                                None <br>
                                <input type="radio" {{@$User->investmentExperience->fixed_income_knowledge == 'Limited' ?'checked':''}}>
                                Limited <br>
                                <input type="radio"  {{@$User->investmentExperience->fixed_income_knowledge == 'Good' ?'checked':''}}>
                                Good <br>
                                <input type="radio"{{@$User->investmentExperience->fixed_income_knowledge == 'Extensive' ?'checked':''}}>
                                Extensive <br>
                        </td>
                    </tr>

                    <tr>
                        <td style="border: 1px solid #ddd;padding : 10px;">
                                <input style="display: inline-block" type="checkbox" {{@$User->investmentExperience->is_options == 1 ?'checked':''}} >
                               
                                <span style="display: inline-block">Options</span>
                        </td>
                        <td style="border: 1px solid #ddd;padding : 10px;">
                                <input type="radio" {{@$User->investmentExperience->options_year_experience == '0' ?'checked':''}}> 0 <br>
                                
                                <input type="radio" {{@$User->investmentExperience->options_year_experience == '1-5' ?'checked':''}} > 1-5 <br>
                                
                                <input type="radio" {{@$User->investmentExperience->options_year_experience == '5+' ?'checked':''}}> 5+ <br>
                                

                            </label>
                        </td>
                        <td style="border: 1px solid #ddd;padding : 10px;">
                                <input type="radio" {{@$User->investmentExperience->options_knowledge == 'None' ?'checked':''}}>
                                None <br>
                                <input type="radio" {{@$User->investmentExperience->options_knowledge == 'Limited' ?'checked':''}}>
                                Limited <br>
                                <input type="radio" {{@$User->investmentExperience->options_knowledge == 'Good' ?'checked':''}}>
                                Good <br>
                                <input type="radio" {{@$User->investmentExperience->options_knowledge == 'Extensive' ?'checked':''}}>
                                Extensive <br>
                        </td>
                    </tr>
                    
                    <tr>
                        <td style="border: 1px solid #ddd;padding : 10px;">
                                <input style="display: inline-block" type="checkbox" {{@$User->investmentExperience->is_futures == 1 ?'checked':''}} >
                               
                                <span style="display: inline-block">Futures</span>
                           
                        </td>
                        <td style="border: 1px solid #ddd;padding : 10px;">
                                <input type="radio" {{@$User->investmentExperience->futures_year_experience == '0' ?'checked':''}} > 0 <br>
                                <input type="radio" {{@$User->investmentExperience->futures_year_experience == '1-5' ?'checked':''}}> 1-5 <br> 
                                
                                <input type="radio" {{@$User->investmentExperience->futures_year_experience == '5+' ?'checked':''}} > 5+ <br>
                                

                        </td>
                        <td style="border: 1px solid #ddd;padding : 10px;">
                                <input type="radio" {{@$User->investmentExperience->futures_knowledge == 'None' ?'checked':''}} > None <br>
                                <input type="radio" {{@$User->investmentExperience->futures_knowledge == 'Limited' ?'checked':''}}> Limited <br>
                                <input type="radio" {{@$User->investmentExperience->futures_knowledge == 'Good' ?'checked':''}} > Good <br>
                                <input type="radio" {{@$User->investmentExperience->futures_knowledge == 'Extensive' ?'checked':''}} > Extensive
                                <br>
                        </td>
                    </tr>
                </tbody>
            </table>
        </div>

        <div style="page-break-before: always">
            <h2 style="text-align: center">Disclosures</h2>
            <div>
                <p>Do you already maintain an account at either Velocity Clearing LLC in which you have control, beneficial interest, or trading authority? *</p>
                <label >
                    <input type="radio"  {{ ((@$User->disclosure ==null)?'':((@$User->disclosure->beneficial == 1)? 'checked':'')) }}>Yes
                </label>

                <label>
                    <input type="radio" {{ ((@$User->disclosure ==null)?'checked':((@$User->disclosure->beneficial == 0)? 'checked':'')) }}>No
                </label>
            </div>
         

            <div>
                <p>Do you have a relationship with an entity that already maintains an account at Velocity Clearing LLC, such as employee, officer, shareholder, member, partner or owner? *</p>
                <label>
                    <input type="radio" {{ ((@$User->disclosure ==null)?'':((@$User->disclosure->shareholder == 1)? 'checked':''))}}>Yes
                </label>

                <label>
                    <input type="radio" {{ ((@$User->disclosure ==null)?'checked':((@$User->disclosure->shareholder == 0)? 'checked':'')) }} >No
                </label>
            </div>
          

            <div>
                <p>Are either you or an immediate family member an officer, director or at least 10% shareholder in a publicly traded company? *</p>
                <label >
                    <input type="radio" name="immediate" data-div=".immediate_div" class="radio_div_display" value="1" {{@Auth::user()->disclosure->immediate == 1?'checked':''}}>Yes
                </label>

                <label >
                    <input type="radio" name="immediate" data-div=".immediate_div" class="radio_div_display" value="0" {{@Auth::user()->disclosure->immediate == 0 || @Auth::user()->disclosure ==null?'checked':''}}>No
                </label>
            </div>
            @php
            $immediate_data = json_decode(@$User->disclosure->immediate_data);
            @endphp
            <div class="col-6 immediate_div" style="{{ ((@$User->disclosure ==null)?'display: none':((@$User->disclosure->immediate == 1)? '':'display: none'))}}">
                <label>If Yes, Company Name:</label>
                <br>
                <input type="text" value="{{@$immediate_data->immediate_company_name}}">
            </div>
         

            <div>
                <p>Are either you or an immediate family member employed by FINRA, a registered broker dealer or a securities exchange? </p>
                <label >
                    <input type="radio" {{ ((@$User->disclosure ==null)?'':((@$User->disclosure->securities == 1)? 'checked':'')) }}>Yes
                </label>

                <label >
                    <input type="radio" {{ ((@$User->disclosure ==null)?'checked':((@$User->disclosure->securities == 0)? 'checked':'')) }}>No
                </label>
            </div>
            @php
            $securities_data = json_decode(@$User->disclosure->securities_data);
            @endphp
            <div style="{{ ((@$User->disclosure ==null)?'display: none':((@$User->disclosure->securities == 1)? '':'display: none'))}}">
                <label>If Yes, Name of the Firm Or Exchange:</label>
                <br>
                <input type="text" value="{{@$securities_data->securities_firm_name}}" >
            </div>
            

            <div>
                <p>Are you a senior officer at a bank, savings and loan institution, investment company, investment advisory firm, or other financial institution? </p>
                <label>
                    <input type="radio" {{ ((@$User->disclosure ==null)?'':((@$User->disclosure->institution == 1)? 'checked':''))}}>Yes
                </label>

                <label>
                    <input type="radio" {{ ((@$User->disclosure ==null)?'checked':((@$User->disclosure->institution == 0)? 'checked':''))}}>No
                </label>
            </div>
            @php
            $institution_data = json_decode(@$User->disclosure->institution_data);
            @endphp
            <div style="{{ ((@$User->disclosure ==null)?'display: none':((@$User->disclosure->institution == 1)? '':'display: none'))}}">
                <label>If Yes, Name of the Firm:</label>
                <br>
                <input type="text" value="{{@$institution_data->disclosure_institution_firm_name}}">
            </div>
           
        </div>

        <div>
            {{-- <img src="{{@$User->signature ==null?'':@$User->signature->signature_image}}" alt=""> --}}

            {{-- {{ $signature }} --}}

            <div id="redrawSignature"></div>

        </div>

        <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script src="{{asset('assets/admin/myplugin/signature/js/jquery.signature.js')}}"></script>

    <script>
        $(function() {
        var sig = $('#redrawSignature').signature().signature('enable');
        signature_XY = `{"lines":[[[58,64.59],[57,64.59],[56,64.59],[54,64.59],[53,64.59],[52,64.59],[52,65.59],[52,66.59],[52,68.59],[54,72.59],[69,81.59],[79,85.59],[90,90.59],[97,93.59],[101,94.59],[101,95.59],[100,96.59],[98,98.59],[96,98.59],[94,98.59],[91,98.59],[90,98.59],[85,98.59],[77,98.59],[71,98.59],[66,98.59],[65,98.59],[64,97.59]],[[87,57.59],[89,59.59],[92,63.59],[95,65.59],[103,73.59],[107,77.59],[110,80.59],[111,81.59],[112,83.59],[114,86.59],[114,87.59],[114,86.59],[114,84.59],[114,83.59],[114,82.59],[115,80.59],[117,80.59],[119,80.59],[120,80.59],[122,80.59],[124,80.59],[127,80.59],[132,80.59],[134,80.59],[137,80.59],[139,80.59],[143,80.59],[144,80.59],[150,79.59],[154,77.59],[159,75.59],[166,72.59],[177,70.59],[197,66.59],[209,62.59],[217,61.59],[220,59.59],[221,59.59],[219,59.59],[215,59.59],[210,60.59],[173,76.59],[138,91.59],[109,103.59],[87,113.59],[74,118.59],[71,119.59],[70,119.59],[78,119.59],[91,119.59],[148,119.59],[224,112.59],[298,97.59],[341,93.59],[361,90.59],[365,89.59]]]}`;
                
        sig.signature('draw', signature_XY);

        sig.signature('disable');
	
        });

        // $(document).ready(function () {
        //     alert('abc');
        // });
    </script>
</body>

</html>