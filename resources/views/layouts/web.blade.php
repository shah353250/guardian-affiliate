<html>

<head>
    <title>Affiliate</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link href="{{asset('assets/frontend/css/bootstrap.min.css')}}" rel="stylesheet" id="bootstrap-css">
    <link href="{{asset('assets/frontend/css/style.css')}}" rel="stylesheet" id="bootstrap-css">
    <link rel="stylesheet" href="{{asset('assets/frontend/font-awesome/css/font-awesome.min.css')}}">
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    
    <script src="{{asset('assets/admin/plugins/jquery-validation/jquery.validate.min.js')}}"></script>
    <script src="{{asset('assets/admin/plugins/jquery-validation/additional-methods.min.js')}}"></script>
    <link href="{{ asset('assets/admin/myplugin/sweetalert/npm/sweetalert.css') }}" rel="stylesheet" type="text/css" media="screen"/>
    <script src="{{ asset('assets/admin/myplugin/sweetalert/npm/sweetalert.min.js') }}"></script>
    <script src="{{ asset('assets/admin/myplugin/sweetalert/dist/sweetalert.min.js') }}"></script>
    <script src="{{ asset('assets/admin/myplugin/validation/jquery.browser.js') }}"></script>
    <script src="{{ asset('assets/admin/myplugin/validation/jquery.form.js') }}"></script>
    <link rel="stylesheet" href="{{ asset('assets/admin/myplugin/validation/toastr.css') }}">
    <script src="{{ asset('assets/admin/myplugin/validation/toastr.js') }}"></script> 
    <script src="{{ asset('assets/admin/myplugin/validation/custom.js') }}"></script>

</head>

<body>
    <div class="header-bg">
        <div class="main-header">
            <div class="top-header">
                <div class="top-header-content container">
                    <p> <a href="tel:888-602-0092">888-602-0092</a></p>
                </div>
            </div>

            <div class="nav-bar">
                <div class="nav-in-container container">
                    <div class="logo-section">
                      <a href="{{ route('layouts.welcome') }}">  <img src="{{asset('assets/frontend/img/logo.png') }}" /></a>
                    </div>
                    <div class="nav-section">
                        <nav class="navbar navbar-icon-top navbar-expand-lg navbar-dark bg-dark">

                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                                <span class="navbar-toggler-icon"></span>
                            </button>

                            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                                <ul class="navbar-nav mr-auto">
                                    <li class="nav-item active">
                                        <a class="nav-link" href="#">
                                            About Us
                                        </a>
                                    </li>
                                    <li class="nav-item dropdown">
                                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Services
                                        </a>
                                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                            <a class="dropdown-item" href="#">Action</a>
                                            <a class="dropdown-item" href="#">Another action</a>
                                            <div class="dropdown-divider"></div>
                                            <a class="dropdown-item" href="#">Something else here</a>
                                        </div>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#">
                                            Platforms
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#">
                                            Pricing
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#">
                                            Resources
                                        </a>
                                    </li>

                                </ul>
                                <ul class="navbar-nav ">
                                    @auth
                                    <li class="nav-item dropdown">
                                        <?php
                                            $url = '';
                                            if(Auth::user()->hasRole('affiliate_customer'))
                                            {
                                                $url = route('affilate_customer.home');
                                            }
                                            else if(Auth::user()->hasRole('customer'))
                                            {
                                                $url = route('customer.home');
                                            }else
                                            {
                                                $url = route('admin.home');
                                            }   
                                        ?>
                                        <a class="dropdown-item" href="{{ $url }}">
                                            {{ Auth::user()->name }}
                                        </a>


                                    </li>
                                    <li class="nav-item">
                                        <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                                                document.getElementById('logout-form').submit();">
                                            {{ __('Logout') }}
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                            @csrf
                                        </form>
                                    </li>
                                    @else
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('customer.login') }}">
                                            Login
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('customer.register') }}">
                                            Open an Account
                                        </a>
                                    </li>
                                    @endauth
                                </ul>

                            </div>
                        </nav>

                    </div>
                </div>
            </div>

            <div class="header-title container">
                <h1><?= (isset($title)?$title:'Get started with the Guardian Trading affiliate program today!') ?></h1>
            </div>

        </div>
    </div>
    <div class="content-area">
        @yield('content')
    </div>


    <div class="footer-outer">
        <div class="footer-content container">
            <div class="foo-col-1 col-equal">
                <h4>Quick Links</h4>
                <ul>
                    <li><a href="#">About Us</a></li>
                    <li><a href="#">Pricing</a></li>
                    <li><a href="#">Contact Us</a></li>
                    <li><a href="#">Platforms</a></li>
                </ul>

            </div>
            <div class="foo-col-2 col-equal">
                <h4>Legal</h4>
                <ul>
                    <li><a href="#">Privacy Policy</a></li>
                    <li><a href="#">Frequent Asked Questions</a></li>
                </ul>
            </div>
            <div class="foo-col-3 col-equal">
                <h4>Services</h4>
                <ul>
                    <li><a href="#">Trading</a></li>
                    <li><a href="#">Clearing</a></li>
                    <li><a href="#">Locates</a></li>
                    <li><a href="#">Lending</a></li>
                </ul>
            </div>
            <div class="foo-col-4 col-equal">
                <h4>Contact</h4>
                <ul>
                    <li><a href="#"><i class="fa fa-phone"></i>888-802-0092</a></li>
                    <li><a href="#"><i class="fa fa-envelope"></i>info@hostedsitedemo.com</a></li>
                    <li><a href="#"><i class="fa fa-map-marker"></i>1301 ROUTE 36 STE 109, HAZLET, NJ 07730</a></li>

                </ul>
            </div>
            <div class="foo-col-5 col-equal">
                <h4>Subscribe</h4>
                <ul>
                    <form action=”mailto:contact@yourdomain.com” method=”POST” enctype=”multipart/form-data” name=”EmailForm”>
                        <input type="email" placeholder="Enter your Email *">
                        <input type="submit" value="Submit">
                    </form>
                </ul>
            </div>

        </div>
    </div>
    <div class="footer-copyright">
        <div class="copyright-content container">
            <div class="copyright-text">
                © 2020 <a href="https://hostedsitedemo.com/gt">www.guardiantrading.com</a>, All copy rights are reserved
            </div>
            <div class="footer-social">
                <ul>
                    <li><a href=""><i class="fa fa-twitter"></i></a></li>
                    <li><a href=""><i class="fa fa-facebook"></i></a></li>
                    <li><a href=""><i class="fa fa-youtube"></i></a></li>
                    <li><a href=""><i class="fa fa-instagram"></i></a></li>
                </ul>
            </div>
        </div>
    </div>




</body>
<script>
  $(document).ready(function(){
    @if(Session::has('reset'))
      success("{{Session::get('reset')}}");
    @endif
  })  
</script>
</html>