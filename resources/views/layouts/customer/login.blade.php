<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8" />
    <meta
      name="viewport"
      content="width=device-width, initial-scale=1, shrink-to-fit=no"
    />
    <meta name="description" content="" />
    <meta name="author" content="" />
    <link rel="icon" href="#" />
    <title>Guardian</title>
    <!-- CSS STYLES -->
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
    <link
      href="https://fonts.googleapis.com/css?family=Lato&display=swap"
      rel="stylesheet"
    />
    <link 
        rel="stylesheet" 
        href="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/css/bootstrap.min.css" 
        integrity="sha384-B0vP5xmATw1+K9KRQjQERJvTumQW0nPEzvF6L/Z6nronJ3oUOFUFpCjEUQouq2+l" 
        crossorigin="anonymous"
    >
    <link 
        rel="stylesheet" 
        href="https://pro.fontawesome.com/releases/v5.10.0/css/all.css" 
        integrity="sha384-AYmEC3Yw5cVb3ZcuHtOA93w35dYTsvhLPVnYs9eStHfGJvOvKxVfELGroGkvsg+p" 
        crossorigin="anonymous"
    />
    <link
      type="text/css"
      href="{{asset('assets_customer/css/jquery.steps.css')}}"
      rel="stylesheet"
    />
    <link type="text/css" href="{{asset('assets_customer/css/aos.css')}}" rel="stylesheet" />
    <link
      type="text/css"
      href="{{asset('assets_customer/css/jquery.datetimepicker.css')}}"
      rel="stylesheet"
    />
    <link type="text/css" href="{{asset('assets_customer/css/wpb_style.css')}}" rel="stylesheet" />
    <link
      rel="stylesheet"
      href="{{asset('assets_customer/css/new_account.css')}}"
    />
    <link
      rel="shortcut icon"
      href="{{asset('assets_customer/img/newfavicon.png')}}"
      type="image/x-icon"
    />
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script src="{{asset('assets/admin/plugins/jquery-validation/jquery.validate.min.js')}}"></script>
    <script src="{{asset('assets/admin/plugins/jquery-validation/additional-methods.min.js')}}"></script>
    <link href="{{ asset('assets/admin/myplugin/sweetalert/npm/sweetalert.css') }}" rel="stylesheet" type="text/css" media="screen"/>
    <script src="{{ asset('assets/admin/myplugin/sweetalert/npm/sweetalert.min.js') }}"></script>
    <script src="{{ asset('assets/admin/myplugin/sweetalert/dist/sweetalert.min.js') }}"></script>
    <script src="{{ asset('assets/admin/myplugin/validation/jquery.browser.js') }}"></script>
    <script src="{{ asset('assets/admin/myplugin/validation/jquery.form.js') }}"></script>
    <link rel="stylesheet" href="{{ asset('assets/admin/myplugin/validation/toastr.css') }}">
    <script src="{{ asset('assets/admin/myplugin/validation/toastr.js') }}"></script> 
    <script src="{{ asset('assets/admin/myplugin/validation/custom.js') }}"></script>
  </head>
  <style>
    .logoholder {
      text-align: center;
    }
    .logoholder img {
      width: 50%;
      margin-right: 5%;
    }
  </style>

  @stack('custom-css')
  <body style="background-color: #000">
    @yield('content')
    
  </body>
  <!-- JS SCRIPTS -->
  
  <script
    type="text/javascript"
    src="{{asset('assets_customer/js/popper.min.js')}}"
  ></script>
  <script
    type="text/javascript"
    src="{{asset('assets_customer/js/bootstrap.js')}}"
  ></script>
  <script
    type="text/javascript"
    src="{{asset('assets_customer/js/jquery.steps.js')}}"
  ></script>
  <script type="text/javascript" src="{{asset('assets_customer/js/aos.js')}}"></script>
  <script
    type="text/javascript"
    src="{{asset('assets_customer/js/jquery.datetimepicker.full.js')}}"
  ></script>
  <script type="text/javascript" src="{{asset('assets_customer/js/wpb_scripts.js')}}"></script>
  <script type="text/javascript" src="{{asset('assets_customer/js/wpb_code.js')}}"></script>
  
@stack('scripts')
</html>

