<html>

<head>
    <title>Affiliate</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="{{asset('assets/frontend/css/bootstrap.min.css')}}">
    <link href="{{asset('assets/frontend/css/style.css')}}" rel="stylesheet" id="bootstrap-css">
    <link rel="stylesheet" href="{{asset('assets/frontend/font-awesome/css/font-awesome.min.css')}}">
    <link href="{{asset('assets/admin/myplugin/signature/css/jquery.signature.css')}}" rel="stylesheet">
    <link rel="stylesheet" href="{{asset('assets/frontend/css/main.css')}}">
    <link rel="stylesheet" href="{{asset('assets/frontend/css/custom-register.css')}}">
    
    <style>
        .invalid-field {
            border-bottom: 2px red solid !important;
        }
        .form-control {
            font-weight: 300;
            height: auto !important;
            padding: 15px;
            color: #000000;
            background-color: #fff;
            /* border: none; */
            box-shadow: 0 0 2px #00000073;
            border-radius: 0px;
        }
        .form-control:valid {
            background-image: none !important;
        }
        .card-header{
            background-color: white;
            border-bottom: none;
        }
        .custom-control{
            padding-left: 15px;
        }
        .card.mt-2 {
            border: 0 none;
            border-radius: 0px;
            box-shadow: 0 0 15px 1px rgb(0 0 0 / 40%);
            padding: 5px 30px;
            box-sizing: border-box;
        }
        .card-body{
            padding: 0px;
        }
        .card-header {
            padding: .75rem 0rem;
        }
        .custom-control-label::before{
            background-color: #eee;
        }
        .custom-checkbox .custom-control-label::before{
            border-radius: 0px;
        }
        .custom-control-input:focus~.custom-control-label::before
        {
            box-shadow:none;
        }
        label.custom-control-label-cursor{
            cursor: pointer;
            padding-left: 20px;
        }
        .custom-control-label::after, .custom-control-label::before{
            width: 25px;
            height: 25px;
            top: 0;
            left: -15px;
        }
        .boxed label {
            display: inline-block;
            border: solid 2px #ccc;
            transition: all 0.3s;
            color: #888;
            cursor: pointer;
            width: 100px;
            line-height: normal;
            padding: 10px 0;
            text-align: center;
        }
        .boxed input[type="radio"] {
            display: none;
        }
        .boxed input[type="radio"]:checked + label {
            border: solid 2px #4290CA;
            background: #4290CA;
            color: white;
        }
        .radiobox_label {
        display: inline !important;
        position: relative;
        padding-left: 35px;
        margin-bottom: 12px;
        cursor: pointer;
        font-size: 18px;
        -webkit-user-select: none;
        -moz-user-select: none;
        -ms-user-select: none;
        user-select: none;
        width: 100%;
        }

        .radiobox_label input {
            position: absolute;
            opacity: 1;
            cursor: pointer;
            height: 100%;
            width: 30px;
            left: 0;
        }

        .checkbox_form_control {
            width: 25px;
            height: 25px !important;
            margin: 15px auto;
        }
    </style>

</head>

<body>
    <div class="">
        <div class="main-header">
            <div class="top-header">
                <div class="top-header-content container">
                    <p> <a href="tel:888-602-0092">888-602-0092</a></p>
                </div>
            </div>

            <div class="nav-bar">
                <div class="nav-in-container container">
                    <div class="logo-section">
                      <a href="{{ route('layouts.welcome') }}">  <img src="{{asset('assets/frontend/img/logo.png') }}" /></a>
                    </div>
                    <div class="nav-section">
                        <nav class="navbar navbar-icon-top navbar-expand-lg navbar-dark bg-dark">

                            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                                <span class="navbar-toggler-icon"></span>
                            </button>

                            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                                <ul class="navbar-nav mr-auto">
                                    <li class="nav-item active">
                                        <a class="nav-link" href="#">
                                            About Us
                                        </a>
                                    </li>
                                    <li class="nav-item dropdown">
                                        <a class="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                            Services
                                        </a>
                                        <div class="dropdown-menu" aria-labelledby="navbarDropdown">
                                            <a class="dropdown-item" href="#">Action</a>
                                            <a class="dropdown-item" href="#">Another action</a>
                                            <div class="dropdown-divider"></div>
                                            <a class="dropdown-item" href="#">Something else here</a>
                                        </div>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#">
                                            Platforms
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#">
                                            Pricing
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="#">
                                            Resources
                                        </a>
                                    </li>

                                </ul>
                                <ul class="navbar-nav ">
                                    @auth
                                    <li class="nav-item dropdown">
                                        <?php
                                            $url = '';
                                            if(Auth::user()->hasRole('affiliate_customer'))
                                            {
                                                $url = route('affilate_customer.home');
                                            }
                                            else if(Auth::user()->hasRole('customer'))
                                            {
                                                $url = route('customer.home');
                                            }else
                                            {
                                                $url = route('admin.home');
                                            }   
                                        ?>
                                        @if (Auth::user()->name)
                                        <a class="dropdown-item" href="{{ $url }}">
                                            {{ Auth::user()->name }}
                                        </a>
                                        @endif
                                        


                                    </li>
                                    <li class="nav-item">
                                        <a class="dropdown-item" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                                                document.getElementById('logout-form').submit();">
                                            {{ __('Logout') }}
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                                            @csrf
                                        </form>
                                    </li>
                                    @else
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('customer.login') }}">
                                            Login
                                        </a>
                                    </li>
                                    <li class="nav-item">
                                        <a class="nav-link" href="{{ route('customer.register') }}">
                                            Open an Account
                                        </a>
                                    </li>
                                    @endauth
                                </ul>

                            </div>
                        </nav>

                    </div>
                </div>
            </div>


        </div>
    </div>
    <div class="content-area">
        @yield('content')
    </div>


    <div class="footer-outer">
        <div class="footer-content container">
            <div class="foo-col-1 col-equal">
                <h4>Quick Links</h4>
                <ul>
                    <li><a href="#">About Us</a></li>
                    <li><a href="#">Pricing</a></li>
                    <li><a href="#">Contact Us</a></li>
                    <li><a href="#">Platforms</a></li>
                </ul>

            </div>
            <div class="foo-col-2 col-equal">
                <h4>Legal</h4>
                <ul>
                    <li><a href="#">Privacy Policy</a></li>
                    <li><a href="#">Frequent Asked Questions</a></li>
                </ul>
            </div>
            <div class="foo-col-3 col-equal">
                <h4>Services</h4>
                <ul>
                    <li><a href="#">Trading</a></li>
                    <li><a href="#">Clearing</a></li>
                    <li><a href="#">Locates</a></li>
                    <li><a href="#">Lending</a></li>
                </ul>
            </div>
            <div class="foo-col-4 col-equal">
                <h4>Contact</h4>
                <ul>
                    <li><a href="#"><i class="fa fa-phone"></i>888-802-0092</a></li>
                    <li><a href="#"><i class="fa fa-envelope"></i>info@hostedsitedemo.com</a></li>
                    <li><a href="#"><i class="fa fa-map-marker"></i>1301 ROUTE 36 STE 109, HAZLET, NJ 07730</a></li>

                </ul>
            </div>
            <div class="foo-col-5 col-equal">
                <h4>Subscribe</h4>
                <ul>
                    <form action=”mailto:contact@yourdomain.com” method=”POST” enctype=”multipart/form-data” name=”EmailForm”>
                        <input type="email" placeholder="Enter your Email *">
                        <input type="submit" value="Submit">
                    </form>
                </ul>
            </div>

        </div>
    </div>
    <div class="footer-copyright">
        <div class="copyright-content container">
            <div class="copyright-text">
                © 2020 <a href="#">www.guardiantrading.com</a>, All copy rights are reserved
            </div>
            <div class="footer-social">
                <ul>
                    <li><a href=""><i class="fa fa-twitter"></i></a></li>
                    <li><a href=""><i class="fa fa-facebook"></i></a></li>
                    <li><a href=""><i class="fa fa-youtube"></i></a></li>
                    <li><a href=""><i class="fa fa-instagram"></i></a></li>
                </ul>
            </div>
        </div>
    </div>
    <script src="{{asset('assets/frontend/js/jquery.min.js')}}"></script>
    {{-- <script src="{{asset('assets/frontend/js/jquery-ui.min.js')}}"></script> --}}
    {{-- <script src="{{asset('assets/admin/myplugin/signature/js/jquery.signature.js')}}"></script> --}}
    <script src="{{asset('assets/frontend/js/popper.min.js')}}"></script>
    <script src="{{asset('assets/frontend/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('js/application-script.js')}}"></script>
    <script src="{{asset('js/validationScript.js')}}"></script>
    @yield('footer')
</body>
</html>