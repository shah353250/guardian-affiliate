@extends('layouts.web')

@section('content')

<!-- <div class="packages-section row container">
    <div class="single-package silver-package col-sm-4">
        <div class="package-header">
            <h2>Silver</h2>
        </div>
        <div class="package-container">
            <ul>
                <li>Minimum No Of Users : 1500</li>
                <li>Transaction Type: Sale</li>
                <li>Commission Rate: 10 %</li>
                <li>Tracking Id: Yes</li>
                <li>Affiliate Sub-Tracking: Yes</li>
            </ul>
        </div>
        <div class="buy-btn">
            <a href="{{ route('register') }}">Sign Up</a>
        </div>
    </div>
    <div class="single-package gold-package col-sm-4">
        <div class="package-header">
            <h2>Gold</h2>
        </div>
        <div class="package-container">
            <ul>
                <li>Minimum No Of Users : 2500</li>
                <li>Transaction Type: Sale</li>
                <li>Commission Rate: 20 %</li>
                <li>Tracking Id: Yes</li>
                <li>Affiliate Sub-Tracking: Yes</li>
            </ul>
        </div>
        <div class="buy-btn">
            <a href="{{ route('register') }}">Sign Up</a>
        </div>
    </div>
    <div class="single-package platinum-package col-sm-4">
        <div class="package-header">
            <h2>Platinum</h2>
        </div>
        <div class="package-container">
            <ul>
                <li>Minimum No Of Users : 5000</li>
                <li>Transaction Type: Sale</li>
                <li>Commission Rate: 30 %</li>
                <li>Tracking Id: Yes</li>
                <li>Affiliate Sub-Tracking: Yes</li>
            </ul>
        </div>
        <div class="buy-btn">
            <a href="{{ route('register') }}">Sign Up</a>
        </div>
    </div>

</div> -->
    <div class="row-1" id="row-1">
        <div class="container row">
            <div class="col-sm-4 affiliate-col">
                <i class="fa fa-bar-chart" aria-hidden="true"></i>
                <h3>Affiliate Tracker</h3>
                <p>Track your referrals and monitor your payouts via the affiliate dashboard.</p>
            </div>
            <div class="col-sm-4 ref-col">
                <i class="fa fa-usd" aria-hidden="true"></i>
                <h3>Heading</h3>
                <p>Work with the sales team to create a custom commissions plan for your followers</p>
            </div>
            <div class="col-sm-4 support-col">
                <i class="fa fa-bar-chart" aria-hidden="true"></i>
                <h3>Promote</h3>
                <p>Post your affiliate link to your social media platform of choice and get paid when users sign up through your custom link.</p>
            </div>
        </div>
    </div>
    <div id="row-2" class="row-2">
        <div class="container">
            <div class="row row-2-head">
            <h2>Why Traders Choose Guardian Trading</h2>
            </div>
            <div class="row row-2-tp">
                <div class="col col-flex-tp">
                    <div class="check-ico"><i class="fa fa-check-square-o" aria-hidden="true"></i></div>
                    <div class="right-side">
                        <h3>Dedicated Stock Locate Desk</h3>
                        <p>Velocity's stock loan team is dedicated to servicing locates for Guardian Trading clients. As a clearing firm, Velocity is able to offer discounted locates from our available inventory.</p>
                    </div>
                </div>
                <div class="col col-flex-tp">
                    <div class="check-ico"><i class="fa fa-check-square-o" aria-hidden="true"></i></div>
                    <div class="right-side">
                        <h3>Self-Clearing</h3>
                        <p>As a self-clearing firm, Velocity does not rely on third-party relationships to clear trades. This allows us to cut costs and pass savings onto our customers.</p>
                    </div>
                </div>
            </div>
            <div class="row row-2-bt">
                <div class="col col-flex-tp">
                <div class="check-ico"><i class="fa fa-check-square-o" aria-hidden="true"></i></div>
                    <div class="right-side">
                        <h3>Fully Paid Securities Lending</h3>
                        <p>Our Fully Paid Lending program offers customers the ability to earn additional income on the fully paid (cash) stocks in their account that have HTB (Hard to Borrow) lending value in the securities lending market.</p>
                    </div>
                </div>
                <div class="col col-flex-tp">
                <div class="check-ico"><i class="fa fa-check-square-o" aria-hidden="true"></i></div>
                    <div class="right-side">
                        <h3>Direct Market Access</h3>
                        <p>Gain direct market access to some of the most well-known liquidity providers in the financial marketplace through BATS, ARCA, and NADQ.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row-3" id="row-3">
    <div class=" container">
        <div class="row-3-head">
            <h2>How Do I Register?</h2>
        </div>
        <div class="row3-inner">
            <i class="fa fa-file-text-o" aria-hidden="true"></i>
            <h3>STEP 1: Sign Up</h3>
            <p>Fill out the form for our affiliate program and submit it to our sales team.</p>
        </div>
        <div class="row3-inner">
            <i class="fa fa-share-alt" aria-hidden="true"></i>
            <h3>STEP 2: Approval/Promotion</h3>
            <p>After our accounts team approves your affiliate account, you will be given a special link to start promoting at Guardian Trading.
            Please contact <a href="mailto:marketing@velocityclearingllc.com">marketing@velocityclearingllc.com</a></p>
        </div>
        <div class="row3-inner">
            <i class="fa fa-money" aria-hidden="true"></i>
            <h3>STEP 3: Get Paid</h3>
            <p>Get paid for referrals that fund with a minimum of $30,000. Track your referrals in the Affiliate Dashboard and receive payment at the end of each month. </p>
        </div>
        <div class="row-3-inner-btn">
            <a href="/register" class="row-3-btn">SIGN UP NOW</a>
        </div>
        </div>
    </div>


@endsection